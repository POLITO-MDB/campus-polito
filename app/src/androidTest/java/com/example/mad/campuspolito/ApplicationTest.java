package com.example.mad.campuspolito;

import android.app.Application;
import android.test.ApplicationTestCase;
import android.test.suitebuilder.annotation.LargeTest;

import com.example.mad.campuspolito.model.AppGlobals;
import com.example.mad.campuspolito.model.AppUser;
import com.example.mad.campuspolito.model.Category;
import com.parse.ParseClassName;
import com.parse.ParseException;
import com.parse.ParseQuery;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * <a href="http://d.android.com/tools/testing/testing_android.html">Testing Fundamentals</a>
 */
public class ApplicationTest extends ApplicationTestCase<Application> {
    public ApplicationTest() {
        super(Application.class);
    }

    @Override
    protected void setUp() throws Exception {
        super.setUp();

        createApplication();
        Thread.sleep(1000);

    }

    @LargeTest
    public void testParseScrapedData(){
        ParseScrapedData psd = ParseScrapedData.getInstance();
        psd.go();
    }


    @LargeTest
    public void testAppGlobalsInitialization(){

        AppGlobals globals = null;
        try{
            ParseQuery<AppGlobals> query = ParseQuery.getQuery(AppGlobals.class);
            List<AppGlobals> appGlobalsList = query.find();

            if(appGlobalsList != null){
                for(AppGlobals appGlobals : appGlobalsList){
                    appGlobals.delete();
                }
            }

            globals = new AppGlobals();

            globals.setCompanySize(Arrays.asList(
                    "Small enterprise",
                    "Medium enterprise",
                    "Large enterprise",
                    "Public organization"));


            globals.setNoticeCategories(Arrays.asList(
                    "Used books",
                    "Apartments",
                    "Technology"
            ));

            globals.setOfferPeriodTypes(Arrays.asList(
                    "Hours",
                    "Days",
                    "Months"));

            globals.setStudentLanguages(Arrays.asList(
                    "Italian",
                    "English",
                    "Spanish",
                    "French",
                    "Russian",
                    "Dutch"
            ));

            globals.setStudentEducationTitles(Arrays.asList(
                    "Technical high school",
                    "Art high school",
                    "Advanced professional course",
                    "Christian school",
                    "Comprehensive high school",
                    "Military school",
                    "Secondary modern school",
                    "Summer school",
                    "Private school",
                    "Workshop"
            ));

            globals.setStudentExperienceTitles(Arrays.asList(
                    "Volunteering",
                    "Internship"
            ));


            globals.setRequirementSkills(Arrays.asList(
                    "PHP",
                    "MySQL",
                    "Java",
                    "C++",
                    "C",
                    "C#",
                    "Python",
                    "Product design",
                    "Photo manipulation",
                    "Problem solving",
                    "Fluent speech",
                    "Creative thinking",
                    "Teamwork",
                    "Database",
                    "Circuits",
                    "Web",
                    "Adobe Photoshop",
                    "Adobe Illustrator",
                    "3D Modeling",
                    "2D Drawing",
                    "Physics"
            ));

            globals.setJobTypeAvailability(Arrays.asList(
                    "Part-time",
                    "Full-time",
                    "Temporary",
                    "Seasonal"
            ));


            globals.save();
        }
        catch (Exception e){
            if(globals != null){
                try {
                    globals.delete();
                } catch (ParseException e1) {
                    fail("Unable to delete unverified AppGlobals instance. Remove it manually.");
                }
            }
            fail("AppGlobals initialization failed with error: " + e.getMessage());
        }

        assertTrue("AppGlobals initialized correctly", true);
    }


    @LargeTest
    public void testEnterpriseCategoriesInitialization(){


        List<Category> enterpriseCategories = new ArrayList<>();

        Category energy = new Category();
        energy.setName("Energy");
        energy.setSubcategories(Arrays.asList(
                "Coil",
                "Oil and gas",
                "Renewable energy"
        ));

        enterpriseCategories.add(energy);

        Category basicMaterials = new Category();
        basicMaterials.setName("Basic materials");
        basicMaterials.setSubcategories(Arrays.asList(
                "Chemicals",
                "Metals and mining"
        ));

        enterpriseCategories.add(basicMaterials);

        Category goodsAndServices = new Category();
        goodsAndServices.setName("Industrial goods and services");
        goodsAndServices.setSubcategories(Arrays.asList(
                "Food and beverages",
                "Household goods",
                "Hotels and entertainment",
                "Textiles"
        ));

        enterpriseCategories.add(goodsAndServices);

        Category financial = new Category();
        financial.setName("Financial");
        financial.setSubcategories(Arrays.asList(
                "Banking and investment",
                "Insurance",
                "Real estate"
        ));

        enterpriseCategories.add(financial);

        Category healthCare = new Category();
        healthCare.setName("Healthcare");
        healthCare.setSubcategories(Arrays.asList(
                "Equipment and supplies",
                "Pharmaceuticals and medical research",
                "Providers and services"
        ));

        enterpriseCategories.add(healthCare);

        Category technology = new Category();
        technology.setName("Technology");
        technology.setSubcategories(Arrays.asList(
                "Communications equipment",
                "Semiconductors",
                "Software and IT services",
                "Telecommunication services"
        ));

        enterpriseCategories.add(technology);

        Category utilities = new Category();
        utilities.setName("Utilities");
        utilities.setSubcategories(Arrays.asList(
                "Electricity",
                "Natural gas",
                "Water"
        ));

        enterpriseCategories.add(utilities);

        try{

            ParseQuery<Category> query = ParseQuery.getQuery(Category.class);
            List<Category> categoriesList = query.find();

            if(categoriesList != null)
                for(Category c: categoriesList)
                    c.delete();


            for(Category c : enterpriseCategories)
                c.save();
        }
        catch (ParseException e){
            try{
                if(enterpriseCategories != null)
                    for(Category c : enterpriseCategories)
                        c.delete();
            }
            catch (ParseException e1){
                fail("Unable to delete unverified category. Remove enterprise categories manually.");
            }

            fail("Initialization of Category objects for enterprises failed with following message: " + e.getMessage());
        }

        assertTrue("Enterprise categories initialized correctly", true);
    }
}