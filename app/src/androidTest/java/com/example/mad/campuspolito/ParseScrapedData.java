package com.example.mad.campuspolito;

import android.util.Log;

import com.example.mad.campuspolito.model.Course;
import com.example.mad.campuspolito.model.Degree;
import com.example.mad.campuspolito.model.Teacher;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by lg on 22/08/15.
 */
public class ParseScrapedData {

    private static final String TAG = ParseScrapedData.class.getName();
    private static ParseScrapedData instance;

    private ParseScrapedData(){}

    public static ParseScrapedData getInstance(){
        if(instance == null)
            instance = new ParseScrapedData();

        return instance;
    }

    public void go() {
        try {
            ParseQuery<Degree> query = ParseQuery.getQuery("Degree");
            query.orderByAscending(Degree.TITLE);
            query.setLimit(500);
            List<Degree> list = query.find();
            Log.i(TAG, list.size() + " degrees retrieved");

            //int degrees = mergeDegrees(list);
            //addCoursesToDegrees(degrees);
            addCoursesToTeachers();
            Log.i(TAG, "done");
        }
        catch(ParseException e){
            //Log.i(TAG, e.getMessage());
            e.printStackTrace();
        }
    }

    private int mergeDegrees(List<Degree> degrees){
        int size = degrees.size();
        List<Degree> duplicates = new ArrayList<Degree>();

        try {
            for (int i = 0; i < size - 1; i++) {

                Degree d_i = degrees.get(i);
                String i_courses = d_i.getString("course_ids");

                for (int j = i + 1; j < size; j++) {

                    Degree d_j = degrees.get(j);

                    if (d_i.getTitle().equals(d_j.getTitle()) && d_i.getLevel().equals(d_j.getLevel())) {
                        Log.i(TAG, "duplicate " + d_j.getTitle() + " " + d_j.getLevel() + " found at pos " + j);
                        duplicates.add(d_j);

                        String j_courses = d_j.getString("course_ids");
                        Log.i(TAG, "courses: i=" + i_courses.split("\\s+").length + " j=" + j_courses.split("\\s+").length);
                        i_courses += " " + j_courses;
                        d_i.put("course_ids", i_courses);

                        d_i.save();
                        Log.i(TAG, "#courses after merging = " + d_i.getString("course_ids").split("\\s+").length);
                    }
                }
            }

            Log.i(TAG, duplicates.size() + " duplicate degrees found");
            ParseObject.deleteAll(duplicates);
            Log.i(TAG, "duplicate degrees deleted");
        }
        catch(ParseException e){
            //Log.i(TAG, e.getMessage());
            e.printStackTrace();
        }

        return size - duplicates.size();
    }


    private void addCoursesToDegrees(int degree_num){

        try {
            ParseQuery<Degree> q1 = ParseQuery.getQuery("Degree");
            q1.orderByAscending(Degree.TITLE);
            q1.setLimit(500);
            List<Degree> d_list = q1.find();


            for (Degree d : d_list) {
                List<String> course_codes = Arrays.asList(d.getString("course_ids").split("\\s+"));
                ParseQuery<Course> q2 = ParseQuery.getQuery(Course.class);
                q2.whereContainedIn(Course.CODE, course_codes);

                List<Course> c_list = q2.find();

                Log.i(TAG, "degree " + d.getTitle() + " " + d.getLevel() + " " + c_list.size() + " courses retrieved");
                for (Course c : c_list) {

                    c.setDegree(d);
                    Log.i(TAG, "course " + c.getTitle() + ": degree " + d.getTitle() + " " + d.getLevel() + " set");
                }
                ParseObject.saveAll(c_list);
                d.setCourses(c_list);
                Log.i(TAG, "degree " + d.getTitle() + " " + d.getLevel() + " " + d.getCourses().size() + " courses set");
            }
            ParseObject.saveAll(d_list);

        }catch(Exception e){
            //Log.i(TAG, e.getMessage());
            e.printStackTrace();
        }

    }

    private void addCoursesToTeachers() {
        try {
            ParseQuery<Teacher> q1 = ParseQuery.getQuery("Teacher");
            q1.orderByAscending(Teacher.SURNAME);
            q1.setLimit(1000);
            List<Teacher> t_list = q1.find();
            Log.i(TAG, t_list.size() + " teachers retrieved");

            List<String> course_codes = new ArrayList<String>();
            for (Teacher t : t_list) {
                if(t.getString("course_ids") != null) {
                    String[] s = t.getString("course_ids").split("\\s+");
                    course_codes.clear();
                    course_codes.addAll(Arrays.asList(s));

                    Log.i(TAG, "#strings " + course_codes.size());

                    ParseQuery<Course> q2 = ParseQuery.getQuery(Course.class);
                    q2.setLimit(100);
                    q2.whereContainedIn(Course.CODE, course_codes);

                    List<Course> c_list = q2.find();

                    Log.i(TAG, t.getSurname() + " " + c_list.size() + " courses retrieved");

                    for (Course c : c_list) {
                        //c.addTeacher(t);
                    }
                    ParseObject.saveAll(c_list);
                    t.setCourses(c_list);
                    /*for(Course c : c_list){
                        t.addCourse(c);
                    }*/
                    //t.save();
                    if(!c_list.isEmpty())
                        Log.i(TAG, t.getSurname() + " " + t.getCourses().size() + " courses set");
                }
                Thread.sleep(500);
            }
            ParseObject.saveAll(t_list);

        }catch(Exception e){
            //Log.i(TAG, e.getMessage());
            e.printStackTrace();
        }
    }
}
