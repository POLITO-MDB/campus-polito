package com.example.mad.campuspolito.utils;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import android.support.design.widget.CollapsingToolbarLayout;
import android.text.TextUtils;
import android.text.format.DateUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.mad.campuspolito.CampusPolitoApplication;
import com.example.mad.campuspolito.R;
import com.example.mad.campuspolito.common.FullscreenImageActivity;
import com.example.mad.campuspolito.common.LectureData;
import com.example.mad.campuspolito.common.NoticeData;
import com.example.mad.campuspolito.common.ProfileActivity;
import com.example.mad.campuspolito.common.TeacherData;
import com.example.mad.campuspolito.model.AppUser;
import com.example.mad.campuspolito.model.Candidature;
import com.example.mad.campuspolito.model.Conversation;
import com.example.mad.campuspolito.model.Course;
import com.example.mad.campuspolito.model.Degree;
import com.example.mad.campuspolito.model.Lecture;
import com.example.mad.campuspolito.model.Offer;
import com.example.mad.campuspolito.model.Student;
import com.example.mad.campuspolito.model.Teacher;
import com.example.mad.campuspolito.student.CareerPlanActivity;
import com.example.mad.campuspolito.student.CourseData;
import com.parse.ParseException;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.MalformedURLException;
import java.text.DateFormat;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Objects;
import java.util.TreeMap;

/**
 * Created by mdb on 31/07/15.
 */
public class DynamicLinearLayoutViewInflater {

    private static final String TAG = DynamicLinearLayoutViewInflater.class.getName();
    static DynamicLinearLayoutViewInflater instance = null;

    public static DynamicLinearLayoutViewInflater getInstance(){
        if(instance == null)
            instance = new DynamicLinearLayoutViewInflater();

        return instance;
    }

    private DynamicLinearLayoutViewInflater(){

    }

    public void addBitmapViewsToLayout(List<String> bitmapList, final Context ctx, LinearLayout layout, View.OnClickListener listener, int tagKey){
        if(bitmapList == null)
            return;

        layout.removeAllViews();
        Bitmap bitmapObject;
        for(String bitmapResource : bitmapList){
            ImageView bitmapView = new ImageView(ctx);
            bitmapView.setLayoutParams(new LinearLayout.LayoutParams(256, 256));
            bitmapView.setScaleType(ImageView.ScaleType.CENTER_CROP);
            bitmapView.setPadding(8, 8, 8, 8);

            try {
                if(bitmapResource.contains("http")){
                    Log.d(TAG, "Creating bitmap from Web resource.");
                    DownloadTask task = new DownloadTask(bitmapResource,bitmapView);
                    task.execute();
                }
                else{
                    Log.d(TAG, "Creating bitmap from local resource.");
                    bitmapObject = CampusPolitoApplication.decodeSampledBitmapFromUri(Uri.parse(bitmapResource),
                            CampusPolitoApplication.SCALE_SIZE + 1,
                            CampusPolitoApplication.SCALE_SIZE + 1);


                    bitmapObject = CampusPolitoApplication.scaleBitmapAndKeepRatio(bitmapObject,
                            CampusPolitoApplication.SCALE_SIZE,
                            CampusPolitoApplication.SCALE_SIZE);


                    Log.d(TAG,"Bitmap decoded and scaled from Uri");

                    bitmapView.setImageBitmap(bitmapObject);
                }

            }
            catch(SecurityException e){
                e.printStackTrace();
                bitmapView.setImageBitmap(null);
            }
            catch (FileNotFoundException e) {
                bitmapView.setImageBitmap(null);
            }
            catch (IOException e){
                bitmapView.setImageBitmap(null);
            }

            if(bitmapResource != null){
                bitmapView.setOnClickListener(listener);
                bitmapView.setTag(tagKey, bitmapResource);
                layout.addView(bitmapView);
            }
        }

    }

    private class DownloadTask extends AsyncTask<Void, Void, Boolean> {

        String URL;

        Bitmap bitmap;
        ImageView imageView;

        public DownloadTask(String URL, ImageView imageView){
            this.URL = URL;
            this.imageView = imageView;
        }
        @Override
        protected Boolean doInBackground(Void... voids) {
            try {

                bitmap = CampusPolitoApplication.decodeSampledBitmapFromURL(URL,
                        CampusPolitoApplication.SCALE_SIZE + 1,
                        CampusPolitoApplication.SCALE_SIZE + 1);


                bitmap = CampusPolitoApplication.scaleBitmapAndKeepRatio(bitmap,
                        CampusPolitoApplication.SCALE_SIZE,
                        CampusPolitoApplication.SCALE_SIZE);

                Log.d(TAG,"Bitmap decoded and scaled from URL");

            } catch (MalformedURLException e) {
                return false;
            } catch (IOException e) {
                return false;
            }

            return true;
        }

        @Override
        protected void onPostExecute(Boolean completed) {
            if(completed){
                Log.d(TAG,"Bitmap set to imageView");
                imageView.setImageBitmap(bitmap);
                imageView.setAlpha(0.8f);
            }
            else {
                imageView.setImageBitmap(null);
            }

        }
    }

    public static void addTeacherViewsToLayout(List<TeacherData> teachers, Context ctx, LinearLayout layout, View.OnClickListener listener, int tagKey) {
        if(teachers == null)
            return;

        layout.removeAllViews();

        for(TeacherData t : teachers){
            View v = getTeacherView(ctx, t, tagKey);

            if(listener != null && t.getUserId() != null && !TextUtils.isEmpty(t.getUserId()))
                v.setOnClickListener(listener);

            layout.addView(v);
        }
    }


    public static void addStudentViewsToLayout(List<Student> favouriteStudents, Context ctx, LinearLayout layout, View.OnClickListener clicKListener, View.OnLongClickListener listener, int tagKey) {
        if(favouriteStudents == null)
            return;

        layout.removeAllViews();

        for(Student s : favouriteStudents){
            if(!s.getIsPublic())
                continue;

            View v = getStudentView(ctx, s, tagKey);

            if(listener != null)
                v.setOnLongClickListener(listener);
            if(clicKListener != null)
                v.setOnClickListener(clicKListener);

            layout.addView(v);
        }
    }



    public static void addCourseViewsWithCheckboxToLayout(TreeMap<CourseData, Boolean> coursesMap, Context ctx, LinearLayout layout, View.OnClickListener clickListener, CompoundButton.OnCheckedChangeListener listener, int tagKey) {

        if(coursesMap == null)
            return;

        layout.removeAllViews();

        for(CourseData cd : coursesMap.keySet()){
            if(coursesMap.get(cd)){
                View v = getCourseViewWithCheckbox(ctx, cd, true, tagKey, listener);


                if(clickListener != null)
                    v.setOnClickListener(clickListener);

                layout.addView(v);
            }
        }

        for(CourseData cd : coursesMap.keySet()){
            if(!coursesMap.get(cd)){
                View v = getCourseViewWithCheckbox(ctx, cd, false, tagKey, listener);


                if(clickListener != null)
                    v.setOnClickListener(clickListener);

                layout.addView(v);
            }
        }

    }

    private static View getCourseViewWithCheckbox(Context ctx, CourseData cd, Boolean isChecked, int tagKey, CompoundButton.OnCheckedChangeListener listener) {

        View v = ((LayoutInflater)ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.course_checkbox_list_item,null);

        CheckBox checkbox = (CheckBox)v.findViewById(R.id.checkbox);
        TextView title = (TextView)v.findViewById(R.id.title);
        TextView description = (TextView)v.findViewById(R.id.description);

        checkbox.setChecked(isChecked);
        checkbox.setOnCheckedChangeListener(listener);

        title.setText(cd.getCode() + " - " + cd.getName());
        description.setText(ctx.getString(R.string.semester)+ " " + cd.getSemester() + " - " + cd.getCfu() + " " + ctx.getString(R.string.cfu));

        checkbox.setTag(tagKey, cd.getId());
        v.setTag(tagKey, cd.getId());
        return v;
    }

    public static void addNoticeViewsToLayout(List<NoticeData> objectsList, Context ctx, LinearLayout layout) {
        if(objectsList == null)
            return;

        layout.removeAllViews();

        DateFormat dateFormat = android.text.format.DateFormat.getDateFormat(ctx);

        Collections.sort(objectsList, new Comparator<NoticeData>() {
            @Override
            public int compare(NoticeData lhs, NoticeData rhs) {
                return new Long(lhs.getTime()).compareTo(rhs.getTime());
            }
        });

        for(NoticeData nd : objectsList){
            View v  = getJSONNoticeView(ctx,nd, dateFormat);

            layout.addView(v);
        }
    }

    private static View getJSONNoticeView(Context ctx,NoticeData notice, DateFormat dateFormat) {


        View v = ((LayoutInflater)ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.teacher_notice_list_item,null);

        TextView mTitle = (TextView)v.findViewById(R.id.notice_title);
        TextView mDesc = (TextView)v.findViewById(R.id.notice_description);
        TextView mDate = (TextView)v.findViewById(R.id.notice_date);


        mTitle.setText(notice.getTeacher());
        mDesc.setText(notice.getMessage());
        mDate.setText("("+dateFormat.format(notice.getTime())+")");


        return v;
    }


    public static void addJSONExpEduViewsToLayout(List<JSONObject> objectsList, Context ctx, LinearLayout layout, View.OnLongClickListener listener,int tagKey) {
        if(objectsList == null)
            return;
        for(Object object : objectsList){
            View v  = getJSONExpEduView(ctx, object, tagKey);
            if(listener != null)
                v.setOnLongClickListener(listener);

            layout.addView(v);
        }
    }

    public static void addStringViewsToLayout(List<String> stringsList, Context ctx, LinearLayout layout, View.OnLongClickListener listener,int tagKey){
        if(stringsList == null)
            return;

        for(String s : stringsList){
            View v = getStringView(ctx, s, tagKey);
            if(listener != null)
                v.setOnLongClickListener(listener);
            layout.addView(v);
        }
    }


    public static void addLectureViewsToLayout(List<Lecture> todayLectures, Context ctx, LinearLayout layout, View.OnClickListener listener, int tagKey) {
            if(todayLectures == null)
                return;

        layout.removeAllViews();
        for(Lecture l : todayLectures){
            View v = getLectureView(ctx, l, tagKey);
            if(listener != null)
                v.setOnClickListener(listener);

            layout.addView(v);

        }
    }

    public static void addLectureDataViewsToLayout(List<LectureData> lectures, Context ctx, LinearLayout layout, View.OnClickListener listener, int tagKey) {
        if(lectures == null)
            return;

        layout.removeAllViews();
        for(LectureData l : lectures){
            View v = getLectureDataView(ctx, l, tagKey);
            if(listener != null)
                v.setOnClickListener(listener);

            layout.addView(v);

        }
    }

    public static void addCandidatureViewsToLayout(List<Candidature> latestCandidatures, Context ctx, LinearLayout layout, View.OnClickListener listener, int tagKey) {

        if(latestCandidatures == null)
            return;

        DateFormat dateFormat = android.text.format.DateFormat.getDateFormat(ctx);
        DateFormat timeFormat = android.text.format.DateFormat.getTimeFormat(ctx);

        layout.removeAllViews();

        for(Candidature c : latestCandidatures){
            View v = getCandidatureView(ctx, c, tagKey, dateFormat, timeFormat);
            if(listener != null)
                v.setOnClickListener(listener);

            layout.addView(v);
        }

    }

    private static View getCandidatureView(Context ctx, Candidature c, int tagKey, DateFormat dateFormat, DateFormat timeFormat) {
        View v = ((LayoutInflater)ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.candidature_list_item, null);


        CircularImageView candidateImage = (CircularImageView)v.findViewById(R.id.candidate_icon);
        TextView titleView = (TextView)v.findViewById(android.R.id.text1);
        TextView descriptionView = (TextView)v.findViewById(android.R.id.text2);
        TextView dateView = (TextView)v.findViewById(R.id.candidature_date);

        Student s = c.getStudent();
        Offer of = c.getOffer();
        AppUser user = s.getUser();
        try {
            user.fetchIfNeeded();
        } catch (ParseException e) {
            e.printStackTrace();
        }

        if(user.getPicture() != null){
            candidateImage.setParseFile(user.getPicture());
            candidateImage.loadInBackground();
        }
        else{
            candidateImage.setPlaceholder(ctx.getResources().getDrawable(R.mipmap.ic_missing_icon_person));
            candidateImage.setParseFile(null);
        }
        titleView.setText(user.toString());
        descriptionView.setText(of.getTitle());
        dateView.setText(timeFormat.format(c.getCreatedAt()) + " " + dateFormat.format(c.getCreatedAt()));


        v.setTag(tagKey, of.getObjectId());

        return v;

    }


    public static void addConversationViewsToLayout(List<Conversation> conversations, Context ctx,
                                                    LinearLayout layout, View.OnClickListener  listener, int tagkey){
        if(conversations == null)
            return;

        DateFormat dateFormat = android.text.format.DateFormat.getDateFormat(ctx);
        DateFormat timeFormat = android.text.format.DateFormat.getTimeFormat(ctx);

        layout.removeAllViews();

        for(Conversation c : conversations){
            View v = getConversationView(ctx, c, tagkey, dateFormat, timeFormat);
            if(listener != null)
                v.setOnClickListener(listener);

            layout.addView(v);
        }
    }

    private static View getLectureView(Context ctx, Lecture l, int tagKey) {
        View v = ((LayoutInflater)ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.lecture_list_item,null);

        TextView courseView = (TextView)v.findViewById(R.id.course_name);
        TextView dateView = (TextView)v.findViewById(R.id.lecture_date);
        TextView roomView = (TextView)v.findViewById(R.id.lecture_room);

        courseView.setText(l.getCourse().getTitle());
        roomView.setText(l.getRoom().getLocation());
        dateView.setText(
                DateUtils.formatDateTime(ctx, l.getStartCalendar().getTimeInMillis(),
                        DateUtils.FORMAT_SHOW_TIME) +
                        " - " +
                        DateUtils.formatDateTime(ctx, l.getEndCalendar().getTimeInMillis(),
                                DateUtils.FORMAT_SHOW_TIME)
        );

        v.setTag(tagKey, l.getObjectId());
        return v;
    }

    private static View getLectureDataView(Context ctx, LectureData l, int tagKey) {
        View v = ((LayoutInflater)ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.lecture_list_item,null);

        TextView courseView = (TextView)v.findViewById(R.id.course_name);
        TextView dateView = (TextView)v.findViewById(R.id.lecture_date);
        TextView roomView = (TextView)v.findViewById(R.id.lecture_room);

        courseView.setText(l.getDay() + " " + l.getDate());
        roomView.setText(l.getRoomName() + " ("+l.getRoomType()+")");
        dateView.setText(
                l.getStart() +
                        " - " +
                        l.getEnd()
        );

        v.setTag(tagKey, l.getId());
        return v;
    }


    private static View getTeacherView(Context ctx, TeacherData t, int tagKey) {
        View v = ((LayoutInflater)ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.teacher_list_item,null);

        TextView nameView = (TextView)v.findViewById(R.id.teacher_name);
        TextView descriptionView = (TextView)v.findViewById(R.id.teacher_description);
        TextView emailView = (TextView)v.findViewById(R.id.email);
        TextView phoneView = (TextView)v.findViewById(R.id.phone);

        nameView.setText(t.getName());
        descriptionView.setText(t.getRole() + " (" + t.getDepartmentId() + ")");
        emailView.setText(t.getEmail());
        phoneView.setText(t.getPhone());

        v.setTag(tagKey,t.getUserId());

        return v;
    }


    private static View getStudentView(Context ctx, Student s, int tagKey) {
        View v = ((LayoutInflater)ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.student_list_item,null);

        CircularImageView imageView = (CircularImageView)v.findViewById(R.id.student_icon);
        TextView titleView = (TextView)v.findViewById(R.id.student_name);
        TextView descrView = (TextView)v.findViewById(android.R.id.text1);

        Degree d = s.getDegree();
        AppUser user = s.getUser();

        try {
            d.fetchIfNeeded();
            user.fetchIfNeeded();
        } catch (ParseException e) {
            e.printStackTrace();
        }

        titleView.setText(s.getName() + " " + s.getSurname());
        descrView.setSingleLine(false);
        descrView.setText(d.getTitle() + "\n" + d.getLevel());


        if(user.getPicture() != null){
            imageView.setParseFile(user.getPicture());
            imageView.loadInBackground();
        }
        else{
            imageView.setPlaceholder(ctx.getResources().getDrawable(R.mipmap.ic_missing_icon_person));
            imageView.setParseFile(null);
        }


        v.setTag(tagKey, user.getObjectId());

        return v;
    }

    private static View getConversationView(Context ctx, Conversation c, int tagkey, DateFormat dateFormat, DateFormat timeFormat) {
        View v = ((LayoutInflater)ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.conversation_list_item,null);

        CircularImageView imageView = (CircularImageView)v.findViewById(R.id.conversation_icon);
        TextView titleView = (TextView)v.findViewById(R.id.title);
        TextView lastMsgView = (TextView)v.findViewById(android.R.id.text1);
        TextView dateView = (TextView)v.findViewById(R.id.conversation_date);


        lastMsgView.setText(c.getLastMsg());
        dateView.setText(timeFormat.format(c.getUpdatedAt()) + " " + dateFormat.format(c.getUpdatedAt()));

        if (c.isGroupChat()){
            imageView.setPlaceholder(ctx.getResources().getDrawable(R.mipmap.ic_group_chat));
            imageView.setParseFile(null);

            titleView.setText(c.getGroupName());

        }
        else {
            titleView.setText(c.getRecipientUser().toString());

            if(c.getRecipientUser().getPicture() != null){
                imageView.setParseFile(c.getRecipientUser().getPicture());
                imageView.loadInBackground();
            }
            else{
                imageView.setPlaceholder(ctx.getResources().getDrawable(R.mipmap.ic_missing_icon_person));
                imageView.setParseFile(null);
            }

            titleView.setText(c.getRecipientUser().toString());
        }

        v.setTag(tagkey, c.getObjectId());

        return v;
    }


    public static void addOfficeHourViewsToLayout(List<JSONObject> objectsList, Context ctx,
                                                  LinearLayout layout, View.OnLongClickListener listener, int tagKey) {
        if(objectsList == null)
            return;


        Collections.sort(objectsList, new Comparator<Object>() {
            @Override
            public int compare(Object lhs, Object rhs) {
                try{
                    JSONObject lhsJSON;
                    JSONObject rhsJSON;
                    if(lhs instanceof JSONObject)
                        lhsJSON = (JSONObject)lhs;
                    else {
                        HashMap<String, Integer> mapObject = (HashMap<String, Integer>) lhs;

                        lhsJSON = Teacher.createJSONOfficePeriod(
                                mapObject.get(Teacher.OFFICE_PERIOD_START_HOUR),
                                mapObject.get(Teacher.OFFICE_PERIOD_START_MINUTE),
                                mapObject.get(Teacher.OFFICE_PERIOD_END_HOUR),
                                mapObject.get(Teacher.OFFICE_PERIOD_END_MINUTE),
                                mapObject.get(Teacher.OFFICE_PERIOD_DAY));
                    }

                    if(rhs instanceof  JSONObject)
                        rhsJSON = (JSONObject)rhs;
                    else {
                        HashMap<String, Integer> mapObject = (HashMap<String, Integer>) rhs;

                        rhsJSON = Teacher.createJSONOfficePeriod(
                                mapObject.get(Teacher.OFFICE_PERIOD_START_HOUR),
                                mapObject.get(Teacher.OFFICE_PERIOD_START_MINUTE),
                                mapObject.get(Teacher.OFFICE_PERIOD_END_HOUR),
                                mapObject.get(Teacher.OFFICE_PERIOD_END_MINUTE),
                                mapObject.get(Teacher.OFFICE_PERIOD_DAY));
                    }

                    Calendar lhsStart = Calendar.getInstance();
                    lhsStart.setTime(Teacher.getStartDateOfPeriod(lhsJSON));

                    Calendar rhsStart = Calendar.getInstance();
                    rhsStart.setTime(Teacher.getEndDateOfPeriod(rhsJSON));

                    if(lhsStart.before(rhsStart)) return -1;
                    else if (rhsStart.before(lhsStart)) return 1;
                    else return 0;
                }
                catch (JSONException e){
                    return 0;
                }
            }
        });

        DateFormat timeFormat = android.text.format.DateFormat.getTimeFormat(ctx.getApplicationContext());
        for(Object object : objectsList) {
            View v = getJSONOfficeHourView(ctx, object, tagKey,timeFormat);
            if (listener != null)
                v.setOnLongClickListener(listener);

            layout.addView(v);
        }
    }

    private static View getJSONOfficeHourView(Context ctx, Object hourView,int tagKey, DateFormat timeFormat){

        int startHour;
        int startMinute;
        int endHour;
        int endMinute;
        int dayOfWeek;

        if(hourView instanceof HashMap){
            HashMap object = (HashMap)hourView;
            startHour = (Integer)object.get(Teacher.OFFICE_PERIOD_START_HOUR);
            startMinute = (Integer)object.get(Teacher.OFFICE_PERIOD_START_MINUTE);
            endHour = (Integer)object.get(Teacher.OFFICE_PERIOD_END_HOUR);
            endMinute = (Integer)object.get(Teacher.OFFICE_PERIOD_END_MINUTE);
            dayOfWeek = (Integer)object.get(Teacher.OFFICE_PERIOD_DAY);
        }
        else {
            JSONObject object = (JSONObject)hourView;
            try{
                startHour = object.getInt(Teacher.OFFICE_PERIOD_START_HOUR);
                startMinute = object.getInt(Teacher.OFFICE_PERIOD_START_MINUTE);
                endHour = object.getInt(Teacher.OFFICE_PERIOD_END_HOUR);
                endMinute = object.getInt(Teacher.OFFICE_PERIOD_END_MINUTE);
                dayOfWeek = object.getInt(Teacher.OFFICE_PERIOD_DAY);
            }
            catch (JSONException e){
                e.printStackTrace();
                return null;
            }
        }


        View v = ((LayoutInflater)ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.teacher_office_hour_list_item,null);


        TextView dayView = (TextView)v.findViewById(R.id.office_hour_day);
        TextView startView = (TextView)v.findViewById(R.id.office_hour_start);
        TextView endView = (TextView)v.findViewById(R.id.office_hour_end);

        switch (dayOfWeek){
            case Calendar.MONDAY :
                dayView.setText(ctx.getString(R.string.monday));
                break;

            case Calendar.TUESDAY:
                dayView.setText(ctx.getString(R.string.tuesday));
                break;
            case Calendar.WEDNESDAY:
                dayView.setText(ctx.getString(R.string.wednesday));
                break;

            case Calendar.THURSDAY:
                dayView.setText(ctx.getString(R.string.thursday));
                break;

            case Calendar.FRIDAY:
                dayView.setText(ctx.getString(R.string.friday));
                break;
        }

        startView.setText(getHoursStringRepresentation(timeFormat,startHour, startMinute));

        endView.setText(getHoursStringRepresentation(timeFormat,endHour, endMinute));

        v.setTag(tagKey,hourView);
        return v;
    }


    private static View getStringView(Context ctx, String s, int tagKey) {
        View v = ((LayoutInflater)ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.common_string_list_item,null);
        TextView textView = (TextView)v.findViewById(android.R.id.text1);
        textView.setText(s);


        v.setTag(tagKey, s);

        return v;
    }

    private static View getJSONExpEduView(Context ctx, Object expEduView,int tagKey) {

        String title = null;
        String description = null;
        Integer startYear = null;
        Integer endYear = null;

        if(expEduView instanceof HashMap){
            HashMap object = (HashMap)expEduView;
            title = (String)object.get(Student.EDU_EXP_TITLE);
            description = (String)object.get(Student.EDU_EXP_DESC);
            startYear = (Integer)object.get(Student.EDU_EXP_START_YEAR);
            if(object.containsKey(Student.EDU_EXP_END_YEAR))
                endYear = (Integer)object.get(Student.EDU_EXP_END_YEAR);
        }
        else{
            JSONObject object = (JSONObject)expEduView;
            try{
                title = object.getString(Student.EDU_EXP_TITLE);
                description = object.getString(Student.EDU_EXP_DESC);
                startYear = object.getInt(Student.EDU_EXP_START_YEAR);
                if(object.has(Student.EDU_EXP_END_YEAR))
                    endYear = object.getInt(Student.EDU_EXP_END_YEAR);
            }
            catch (JSONException e){
                e.printStackTrace();
            }
        }

        View v = ((LayoutInflater)ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.student_exp_edu_list_item,null);

        TextView mTitle = (TextView)v.findViewById(R.id.exp_edu_title);
        TextView mDesc = (TextView)v.findViewById(R.id.exp_edu_description);
        TextView mYears = (TextView)v.findViewById(R.id.exp_edu_years);


        mTitle.setText(title);
        mDesc.setText(description);
        mYears.setText(getYearsStringRepresentation(
                startYear,
                endYear));

        v.setTag(tagKey,expEduView);

        return v;
    }

    private static String getYearsStringRepresentation(Integer start,Integer end) {
        if (end == null)
            return "(" + start + "-)";
        else
            return "(" + start + "-" + end + ")";
    }

    private static String getHoursStringRepresentation(DateFormat timeFormat, int startHour, int startMinute) {
        Calendar c = Calendar.getInstance();
        c.set(Calendar.HOUR_OF_DAY, startHour);
        c.set(Calendar.MINUTE, startMinute);

        return timeFormat.format(c.getTime());
    }
}
