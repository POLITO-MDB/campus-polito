package com.example.mad.campuspolito.locations;

import com.google.android.gms.maps.model.LatLngBounds;

/**
 * Created by lg on 04/08/15.
 */
public interface ISetBounds {
    void boundsDone(LatLngBounds.Builder bounds);
}
