package com.example.mad.campuspolito.offer;

import android.app.Activity;
import android.app.SearchManager;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.NavUtils;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.FrameLayout;
import android.widget.LinearLayout;

import com.example.mad.campuspolito.CampusPolitoApplication;
import com.example.mad.campuspolito.R;
import com.example.mad.campuspolito.common.MainActivity;
import com.example.mad.campuspolito.model.AppUser;

/**
 * Created by lg on 11/08/15.
 */

/* TODO
 * The offers list should be updated when an offer is modified in OffersDetailFragment;
 * currently this doesn't happen, not even using the back button. Overriding onBackPressed() in
 * OfferDetailActivity is a partial solution (it works for single-pane only) and not a good idea
 */
public class OfferListActivity extends AppCompatActivity implements OfferListFragment.Callbacks, PopupMenu.OnMenuItemClickListener, MenuItem.OnMenuItemClickListener {

    private static final String QUERY_STRING = "query_string";
    private static final String QUERY_TYPE = "query_type";
    private static final String QUERY_SORT = "query_sort";
    private static final String QUERY = "query";
    private static final int DATE_ASC = 90;
    private static final int DATE_DESC = 91;


    private final String TAG = OfferListActivity.class.getName();
    private final String OFFER_LIST = "offer_list_tag";
    private static final String SHOW_FAVOURITES = "show_favourites";

    private Toolbar toolbar;
    private LinearLayout mListProgress;
    private FrameLayout mContent;
    private AppUser user;

    private boolean showFavourites;
    private boolean query = false;
    private int queryType;
    private String queryString = "";
    private int querySort = DATE_DESC;
    private MenuItem showFavouritesButton;
    private MenuItem showAllOffersButton;
    private MenuItem searchItemButton;
    private SearchView searchView;

    /**
     * Whether or not the activity is in two-pane mode, i.e. running on a tablet
     * device.
     */
    protected boolean mTwoPane;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_offer_list);

        handleIntent(getIntent());

        user = (AppUser)AppUser.getCurrentUser();

        if(findViewById(R.id.offer_detail_container) != null) {
            mTwoPane = true;
        }

        if(savedInstanceState != null){
            showFavourites = savedInstanceState.getBoolean(SHOW_FAVOURITES);
            Log.d(TAG, "savedInstanceState != null, showFavourites = " + showFavourites);
            query = savedInstanceState.getBoolean(QUERY);
            Log.d(TAG, "savedInstanceState != null, query = " + query);
            queryString = savedInstanceState.getString(QUERY_STRING);
            Log.d(TAG, "savedInstanceState != null, queryString = " + queryString);
            queryType = savedInstanceState.getInt(QUERY_TYPE);
            Log.d(TAG, "savedInstanceState != null, queryType = " + queryType);
            querySort = savedInstanceState.getInt(QUERY_SORT);
            Log.d(TAG, "savedInstanceState != null, querySort = " + querySort);
        }

        if(user.isStudent()){
            if(query){
                queryType = OfferListFragment.SEARCH_OFFERS;
            }
            else if(showFavourites){
                queryType = OfferListFragment.STUDENT_FAVOURITES;
            }
            else{
                queryType = OfferListFragment.STUDENT_ALL;

            }
        }
        else{
            queryType = OfferListFragment.COMPANY_ALL;
        }
        getSupportFragmentManager().beginTransaction().replace(R.id.contentFragment, OfferListFragment.newInstance(queryType, queryString, querySort), OFFER_LIST).commit();

        toolbar = (Toolbar)findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    protected void onNewIntent(Intent intent) {
        Log.i(TAG, "onNewIntent");
        setIntent(intent);
        handleIntent(intent);
    }

    private void handleIntent(Intent intent) {
        Log.i(TAG, "handleIntent");

        if (Intent.ACTION_SEARCH.equals(intent.getAction())) {
            Log.i(TAG, "handleIntent, action search");
            query = true;
            queryString = intent.getStringExtra(SearchManager.QUERY);
            //use the query to search your data somehow
            Log.i(TAG, "query: " + queryString);
            queryType = OfferListFragment.SEARCH_OFFERS;
            getSupportFragmentManager().beginTransaction().replace(R.id.contentFragment, OfferListFragment.newInstance(queryType, queryString, querySort), OFFER_LIST).commit();
        }
    }


    /**
     * Callback method from {@link OfferListFragment.Callbacks}
     * indicating that the item with the given ID was selected.
     */
    @Override
    public void onOfferSelected(String id) {
        if (mTwoPane) {
            // In two-pane mode, show the detail view in this activity by
            // adding or replacing the detail fragment using a
            // fragment transaction.
            Log.i(TAG, "onOfferSelected, two-pane");
            Bundle arguments = new Bundle();
            arguments.putString(OfferDetailFragment.OFFER_ID, id);
            OfferDetailFragment fragment = new OfferDetailFragment();
            fragment.setArguments(arguments);
            getFragmentManager().beginTransaction()
                    .replace(R.id.offer_detail_container, fragment)
                    .commit();

        } else {
            // In single-pane mode, simply start the detail activity
            // for the selected item ID.
            Log.i(TAG, "onOfferSelected, single-pane");
            Intent detailIntent = new Intent(this, OfferDetailActivity.class);
            detailIntent.putExtra(OfferDetailFragment.OFFER_ID, id);
            startActivityForResult(detailIntent, OfferDetailActivity.DETAIL_REQUEST);
        }
    }


    public void onUpdateContent() {
        if(!isFinishing()) {
            ((OfferListFragment) getSupportFragmentManager().findFragmentByTag(OFFER_LIST)).onUpdateContent();
        }
    }


    @Override
    protected void onStop() {
        super.onStop();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.

        AppUser user = (AppUser)AppUser.getCurrentUser();
        if(user != null){
            if(user.isStudent()){
                getMenuInflater().inflate(R.menu.menu_offer_list_student, menu);

                showFavouritesButton = menu.findItem(R.id.show_fav_offers);
                showAllOffersButton = menu.findItem(R.id.show_all_offers);

                if(showFavourites){
                    showFavouritesButton.setVisible(false);
                    showAllOffersButton.setVisible(true);
                }
                else{
                    showAllOffersButton.setVisible(false);
                    showFavouritesButton.setVisible(true);
                }

                searchItemButton = menu.findItem(R.id.action_search);
                searchView = (SearchView) MenuItemCompat.getActionView(searchItemButton);
                SearchManager searchManager = (SearchManager) getSystemService(SEARCH_SERVICE);
                searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
                //searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
                //    @Override
                //    public boolean onQueryTextSubmit(String query) {
                //        queryString = query;
                 //       getSupportFragmentManager().beginTransaction().replace(R.id.contentFragment, OfferListFragment.newInstance(OfferListFragment.SEARCH_OFFERS, queryString), OFFER_LIST).commit();
                //        return false;
                //    }

                  //  @Override
                  //  public boolean onQueryTextChange(String newText) {
                    //    return false;
                   // }
                //});
            }

            else{
                getMenuInflater().inflate(R.menu.menu_offer_list_company, menu);
            }
        }

        getMenuInflater().inflate(R.menu.menu_home, menu);


        return true;
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {

        Log.d(TAG, "onPrepareOptionsMenu");

        if(user.isStudent()) {
            Log.d(TAG, "onPrepareOptionsMenu, user is student");
            //menu.findItem(R.id.action_sort_offers).setVisible(true).setCheckable(true);
            menu.findItem(R.id.action_sort_offers).setOnMenuItemClickListener(this);

            showFavouritesButton = menu.findItem(R.id.show_fav_offers);
            showAllOffersButton = menu.findItem(R.id.show_all_offers);

            if (showFavourites) {
                showAllOffersButton.setVisible(true);
                showFavouritesButton.setVisible(false);
            } else {
                showAllOffersButton.setVisible(false);
                showFavouritesButton.setVisible(true);
            }
        }

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        switch(id) {

            case R.id.action_logout:
                CampusPolitoApplication.unsubscribeFromAllChannels();
                AppUser.logOut();
                Intent homeIntent = new Intent(this, MainActivity.class);
                homeIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(homeIntent);
                finish();
                break;

            case R.id.show_fav_offers:
                queryType = OfferListFragment.STUDENT_FAVOURITES;
                showFavourites = true;
                getSupportFragmentManager().beginTransaction().replace(R.id.contentFragment, OfferListFragment.newInstance(queryType, queryString, querySort), OFFER_LIST).commit();
                invalidateOptionsMenu();
                break;

            case R.id.show_all_offers:
                queryType = OfferListFragment.STUDENT_ALL;
                showFavourites = false;
                getSupportFragmentManager().beginTransaction().replace(R.id.contentFragment, OfferListFragment.newInstance(queryType, queryString, querySort), OFFER_LIST).commit();                showFavourites = false;
                invalidateOptionsMenu();
                break;

            case R.id.action_reset_filters:
                queryString = "";
                query = false;
                querySort = DATE_DESC;
                queryType = OfferListFragment.STUDENT_ALL;
                showFavourites = false;
                getSupportFragmentManager().beginTransaction().replace(R.id.contentFragment, OfferListFragment.newInstance(queryType, queryString, querySort), OFFER_LIST).commit();                showFavourites = false;
                invalidateOptionsMenu();
                break;

            case R.id.create_offer:
                Log.i(TAG, "createOfferButton");
                Intent intent = new Intent(this, EditOfferActivity.class);
                startActivityForResult(intent, EditOfferActivity.CREATE_REQUEST);
                break;


            default:
                return false;

        }

        return true;
    }


    @Override
    public boolean onMenuItemClick(MenuItem menuItem) {
        switch (menuItem.getItemId()){


            case R.id.action_sort_offers:
                PopupMenu popup = new PopupMenu(this, findViewById(R.id.action_search));
                popup.setOnMenuItemClickListener(this);
                MenuInflater inflater = popup.getMenuInflater();
                inflater.inflate(R.menu.sort_offers_options, popup.getMenu());
                popup.show();
                break;

            case R.id.action_sort_date_desc:
                querySort = DATE_DESC;
                getSupportFragmentManager().beginTransaction().replace(R.id.contentFragment, OfferListFragment.newInstance(queryType, queryString, querySort), OFFER_LIST).commit();
                break;
            /*case R.id.action_sort_expire_desc:
                performQuery(SORT_COST_PARAM, SORT_DESC);
                break;*/


            case R.id.action_sort_date_asc:
                querySort = DATE_ASC;
                getSupportFragmentManager().beginTransaction().replace(R.id.contentFragment, OfferListFragment.newInstance(queryType, queryString, querySort), OFFER_LIST).commit();
                break;
            /*case R.id.action_sort_expire_asc:
                performQuery(SORT_COST_PARAM, SORT_ASC);
                break;*/

            default:
                return false;
        }
        return true;
    }



    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putBoolean(SHOW_FAVOURITES, showFavourites);
        outState.putBoolean(QUERY, query);
        outState.putString(QUERY_STRING, queryString);
        outState.putInt(QUERY_TYPE, queryType);
        outState.putInt(QUERY_SORT, querySort);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if(requestCode == EditOfferActivity.CREATE_REQUEST && resultCode == Activity.RESULT_OK){
            Snackbar.make(toolbar, getString(R.string.offer_saved), Snackbar.LENGTH_SHORT).show();
            onUpdateContent();
        }

        else if(requestCode == OfferDetailActivity.DETAIL_REQUEST && resultCode == OfferDetailActivity.OFFER_DELETED){
            Snackbar.make(toolbar, getString(R.string.offer_deleted), Snackbar.LENGTH_SHORT).show();
            onUpdateContent();
        }

        super.onActivityResult(requestCode, resultCode, data);
    }


}
