package com.example.mad.campuspolito.student;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.mad.campuspolito.R;
import com.example.mad.campuspolito.model.AppUser;
import com.example.mad.campuspolito.model.Student;
import com.example.mad.campuspolito.utils.CircularImageView;
import com.parse.ParseException;
import com.parse.ParseQueryAdapter;

import java.text.DateFormat;

/**
 * Created by mdb on 23/07/15.
 */
public class StudentsAdapter extends ParseQueryAdapter<Student> {

    private Context context;

    public StudentsAdapter(Context context, com.parse.ParseQueryAdapter.QueryFactory<Student> queryFactory) {
        super(context, queryFactory);
        this.context = context;
    }


    @Override
    public View getItemView(Student object, View v, ViewGroup parent) {
        if (v == null) {
            v = View.inflate(getContext(), R.layout.student_list_item, null);
        }

        // Take advantage of ParseQueryAdapter's getItemView logic for
        // populating the main TextView/ImageView.
        // The IDs in your custom layout must match what ParseQueryAdapter expects
        // if it will be populating a TextView or ImageView for you.
        super.getItemView(object, v, parent);

        // Do additional configuration before returning the View.

        CircularImageView imageView = (CircularImageView) v.findViewById(R.id.student_icon);
        TextView textView = (TextView) v.findViewById(R.id.student_name);

        AppUser studentUser = object.getUser();
        try {
            studentUser.fetchIfNeeded();
        } catch (ParseException e) {
            e.printStackTrace();
        }
        if(studentUser.getPicture() != null) {

            imageView.setParseFile(studentUser.getPicture());
            imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
            imageView.loadInBackground();
        }
        else{
            imageView.setPlaceholder(context.getResources().getDrawable(R.mipmap.ic_missing_icon_person));
            imageView.setParseFile(null);
        }

        textView.setText(studentUser.toString());

        return v;
    }

}