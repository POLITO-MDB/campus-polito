package com.example.mad.campuspolito.locations;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.mad.campuspolito.CampusPolitoApplication;
import com.example.mad.campuspolito.R;
import com.example.mad.campuspolito.model.Location;
import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseQuery;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import fr.ganfra.materialspinner.MaterialSpinner;

/**
 * A list fragment representing a list of Items. This fragment
 * also supports tablet devices by allowing list items to be given an
 * 'activated' state upon selection. This helps indicate which item is
 * currently being viewed in a {@link LocationSearchMasterFragment}.
 * <p/>
 * Activities containing this fragment MUST implement the {@link Callbacks}
 * interface.
 */
public class LocationSearchMasterFragment extends Fragment {
    /**
     * The serialization (saved instance state) Bundle key representing the
     * activated item position. Only used on tablets.
     */
    private static final String STATE_ACTIVATED_POSITION = "activated_position";
    private static final String TAG = LocationSearchMasterFragment.class.getName();

    /**
     * The fragment's current callback object, which is notified of list item
     * clicks.
     */
    private Callbacks mCallbacks = sDummyCallbacks;
    private MaterialSpinner autocomplete_site;
    private MaterialSpinner autocomplete_type;
    private AutoCompleteTextView autocomplete_location;
    private CollapsingToolbarLayout collapsingToolbarLayout;
    private Toolbar toolbar;
    private FloatingActionButton search_button;
    private InputMethodManager imm;
    private String site;
    private String type;
    private String location;
    private Map<String, Location> all_locations;
    private Map<String, Location> filtered_locations;
    private List<String> locationList;
    private ArrayAdapter<String> adapter_location;
    private NestedScrollView mContent;
    private LinearLayout mProgress;

    /**
     * The current activated item position. Only used on tablets.
     */
    //private int mActivatedPosition = ListView.INVALID_POSITION;

    /**
     * A callback interface that all activities containing this fragment must
     * implement. This mechanism allows activities to be notified of item
     * selections.
     */
    public interface Callbacks {
        void onSearch(String site, String type, ArrayList<String> ids);
    }

    /**
     * A dummy implementation of the {@link Callbacks} interface that does
     * nothing. Used only when this fragment is not attached to an activity.
     */
    private static Callbacks sDummyCallbacks = new Callbacks() {

        public void onSearch(String site, String type, ArrayList<String> ids){}
    };

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public LocationSearchMasterFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_location_search_master, container, false);

        //collapsingToolbarLayout = (CollapsingToolbarLayout)rootView.findViewById(R.id.collapsingToolbarLayout);
        //collapsingToolbarLayout.setTitle(getString(R.string.title_activity_location_search_master));
        toolbar = (Toolbar)rootView.findViewById(R.id.toolbar);
        toolbar.setTitle(getString(R.string.title_activity_location_search_master));

        autocomplete_site = (MaterialSpinner) rootView.findViewById(R.id.autocomplete_site);
        autocomplete_type = (MaterialSpinner) rootView.findViewById(R.id.autocomplete_type);
        autocomplete_location = (AutoCompleteTextView)rootView.findViewById(R.id.autocomplete_location);

        search_button = (FloatingActionButton)rootView.findViewById(R.id.search_button);

        mContent = (NestedScrollView)rootView.findViewById(R.id.search_form);
        mProgress = (LinearLayout)rootView.findViewById(R.id.search_progress);

        fetchData();


        search_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mContent.setVisibility(View.GONE);
                mProgress.setVisibility(View.VISIBLE);
                search_button.setVisibility(View.GONE);

                Log.i(TAG, "search button clicked");
                site = ((String)autocomplete_site.getSelectedItem()).trim();
                type = ((String)autocomplete_type.getSelectedItem()).trim();
                location = autocomplete_location.getText().toString().trim();

                if(site.equals(getString(R.string.prompt_site)))
                    site = "";
                if(type.equals(getString(R.string.prompt_type)))
                    type = "";
                if(TextUtils.isEmpty(location))
                    location = "";

                if(site.isEmpty() && type.isEmpty() && location.isEmpty()){
                    Snackbar.make(search_button, getString(R.string.fill_at_least_one), Snackbar.LENGTH_SHORT).show();
                    mContent.setVisibility(View.VISIBLE);
                    mProgress.setVisibility(View.GONE);
                    search_button.setVisibility(View.VISIBLE);
                    return;
                }

                ArrayList<String> ids = new ArrayList<String>();
                if(!location.isEmpty()){
                    if(filtered_locations.get(location)!= null)
                    ids.add(filtered_locations.get(location).getObjectId());
                }
                else {
                    for (Location r : filtered_locations.values()) {
                        Log.i(TAG, "sel: " + r.getLocation());
                        ids.add(r.getObjectId());
                    }
                }
                Log.i(TAG, "#ids=" + ids.size());
                if(ids.size() == 0){
                    mContent.setVisibility(View.VISIBLE);
                    mProgress.setVisibility(View.GONE);
                    search_button.setVisibility(View.VISIBLE);
                    Snackbar.make(search_button, getString(R.string.search_no_result), Snackbar.LENGTH_SHORT).show();

                }
                else {
                    mContent.setVisibility(View.VISIBLE);
                    mProgress.setVisibility(View.GONE);
                    search_button.setVisibility(View.VISIBLE);
                    mCallbacks.onSearch(site, type, ids);
                }


            }
        });

        return rootView;
    }

    @Override
    public void onStop() {
        super.onStop();
        FindCallback<Location> callback;

    }

    private void fetchData(){
        mContent.setVisibility(View.GONE);
        mProgress.setVisibility(View.VISIBLE);
        search_button.setVisibility(View.GONE);
        getLocations(new FindCallback<Location>() {
            @Override
            public void done(List<Location> list, ParseException e) {


                try{
                    if (e == null) {
                        if (list != null)
                            Log.i(TAG, "" + list.size() + " items fetched");
                        else
                            Log.i(TAG, "No items fetched.");

                        all_locations = new HashMap<String, Location>();
                        for (Location r : list) {
                            all_locations.put(r.getLocation(), r);
                            filtered_locations = all_locations;
                        }


                        Set<String> siteList = new TreeSet<String>();
                        Set<String> typeList = new TreeSet<String>();
                        locationList = new ArrayList<String>();

                        for (Location r : list) {
                            siteList.add(r.getSite());
                            typeList.add(r.getType());
                            locationList.add(r.getLocation());
                        }

                        ArrayAdapter<String> adapter_course = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_dropdown_item, new ArrayList<String>(siteList));
                        autocomplete_site.setAdapter(adapter_course);
                        autocomplete_site.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                            @Override
                            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                                doFilter();
                            }

                            @Override
                            public void onNothingSelected(AdapterView<?> parent) {

                            }
                        });

                        ArrayAdapter<String> adapter_teacher = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_dropdown_item, new ArrayList<String>(typeList));
                        autocomplete_type.setAdapter(adapter_teacher);

                        autocomplete_type.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                            @Override
                            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                                doFilter();
                            }

                            @Override
                            public void onNothingSelected(AdapterView<?> parent) {

                            }
                        });

                        adapter_location = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_dropdown_item_1line, locationList);
                        autocomplete_location.setAdapter(adapter_location);

                        if (site != null)
                            autocomplete_site.setSelection(CampusPolitoApplication.getStringItemPosition(new ArrayList<String>(siteList), site) + 1);

                        if (type != null)
                            autocomplete_type.setSelection(CampusPolitoApplication.getStringItemPosition(new ArrayList<String>(typeList), type) + 1);

                        if (location != null)
                            autocomplete_location.setText(location);
                    } else {
                        Log.i(TAG, "fetch error: " + e.getMessage());
                    }

                    mContent.setVisibility(View.VISIBLE);
                    mProgress.setVisibility(View.GONE);
                    search_button.setVisibility(View.VISIBLE);
                }
                catch (Exception ex){
                    Log.d(TAG, "Exception during background task : "+ ex.getMessage());
                }


            }
        });
    }


    public void getLocations(FindCallback<Location> callback) {
        ParseQuery<Location> query = ParseQuery.getQuery("Location");
        query.orderByAscending("location");
        query.setLimit(500);
        query.findInBackground(callback);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        Log.i(TAG, "onViewCreated");
        super.onViewCreated(view, savedInstanceState);

        if(all_locations == null){
            Log.i(TAG, "onViewCreated, retrieving data");
            fetchData();
        }

        // Restore data
        if (savedInstanceState != null) {
            Log.i(TAG, "onViewCreated, restoring saved state");
            site = savedInstanceState.getString(Location.SITE);
            type = savedInstanceState.getString(Location.TYPE);
            location = savedInstanceState.getString(Location.LOCATION);

        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        // Activities containing this fragment must implement its callbacks.
        if (!(activity instanceof Callbacks)) {
            throw new IllegalStateException("Activity must implement fragment's callbacks.");
        }

        mCallbacks = (Callbacks) activity;

        //TODO dove?
        imm = (InputMethodManager)activity.getSystemService(Context.INPUT_METHOD_SERVICE);
    }

    @Override
    public void onDetach() {
        super.onDetach();

        // Reset the active callbacks interface to the dummy implementation.
        mCallbacks = sDummyCallbacks;
    }


    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        outState.putString(Location.LOCATION, autocomplete_location.getText().toString().trim());
        outState.putString(Location.SITE, ((String)autocomplete_site.getSelectedItem()).trim());
        outState.putString(Location.TYPE, ((String)autocomplete_type.getSelectedItem()).trim());

    }

    private void doFilter(){
        site = ((String)autocomplete_site.getSelectedItem()).trim();
        type = ((String)autocomplete_type.getSelectedItem()).trim();




        if(site.equals(getString(R.string.prompt_site)))
            site = "";

        if(type.equals(getString(R.string.prompt_type)))
            type = "";

        Log.d(TAG, "site: " + site + " type: " + type);


        filtered_locations = new HashMap<String, Location>();
        locationList = new ArrayList<String>();
        List<String> new_list = new ArrayList<String>();
        for(Location location : all_locations.values()){
            if((site == null || site.isEmpty() || location.getSite().equals(site))
                    && (type == null || type.isEmpty() || location.getType().equals(type))){
                filtered_locations.put(location.getLocation(), location);
                locationList.add(location.getLocation());
            }
            /*else{
                Log.i(TAG, ""+ location.getObjectId() + " discarded");
            }*/
        }

        Log.i(TAG, ""+locationList.size() + " locations retrieved");
        adapter_location = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_dropdown_item, locationList);
        autocomplete_location.setAdapter(adapter_location);
    }

}
