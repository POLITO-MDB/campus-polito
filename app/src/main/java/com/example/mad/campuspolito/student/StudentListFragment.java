package com.example.mad.campuspolito.student;

import android.app.Activity;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ListFragment;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.ListView;

import com.example.mad.campuspolito.R;
import com.example.mad.campuspolito.model.AppUser;
import com.example.mad.campuspolito.model.Student;
import com.parse.ParseQuery;
import com.parse.ParseQueryAdapter;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by mdb on 23/07/15.
 */
public class StudentListFragment extends ListFragment implements ParseQueryAdapter.OnQueryLoadListener<Student>{
    
    public static final String LIST_TYPE = "LIST_TYPE";
    public static final String LIST_NEW_CONVERSATION = "LIST_NEW_CONVERSATION";
    public static final String LIST_SEARCH_STUDENTS = "LIST_SEARCH_STUDENTS";
    public static final String QUERY_STRING = "QUERY_STRING";

    private static final String SEARCH_PARAMS = "SEARCH_PARAMS";
    //min param length for input strings in search with keywords
    private static final int MIN_PARAM_LENGTH = 3;

    private static final String TAG = StudentListFragment.class.getName();

    private LinearLayout mProgressBar;
    private FrameLayout mContent;
    private List<Student> studentList;
    private ArrayList<String> searchParamsList;

    private String mListType;

    private StudentsAdapter adapter = null;
    /**
     * The serialization (saved instance state) Bundle key representing the
     * activated item position. Only used on tablets.
     */
    private static final String STATE_ACTIVATED_POSITION = "activated_position";

    /**
     * The fragment's current callback object, which is notified of list item
     * clicks.
     */
    private Callbacks mCallbacks = sDummyCallbacks;

    /**
     * The current activated item position. Only used on tablets.
     */
    private int mActivatedPosition = ListView.INVALID_POSITION;

    @Override
    public void onLoading() {
        Log.d(TAG, "Loading students...");
        mProgressBar.setVisibility(View.VISIBLE);
        mContent.setVisibility(View.GONE);
    }

    @Override
    public void onLoaded(List<Student> list, Exception e) {
        if(list != null)
            Log.d(TAG, "Students loaded...(number "+list.size()+" )");
        else
            Log.d(TAG, "Students loaded...empty list.");

        mProgressBar.setVisibility(View.GONE);
        mContent.setVisibility(View.VISIBLE);

        if(e == null)
            studentList = list;
        else{
            Log.e(TAG, "An error occurred while loading students list from Parse service...",e);
            Snackbar.make(mContent, R.string.parse_generic_error, Snackbar.LENGTH_LONG).show();
        }

    }

    public void onUpdateContent() {
        if(adapter != null)
            adapter.loadObjects();
    }

    public static ArrayList<String> createSearchParamsList(String queryString){
        ArrayList<String> strings = new ArrayList<>();
        if(queryString != null && !TextUtils.isEmpty(queryString)){
            String[] params = queryString.split("\\s+");
            for(String param : params){
                if(param.length() >= MIN_PARAM_LENGTH)
                    strings.add(param.toLowerCase());
            }
        }
        return strings;
    }

    public void onSearchWithQueryString(String queryString){

        searchParamsList.clear();

        searchParamsList = createSearchParamsList(queryString);

        performQuery();
    }

    /**
     * A callback interface that all activities containing this fragment must
     * implement. This mechanism allows activities to be notified of item
     * selections.
     */
    public interface Callbacks {
        /**
         * Callback for when an item has been selected.
         */
        public void onStudentSelected(String id);
    }

    /**
     * A dummy implementation of the {@link Callbacks} interface that does
     * nothing. Used only when this fragment is not attached to an activity.
     */
    private static Callbacks sDummyCallbacks = new Callbacks() {
        @Override
        public void onStudentSelected(String id) {
        }
    };

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public StudentListFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        searchParamsList = null;

        if(getArguments().containsKey(LIST_TYPE)){
            mListType = getArguments().getString(LIST_TYPE);
            if(getArguments().containsKey(QUERY_STRING))
                searchParamsList = createSearchParamsList(getArguments().getString(QUERY_STRING));

        }

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.students_list,container,false);

        mProgressBar = (LinearLayout)v.findViewById(R.id.list_progress);
        mContent = (FrameLayout)v.findViewById(R.id.content);
        return v;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        // Restore the previously serialized activated item position.
        if (savedInstanceState != null
                && savedInstanceState.containsKey(STATE_ACTIVATED_POSITION)) {
            setActivatedPosition(savedInstanceState.getInt(STATE_ACTIVATED_POSITION));
        }
        if(savedInstanceState != null){
            mListType = savedInstanceState.getString(LIST_TYPE);
            if(savedInstanceState.containsKey(SEARCH_PARAMS))
                searchParamsList = savedInstanceState.getStringArrayList(SEARCH_PARAMS);
        }



        if(mListType == null){
            Snackbar.make(mContent, R.string.parse_generic_error, Snackbar.LENGTH_SHORT).show();
            Log.d(TAG, "No list type has been selected for StudentListFragment");
        }
        else
            performQuery();
    }

    private void performQuery(){
        if(mListType.equals(LIST_NEW_CONVERSATION)){
            adapter = new StudentsAdapter(getActivity(), new ParseQueryAdapter.QueryFactory<Student>() {
                @Override
                public ParseQuery<Student> create() {
                    ParseQuery<Student> query = ParseQuery.getQuery(Student.class);

                    query.whereNotEqualTo(Student.USER, AppUser.getCurrentUser());
                    query.orderByAscending(Student.SURNAME);
                    query.whereExists(Student.USER);
                    query.whereEqualTo(Student.IS_PUBLIC, true);

                    return query;

                }
            });
        }
        else if (mListType.equals(LIST_SEARCH_STUDENTS)){
            adapter = new StudentsAdapter(getActivity(), new ParseQueryAdapter.QueryFactory<Student>() {
                @Override
                public ParseQuery<Student> create() {

                    if(searchParamsList == null || searchParamsList.size() == 0){
                        ParseQuery<Student> fetchAllStudentsQuery = ParseQuery.getQuery(Student.class);
                        fetchAllStudentsQuery.orderByAscending(Student.SURNAME);
                        fetchAllStudentsQuery.whereExists(Student.USER);
                        fetchAllStudentsQuery.whereEqualTo(Student.IS_PUBLIC, true);
                        return fetchAllStudentsQuery;
                    }

                    ArrayList<ParseQuery<Student>> searchParamsQueries = new ArrayList<>();

                    for(String param : searchParamsList){
                        ParseQuery<Student> searchStudentOnParam = ParseQuery.getQuery(Student.class);
                        searchStudentOnParam.whereContains(Student.KEYWORDS,  param);
                        searchParamsQueries.add(searchStudentOnParam);
                    }

                    ParseQuery<Student> searchStudentsOnOrCondition = ParseQuery.or(searchParamsQueries);
                    searchStudentsOnOrCondition.orderByAscending(Student.SURNAME);
                    searchStudentsOnOrCondition.whereExists(Student.USER);
                    searchStudentsOnOrCondition.whereEqualTo(Student.IS_PUBLIC, true);

                    return searchStudentsOnOrCondition;
                }
            });
        }

        if(adapter != null){
            adapter.setTextKey(Student.SHORT_DESC);
            adapter.addOnQueryLoadListener(this);

            setListAdapter(adapter);

            if(getActivity() != null && getActivity() instanceof ConversationListActivity)
                if(((ConversationListActivity)getActivity()).mTwoPane)
                    setActivateOnItemClick(true);

            Log.d(TAG, "Adapter set.");
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        // Activities containing this fragment must implement its callbacks.
        if (!(activity instanceof Callbacks)) {
            throw new IllegalStateException("Activity must implement fragment's callbacks.");
        }

        mCallbacks = (Callbacks) activity;
    }

    @Override
    public void onDetach() {
        super.onDetach();

        // Reset the active callbacks interface to the dummy implementation.
        mCallbacks = sDummyCallbacks;
    }

    @Override
    public void onListItemClick(ListView listView, View view, int position, long id) {
        super.onListItemClick(listView, view, position, id);

        // Notify the active callbacks interface (the activity, if the
        // fragment is attached to one) that an item has been selected.
        mCallbacks.onStudentSelected(studentList.get(position).getObjectId());
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        if (mActivatedPosition != ListView.INVALID_POSITION) {
            // Serialize and persist the activated item position.
            outState.putInt(STATE_ACTIVATED_POSITION, mActivatedPosition);
        }
        outState.putString(LIST_TYPE, mListType);
        outState.putStringArrayList(SEARCH_PARAMS, searchParamsList);
    }

        /**
         * Turns on activate-on-click mode. When this mode is on, list items will be
         * given the 'activated' state when touched.
         */
    public void setActivateOnItemClick(boolean activateOnItemClick) {
        // When setting CHOICE_MODE_SINGLE, ListView will automatically
        // give items the 'activated' state when touched.
        getListView().setChoiceMode(activateOnItemClick
                ? ListView.CHOICE_MODE_SINGLE
                : ListView.CHOICE_MODE_NONE);
    }

    private void setActivatedPosition(int position) {
        if (position == ListView.INVALID_POSITION) {
            getListView().setItemChecked(mActivatedPosition, false);
        } else {
            getListView().setItemChecked(position, true);
        }

        mActivatedPosition = position;
    }
}
