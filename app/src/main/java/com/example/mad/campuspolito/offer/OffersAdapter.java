package com.example.mad.campuspolito.offer;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.mad.campuspolito.R;
import com.example.mad.campuspolito.model.AppUser;
import com.example.mad.campuspolito.model.Company;
import com.example.mad.campuspolito.model.Offer;
import com.example.mad.campuspolito.model.Student;
import com.parse.ParseException;
import com.parse.ParseQueryAdapter;
import com.wefika.flowlayout.FlowLayout;

import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * Created by lg on 12/08/15.
 */
public class OffersAdapter extends ParseQueryAdapter<Offer> {

    private static final String STUDENT = "student";

    private Context context;
    private AppUser user;
    private DateFormat dateFormat;
    private List<Offer> studentFavouriteOffers = new ArrayList<Offer>();

    public OffersAdapter(Context context, AppUser user, com.parse.ParseQueryAdapter.QueryFactory<Offer> queryFactory) {
        super(context, queryFactory);
        try {
            this.context = context;
            this.user = user;
            this.dateFormat = android.text.format.DateFormat.getDateFormat(context);
            if(user.isStudent()) {
                Student s = user.getStudentProfile();
                s.fetchIfNeeded();
                this.studentFavouriteOffers = s.getFavouriteOffers();
            }
        }catch(ParseException e){
            e.printStackTrace();
        }
    }

    @Override
    public View getItemView(Offer offer, View v, ViewGroup parent) {
        if (v == null) {
            v = View.inflate(getContext(), R.layout.offer_list_item, null);
        }

        // Take advantage of ParseQueryAdapter's getItemView logic for
        // populating the main TextView/ImageView.
        // The IDs in your custom layout must match what ParseQueryAdapter expects
        // if it will be populating a TextView or ImageView for you.
        super.getItemView(offer, v, parent);
        TextView tag = (TextView)v.findViewById(R.id.tag);
        TextView title = (TextView)v.findViewById(R.id.title);
        TextView company = (TextView)v.findViewById(R.id.company);
        TextView description = (TextView)v.findViewById(R.id.description);
        //TextView expiration_date = (TextView)v.findViewById(R.id.expiration_date);
        ImageView visibility_icon = (ImageView)v.findViewById(R.id.visibility_icon);
        ImageView star_icon = (ImageView)v.findViewById(R.id.star_icon);
        FlowLayout flow_layout = (FlowLayout)v.findViewById(R.id.requirements_tags);

        title.setText(offer.getTitle());
        description.setText(offer.getDescription());

        List<String> requirements = offer.getRequirements();
        flow_layout.removeAllViews();
        //expiration_date.setText(context.getString(R.string.expires_on) + " " + dateFormat.format(offer.getExpireDate()));

        if(user.isStudent()){
            try {
                company.setVisibility(View.VISIBLE);
                Company c = offer.getCompany().fetchIfNeeded();
                company.setText(c.getName());

                Date offer_date = offer.getPublishedAt();
                Date current_date = (Date) Calendar.getInstance().getTime();
                long diff = current_date.getTime() - offer_date.getTime();
                long hours = TimeUnit.HOURS.convert(diff, TimeUnit.MILLISECONDS);
                if (hours < 1) {
                    tag.setText(context.getString(R.string.now));
                } else if (hours < 24) {
                    tag.setText(hours + " " + context.getString(R.string.hours_ago));
                } else if (hours / 24 < 7) {
                    tag.setText(String.format("%d " + context.getString(R.string.days_ago), Math.round(hours / 24.0)));
                } else if (hours / (24.0 * 7) < 8) {
                    tag.setText(String.format("%d " + context.getString(R.string.weeks_ago), Math.round(hours / (24.0 * 7))));
                } else {
                    tag.setText(String.format("%d " + context.getString(R.string.months_ago), hours / Math.round(24.0 * 30)));
                }

                if(requirements != null) {
                    for (String req : requirements) {
                        addTagToFlowLayout(context, req, flow_layout, false);
                    }
                }

                //TODO: click handler to toggle favourite?
                if(studentFavouriteOffers != null && studentFavouriteOffers.contains(offer)){
                    star_icon.setVisibility(View.VISIBLE);
                }
                else{
                    star_icon.setVisibility(View.GONE);
                }
            }catch(ParseException e){
                e.printStackTrace();
            }
        }
        else{
            company.setVisibility(View.GONE);
            if(!offer.getIsPublished()) {
                visibility_icon.setVisibility(View.VISIBLE);
            }
            else{
                visibility_icon.setVisibility(View.GONE);
            }

            String msg = "";
            if(offer.getCandidatures() == null || offer.getCandidatures().size() == 0) {
                msg = context.getString(R.string.no_candidates_yet);
            }
            else {
                int count = offer.getCandidatures().size();
                if (count == 1) {
                    msg += count + " " + context.getString(R.string.candidate);
                } else {
                    msg += count + " " + context.getString(R.string.candidates);
                }
            }
            addTagToFlowLayout(context, msg, flow_layout, true);
        }

        return v;
    }

    private void addTagToFlowLayout(Context c, String tag, FlowLayout tagsContainer, boolean hideHash) {
        View t = null;
        if(hideHash){
            t = ((LayoutInflater)c.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.tag_layout_2,tagsContainer, false);
        }
        else {
            t = ((LayoutInflater) c.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.tag_layout_inactive, tagsContainer, false);
        }
        FlowLayout.LayoutParams params = new FlowLayout.LayoutParams(FlowLayout.LayoutParams.WRAP_CONTENT, FlowLayout.LayoutParams.WRAP_CONTENT);
        params.setMargins(2, 2, 2, 2);
        t.setLayoutParams(params);
        TextView tagText = (TextView) t.findViewById(R.id.tag);
        tagText.setText(tag);
        tagsContainer.addView(t);
    }
}
