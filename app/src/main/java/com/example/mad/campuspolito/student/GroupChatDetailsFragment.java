package com.example.mad.campuspolito.student;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.example.mad.campuspolito.CampusPolitoApplication;
import com.example.mad.campuspolito.R;
import com.example.mad.campuspolito.common.ProfileActivity;
import com.example.mad.campuspolito.model.AppUser;
import com.example.mad.campuspolito.model.Conversation;
import com.example.mad.campuspolito.model.Message;
import com.example.mad.campuspolito.model.Student;
import com.parse.ParseException;
import com.parse.ParsePush;
import com.parse.ParseQuery;
import com.parse.ParseQueryAdapter;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by mdb on 26/07/15.
 */

public class GroupChatDetailsFragment extends Fragment {

    public static final String TAG = GroupChatDetailsFragment.class.getName();
    public static final String ARG_ITEM_ID = "item_id";
    public static final int UPDATE_LIST = 5;

    private String conversationId;
    private Toolbar mToolbar;
    private TextView mGroupName;
    private TextView mGroupDescription;
    private ListView mStudentsList;
    private LinearLayout mContent;
    private LinearLayout mProgress;
    private AutoCompleteTextView mAddStudent;
    private LinearLayout mAddStudentWrapper;
    private FloatingActionButton mAddStudentButton;

    private FetchConversationData task;
    private Conversation conversation;
    private StudentsAdapter adapter;

    Map<String, AppUser> usersMap;
    List<AppUser> usersList;


    public GroupChatDetailsFragment(){

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments().containsKey(ARG_ITEM_ID)) {
            // Load the dummy content specified by the fragment
            // arguments. In a real-world scenario, use a Loader
            // to load content from a content provider.
            //mItem = DummyContent.ITEM_MAP.get(getArguments().getString(ARG_ITEM_ID));
            conversationId = getArguments().getString(ARG_ITEM_ID);
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_show_group_members, container, false);


        mToolbar = (Toolbar)rootView.findViewById(R.id.toolbar);

        // Show the Up button in the action bar.
        if(getActivity() instanceof GroupChatDetailsActivity){
            GroupChatDetailsActivity activity = (GroupChatDetailsActivity)getActivity();
            activity.setSupportActionBar(mToolbar);
            activity.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
        else if (getActivity() instanceof  ConversationListActivity){
            mToolbar.setTitle(R.string.title_activity_group_chat_details);
            mToolbar.setNavigationIcon(R.drawable.ic_clear);
            mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ((ConversationListActivity) getActivity()).onConversationSelected(conversationId);
                }
            });
        }

        mGroupName = (TextView)rootView.findViewById(R.id.group_name);
        mGroupDescription = (TextView)rootView.findViewById(R.id.group_description);
        mStudentsList = (ListView)rootView.findViewById(android.R.id.list);

        mProgress = (LinearLayout)rootView.findViewById(R.id.group_details_progress);
        mContent = (LinearLayout)rootView.findViewById(R.id.group_details_content);
        rootView.findViewById(android.R.id.empty).setVisibility(View.GONE);

        mAddStudentWrapper = (LinearLayout)rootView.findViewById(R.id.add_student_group_wrapper);
        mAddStudent = (AutoCompleteTextView)rootView.findViewById(R.id.add_student_group);
        mAddStudentButton = (FloatingActionButton)rootView.findViewById(R.id.add_student_group_button);

        return rootView;
    }



    @Override
    public void onStop() {
        super.onStop();
        if(task != null)
            task.cancel(true);
    }

    @Override
    public void onResume() {
        super.onResume();

        if(task != null)
            task.cancel(true);

        task = new FetchConversationData(getActivity(),conversationId);
        task.execute();
    }

    private class RemoveUserFromGroupChat extends AsyncTask<Void,Void,Boolean> {


        private final Context ctx;
        private final AppUser user;
        private final Conversation conversation;

        public RemoveUserFromGroupChat(Context ctx, AppUser user, Conversation conversation){
            this.ctx = ctx;
            this.user = user;
            this.conversation = conversation;
        }

        @Override
        protected void onPreExecute() {
            mProgress.setVisibility(View.VISIBLE);
            mStudentsList.setVisibility(View.GONE);
        }

        @Override
        protected Boolean doInBackground(Void... params) {
            try{
                usersMap.put(user.toString(),user);
                usersList.add(user);

                conversation.removeUser(user);
                conversation.save();

                return true;
            }
            catch (ParseException e){
                Log.e(TAG, "Error while removing user from group chat.",e);
                return false;
            }
        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {
            mAddStudent.setAdapter(new ArrayAdapter<AppUser>(getActivity(), android.R.layout.simple_dropdown_item_1line, usersList));
            mProgress.setVisibility(View.GONE);
            mStudentsList.setVisibility(View.VISIBLE);

            if(aBoolean){
                Snackbar.make(mContent,R.string.remove_user_group_result,Snackbar.LENGTH_SHORT).show();
                adapter.loadObjects();
            }

        }
    }
    private class FetchConversationData extends AsyncTask<Void,Void,Boolean> {

        final String conversationId;
        final Context context;

        /**
         * Used in group conversations
         **/
        String groupName;
        String groupDescription;

        public FetchConversationData(Context context, String conversationId) {
            this.context = context;
            this.conversationId = conversationId;
        }

        @Override
        protected void onPreExecute() {
            mProgress.setVisibility(View.VISIBLE);
            mContent.setVisibility(View.GONE);
        }

        @Override
        protected Boolean doInBackground(Void... voids) {

            ParseQuery<Conversation> query = ParseQuery.getQuery(Conversation.class);
            Log.d(TAG, "Performing Async Task for conversation id=" + conversationId);

            try {
                conversation = query.fromLocalDatastore().get(conversationId);
                conversation.fetchIfNeeded();
            } catch (ParseException e) {
                Log.d(TAG, "Failure while fetching from local datastore (no object saved)");
                conversation = null;
            }
            try {

                if (conversation == null) {
                    Log.d(TAG, "Retrieving object from remote store.");
                    query = ParseQuery.getQuery(Conversation.class);
                    conversation = query.get(conversationId);
                    //conversation.pinInBackground();
                    Log.d(TAG, "Object retrieved.");
                }

                groupName = conversation.getGroupName();
                groupDescription = conversation.getGroupDescription();

                Log.d(TAG, "Basic data of conversation retrieved.");


                ParseQuery<Student> query1 = ParseQuery.getQuery(Student.class);
                query1.whereNotContainedIn(Student.USER, conversation.getUsers());
                query1.whereEqualTo(Student.IS_PUBLIC, true);

                List<Student> students  = query1.find();

                usersMap = new HashMap<>();
                usersList = new ArrayList<>();
                AppUser user;
                for(Student s: students){
                    user = s.getUser();
                    user.fetchIfNeeded();
                    usersMap.put(user.toString(), user);
                    usersList.add(user);
                }

                Log.d(TAG, "Get students to add.");

                return true;
            } catch (ParseException e) {
                Log.e(TAG, "Error while fetching conversation data", e);
                return false;
            }
        }

        @Override
        protected void onPostExecute(Boolean success) {

            if (success) {

                mGroupName.setText(groupName);
                mGroupDescription.setText(groupDescription);

                adapter = new StudentsAdapter(getActivity(), new ParseQueryAdapter.QueryFactory<Student>() {
                    @Override
                    public ParseQuery<Student> create() {

                        ParseQuery<Student> query = ParseQuery.getQuery(Student.class);
                        query.whereContainedIn(Student.USER, conversation.getUsers());
                        query.whereEqualTo(Student.IS_PUBLIC,  true);

                        return query;
                    }
                });
                adapter.addOnQueryLoadListener(new ParseQueryAdapter.OnQueryLoadListener<Student>() {
                    @Override
                    public void onLoading() {

                    }

                    @Override
                    public void onLoaded(List<Student> list, Exception e) {
                        mContent.setVisibility(View.VISIBLE);
                        mProgress.setVisibility(View.GONE);
                    }
                });

                adapter.setTextKey(Student.SHORT_DESC);
                mStudentsList.setAdapter(adapter);

                mStudentsList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {


                        Student selected = (Student) parent.getItemAtPosition(position);

                        Intent showStudentProfile = new Intent(getActivity(), ProfileActivity.class);

                        showStudentProfile.putExtra(ProfileActivity.APP_USER_OBJECT_ID, selected.getUser().getObjectId());
                        showStudentProfile.putExtra(ProfileActivity.APP_USER_TYPE, AppUser.PROFILE_STUDENT);

                        startActivity(showStudentProfile);
                    }
                });

                if(conversation.getGroupAdministrator().getObjectId().equals(AppUser.getCurrentUser().getObjectId())){
                    Log.d(TAG, "Activating optional remove of group members and delete group functionality");

                    mAddStudentWrapper.setVisibility(View.VISIBLE);
                    mAddStudent.setAdapter(new ArrayAdapter<AppUser>(getActivity(), android.R.layout.simple_dropdown_item_1line, usersList));
                    mAddStudentButton.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                            String userName = mAddStudent.getText().toString();

                            AppUser selectedUser = usersMap.get(userName);

                            if(selectedUser != null){
                                mAddStudent.setText("");
                                AddUserToConversation task = new AddUserToConversation(context, conversation, selectedUser);
                                task.execute();
                            }
                            else {
                                Snackbar.make(mContent, R.string.add_new_user_to_group_error,Snackbar.LENGTH_LONG).show();
                            }
                        }
                    });

                    if(mToolbar.getMenu().findItem(R.id.action_delete_group) == null)
                        mToolbar.inflateMenu(R.menu.menu_group_details);
                    mToolbar.setOnMenuItemClickListener(new Toolbar.OnMenuItemClickListener() {
                        @Override
                        public boolean onMenuItemClick(MenuItem item) {
                            if (item.getItemId() == R.id.action_delete_group) {
                                new AlertDialog.Builder(getActivity())
                                        .setTitle(getString(R.string.delete_group_title))
                                        .setMessage(getString(R.string.delete_group_message))
                                        .setPositiveButton(getString(R.string.delete_group_positive), new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface dialog, int whichButton) {
                                                DeleteConversationData task = new DeleteConversationData(context, conversation);
                                                task.execute();
                                            }
                                        }).setNegativeButton(getString(R.string.delete_group_negative), new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int whichButton) {
                                        //Do nothing
                                    }
                                }).show();


                                return true;
                            } else return getActivity().onOptionsItemSelected(item);
                        }
                    });

                    Snackbar.make(mContent, R.string.remove_user_group_hint,Snackbar.LENGTH_LONG).show();

                    mStudentsList.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
                        @Override
                        public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {

                            if(conversation.getUsers().size() == 3){
                                Snackbar.make(mContent,R.string.remove_user_group_enough_students,Snackbar.LENGTH_SHORT).show();
                                return true;
                            }
                            Student selected = (Student)parent.getItemAtPosition(position);

                            AppUser selectedUser = null;
                            for(AppUser user : conversation.getUsers()){
                                if(user.getObjectId().equals(selected.getUser().getObjectId())){
                                    selectedUser = user;
                                    break;
                                }
                            }
                            if(selectedUser == null)
                                return true;

                            if(selectedUser.getObjectId().equals(conversation.getGroupAdministrator().getObjectId())){
                                Snackbar.make(mContent,R.string.remove_user_group_administrator,Snackbar.LENGTH_SHORT).show();
                                return true;
                            }

                            final AppUser finalSelectedUser = selectedUser;
                            new AlertDialog.Builder(getActivity())
                                    .setTitle(selectedUser.toString())
                                    .setMessage(getString(R.string.remove_user_group_message))
                                    .setPositiveButton(getString(R.string.remove_user_group_positive), new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int whichButton) {
                                            RemoveUserFromGroupChat task = new RemoveUserFromGroupChat(getActivity(), finalSelectedUser, conversation);
                                            task.execute();
                                        }
                                    }).setNegativeButton(getString(R.string.remove_user_group_negative), new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int whichButton) {
                                    //Do nothing
                                }
                            }).show();

                        return true;
                        }
                    });
                }
                else {
                    mAddStudentWrapper.setVisibility(View.GONE);
                }

            } else {
                mProgress.setVisibility(View.GONE);
                Snackbar.make(mContent, R.string.parse_generic_error, Snackbar.LENGTH_SHORT).show();
                Log.d(TAG, "Fetch of data was unsuccessful");
            }
        }

        @Override
        protected void onCancelled() {
            mProgress.setVisibility(View.GONE);
            Snackbar.make(mContent, R.string.parse_generic_error, Snackbar.LENGTH_SHORT).show();
            Log.d(TAG, "Fetch of data was cancelled");
        }
    }

    private class DeleteConversationData extends AsyncTask<Void,Void,Boolean>{

        private final Context ctx;
        private final Conversation conversation;

        public DeleteConversationData(Context ctx, Conversation conversation){
            this.ctx = ctx;
            this.conversation = conversation;
        }

        @Override
        protected void onPreExecute() {
            mProgress.setVisibility(View.VISIBLE);
            mContent.setVisibility(View.GONE);
        }

        @Override
        protected Boolean doInBackground(Void... params) {
            try{
                Log.d(TAG, "Removing group conversation.");
                ParseQuery<Message> query = ParseQuery.getQuery(Message.class);
                query.whereEqualTo(Message.CONVERSATION,conversation);
                List<Message> messagesList = query.find();
                Log.d(TAG, messagesList.size() + " messages fetched for conversation");

                for(Message m : messagesList){
                    m.delete();
                }
                Log.d(TAG, "Messages removed from conversation.");

                conversation.delete();

                Log.d(TAG, "Conversation removed.");
                return true;

            }
            catch (ParseException e){
                Log.e(TAG, "Error occurred while removing conversation",e);
                return false;
            }
        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {

            if(aBoolean){
                if(getActivity() instanceof GroupChatDetailsActivity){
                    getActivity().setResult(GroupChatDetailsFragment.UPDATE_LIST);
                    getActivity().finish();
                }

                else if (getActivity() instanceof ConversationListActivity) {
                    getActivity().getFragmentManager().popBackStack();
                    mProgress.setVisibility(View.GONE);
                    ((ConversationListActivity) getActivity()).showConversations();
                    mToolbar.removeAllViews();
                }
            }
            else {
                Snackbar.make(mContent,R.string.parse_generic_error,Snackbar.LENGTH_LONG).show();
            }
        }
    }

    private class AddUserToConversation extends AsyncTask<Void,Void,Boolean>{

        private final Context ctx;
        private final Conversation conversation;
        private final AppUser user;

        public AddUserToConversation(Context context, Conversation conversation, AppUser selectedUser) {
            this.ctx = context;
            this.conversation = conversation;
            this.user = selectedUser;
        }

        @Override
        protected void onPreExecute() {
            mProgress.setVisibility(View.VISIBLE);
            mContent.setVisibility(View.GONE);
        }

        @Override
        protected Boolean doInBackground(Void... params) {
            try{
                conversation.addUser(user);
                conversation.save();


                ParsePush studentPush = new ParsePush();
                studentPush.setChannel(CampusPolitoApplication.STUDENT_CHANNEL+user.getObjectId());
                studentPush.setData(getJSONGroupChatPushMessage(conversation.getGroupName()));
                studentPush.send();

                return true;
            }
            catch (ParseException e){
                return false;
            }
        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {

            mProgress.setVisibility(View.GONE);
            mContent.setVisibility(View.VISIBLE);


            if(aBoolean){
                adapter.loadObjects();
                usersMap.remove(user.toString());
                usersList.remove(user);
                mAddStudent.setAdapter(new ArrayAdapter<AppUser>(getActivity(), android.R.layout.simple_dropdown_item_1line, usersList));

                Snackbar.make(mContent,R.string.user_added_to_conversation, Snackbar.LENGTH_SHORT).show();
            }
            else {
                Snackbar.make(mContent,R.string.parse_generic_error,Snackbar.LENGTH_LONG).show();
            }
        }

        private JSONObject getJSONGroupChatPushMessage(String groupName) {
            JSONObject message = new JSONObject();
            try{
                message.put("title",getString(R.string.new_group_conversation_push_msg));
                message.put("alert", groupName);

                return message;

            }catch (JSONException e){
                Log.e(TAG, "Error while creating push message for student.",e);
                return null;
            }
        }
    }
}
