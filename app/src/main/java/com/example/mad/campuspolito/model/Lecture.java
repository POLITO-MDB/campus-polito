package com.example.mad.campuspolito.model;

import com.example.mad.campuspolito.CampusPolitoApplication;
import com.parse.ParseClassName;
import com.parse.ParseObject;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

/**
 * Created by mdb on 20/07/15.
 */
@ParseClassName("Lecture")
public class Lecture extends ParseObject{

    public static final String DAY = "day";
    public static final String MONTH = "month";
    public static final String YEAR = "year";
    public static final String DATE = "date";
    public static final String TIME = "time";
    public static final String START_SLOT = "start_slot";
    public static final String DURATION = "duration";
    public static final String ROOM = "room";
    public static final String COURSE = "course";
    public static final String LECTURE = "lecture";
    public static final String TEACHER = "teacher";

    public Lecture(){

    }

    public void setDay(int day){
        put(DAY, day);
    }
    public int getDay(){
        return getInt(DAY);
    }

    public void setMonth(int month){
        put(MONTH,month);
    }
    public int getMonth(){
        return getInt(MONTH);
    }

    public void setYear(int year){
        put(YEAR,year);
    }

    public int getYear(){
        return getInt(YEAR);
    }

    public void setDate(){
        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd", Locale.getDefault());
        put(DATE,dateFormat.format(getStartCalendar().getTime()));
        put(TIME,getStartCalendar().getTimeInMillis());
    }

    public String getDate(){
        return (String)get(DATE);
    }

    public long getTime(){
        return getLong(TIME);
    }

    public void setStartSlot(int startSlot){
        put(START_SLOT, startSlot);
    }
    public int getStartSlot(){
        return getInt(START_SLOT);
    }

    public void setDuration(int duration){
        put(DURATION, duration);
    }
    public int getDuration(){
        return getInt(DURATION);
    }
    public void setRoom(Location room){
        put(ROOM, room);
    }
    public Location getRoom(){
        return (Location)getParseObject(ROOM);
    }
    public void setCourse(Course course){
        put(COURSE, course);
    }
    public Course getCourse(){
        return (Course) getParseObject(COURSE);
    }

    public Calendar getStartCalendar(){
        Calendar cal = (Calendar) CampusPolitoApplication.startingTime.clone();

        cal.add(Calendar.HOUR_OF_DAY, (getStartSlot() - 1) * CampusPolitoApplication.slotDuration.get(Calendar.HOUR_OF_DAY));
        cal.add(Calendar.MINUTE, (getStartSlot() - 1) * CampusPolitoApplication.slotDuration.get(Calendar.MINUTE) );

        cal.set(Calendar.DAY_OF_MONTH, getDay());
        cal.set(Calendar.MONTH, getMonth());
        cal.set(Calendar.YEAR,getYear());

        return cal;
    }

    public Calendar getEndCalendar(){
        Calendar cal = (Calendar) CampusPolitoApplication.startingTime.clone();

        cal.add(Calendar.HOUR_OF_DAY, (getStartSlot() + getDuration() - 1) * CampusPolitoApplication.slotDuration.get(Calendar.HOUR_OF_DAY));
        cal.add(Calendar.MINUTE, (getStartSlot() + getDuration() - 1) * CampusPolitoApplication.slotDuration.get(Calendar.MINUTE));

        cal.set(Calendar.DAY_OF_MONTH, getDay());
        cal.set(Calendar.MONTH, getMonth());
        cal.set(Calendar.YEAR,getYear());

        return cal;
    }

    //Notifications
    public JSONObject getJSONRepresentation(String message, String teacher) {
        JSONObject msgJSON = new JSONObject();

        try {
            msgJSON.put("title", getCourse().getCode() + " - "
                    +   getCourse().getTitle());
            msgJSON.put("alert", getDate() + " - " + message);
            msgJSON.put(COURSE, getCourse().getObjectId());
            msgJSON.put(LECTURE, getObjectId());
            msgJSON.put(TEACHER, teacher);
        }
        catch (JSONException e){
            return null;
        }
        return msgJSON;
    }
}
