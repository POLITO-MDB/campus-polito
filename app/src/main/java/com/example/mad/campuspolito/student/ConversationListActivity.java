package com.example.mad.campuspolito.student;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TabLayout;
import android.support.v4.app.NavUtils;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.LinearLayout;

import com.example.mad.campuspolito.CampusPolitoApplication;
import com.example.mad.campuspolito.R;
import com.example.mad.campuspolito.common.MainActivity;
import com.example.mad.campuspolito.model.AppUser;
import com.example.mad.campuspolito.model.Conversation;
import com.example.mad.campuspolito.model.Message;
import com.example.mad.campuspolito.model.Student;
import com.parse.ParseException;
import com.parse.ParseQuery;

import java.util.Arrays;
import java.util.List;

/**
 * An activity representing a list of Conversations. This activity
 * has different presentations for handset and tablet-size devices. On
 * handsets, the activity presents a list of items, which when touched,
 * lead to a {@link ConversationDetailActivity} representing
 * item details. On tablets, the activity presents the list of items and
 * item details side-by-side using two vertical panes.
 * <p/>
 * The activity makes heavy use of fragments. The list of items is a
 * {@link ConversationListFragment} and the item details
 * (if present) is a {@link ConversationDetailFragment}.
 * <p/>
 * This activity also implements the required
 * {@link ConversationListFragment.Callbacks} interface
 * to listen for item selections.
 */
public class ConversationListActivity extends AppCompatActivity
        implements ConversationListFragment.Callbacks, StudentListFragment.Callbacks, ConversationDetailFragment.Callbacks{

    private final String TAG = ConversationListActivity.class.getName();
    private final String CONVERSATION_LIST = "conversation_list_tag";
    private final String STUDENTS_LIST = "student_list_tag";
    private Toolbar toolbar;

    private LinearLayout mListProgress;
    private FrameLayout mContent;

    private CreateConversation task;
    private TabLayout.Tab conversationsTab;
    private TabLayout.Tab studentsTab;
    private boolean displayConversations;
    private TabLayout tabLayout;

   /**
     * Whether or not the activity is in two-pane mode, i.e. running on a tablet
     * device.
     */
    protected boolean mTwoPane;
    private String DISPLAY_CONVERSATIONS = "DISPLAY_CONVERSATIONS";

    public static String INTENT_DISPLAY_CONVERSATION = "intent_display_conversation";

    private final String SELECTED_CONVERSATION_ID = "SELECTED_CONVERSATION_ID";
    private String conversationId;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_conversation_list);
        // Show the Up button in the action bar.

        conversationId = null;

        if(getIntent() != null && getIntent().getExtras() != null){
            if(getIntent().getExtras().containsKey(INTENT_DISPLAY_CONVERSATION)){
                if(findViewById(R.id.conversation_detail_container) != null)
                    mTwoPane = true;
                else mTwoPane = false;

                onConversationSelected(getIntent().getExtras().getString(INTENT_DISPLAY_CONVERSATION));
            }
            if(getIntent().getExtras().containsKey(ConversationDetailFragment.ARG_ITEM_ID)){
                conversationId = getIntent().getStringExtra(ConversationDetailFragment.ARG_ITEM_ID);
            }
        }

        toolbar = (Toolbar)findViewById(R.id.toolbar);

        tabLayout = (TabLayout) findViewById(R.id.tabLayout);
        conversationsTab = tabLayout.newTab().setText(getString(R.string.title_conversation_list));
        studentsTab = tabLayout.newTab().setText(getString(R.string.title_student_list));
        tabLayout.addTab(conversationsTab);
        tabLayout.addTab(studentsTab);


        if(savedInstanceState != null){
            if(savedInstanceState.containsKey(SELECTED_CONVERSATION_ID)){
                conversationId = savedInstanceState.getString(SELECTED_CONVERSATION_ID);
            }

            displayConversations = savedInstanceState.getBoolean(DISPLAY_CONVERSATIONS);

            if(displayConversations)
                conversationsTab.select();
            else
                studentsTab.select();
        }
        else{
            conversationsTab.select();
            showConversations();
        }

        Log.d(TAG, "Selected conversation id is * " + conversationId);
        if(conversationId != null && !(getIntent() != null && getIntent().hasExtra(INTENT_DISPLAY_CONVERSATION))) {

            DeleteEventuallyEmptyConversation task = new DeleteEventuallyEmptyConversation(this,conversationId);
            task.execute();
        }

        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                switch (tab.getPosition()) {
                    case 0:
                        showConversations();
                        break;
                    case 1:
                        showStudents();
                        break;
                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    public void showStudents() {

        Bundle arguments = new Bundle();
        arguments.putString(StudentListFragment.LIST_TYPE, StudentListFragment.LIST_NEW_CONVERSATION);

        StudentListFragment fragment = new StudentListFragment();
        fragment.setArguments(arguments);

        if (findViewById(R.id.conversation_detail_container) != null) {
            mTwoPane = true;
        }

        getSupportFragmentManager().beginTransaction()
                .replace(R.id.contentFragment, fragment, STUDENTS_LIST)
                .commit();

    }

    public void showConversations() {
        ConversationListFragment fragment = new ConversationListFragment();

        if (findViewById(R.id.conversation_detail_container) != null) {
            mTwoPane = true;
        }

        getSupportFragmentManager().beginTransaction()
                .replace(R.id.contentFragment, fragment,CONVERSATION_LIST)
                .commit();

    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        outState.putBoolean(DISPLAY_CONVERSATIONS, conversationsTab.isSelected());
        if(conversationId != null)
            outState.putString(SELECTED_CONVERSATION_ID, conversationId);
    }


    /**
     * Callback method from {@link ConversationListFragment.Callbacks}
     * indicating that the item with the given ID was selected.
     */
    @Override
    public void onConversationSelected(String id) {

        conversationId = id;
        Log.d(TAG, "Conversation selected is : " + conversationId);

        if (mTwoPane) {
            // In two-pane mode, show the detail view in this activity by
            // adding or replacing the detail fragment using a
            // fragment transaction.
            Bundle arguments = new Bundle();
            arguments.putString(ConversationDetailFragment.ARG_ITEM_ID, id);
            ConversationDetailFragment fragment = new ConversationDetailFragment();
            fragment.setArguments(arguments);
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.conversation_detail_container, fragment)
                    .commit();

        } else {
            // In single-pane mode, simply start the detail activity
            // for the selected item ID.
            Intent detailIntent = new Intent(this, ConversationDetailActivity.class);
            detailIntent.putExtra(ConversationDetailFragment.ARG_ITEM_ID, id);
            startActivityForResult(detailIntent, 2);
        }
    }

    @Override
    public void onStudentSelected(String id) {
        if(!mTwoPane){
            mContent = (FrameLayout)findViewById(R.id.content);
            mListProgress = (LinearLayout)findViewById(R.id.list_progress);
        }
        task = new CreateConversation(this,id);
        task.execute();

    }

    @Override
    protected void onStop() {
        super.onStop();
        if(task != null)
            task.cancel(true);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_home,menu);
        getMenuInflater().inflate(R.menu.menu_msg, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if (id == android.R.id.home) {
            // This ID represents the Home or Up button. In the case of this
            // activity, the Up button is shown. Use NavUtils to allow users
            // to navigate up one level in the application structure. For
            // more details, see the Navigation pattern on Android Design:
            //
            // http://developer.android.com/design/patterns/navigation.html#up-vs-back
            //
            NavUtils.navigateUpFromSameTask(this);
            return true;
        }

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        if (id == R.id.action_logout) {
            CampusPolitoApplication.unsubscribeFromAllChannels();
            AppUser.logOut();
            Intent homeIntent = new Intent(this, MainActivity.class);
            homeIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(homeIntent);
            finish();
            return true;
        }

        if(id == R.id.action_new_group){
            Intent newGroupIntent = new Intent(this,CreateGroupActivity.class);
            startActivityForResult(newGroupIntent, 1);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, final Intent data) {

        if(requestCode == 1){
            if(resultCode == CreateGroupActivity.ACTION_RESULT_GROUP_CREATED){
                ((ConversationListFragment) getSupportFragmentManager()
                        .findFragmentByTag(CONVERSATION_LIST)).onUpdateContent();

                Snackbar.make(findViewById(R.id.content),R.string.new_group_success,Snackbar.LENGTH_SHORT).
                        setAction(R.string.show, new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                onConversationSelected(data.getStringExtra(CreateGroupActivity.NEW_CONVERSATION_ID));
                            }
                        }).show();
            }
        }
        if(requestCode == 2){
                onUpdateContent();
        }
    }

    @Override
    public void onGroupDetailsSelected(String id) {

        if(findViewById(R.id.conversation_detail_container) != null){ //two-panel
            GroupChatDetailsFragment fragment = new GroupChatDetailsFragment();
            Bundle arguments = new Bundle();
            arguments.putString(GroupChatDetailsFragment.ARG_ITEM_ID, id);
            fragment.setArguments(arguments);

            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.conversation_detail_container,fragment).commit();
        }

    }

    public void onUpdateContent() {
        if(conversationsTab.isSelected()){
            if(!isFinishing())
            ((ConversationListFragment) getSupportFragmentManager()
                    .findFragmentByTag(CONVERSATION_LIST)).onUpdateContent();

        }
        else if (studentsTab.isSelected()){
            if(!isFinishing())
            ((StudentListFragment) getSupportFragmentManager()
                    .findFragmentByTag(STUDENTS_LIST)).onUpdateContent();
        }
    }

    private class CreateConversation extends AsyncTask<Void,Void,Boolean> {

        final Context context;
        String studentObjectId;




        public CreateConversation(Context context, String studentObjectId){
            this.context = context;
            this.studentObjectId = studentObjectId;
        }

        @Override
        protected void onPreExecute() {
            if(!mTwoPane){
                mListProgress.setVisibility(View.VISIBLE);
                mContent.setVisibility(View.GONE);
            }
        }

        @Override
        protected Boolean doInBackground(Void... voids) {

            try{

                ParseQuery<Student> query = ParseQuery.getQuery(Student.class);
                Student other = query.get(studentObjectId);

                ParseQuery<Conversation> query1 = ParseQuery.getQuery(Conversation.class);

                query1.whereContainsAll(Conversation.USERS, Arrays.asList(AppUser.getCurrentUser(), other.getUser()));
                query1.whereDoesNotExist(Conversation.OPT_GRP_NAME);

                List<Conversation> conversation = query1.find();

                if(conversation.isEmpty()){
                    Conversation c = new Conversation();
                    c.addUser((AppUser) AppUser.getCurrentUser());
                    c.addUser(other.getUser());

                    c.save();

                    conversationId = c.getObjectId();
                }
                else{
                    conversationId = conversation.get(0).getObjectId();
                }


                Log.d(TAG, "Conversation selected is : " + conversationId);



                return true;
            }
            catch (ParseException e ){
                Log.e(TAG, "Error while creating conversation",e);
                return false;
            }

        }

        @Override
        protected void onPostExecute(Boolean success) {

            if(success){

                if (mTwoPane) {
                    // In two-pane mode, show the detail view in this activity by
                    // adding or replacing the detail fragment using a
                    // fragment transaction.
                    Bundle arguments = new Bundle();
                    arguments.putString(ConversationDetailFragment.ARG_ITEM_ID, conversationId);
                    ConversationDetailFragment fragment = new ConversationDetailFragment();
                    fragment.setArguments(arguments);
                    getSupportFragmentManager().beginTransaction()
                            .replace(R.id.conversation_detail_container, fragment)
                            .commit();

                } else {
                    // In single-pane mode, simply start the detail activity
                    // for the selected item ID.
                    Intent detailIntent = new Intent(context, ConversationDetailActivity.class);
                    detailIntent.putExtra(ConversationDetailFragment.ARG_ITEM_ID, conversationId);
                    startActivityForResult(detailIntent,2);
                }
            }
            else {
                mListProgress.setVisibility(View.GONE);
                Snackbar.make(mContent, R.string.parse_generic_error, Snackbar.LENGTH_SHORT).show();
                Log.d(TAG, "Save of data was unsuccessful");
            }
        }

        @Override
        protected void onCancelled() {
            mListProgress.setVisibility(View.GONE);
            Snackbar.make(mContent, R.string.parse_generic_error, Snackbar.LENGTH_SHORT).show();
            Log.d(TAG, "Saving of data was cancelled");
        }
    }

    private class DeleteEventuallyEmptyConversation extends AsyncTask<Void,Void,Boolean>{

        private final Context ctx;
        private Conversation conversation;
        private final String conversationId;
        private boolean deleted;

        public DeleteEventuallyEmptyConversation(Context ctx, String conversationId){
            this.ctx = ctx;
            this.conversationId = conversationId;
        }

        @Override
        protected void onPreExecute() {
            deleted = false;
        }

        @Override
        protected Boolean doInBackground(Void... params) {
            try{
                ParseQuery<Conversation> query1 = ParseQuery.getQuery(Conversation.class);
                conversation = query1.get(conversationId);
                if(conversation == null)
                {
                    Log.e(TAG, "Something wrong happened, conversation is null.");
                    return true;
                }
                else if(conversation.isGroupChat()){
                    Log.d(TAG, "Conversation is a group chat, do not remove if empty.");
                    return true;
                }

                Log.d(TAG, "Checking if conversation is empty.");
                ParseQuery<Message> query = ParseQuery.getQuery(Message.class);
                query.whereEqualTo(Message.CONVERSATION, conversation);
                int messageCount = query.count();
                Log.d(TAG, messageCount + " messages fetched for conversation");

                if(messageCount == 0){
                    conversation.delete();
                    deleted = true;
                    Log.d(TAG, "Conversation is empty, so it is removed.");
                }
                return true;

            }
            catch (ParseException e){
                Log.e(TAG, "Error occurred while removing conversation",e);
                return false;
            }
        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {
            if(!aBoolean){
                Snackbar.make(findViewById(R.id.rootLayout),R.string.parse_generic_error,Snackbar.LENGTH_LONG).show();
            }
            else{
                if(deleted){
                    Snackbar.make(findViewById(R.id.rootLayout),R.string.automatic_remove_empty_conversation, Snackbar.LENGTH_SHORT).show();
                    onUpdateContent();
                }
            }

        }
    }
}
