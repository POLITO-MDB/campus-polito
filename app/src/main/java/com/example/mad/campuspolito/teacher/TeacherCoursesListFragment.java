package com.example.mad.campuspolito.teacher;

import android.content.Intent;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.ListView;

import com.example.mad.campuspolito.R;
import com.example.mad.campuspolito.common.TimetableListActivity;
import com.example.mad.campuspolito.common.ShowCourseDetailsActivity;
import com.example.mad.campuspolito.model.AppUser;
import com.example.mad.campuspolito.model.Course;
import com.example.mad.campuspolito.model.Lecture;
import com.example.mad.campuspolito.model.Teacher;

import java.util.ArrayList;
import java.util.List;

/**
 * A placeholder fragment containing a simple view.
 */
public class TeacherCoursesListFragment extends Fragment {

    private static final String TAG = TeacherCoursesListFragment.class.getName();

    private LinearLayout mProgress;
    private LinearLayout mEmpty;
    private ListView mList;
    private FrameLayout mContent;

    private Teacher teacher = ((AppUser)AppUser.getCurrentUser()).getTeacherProfile();

    public TeacherCoursesListFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final View v = inflater.inflate(R.layout.fragment_teacher_courses_list, container, false);

        mProgress = (LinearLayout)v.findViewById(R.id.list_progress);
        mEmpty = (LinearLayout)v.findViewById(android.R.id.empty);
        mList = (ListView)v.findViewById(android.R.id.list);
        mContent = (FrameLayout)v.findViewById(R.id.content);


        if((teacher.getCourses() != null) && !teacher.getCourses().isEmpty()){
            mEmpty.setVisibility(View.GONE);

            CourseAdapter courseAdapter = new CourseAdapter(getActivity(), teacher.getCourses(), new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String courseId = (String)v.getTag();
                    Intent intent = new Intent(v.getContext(), LectureEditorActivity.class);
                    intent.putExtra(Lecture.COURSE, courseId);
                    startActivityForResult(intent, LectureEditorActivity.EDIT_LECTURE);
                }
            });
            mList.setAdapter(courseAdapter);

            mList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    Course courseItem = (Course) parent.getItemAtPosition(position);

                    Intent showCourseDetails = new Intent(getActivity(), ShowCourseDetailsActivity.class);
                    showCourseDetails.putExtra(ShowCourseDetailsActivity.COURSE_ID, courseItem.getObjectId());
                    startActivity(showCourseDetails);

                }
            });

        }
        else {
            mList.setVisibility(View.GONE);
        }

        /**
        CourseAdapter courseAdapter = new CourseAdapter(getActivity(), teacher.getCourses());
        mList.setAdapter(courseAdapter);

        mList.setOnItemClickListener(new AdapterView.OnItemClickListener(){
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Course courseItem = (Course)parent.getItemAtPosition(position);
                Intent intent = new Intent(getActivity().getApplicationContext(), TimetableListActivity.class);
                intent.putExtra(Lecture.COURSE, courseItem.getObjectId());
                startActivity(intent);
            }
        });
        **/

        return v;
    }

    /*@Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == LectureEditorActivity.EDIT_LECTURE){
            if(resultCode == LectureEditorActivity.LECTURE_EDITED){
                Log.d(TAG,"Lecture modified");
                //setResult(LECTURE_STATUS_MODIFIED);
            }
        }
    }*/

}
