package com.example.mad.campuspolito.locations;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.example.mad.campuspolito.CampusPolitoApplication;
import com.example.mad.campuspolito.R;
import com.example.mad.campuspolito.common.MainActivity;
import com.example.mad.campuspolito.model.AppUser;
import com.example.mad.campuspolito.model.Location;
import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseQuery;

import java.util.ArrayList;
import java.util.List;


/**
 * An activity representing a single Item detail screen. This
 * activity is only used on handset devices. On tablet-size devices,
 * item details are presented side-by-side with a list of items
 * in a {@link LocationSearchMasterActivity}.
 * <p/>
 * This activity is mostly just a 'shell' activity containing nothing
 * more than a {@link LocationSearchDetailFragment}.
 */
public class LocationSearchDetailActivity extends AppCompatActivity {

    private static final String TAG = LocationSearchDetailActivity.class.getName();

    public static final String LOCATION = "location";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_location_search_detail);

        setSupportActionBar((Toolbar) findViewById(R.id.toolbar));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        // savedInstanceState is non-null when there is fragment state
        // saved from previous configurations of this activity
        // (e.g. when rotating the screen from portrait to landscape).
        // In this case, the fragment will automatically be re-added
        // to its container so we don't need to manually add it.
        // For more information, see the Fragments API guide at:
        //
        // http://developer.android.com/guide/components/fragments.html
        //
        // Create the detail fragment and add it to the activity        // using a fragment transaction.

        if (savedInstanceState == null) {
            Log.i(TAG, "onCreate, savedInstanceState is null");
            final LocationSearchDetailFragment fragment = new LocationSearchDetailFragment();
            final Bundle arguments = new Bundle();
            final Intent intent = getIntent();

            if(intent.getAction() == null || intent.getAction().isEmpty()){
                Log.i(TAG, "bypassing master activity");
                final String location = intent.getStringExtra(LocationSearchDetailActivity.LOCATION);
                if(location == null || location.isEmpty()){
                    Toast.makeText(getApplicationContext(), getString(R.string.location_not_found), Toast.LENGTH_SHORT).show();
                    onBackPressed();
                    return;
                }
                Log.i(TAG, "retrieving location id for location " + location);
                getLocation(location, new FindCallback<Location>() {
                    @Override
                    public void done(List<Location> locations, ParseException e) {
                        if (e == null) {

                            if(locations == null || locations.size() == 0){
                                Toast.makeText(getApplicationContext(), getString(R.string.location_not_found), Toast.LENGTH_SHORT).show();
                                onBackPressed();
                                return;
                            }

                            ArrayList<String> ids = new ArrayList<String>();
                            for(Location l : locations){
                                ids.add(l.getObjectId());
                            }
                            arguments.putStringArrayList(LocationSearchDetailFragment.IDS, ids);
                            fragment.setArguments(arguments);
                            getFragmentManager().beginTransaction()
                                    .add(R.id.item_detail_container, fragment, "pippo")
                                            //.addToBackStack("pippo")
                                    .commit();
                        } else {
                            Toast.makeText(getApplicationContext(), getString(R.string.location_not_found), Toast.LENGTH_SHORT).show();
                            Log.i(TAG, "Error: " + e.getMessage());
                            onBackPressed();
                            return;
                        }
                    }
                });

            }
            else if(intent.getAction().equals(LocationSearchMasterActivity.class.getName())){
                Log.i(TAG, "from master");
                //arguments.putString(LocationSearchDetailFragment.SITE, intent.getStringExtra(LocationSearchDetailFragment.SITE));
                //arguments.putString(LocationSearchDetailFragment.TYPE, intent.getStringExtra(LocationSearchDetailFragment.TYPE));
                arguments.putStringArrayList(LocationSearchDetailFragment.IDS, intent.getStringArrayListExtra(LocationSearchDetailFragment.IDS));
                fragment.setArguments(arguments);

                getFragmentManager().beginTransaction()
                        .add(R.id.item_detail_container, fragment, "pippo")
                                //.addToBackStack("pippo")
                        .commit();
            }
        }

    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }


    public void getLocation(String location, FindCallback<Location> callback) {
        ParseQuery<Location> query = ParseQuery.getQuery(Location.class);
        query.whereEqualTo(Location.LOCATION, location);
        query.findInBackground(callback);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.menu_home,menu);

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            // This ID represents the Home or Up button. In the case of this
            // activity, the Up button is shown. Use NavUtils to allow users
            // to navigate up one level in the application structure. For
            // more details, see the Navigation pattern on Android Design:
            //
            // http://developer.android.com/design/patterns/navigation.html#up-vs-back
            //
            NavUtils.navigateUpTo(this, new Intent(this, LocationSearchMasterActivity.class));
            return true;
        }
        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        if (id == R.id.action_logout) {
            CampusPolitoApplication.unsubscribeFromAllChannels();
            AppUser.logOut();
            Intent homeIntent = new Intent(this, MainActivity.class);
            homeIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(homeIntent);
            finish();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

}
