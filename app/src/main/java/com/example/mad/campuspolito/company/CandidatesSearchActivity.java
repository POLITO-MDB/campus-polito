package com.example.mad.campuspolito.company;

import android.app.Activity;
import android.app.SearchManager;
import android.content.Intent;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.example.mad.campuspolito.CampusPolitoApplication;
import com.example.mad.campuspolito.R;
import com.example.mad.campuspolito.common.MainActivity;
import com.example.mad.campuspolito.common.ProfileActivity;
import com.example.mad.campuspolito.model.AppUser;
import com.example.mad.campuspolito.model.Student;
import com.example.mad.campuspolito.student.ConversationListFragment;
import com.example.mad.campuspolito.student.StudentListFragment;
import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseQuery;

import java.util.List;

public class CandidatesSearchActivity extends AppCompatActivity implements StudentListFragment.Callbacks, FindCallback<AppUser>{
    private static final String TAG = CandidatesSearchActivity.class.getName();

    private static final String FRAGMENT_TAG = "FRAGMENT_TAG";
    //private ArrayList<TagGroup> tagList;
    private String searchKeywords;
    private SearchView searchView;
    private LinearLayout mProgress;
    private FrameLayout mContent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_candidates_search);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        handleIntent(getIntent());

    }

    @Override
    protected void onNewIntent(Intent intent) {
        setIntent(intent);
        handleIntent(intent);
    }

    private void handleIntent(Intent intent){

        if (Intent.ACTION_VIEW.equals(intent.getAction())) {
            // actions performed after a search result is clicked?



            finish();
        } else if (Intent.ACTION_SEARCH.equals(intent.getAction())) {
            // actions performed after search button is clicked?


            //searchKeywords gets string from search box
            searchKeywords = intent.getStringExtra(SearchManager.QUERY);

            Log.i(TAG,"The user searched the keyword:"+searchKeywords);


        }

        if(StudentListFragment.createSearchParamsList(searchKeywords).size() > 0){
            Bundle arguments = new Bundle();
            arguments.putString(StudentListFragment.LIST_TYPE, StudentListFragment.LIST_SEARCH_STUDENTS);
            arguments.putString(StudentListFragment.QUERY_STRING, searchKeywords);

            StudentListFragment fragment = new StudentListFragment();
            fragment.setArguments(arguments);

            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.contentFragment, fragment, FRAGMENT_TAG)
                    .commit();

            findViewById(R.id.prompt).setVisibility(View.GONE);
        }
    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_candidates_search, menu);
        getMenuInflater().inflate(R.menu.menu_home, menu);
        MenuItem searchItem = menu.findItem(R.id.action_search);
        // Retrieve the SearchView and plug it into SearchManager
        searchView = (SearchView) MenuItemCompat.getActionView(searchItem);
        SearchManager searchManager = (SearchManager) getSystemService(SEARCH_SERVICE);
        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        if (id == R.id.action_logout) {
            CampusPolitoApplication.unsubscribeFromAllChannels();
            AppUser.logOut();
            Intent homeIntent = new Intent(this, MainActivity.class);
            homeIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(homeIntent);
            finish();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if(mProgress != null)
            mProgress.setVisibility(View.GONE);
        if(mContent != null)
            mContent.setVisibility(View.VISIBLE);
    }

    @Override
    public void onStudentSelected(String id) {
        mProgress = (LinearLayout) findViewById(R.id.list_progress);
        mContent = (FrameLayout) findViewById(R.id.content);

        mProgress.setVisibility(View.VISIBLE);
        mContent.setVisibility(View.GONE);

        ParseQuery<Student> studentQuery = ParseQuery.getQuery(Student.class);
        studentQuery.whereEqualTo("objectId", id);

        ParseQuery<AppUser> query = ParseQuery.getQuery(AppUser.class);
        query.whereMatchesQuery(AppUser.PROFILE_STUDENT, studentQuery);

        query.findInBackground(this);

    }

    @Override
    public void done(List<AppUser> list, ParseException e) {

        if(e != null){
            Snackbar.make(mContent, R.string.parse_generic_error, Snackbar.LENGTH_LONG).show();
            Log.e(TAG, "Something happened while fetching student data.", e);
        }
        else {
            if(list.size() > 0){
                Intent profileIntent = new Intent(this, ProfileActivity.class);
                profileIntent.putExtra(ProfileActivity.APP_USER_OBJECT_ID, list.get(0).getObjectId());
                profileIntent.putExtra(ProfileActivity.APP_USER_TYPE, AppUser.PROFILE_STUDENT);
                startActivity(profileIntent);
            }
        }

    }
}
