package com.example.mad.campuspolito.locations;

import android.content.Intent;
import android.support.v4.app.NavUtils;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

import com.example.mad.campuspolito.CampusPolitoApplication;
import com.example.mad.campuspolito.R;
import com.example.mad.campuspolito.common.MainActivity;
import com.example.mad.campuspolito.model.AppUser;

import java.util.ArrayList;

public class LocationSearchMasterActivity extends AppCompatActivity implements LocationSearchMasterFragment.Callbacks {

    private static final String TAG = LocationSearchMasterActivity.class.getName();
    /**
     * Whether or not the activity is in two-pane mode, i.e. running on a tablet
     * device.
     */
    private boolean mTwoPane;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_location_search_master);

        setSupportActionBar((Toolbar) findViewById(R.id.toolbar));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        if (findViewById(R.id.item_detail_container) != null) {
            // The detail container view will be present only in the
            // large-screen layouts (res/values-large and
            // res/values-sw600dp). If this view is present, then the
            // activity should be in two-pane mode.
            mTwoPane = true;

            // In two-pane mode, list items should be given the
            // 'activated' state when touched.
            getSupportFragmentManager().findFragmentById(R.id.item_list);
        }

        // TODO: If exposing deep links into your app, handle intents here.
    }

    //TODO site and type currently useless
    @Override
    public void onSearch(String site, String type, ArrayList<String> ids) {
        if (mTwoPane) {
            // In two-pane mode, show the detail view in this activity by
            // adding or replacing the detail fragment using a
            // fragment transaction.
            Log.i(TAG, "onSearch, twoPane");
            Bundle arguments = new Bundle();
            //arguments.putString(LocationSearchDetailFragment.SITE, site);
            //arguments.putString(LocationSearchDetailFragment.TYPE, type);
            arguments.putStringArrayList(LocationSearchDetailFragment.IDS, ids);
            LocationSearchDetailFragment fragment = new LocationSearchDetailFragment();
            fragment.setArguments(arguments);
            getFragmentManager().beginTransaction()
                    .replace(R.id.item_detail_container, fragment)
                    .commit();

        } else {
            // In single-pane mode, simply start the detail activity
            // for the selected item ID.
            Log.i(TAG, "onSearch, single-pane");
            Intent detailIntent = new Intent(this, LocationSearchDetailActivity.class);
            //detailIntent.putExtra(LocationSearchDetailFragment.SITE, site);
            //detailIntent.putExtra(LocationSearchDetailFragment.TYPE, type);
            detailIntent.putStringArrayListExtra(LocationSearchDetailFragment.IDS, ids);
            detailIntent.setAction(LocationSearchMasterActivity.class.getName());
            startActivity(detailIntent);
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_home, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if (id == android.R.id.home) {
            // This ID represents the Home or Up button. In the case of this
            // activity, the Up button is shown. Use NavUtils to allow users
            // to navigate up one level in the application structure. For
            // more details, see the Navigation pattern on Android Design:
            //
            // http://developer.android.com/design/patterns/navigation.html#up-vs-back
            //

            NavUtils.navigateUpFromSameTask(this);
            return true;
        }

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        if (id == R.id.action_logout) {
            CampusPolitoApplication.unsubscribeFromAllChannels();
            AppUser.logOut();
            Intent homeIntent = new Intent(this, MainActivity.class);
            homeIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(homeIntent);
            finish();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


}
