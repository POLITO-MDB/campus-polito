package com.example.mad.campuspolito.student;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.mad.campuspolito.R;
import com.example.mad.campuspolito.model.AppUser;
import com.example.mad.campuspolito.model.Conversation;
import com.example.mad.campuspolito.utils.CircularImageView;
import com.parse.ParseException;
import com.parse.ParseQueryAdapter;

import java.text.DateFormat;

/**
 * Created by mdb on 22/07/15.
 */
public class ConversationsAdapter extends ParseQueryAdapter<Conversation> {

    private DateFormat dateFormat;
    private DateFormat timeFormat;
    private Context context;

    public ConversationsAdapter(Context context, com.parse.ParseQueryAdapter.QueryFactory<Conversation> queryFactory) {
        super(context, queryFactory);
        this.context = context;
        this.dateFormat = android.text.format.DateFormat.getDateFormat(context);
        this.timeFormat = android.text.format.DateFormat.getTimeFormat(context);
    }


    @Override
    public View getItemView(Conversation object, View v, ViewGroup parent) {
        if (v == null) {
            v = View.inflate(getContext(), R.layout.conversation_list_item, null);
        }

        // Take advantage of ParseQueryAdapter's getItemView logic for
        // populating the main TextView/ImageView.
        // The IDs in your custom layout must match what ParseQueryAdapter expects
        // if it will be populating a TextView or ImageView for you.
        super.getItemView(object, v, parent);

        // Do additional configuration before returning the View.

        CircularImageView imageView = (CircularImageView) v.findViewById(R.id.conversation_icon);
        TextView conversationNameView = (TextView) v.findViewById(R.id.title);

        if (!object.isGroupChat()) {
            AppUser recipient = object.getRecipientUser();

            try {
                recipient.fetchIfNeeded();
            } catch (ParseException e) {
                e.printStackTrace();
            }

            if(recipient.getPicture() != null){
                imageView.setParseFile(recipient.getPicture());
                imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
                imageView.loadInBackground();
            }
            else {
                imageView.setParseFile(null);
                imageView.setPlaceholder(context.getResources().getDrawable(R.mipmap.ic_missing_icon_person));
            }

            conversationNameView.setText(recipient.toString());


        } else {
             imageView.setParseFile(null);
            imageView.setPlaceholder(context.getResources().getDrawable(R.mipmap.ic_group_chat));
            conversationNameView.setText(object.getGroupName());
         }

        TextView dateView = (TextView) v.findViewById(R.id.conversation_date);
        dateView.setText(timeFormat.format(object.getUpdatedAt()) + " " + dateFormat.format(object.getUpdatedAt()));

        return v;
    }

}
