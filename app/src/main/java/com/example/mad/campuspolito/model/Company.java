package com.example.mad.campuspolito.model;

import com.parse.ParseClassName;
import com.parse.ParseGeoPoint;
import com.parse.ParseObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by mdb on 20/07/15.
 */
@ParseClassName("Company")
public class Company extends ParseObject {


    public static final String VAT_NUMBER = "VATNumber";
    public static final String NAME = "name";
    public static final String CATEGORY = "category";
    public static final String SUBCATEGORY = "subcategory";
    public static final String ENTERPRISE_SIZE = "enterpriseSize";
    public static final String MISSION = "mission";

    //public static final String IS_PUBLISHED = "isPublished";
    public static final String OFFERS = "offers";
    public static final String FAVOURITE_STUDENTS  = "favouriteStudents";
    public static final String USER = "user";
    public static final String COMPANY_URL = "companyURL";
    public static final String KEYWORDS = "keywords";

    /**Location is defined by the following elements**/
    public static final String GEOPOINT = "geoPoint";
    public static final String PLACE_ID = "placeId";
    public static final String LOCATION_NAME = "location_NAME";
    public static final String IS_PUBLIC = "is_public";


    /**
     * Default zero-argument constructor required by Parse API
     */
    public Company(){

    }


    public void setIsPublic(boolean isPublic){
        put(IS_PUBLIC, isPublic);
    }
    public boolean getIsPublic(){
        return getBoolean(IS_PUBLIC);
    }


    @Override
    public String toString() {
        return getName();
    }

    public void setName(String name){
        put(NAME, name);
    }
    public String getName(){
        return getString(NAME);
    }

    public String getVATNumber(){
        return getString(VAT_NUMBER);
    }
    public void setVATNumber(String VATNumber){
        put(VAT_NUMBER, VATNumber);
    }



    public String getCategory() {
        return getString(CATEGORY);
    }
    public void setCategory(String category){
        put(CATEGORY, category);
    }

    public String getSubcategory() {
        return getString(SUBCATEGORY);
    }
    public void setSubcategory(String subcategory){
        put(SUBCATEGORY, subcategory);
    }

    public String getEnterpriseSize(){
        return getString(ENTERPRISE_SIZE);
    }
    public void setEnterpriseSize(String enterpriseSize){
        put(ENTERPRISE_SIZE, enterpriseSize);
    }

    public String getMission(){
        return getString(MISSION);
    }
    public void setMission(String mission){
        put(MISSION, mission);
    }

    public void setGeoPoint(ParseGeoPoint geoPoint){
        put(GEOPOINT, geoPoint);
    }
    public ParseGeoPoint getGeoPoint(){
        return getParseGeoPoint(GEOPOINT);
    }

    public void setPlaceId(String placeId){
        put(PLACE_ID, placeId);
    }
    public String getPlaceId(){
        return getString(PLACE_ID);
    }

    public void setLocationName(String locationName){
        put(LOCATION_NAME,locationName);
    }
    public String getLocationName(){
        return getString(LOCATION_NAME);
    }



    public List<Offer> getOffers(){
        if(getList(OFFERS) == null){
            ArrayList<Offer> emptyList = new ArrayList<Offer>();
            setOffers(emptyList);
            return emptyList;
        }
        else
            return getList(OFFERS);
    }
    public void setOffers(List<Offer> offers){
        put(OFFERS, offers);
    }

    public List<Student> getFavouriteStudents(){
        return getList(FAVOURITE_STUDENTS);
    }
    public void setFavouriteStudents(List<Student> favouriteStudents){
        put(FAVOURITE_STUDENTS, favouriteStudents);
    }

    //public boolean getIsPublished(){
    //    return getBoolean(IS_PUBLISHED);
    //}
    //public void setIsPublished(boolean isPublished){
    //    put(IS_PUBLISHED, isPublished);
    //}


    /**
     * This method is used to add a new offer to the company.
     *
     * @param offer The offer to be added
     */
    public void addOffer(Offer offer){
        addUnique(OFFERS, offer);
    }

    /**
     * This method is used to remove one offer from the company list
     *
     * @param offer The offer to be removed
     */
    public void removeOffer(Offer offer){
        removeAll(OFFERS, Arrays.asList(offer));
    }

    /**
     * This method is used to add a new student to the favourites list of a company.
     *
     * @param favouriteStudent The student to be added
     */
    public void addFavouriteStudent(Student favouriteStudent){
        addUnique(FAVOURITE_STUDENTS, favouriteStudent);
    }

    /**
     * This method is used to remove one favourite student from the list of a company.
     *
     * @param favouriteStudent The student to be removed
     */
    public void removeFavouriteStudent(Student favouriteStudent){
        removeAll(FAVOURITE_STUDENTS, Arrays.asList(favouriteStudent));
    }

    public AppUser getUser(){
        return (AppUser)get(USER);
    }
    public void setUser(AppUser user){
        put(USER, user);
    }

    public String getCompanyURL() {
        return getString(COMPANY_URL);
    }
    public void setCompanyURL(String companyURL){
        put(COMPANY_URL, companyURL);
    }

    public List<String> getKeywords() {
        return getList(KEYWORDS);
    }
    public void setKeywords(List<String> keywords){
        put(KEYWORDS, keywords);
    }
}
