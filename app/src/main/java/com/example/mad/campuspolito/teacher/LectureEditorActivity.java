package com.example.mad.campuspolito.teacher;

import android.content.Context;
import android.os.AsyncTask;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.DialogFragment;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Switch;
import android.widget.Toast;

import com.example.mad.campuspolito.CampusPolitoApplication;
import com.example.mad.campuspolito.R;
import com.example.mad.campuspolito.common.LectureDetailActivity;
import com.example.mad.campuspolito.model.AppUser;
import com.example.mad.campuspolito.model.Course;
import com.example.mad.campuspolito.model.Lecture;
import com.example.mad.campuspolito.model.Location;
import com.example.mad.campuspolito.utils.DatePickerFragment;
import com.parse.DeleteCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParsePush;
import com.parse.ParseQuery;
import com.parse.SendCallback;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import fr.ganfra.materialspinner.MaterialSpinner;

public class LectureEditorActivity extends AppCompatActivity implements DatePickerFragment.DatePickerInteraction, View.OnClickListener {

    private static final String TAG = LectureEditorActivity.class.getName();
    public static final int EDIT_LECTURE = 0;
    public static final int LECTURE_EDITED = 1;
    private static final String DATE_FRAGMENT = "datePicker";
    private static final String END_DATE_FRAGMENT = "endDatePicker";
    private static final String SEND_NOTIFICATION = "SEND_NOTIFICATION";
    private static final String NOTIFICATION_MSG = "NOTIFICATION_MSG";
    private static final String END_YEAR = "END_YEAR";
    private static final String END_MONTH = "END_MONTH";
    private static final String END_DAY = "END_DAY";

    private String lectureId;
    private Lecture lecture;
    private String courseId;
    private Course course;
    private Location mLocation;

    private EditText course_name;
    private EditText date;
    private MaterialSpinner hour;
    private MaterialSpinner duration;
    private MaterialSpinner autocomplete_site;
    private MaterialSpinner autocomplete_type;
    private AutoCompleteTextView autocomplete_location;
    private FloatingActionButton save_fab;
    private Toolbar toolbar;
    private NestedScrollView mContent;
    private LinearLayout mProgress;
    private Switch mMultipleLecture;
    private EditText mEndDate;

    private Map<String, Location> all_locations;
    private List<String> locationList;
    private Set<String> siteList;
    private Set<String> typeList;
    private String site;
    private String type;
    private String location;
    private int mYear = -1;
    private int mMonth = -1;
    private int mDay = -1;
    private int mEndYear = Calendar.getInstance().get(Calendar.YEAR);
    private int mEndMonth = Calendar.getInstance().get(Calendar.MONTH);
    private int mEndDay = Calendar.getInstance().get(Calendar.DAY_OF_MONTH);
    private int mStartSlot = -1;
    private int mDurationSlot = -1;
    private DateFormat Date_format;
    private DialogFragment DateFragment;
    //Notifications
    private EditText mNotificationMsg;
    private CheckBox mNotificationToggle;
    private String notificationMsg;
    private boolean sendNotification;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lesson_editor);

        toolbar = (Toolbar)findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        Date_format = new SimpleDateFormat("dd/MM/yyyy",Locale.getDefault());

        if(savedInstanceState!=null) {
            mYear = savedInstanceState.getInt(Lecture.YEAR);
            mMonth = savedInstanceState.getInt(Lecture.MONTH);
            mDay = savedInstanceState.getInt(Lecture.DAY);
            mStartSlot = savedInstanceState.getInt(Lecture.START_SLOT);
            location = savedInstanceState.getString(Location.LOCATION);
            type = savedInstanceState.getString(Location.TYPE);
            site = savedInstanceState.getString(Location.SITE);
            mEndYear = savedInstanceState.getInt(END_YEAR);
            mEndMonth = savedInstanceState.getInt(END_MONTH);
            mEndDay = savedInstanceState.getInt(END_DAY);
            mDurationSlot = savedInstanceState.getInt(Lecture.DURATION);
            sendNotification = savedInstanceState.getBoolean(SEND_NOTIFICATION);
            notificationMsg = savedInstanceState.getString(NOTIFICATION_MSG);
        }

        course_name = (EditText)findViewById(R.id.course_name);
        date = (EditText)findViewById(R.id.lecture_date);
        hour = (MaterialSpinner)findViewById(R.id.lecture_hour);
        duration = (MaterialSpinner)findViewById(R.id.lecture_duration);
        autocomplete_site = (MaterialSpinner)findViewById(R.id.site_spinner);
        autocomplete_type = (MaterialSpinner)findViewById(R.id.room_type_spinner);
        autocomplete_location = (AutoCompleteTextView)findViewById(R.id.lecture_room_spinner);
        save_fab = (FloatingActionButton)findViewById(R.id.save_lecture_button);
        mContent = (NestedScrollView)findViewById(R.id.edit_lesson_content);
        mProgress = (LinearLayout)findViewById(R.id.load_progress);
        mMultipleLecture = (Switch)findViewById(R.id.multiple_lecture_switch);
        mEndDate = (EditText)findViewById(R.id.end_date);

        mNotificationMsg = (EditText)findViewById(R.id.notification_message);
        mNotificationToggle = (CheckBox)findViewById(R.id.notification_toggle);

        if(sendNotification)
            mNotificationToggle.setChecked(true);
        else
            mNotificationToggle.setChecked(false);
        if(notificationMsg != null && !TextUtils.isEmpty(notificationMsg))
            mNotificationMsg.setText(notificationMsg);


        if(getIntent().getStringExtra(LectureDetailActivity.LECTURE_ID)!=null){
            lectureId = getIntent().getStringExtra(LectureDetailActivity.LECTURE_ID);
            setTitle(getString(R.string.title_activity_lesson_editor));
        }
        else if(getIntent().getStringExtra(Lecture.COURSE)!=null){
            courseId = getIntent().getStringExtra(Lecture.COURSE);
            setTitle(getString(R.string.title_activity_create_lesson));
            mMultipleLecture.setVisibility(View.VISIBLE);

        }
        else{
            mProgress.setVisibility(View.GONE);
            return;
        }

        mEndDate.setFocusable(false);
        mEndDate.setFocusableInTouchMode(true);
        mEndDate.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    DateFragment = new DatePickerFragment();
                    DateFragment.show(getSupportFragmentManager(), END_DATE_FRAGMENT);
                }
            }
        });
        mEndDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DateFragment = new DatePickerFragment();
                DateFragment.show(getSupportFragmentManager(),END_DATE_FRAGMENT);
            }
        });

        mMultipleLecture.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked){
                    mEndDate.setVisibility(View.VISIBLE);
                }else{
                    mEndDate.setVisibility(View.GONE);
                }
            }
        });

        autocomplete_location.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                autocomplete_location.showDropDown();
            }
        });

        date.setFocusable(false);
        date.setFocusableInTouchMode(true);
        date.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                if (b){
                    DateFragment = new DatePickerFragment();
                    DateFragment.show(getSupportFragmentManager(), DATE_FRAGMENT);
                }
            }
        });
        date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DateFragment = new DatePickerFragment();
                DateFragment.show(getSupportFragmentManager(), DATE_FRAGMENT);
            }
        });

        fillTimeSlots();

        save_fab.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {

        if(v.getId() == R.id.save_lecture_button){
            course_name.setError(null);
            date.setError(null);
            hour.setError(null);
            duration.setError(null);
            mEndDate.setError(null);
            autocomplete_location.setError(null);
            autocomplete_type.setError(null);
            autocomplete_site.setError(null);

            View focusView = null;
            boolean error = false;

            if(TextUtils.isEmpty(date.getText().toString())){
                date.setError(getString(R.string.wrong_parameter));
                focusView = date;
                error = true;
            }else{
                boolean valid_date = false;
                Calendar c = Calendar.getInstance();
                c.set(Calendar.DAY_OF_MONTH,mDay);
                c.set(Calendar.MONTH,mMonth);
                c.set(Calendar.YEAR,mYear);

                Calendar today = Calendar.getInstance();

                if(c.compareTo(today)>=0)
                    valid_date = true;

                if(!valid_date){
                    date.setError(getString(R.string.previous_date_error));
                    focusView = date;
                    error = true;
                }

                if(c.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY && valid_date){
                    date.setError(getString(R.string.date_error_sunday));
                    focusView = date;
                    error = true;
                }

            }

            if(mMultipleLecture.getVisibility() == View.VISIBLE){
                if(mMultipleLecture.isChecked() && !error){
                    if(TextUtils.isEmpty(mEndDate.getText().toString())){
                        mEndDate.setError(getString(R.string.wrong_parameter));
                        focusView = mEndDate;
                        error = true;
                    }else{
                        Calendar c = Calendar.getInstance();
                        c.set(Calendar.DAY_OF_MONTH,mDay);
                        c.set(Calendar.MONTH,mMonth);
                        c.set(Calendar.YEAR,mYear);

                        Calendar cEnd = Calendar.getInstance();
                        cEnd.set(Calendar.DAY_OF_MONTH,mEndDay);
                        cEnd.set(Calendar.MONTH,mEndMonth);
                        cEnd.set(Calendar.YEAR,mEndYear);
                        if(cEnd.compareTo(c)<=0){
                            mEndDate.setError(getString(R.string.error_bigger_date));
                            focusView = mEndDate;
                            error = true;
                        }
                    }
                }
            }

            if(hour.getSelectedItemPosition()==0 && !error){
                hour.setError(getString(R.string.wrong_parameter));
                focusView = hour;
                error = true;
            }
            if(duration.getSelectedItemPosition()==0 && !error){
                duration.setError(getString(R.string.wrong_parameter));
                focusView = duration;
                error = true;
            }

            if(TextUtils.isEmpty(autocomplete_location.getText()) && !error){
                autocomplete_location.setError(getString(R.string.wrong_parameter));
                focusView = autocomplete_location;
                error = true;
            }


            if(error){
                focusView.requestFocus();
            }else{
                location = autocomplete_location.getText().toString().trim();
                type = (String)autocomplete_type.getSelectedItem();
                site = (String)autocomplete_site.getSelectedItem();
                mDurationSlot = duration.getSelectedItemPosition();
                mStartSlot = hour.getSelectedItemPosition();

                sendNotification = mNotificationToggle.isChecked();
                notificationMsg = mNotificationMsg.getText().toString().trim();

                SaveLectureDetails task = new SaveLectureDetails(this,mMultipleLecture.getVisibility() == View.VISIBLE && mMultipleLecture.isChecked());
                task.execute();

            }
        }
    }

    private class SaveLectureDetails extends AsyncTask<Void,Void,Boolean>{

        private final Context ctx;
        private final boolean multipleLectures;
        private List<Lecture> lectureList;
        private Calendar tempDate;
        private Calendar endDate;
        private boolean noLocationResult;

        public SaveLectureDetails(Context ctx, boolean multipleLectures){
            this.ctx = ctx;
            this.multipleLectures = multipleLectures;
            noLocationResult = false;
        }

        @Override
        protected void onPreExecute() {
            mProgress.setVisibility(View.VISIBLE);
            save_fab.setVisibility(View.GONE);
            mContent.setVisibility(View.GONE);
        }

        @Override
        protected Boolean doInBackground(Void... params) {
            try {


                ParseQuery<Location> parseQuery = ParseQuery.getQuery(Location.class);
                parseQuery.whereEqualTo(Location.LOCATION,location);

                List<Location> results = parseQuery.find();

                if(results == null || results.size() == 0){
                    noLocationResult = true;
                    return false;
                }
                mLocation = results.get(0);

                if(lecture!=null && lectureId != null){
                    lecture.setDay(mDay);
                    lecture.setMonth(mMonth);
                    lecture.setYear(mYear);
                    lecture.setStartSlot(mStartSlot);
                    lecture.setDuration(mDurationSlot);
                    lecture.setDate();

                    lecture.setRoom(mLocation);
                    lecture.save();

                    if(sendNotification){
                        Log.d(TAG, "Sending a push notification...");
                        String message;
                        if(TextUtils.isEmpty(notificationMsg)){
                            Log.e(TAG, "Notification message is null.");
                            message = getString(R.string.lecture_details_changed);
                        }
                        else {
                            message = notificationMsg;
                        }

                        AppUser currentUser = (AppUser)AppUser.getCurrentUser();
                        currentUser.fetchIfNeeded();
                        Course course = lecture.getCourse();
                        course.fetchIfNeeded();


                        ParsePush push = new ParsePush();
                        push.setChannel(CampusPolitoApplication.COURSE_CHANNEL + course.getObjectId());
                        push.setData(lecture.getJSONRepresentation(message, currentUser.toString()));
                        push.sendInBackground(new SendCallback() {
                            @Override
                            public void done(ParseException e) {
                                if (e == null) {
                                    Log.d(TAG, "Push notification sent correctly.");
                                } else
                                    Log.e(TAG, "Error while sending push notification", e);
                            }
                        });
                        Log.d(TAG, "Pushing message: " + lecture.getJSONRepresentation(message, currentUser.toString()).toString());


                    }
                    else
                        Log.d(TAG, "No push notification...");


                    return true;

                }else if(course!=null && courseId!=null){
                    if(multipleLectures){

                        boolean error = false;
                        lectureList = new LinkedList<>();

                        tempDate = Calendar.getInstance();
                        tempDate.set(Calendar.DAY_OF_MONTH,mDay);
                        tempDate.set(Calendar.MONTH,mMonth);
                        tempDate.set(Calendar.YEAR,mYear);

                        endDate = Calendar.getInstance();
                        endDate.set(Calendar.DAY_OF_MONTH,mEndDay);
                        endDate.set(Calendar.MONTH,mEndMonth);
                        endDate.set(Calendar.YEAR,mEndYear);

                        while (tempDate.compareTo(endDate)<=0 && !isCancelled()){
                            Lecture lecture = new Lecture();
                            lecture.setCourse(course);
                            lecture.setRoom(mLocation);
                            lecture.setDay(tempDate.get(Calendar.DAY_OF_MONTH));
                            lecture.setMonth(tempDate.get(Calendar.MONTH));
                            lecture.setYear(tempDate.get(Calendar.YEAR));
                            lecture.setStartSlot(mStartSlot);
                            lecture.setDuration(mDurationSlot);
                            lecture.setDate();

                            lectureList.add(lecture);
                            tempDate.add(Calendar.DAY_OF_MONTH,7);
                        }
                        if(isCancelled())
                            return false;

                        ParseObject.saveAll(lectureList);

                        course.addLectures(lectureList);
                        try {
                            course.save();
                        } catch (ParseException e) {
                            Log.e(TAG, "Error while adding lectures to the course", e);
                            error = true;

                        }

                        if(error){
                            ParseObject.deleteAll(lectureList);
                            return false;
                        }
                        if(isCancelled()){
                            course.removeLectures(lectureList);
                            ParseObject.deleteAll(lectureList);
                            return false;
                        }

                    }else {
                        Lecture nLecture = new Lecture();
                        nLecture.setCourse(course);
                        nLecture.setDay(mDay);
                        nLecture.setMonth(mMonth);
                        nLecture.setYear(mYear);
                        nLecture.setStartSlot(mStartSlot);
                        nLecture.setDuration(mDurationSlot);
                        nLecture.setDate();

                        nLecture.setRoom(mLocation);

                        nLecture.save();

                        course.addLecture(nLecture);
                        try {
                            course.save();
                        } catch (ParseException e) {
                            nLecture.delete();
                            return false;
                        }
                    }
                }
                return true;
            } catch (ParseException e) {
                Log.e(TAG, "Error while saving lecture details", e);
                return false;

            }
        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {
            if(aBoolean){
                setResult(LECTURE_EDITED);
                finish();
            }
            else {
                if(noLocationResult){
                    mProgress.setVisibility(View.GONE);
                    save_fab.setVisibility(View.VISIBLE);
                    mContent.setVisibility(View.VISIBLE);
                    autocomplete_location.setError(getString(R.string.location_not_found));
                    autocomplete_location.requestFocus();
                }
                else {
                    Toast.makeText(getApplicationContext(),R.string.parse_generic_error,Toast.LENGTH_LONG).show();
                    finish();
                }
            }
        }

    }

    protected void fillTimeSlots(){
        ArrayList<String> timeSlots = new ArrayList<>();
        timeSlots.add("8:30");
        timeSlots.add("10:00");
        timeSlots.add("11:30");
        timeSlots.add("13:00");
        timeSlots.add("14:30");
        timeSlots.add("16:00");
        timeSlots.add("17:30");
        timeSlots.add("19:00");

        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(this,android.R.layout.simple_dropdown_item_1line, timeSlots);
        hour.setAdapter(arrayAdapter);

        ArrayList<String> durationSlots = new ArrayList<>();
        durationSlots.add("1:30");
        durationSlots.add("3:00");
        durationSlots.add("4:30");
        durationSlots.add("6:00");

        ArrayAdapter<String> arrayAdapter1 = new ArrayAdapter<String>(this,android.R.layout.simple_dropdown_item_1line,durationSlots);
        duration.setAdapter(arrayAdapter1);
    }

    protected void updateDisplay(int id) {

        if(id==0) {
            Calendar c = Calendar.getInstance();
            c.set(Calendar.DAY_OF_MONTH, mDay);
            c.set(Calendar.MONTH, mMonth);
            c.set(Calendar.YEAR, mYear);
            date.setText(Date_format.format(c.getTime()));
        }else if(id==1){
            Calendar c = Calendar.getInstance();
            c.set(Calendar.DAY_OF_MONTH,mEndDay);
            c.set(Calendar.MONTH,mEndMonth);
            c.set(Calendar.YEAR,mEndYear);
            mEndDate.setText(Date_format.format(c.getTime()));
        }
    }


    private class FetchLectureData extends AsyncTask<Void,Void,Boolean> {

        private final String lectureId;
        private final Context ctx;

        public  FetchLectureData(Context ctx, String lectureId){
            this.ctx = ctx;
            this.lectureId = lectureId;

        }

        @Override
        protected void onPreExecute() {
            mProgress.setVisibility(View.VISIBLE);
            save_fab.setVisibility(View.GONE);
            mContent.setVisibility(View.GONE);
        }

        @Override
        protected Boolean doInBackground(Void... params) {
            ParseQuery<Lecture> query = ParseQuery.getQuery(Lecture.class);
            try {

                fetchLocationData();

                query.include(Lecture.COURSE);
                lecture = query.get(lectureId);
                lecture.fetchIfNeeded();

                if(mYear==-1)
                    mYear = lecture.getYear();
                if(mMonth==-1)
                    mMonth = lecture.getMonth();
                if(mDay==-1)
                    mDay = lecture.getDay();
                if(mStartSlot==-1)
                    mStartSlot = lecture.getStartSlot();
                if(mDurationSlot == -1)
                    mDurationSlot = lecture.getDuration();


                if(location==null)
                    location = lecture.getRoom().getLocation().trim();

                return true;

            } catch (ParseException e) {
                Log.e(TAG, "Error while loading lecture", e);
                return false;
            }

        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {
            if(aBoolean){

                setLocationData();
                course_name.setText(lecture.getCourse().getTitle());
                date.setText("" + mDay + "/" + (mMonth + 1) + "/"+mYear);
                duration.setSelection(mDurationSlot);
                hour.setSelection(mStartSlot);

                autocomplete_site.setSelection(CampusPolitoApplication.getStringItemPosition(new ArrayList<String>(siteList), lecture.getRoom().getSite()) + 1);
                autocomplete_type.setSelection(CampusPolitoApplication.getStringItemPosition(new ArrayList<String>(typeList), lecture.getRoom().getType()) + 1);
                doFilter();
                autocomplete_location.setText(location);

                save_fab.setVisibility(View.VISIBLE);
                mContent.setVisibility(View.VISIBLE);
                mProgress.setVisibility(View.GONE);
            }
            else {
                mContent.setVisibility(View.VISIBLE);
                mProgress.setVisibility(View.GONE);
                Snackbar.make(mContent,getString(R.string.parse_generic_error),Snackbar.LENGTH_LONG).show();
            }
        }
    }

    private class FetchCourseData extends AsyncTask<Void,Void,Boolean>{

        private final String courseId;
        private final Context ctx;

        public FetchCourseData(Context ctx, String courseId){
            this.ctx = ctx;
            this.courseId = courseId;
        }

        @Override
        protected void onPreExecute() {
            mProgress.setVisibility(View.VISIBLE);
            save_fab.setVisibility(View.GONE);
            mContent.setVisibility(View.GONE);
        }

        @Override
        protected Boolean doInBackground(Void... params) {
            ParseQuery<Course> parseQuery = ParseQuery.getQuery(Course.class);
            try {

                fetchLocationData();

                course = parseQuery.get(courseId);

                if(mDay==-1)
                    mDay = Calendar.getInstance().get(Calendar.DAY_OF_MONTH);
                if(mMonth==-1)
                    mMonth = Calendar.getInstance().get(Calendar.MONTH);
                if(mYear==-1)
                    mYear = Calendar.getInstance().get(Calendar.YEAR);
                if(mStartSlot==-1)
                    mStartSlot = 0;
                if(mDurationSlot==-1)
                    mDurationSlot = 0;


                return true;
            } catch (ParseException e) {
                Log.e(TAG, "Error while loading course", e);
                return false;

            }
        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {
            if(aBoolean){

                findViewById(R.id.notification_section).setVisibility(View.GONE);

                setLocationData();
                course_name.setText(course.getTitle());

                mEndDate.setText("" + mEndDay + "/" + (mEndMonth + 1) + "/" + mEndYear);

                hour.setSelection(mStartSlot);
                duration.setSelection(mDurationSlot);
                date.setText("" + mDay + "/" + (mMonth + 1) + "/" + mYear);

                save_fab.setVisibility(View.VISIBLE);
                mContent.setVisibility(View.VISIBLE);
                mProgress.setVisibility(View.GONE);
            }
            else {
                mContent.setVisibility(View.VISIBLE);
                mProgress.setVisibility(View.GONE);
                Snackbar.make(mContent,getString(R.string.parse_generic_error),Snackbar.LENGTH_LONG).show();
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        if(lectureId!=null){
            FetchLectureData task = new FetchLectureData(this,lectureId);
            task.execute();
        }
        else if(courseId!=null){
            FetchCourseData task = new FetchCourseData(this,courseId);
            task.execute();
        }


    }

    @Override
    protected void onPause() {
        super.onPause();
        if(DateFragment!=null){
            DateFragment.dismiss();
            DateFragment = null;
        }

    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        outState.putInt(Lecture.YEAR, mYear);
        outState.putInt(Lecture.MONTH, mMonth);
        outState.putInt(Lecture.DAY, mDay);
        outState.putInt(Lecture.START_SLOT,hour.getSelectedItemPosition());
        outState.putString(Location.LOCATION, autocomplete_location.getText().toString().trim());
        outState.putString(Location.SITE, ((String) autocomplete_site.getSelectedItem()).trim());
        outState.putString(Location.TYPE, ((String) autocomplete_type.getSelectedItem()).trim());
        outState.putInt(END_YEAR, mEndYear);
        outState.putInt(END_MONTH, mEndMonth);
        outState.putInt(END_DAY,mEndDay);
        outState.putInt(Lecture.DURATION, duration.getSelectedItemPosition());
        outState.putBoolean(SEND_NOTIFICATION, mNotificationToggle.isChecked());
        outState.putString(NOTIFICATION_MSG, mNotificationMsg.getText().toString());
    }




    private void fetchLocationData() throws ParseException{

        List<Location> list = getLocations();


        if(list!=null) {
            Log.d(TAG, "" + list.size() + " items fetched");
            all_locations = new HashMap<String, Location>();
            siteList = new TreeSet<String>();
            typeList = new TreeSet<String>();
            locationList = new ArrayList<String>();
            for (Location r : list) {
                all_locations.put(r.getObjectId(), r);
                siteList.add(r.getSite());
                typeList.add(r.getType());
                locationList.add(r.getLocation());
            }
        }
        else
            Log.i(TAG, "No items fetched.");
    }

    private void setLocationData(){
        ArrayAdapter<String> adapter_course = new ArrayAdapter<String>(this, android.R.layout.simple_dropdown_item_1line, new ArrayList<String>(siteList));
        autocomplete_site.setAdapter(adapter_course);
        autocomplete_site.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                doFilter();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        ArrayAdapter<String> adapter_teacher = new ArrayAdapter<String>(this, android.R.layout.simple_dropdown_item_1line,new ArrayList<String>(typeList));
        autocomplete_type.setAdapter(adapter_teacher);

        autocomplete_type.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                doFilter();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        ArrayAdapter<String> adapter_location = new ArrayAdapter<String>(this, android.R.layout.simple_dropdown_item_1line, locationList);
        autocomplete_location.setAdapter(adapter_location);

        if (site != null)
            autocomplete_site.setSelection(CampusPolitoApplication.getStringItemPosition(new ArrayList<String>(siteList), site) + 1);

        if (type != null)
            autocomplete_type.setSelection(CampusPolitoApplication.getStringItemPosition(new ArrayList<String>(typeList), type) + 1);

        if (location != null)
            autocomplete_location.setText(location);
    }

    private void doFilter(){
        site = ((String)autocomplete_site.getSelectedItem()).trim();
        type = ((String)autocomplete_type.getSelectedItem()).trim();

        if(site.equals(getString(R.string.prompt_site)))
            site = "";

        if(type.equals(getString(R.string.prompt_type)))
            type = "";

        Log.d(TAG, "site: " + site + " type: " + type);


        locationList = new ArrayList<String>();
        for(Location location : all_locations.values()){
            if((site == null || site.isEmpty() || location.getSite().equals(site))
                    && (type == null || type.isEmpty() || location.getType().equals(type))){

                locationList.add(location.getLocation());
            }
        }

        Log.i(TAG, "" + locationList.size() + " locations retrieved");
        ArrayAdapter<String> adapter_location = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, locationList);
        autocomplete_location.setAdapter(adapter_location);
    }

    public List<Location> getLocations() throws ParseException{
        List<Location> locationList = null;
        ParseQuery<Location> query = ParseQuery.getQuery(Location.class);
        query.orderByAscending(Location.LOCATION);
        query.setLimit(500);

        locationList = query.find();
        Log.d(TAG,"Locations successfully retrieved!");

        return locationList;
    }

    @Override
    public Calendar setDate() {
        Calendar c = Calendar.getInstance();

        if(DateFragment.getTag().compareTo(DATE_FRAGMENT)==0) {
            c.set(Calendar.DAY_OF_MONTH, mDay);
            c.set(Calendar.MONTH, mMonth);
            c.set(Calendar.YEAR, mYear);
        }else if(DateFragment.getTag().compareTo(END_DATE_FRAGMENT)==0){
            c.set(Calendar.DAY_OF_MONTH,mEndDay);
            c.set(Calendar.MONTH,mEndMonth);
            c.set(Calendar.YEAR, mEndYear);
        }
        return c;

    }

    @Override
    public void getDate(int year, int month, int day) {
        if(DateFragment.getTag().compareTo(DATE_FRAGMENT)==0){
            mYear = year;
            mMonth = month;
            mDay = day;
            updateDisplay(0);
        }else if(DateFragment.getTag().compareTo(END_DATE_FRAGMENT)==0){
            mEndYear = year;
            mEndMonth = month;
            mEndDay = day;
            updateDisplay(1);
        }
    }

}
