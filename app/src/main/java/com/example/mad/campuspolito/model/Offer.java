package com.example.mad.campuspolito.model;

import com.google.android.gms.location.places.Place;
import com.parse.ParseClassName;
import com.parse.ParseGeoPoint;
import com.parse.ParseObject;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;
import java.util.Date;
import java.util.List;

/**
 * Created by mdb on 20/07/15.
 */
@ParseClassName("Offer")
public class Offer extends ParseObject {

    public static final String TITLE = "title";
    public static final String DESCRIPTION = "description";
    public static final String JOB_TYPE = "job_type";
    public static final String EXPIRE_DATE = "expire_date";
    public static final String DURATION = "duration";
    public static final String DURATION_PERIOD_TYPE = "periodType";
    public static final String DURATION_PERIOD_LENGTH = "periodLength";
    public static final String REQUIREMENTS = "requirements";
    public static final String CANDIDATURES = "candidatures";
    public static final String COMPANY = "company";
    public static final String IS_PUBLISHED = "is_published";
    public static final String CATEGORY = "category";
    public static final String KEYWORDS = "keywords";
    /**Location is defined by the following elements**/
    public static final String GEOPOINT = "geoPoint";
    public static final String PLACE_ID = "placeId";
    public static final String LOCATION_NAME = "location_name";
    public static final String PUBLISHED_AT = "publishedAt";
    private static final CharSequence TAG_SEPARATOR = "_";


    /**
     * Default zero-argument constructor required by Parse API
     */
    public Offer(){
    }

    public String getTitle(){
        return getString(TITLE);
    }
    public void setTitle(String title){
        put(TITLE, title);
    }

    public String getDescription() {
        return getString(DESCRIPTION);
    }
    public void setDescription(String description){
        put(DESCRIPTION, description);
    }

    public Date getExpireDate() { return getDate(EXPIRE_DATE);}
    public void setExpireDate(Date expireDate){
        put(EXPIRE_DATE,expireDate);
    }

    public String getJobType(){
        return getString(JOB_TYPE);
    }
    public void setJobType(String jobType){
        put(JOB_TYPE, jobType);
    }

    public JSONObject getDuration(){
        return getJSONObject(DURATION);
    }
    public void setDuration(JSONObject duration){
        put(DURATION, duration);
    }

    public List<String> getRequirements(){
         return getList(REQUIREMENTS);
    }
    public void setRequirements(List<String> requirements){
        put(REQUIREMENTS, requirements);
    }

    public List<Candidature> getCandidatures(){
        return getList(CANDIDATURES);
    }
    public void setCandidatures(List<Candidature> candidatures){
        put(CANDIDATURES, candidatures);
    }

    public String getCategory() {
        return getString(CATEGORY);
    }
    public void setCategory(String category){
        put(CATEGORY, category);
    }

    public Company getCompany(){
        return (Company)get(COMPANY);
    }
    public void setCompany(Company company){
        put(COMPANY, company);
    }


    public boolean getIsPublished(){
        return getBoolean(IS_PUBLISHED);
    }
    public void setIsPublished(boolean isPublished){
        put(IS_PUBLISHED, isPublished);
    }


    /**
     * This method is used to add a new candidate to the candidates list of an offer.
     *
     * @param candidature The candidature to be added
     */
    public void addCandidature(Candidature candidature){
        addUnique(CANDIDATURES, candidature);
    }

    /**
     * This method is used to remove one candidate from the list of candidates
     * for an offer.
     *
     * @param candidature The candidature to be removed
     */
    public void removeCandidature(Candidature candidature){
        removeAll(CANDIDATURES, Arrays.asList(candidature));
    }

    /**
     * This method is used to add a new requirement to the offer requested skills
     *
     * @param requirement The requirement to be added
     */
    public void addRequirement(String requirement){

        addUnique(REQUIREMENTS, requirement);
    }

    /**
     * This method is used to remove one requirement from the requested skills of the offer
     *
     * @param requirement The requirement to be removed
     */
    public void removeRequirement(String requirement){
        removeAll(REQUIREMENTS, Arrays.asList(requirement));
    }

    /**
     * This method creates a JSONObject representing an offer duration.
     *
     * @param periodType Enumeration which expresses the type of period for the offer
     * @param periodLength Integer which expresses the length of the time period
     * @return JSONObject
     * @throws JSONException
     */
    public static JSONObject createJSONDuration(String periodType,
                                                int periodLength) throws JSONException {
        JSONObject duration = new JSONObject();

        duration.put(DURATION_PERIOD_TYPE, periodType);
        duration.put(DURATION_PERIOD_LENGTH, periodLength);

        return duration;
    }

    public List<String> getKeywords() {
        return getList(KEYWORDS);
    }
    public void setKeywords(List<String> keywords){
        put(KEYWORDS, keywords);
    }

    public void setGeoPoint(ParseGeoPoint geoPoint){
        put(GEOPOINT, geoPoint);
    }
    public ParseGeoPoint getGeoPoint(){
        return getParseGeoPoint(GEOPOINT);
    }

    public void setPlaceId(String placeId){
        put(PLACE_ID, placeId);
    }
    public String getPlaceId(){
        return getString(PLACE_ID);
    }

    public void setLocationName(String locationName){
        put(LOCATION_NAME,locationName);
    }
    public String getLocationName(){
        return getString(LOCATION_NAME);
    }

    public Date getPublishedAt(){
        return getDate(PUBLISHED_AT);
    }
    public void setPublishedAt(Date date){
        put(PUBLISHED_AT, date);
    }

    /**
     * Utility method used to create a valid tag from the user input, to perform creation of notices
     * tags and search on tag
     * @param input
     * @return
     */
    public static String generateValidTag(String input) {

        return input.toLowerCase().trim().
                replaceAll(" +", " ").replace(" ", Offer.TAG_SEPARATOR);

    }
}
