package com.example.mad.campuspolito;

import android.app.Application;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.RectF;
import android.media.ThumbnailUtils;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.text.format.DateUtils;
import android.util.Log;

import com.crashlytics.android.Crashlytics;
import com.example.mad.campuspolito.model.AppGlobals;
import com.example.mad.campuspolito.model.AppUser;
import com.example.mad.campuspolito.model.Candidature;
import com.example.mad.campuspolito.model.Category;
import com.example.mad.campuspolito.model.Company;
import com.example.mad.campuspolito.model.Conversation;
import com.example.mad.campuspolito.model.Course;
import com.example.mad.campuspolito.model.Degree;
import com.example.mad.campuspolito.model.Lecture;
import com.example.mad.campuspolito.model.Location;
import com.example.mad.campuspolito.model.Message;
import com.example.mad.campuspolito.model.Notice;
import com.example.mad.campuspolito.model.Offer;
import com.example.mad.campuspolito.model.Student;
import com.example.mad.campuspolito.model.Teacher;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.parse.Parse;
import com.parse.ParseClassName;
import com.parse.ParseException;
import com.parse.ParseInstallation;
import com.parse.ParseObject;
import com.parse.ParsePush;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.parse.SaveCallback;

import io.fabric.sdk.android.Fabric;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;

/**
 * Created by mdb on 20/07/15.
 */
public class CampusPolitoApplication extends Application {

    /* General data */
    public static final int MIN_PASSWORD_LENGTH = 4;
    /* Starting hour */
    protected static final int startingHour = 8;
    protected static final int startingMinute = 30;

    /* Time slot duration */
    protected static final int slotHourDuration = 1;
    protected static final int slotMinuteDuration = 30;
    public static final LatLngBounds BOUNDS_TURIN = new LatLngBounds(
            new LatLng(45.00677659999999, 7.5778502), new LatLng(45.1335014, 7.7623282));
    public static final LatLng POLITO_POS = new LatLng(45.0628484, 7.6602297);


    public static Calendar startingTime;
    public static Calendar slotDuration;
    public static Context ctx;

    /**
     * Size of signing user profile picture after scaling
     */
    public static final int SCALE_SIZE = 1024;

    public static final String STUDENT_CHANNEL = "student_";
    public static final String CONVERSATION_CHANNEL = "conversation_";
    public static final String COURSE_CHANNEL = "course_";

    private static GoogleApiClient googleApiClient;

    @Override
    public void onCreate() {
        super.onCreate();
        Fabric.with(this, new Crashlytics());

        ctx = getApplicationContext();

        /**Initialize time reference **/
        startingTime = Calendar.getInstance();
        slotDuration = Calendar.getInstance();
        startingTime.set(Calendar.HOUR_OF_DAY, startingHour);
        startingTime.set(Calendar.MINUTE, startingMinute);
        slotDuration.set(Calendar.HOUR_OF_DAY, slotHourDuration);
        slotDuration.set(Calendar.MINUTE, slotMinuteDuration);

        /**Initialize parse service **/
        ParseObject.registerSubclass(Category.class);
        ParseObject.registerSubclass(AppGlobals.class);
        ParseObject.registerSubclass(AppUser.class);
        ParseObject.registerSubclass(Candidature.class);
        ParseObject.registerSubclass(Company.class);
        ParseObject.registerSubclass(Course.class);
        ParseObject.registerSubclass(Lecture.class);
        ParseObject.registerSubclass(Notice.class);
        ParseObject.registerSubclass(Offer.class);
        ParseObject.registerSubclass(Student.class);
        ParseObject.registerSubclass(Teacher.class);
        ParseObject.registerSubclass(Message.class);
        ParseObject.registerSubclass(Conversation.class);
        ParseObject.registerSubclass(Degree.class);
        ParseObject.registerSubclass(Location.class);

        Parse.enableLocalDatastore(this);
        Parse.initialize(this, getString(R.string.parse_application_id), getString(R.string.parse_client_key));
        Parse.setLogLevel(Parse.LOG_LEVEL_VERBOSE);


        if(ParseInstallation.getCurrentInstallation().getList("channels") == null ||
                ParseInstallation.getCurrentInstallation().getList("channels").size() == 0) {
            ParsePush.subscribeInBackground("", new SaveCallback() {
                @Override
                public void done(ParseException e) {
                    if (e == null) {
                        Log.d("com.parse.push", "successfully subscribed to the broadcast channel.");
                    } else {
                        Log.e("com.parse.push", "failed to subscribe for push", e);
                    }
                }
            });
        }
    }


    public static void unsubscribeFromAllChannels(){
        List<String> channels = ParseInstallation.getCurrentInstallation().getList("channels");

        List<String> removedChannels = new ArrayList<String>();
        for(String channel : channels){
            if(channel.startsWith(CampusPolitoApplication.COURSE_CHANNEL)||
                    channel.startsWith(CampusPolitoApplication.CONVERSATION_CHANNEL) ||
                    channel.startsWith(CampusPolitoApplication.STUDENT_CHANNEL)){
                removedChannels.add(channel);
            }
        }
        try {
            ParseInstallation.getCurrentInstallation().removeAll("channels", removedChannels);
            ParseInstallation.getCurrentInstallation().save();
            Log.d("CampusPolitoApplication", "All channels removed from installation");
        } catch (ParseException e) {
            Log.e("CampusPolitoApplication", "Error while removing push channels.",e);
        }

    }
    /**
     * Utility function to extract time string
     * @param time The Calendar object
     * @return
     */
    public static String getTimeString(Calendar time){
        return DateUtils.formatDateTime(ctx, time.getTimeInMillis(),
                DateUtils.FORMAT_SHOW_TIME);
    }

    /**
     * Utility function to extract day of week
     * @param time The Calendar object
     * @return
     */
    public static String getDayString(Calendar time){
        return DateUtils.formatDateTime(ctx, time.getTimeInMillis(),
                DateUtils.FORMAT_SHOW_WEEKDAY);
    }

    /**
     * This method checks if there is online connectivity in the application.
     *
     * @param ctx The context used to get the system connectivity manager
     * @return true if there is connection, false otherwise
     */
    public static boolean checkConnectivity(Context ctx){
        ConnectivityManager cm = (ConnectivityManager) ctx.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo ni = cm.getActiveNetworkInfo();
        if (ni != null && ni.isConnected()) {
            return true;
        } else {
            return false;
        }
    }

    public static Bitmap scaleBitmapAndKeepRatio(Bitmap inputBmp, int reqHeightInPixels, int reqWidthInPixels)
    {
        return ThumbnailUtils.extractThumbnail(inputBmp, reqWidthInPixels, reqHeightInPixels);
    }

    public static GoogleApiClient getGoogleApiClient() {
        return googleApiClient;
    }

    public static void setGoogleApiClient(GoogleApiClient googleApiClient) {
        CampusPolitoApplication.googleApiClient = googleApiClient;
    }

    public static Bitmap decodeSampledBitmapFromUri(Uri mPictureUri,
                                                    int reqWidth, int reqHeight) throws IOException {


        InputStream bitmapStream = ctx.getContentResolver().openInputStream(mPictureUri);

        try{
            // First decode with inJustDecodeBounds=true to check dimensions
            final BitmapFactory.Options options = new BitmapFactory.Options();
            options.inJustDecodeBounds = true;
            BitmapFactory.decodeStream(bitmapStream, null, options);

            // Calculate inSampleSize
            options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);

            bitmapStream.close();
            InputStream bitmapStream1 = ctx.getContentResolver().openInputStream(mPictureUri);
            // Decode bitmap with inSampleSize set
            options.inJustDecodeBounds = false;
            Bitmap bmp = BitmapFactory.decodeStream(bitmapStream1, null, options);
            bitmapStream1.close();

            return bmp;
        }

        catch (IOException e){
            bitmapStream.close();
            throw e;
        }
    }

    public static Bitmap decodeSampledBitmapFromURL(String mPictureURL,
                                                    int reqWidth, int reqHeight)
            throws IOException, MalformedURLException {

        URL URL  = new URL(mPictureURL);
        InputStream bitmapStream = URL.openConnection().getInputStream();

        try{
            // First decode with inJustDecodeBounds=true to check dimensions
            final BitmapFactory.Options options = new BitmapFactory.Options();
            options.inJustDecodeBounds = true;
            BitmapFactory.decodeStream(bitmapStream, null, options);

            // Calculate inSampleSize
            options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);

            bitmapStream.close();
            InputStream bitmapStream1 = URL.openConnection().getInputStream();
            // Decode bitmap with inSampleSize set
            options.inJustDecodeBounds = false;
            Bitmap bmp = BitmapFactory.decodeStream(bitmapStream1, null, options);
            bitmapStream1.close();

            return bmp;
        }
        catch (IOException e){
            bitmapStream.close();
            throw e;
        }

    }

    public static int calculateInSampleSize(
            BitmapFactory.Options options, int reqWidth, int reqHeight) {
        // Raw height and width of image
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {

            final int halfHeight = height / 2;
            final int halfWidth = width / 2;

            // Calculate the largest inSampleSize value that is a power of 2 and keeps both
            // height and width larger than the requested height and width.
            while ((halfHeight / inSampleSize) > reqHeight
                    && (halfWidth / inSampleSize) > reqWidth) {
                inSampleSize *= 2;
            }
        }

        return inSampleSize;
    }

    public static int getStringItemPosition(List<String> list, String item){
        if(item == null || list == null || list.isEmpty())
            return -1;

        for(int i = 0; i < list.size(); i++){
            if(list.get(i).equals(item))
                return i;
        }

        return -1;
    }

}
