package com.example.mad.campuspolito.utils;


import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.app.Fragment;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.TextView;

import com.example.mad.campuspolito.R;

import java.util.Calendar;

/**
 * A simple {@link Fragment} subclass.
 */
public class DatePickerFragment extends DialogFragment implements DatePickerDialog.OnDateSetListener {


    private DatePickerInteraction datePickerInteraction;
    private int Year;
    private int Month;
    private int Day;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        final Calendar c;
        if(datePickerInteraction!=null){
            c = datePickerInteraction.setDate();
        }else{
            // Use the current date as the default date in the picker
            c = Calendar.getInstance();
        }

        Year = c.get(Calendar.YEAR);
        Month = c.get(Calendar.MONTH);
        Day = c.get(Calendar.DAY_OF_MONTH);

        // Create a new instance of DatePickerDialog and return it
        return new DatePickerDialog(getActivity(), this, Year, Month, Day);
    }

    public void onDateSet(DatePicker view, int year, int month, int day) {
        // Do something with the date chosen by the user
        Year = year;
        Month = month;
        Day = day;
        if(datePickerInteraction!=null){
            datePickerInteraction.getDate(Year,Month,Day);
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        if(activity instanceof DatePickerInteraction){
            datePickerInteraction = (DatePickerInteraction)activity;
        }else
            throw new RuntimeException("Activity must implements FragmentInteraction interface");

    }

    @Override
    public void onDetach() {
        super.onDetach();
        datePickerInteraction = null;
    }

    public interface DatePickerInteraction{
        Calendar setDate();
        void getDate(int year,int month,int day);

    }


}
