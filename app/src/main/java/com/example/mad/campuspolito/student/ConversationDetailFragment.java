package com.example.mad.campuspolito.student;

import android.app.NotificationManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.provider.ContactsContract;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.NotificationCompat;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.mad.campuspolito.CampusPolitoApplication;
import com.example.mad.campuspolito.R;
import com.example.mad.campuspolito.common.ProfileActivity;
import com.example.mad.campuspolito.model.AppUser;
import com.example.mad.campuspolito.model.Conversation;
import com.example.mad.campuspolito.model.Message;
import com.example.mad.campuspolito.utils.CircularImageView;
import com.example.mad.campuspolito.utils.MessagePushBroadcastReceiver;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseInstallation;
import com.parse.ParsePush;
import com.parse.ParseQuery;
import com.parse.ParseQueryAdapter;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * A fragment representing a single Conversation detail screen.
 * This fragment is either contained in a {@link ConversationListActivity}
 * in two-pane mode (on tablets) or a {@link ConversationDetailActivity}
 * on handsets.
 */
public class ConversationDetailFragment extends Fragment {

    public static final String PUSH_MESSAGE_RECEIVED = "com.example.mad.campuspolito.student.PUSH_MESSAGE_RECEIVED";
    private static final int PAGE_LIMIT = 50;
    /**
     * The fragment argument representing the item ID that this fragment
     * represents.
     */

    public static boolean FOREGROUND = false;
    public static String CONV_ID = null;

    public static final String ARG_ITEM_ID = "item_id";
    private LinearLayout mProgressBar;
    private Toolbar mToolbar;
    private CircularImageView mPicture;
    private Conversation conversation;
    private FetchConversationData task;
    private String conversationId;
    private CardView mMsgInputView;
    private FloatingActionButton mMsgSendView;
    private EditText mMsgFieldView;
    private MessagesAdapter adapter;
    private ListView mListView;
    private RelativeLayout mAddMessageLayout;
    private SendMessageData sendMsgTask;

    private int messagesCount = 0;

    private NotificationManager notificationManager;
    private NotificationCompat.Builder mBuilder;

    private static final String TAG = ConversationDetailFragment.class.getName();

    private BroadcastReceiver receiver;

    public interface Callbacks {
        /**
         * Callback for when an item has been selected.
         */
        public void onGroupDetailsSelected(String id);
    }

    /**
     * A dummy implementation of the {@link Callbacks} interface that does
     * nothing. Used only when this fragment is not attached to an activity.
     */
    private static Callbacks sDummyCallbacks = new Callbacks() {
        @Override
        public void onGroupDetailsSelected(String id) {
        }
    };

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public ConversationDetailFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments().containsKey(ARG_ITEM_ID)) {
            // Load the dummy content specified by the fragment
            // arguments. In a real-world scenario, use a Loader
            // to load content from a content provider.
            //mItem = DummyContent.ITEM_MAP.get(getArguments().getString(ARG_ITEM_ID));
            conversationId = getArguments().getString(ARG_ITEM_ID);
        }

        //setHasOptionsMenu(true);
    }

    private void setUpSoundNotification(){
        notificationManager = (NotificationManager) getActivity()
                .getSystemService(Context.NOTIFICATION_SERVICE);

        //Define sound URI
        Uri soundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

        mBuilder = new NotificationCompat.Builder(getActivity()
                .getApplicationContext())
                .setSound(soundUri); //This sets the sound to play

    }

    private void playNotificationSound(){
        notificationManager.notify(0, mBuilder.build());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_conversation_detail, container, false);


        mToolbar = (Toolbar)rootView.findViewById(R.id.toolbar);

        // Show the Up button in the action bar.
        if(getActivity() instanceof ConversationDetailActivity){
            ConversationDetailActivity activity = (ConversationDetailActivity)getActivity();
            activity.setSupportActionBar(mToolbar);
            activity.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        mAddMessageLayout = (RelativeLayout) rootView.findViewById(R.id.add_msg_layout);
        mListView = (ListView) rootView.findViewById(R.id.message_list);

        mMsgInputView = (CardView) rootView.findViewById(R.id.msg_input_cardview);
        mMsgFieldView = (EditText) rootView.findViewById(R.id.msg_field);
        mMsgSendView = (FloatingActionButton) rootView.findViewById(R.id.send_msg_button);

        mMsgSendView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendMessage();
            }
        });

        mProgressBar = (LinearLayout)rootView.findViewById(R.id.conversation_detail_progress);
        mPicture = (CircularImageView)rootView.findViewById(R.id.picture);
        
        return rootView;
    }

    private void sendMessage() {
        Log.d(TAG, "Sending message.");
        if(!TextUtils.isEmpty(mMsgFieldView.getText().toString())){
            sendMsgTask = new SendMessageData(mMsgFieldView.getText().toString(),conversation);
            Log.d(TAG, "Executing send task.");
            sendMsgTask.execute();
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        if(receiver != null)
            getActivity().unregisterReceiver(receiver);
        if(task != null)
            task.cancel(true);

    }

    @Override
    public void onStart() {
        super.onStart();
        if(receiver == null)
            receiver = new BroadcastReceiver() {
                @Override
                public void onReceive(Context context, Intent intent) {
                    if(adapter != null)
                        adapter.loadObjects();
                }
            };

        getActivity().registerReceiver(receiver, new IntentFilter(PUSH_MESSAGE_RECEIVED));
    }

    @Override
    public void onPause() {
        super.onPause();
        FOREGROUND = false;
        CONV_ID = null;
    }

    @Override
    public void onResume() {
        super.onResume();
        FOREGROUND =  true;
        CONV_ID = conversationId;

        setUpSoundNotification();

        task = new FetchConversationData(getActivity(),conversationId);

        task.execute();
    }

    private void refreshMessages() {
        if(adapter != null)
            adapter.loadObjects();
    }

    private class SendMessageData extends  AsyncTask<Void,Void,Boolean> {


        private final String messageString;
        private final Conversation conversation;

        public SendMessageData(String msg, Conversation conversation){
            this.messageString = msg;
            this.conversation = conversation;
        }
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            mMsgSendView.setClickable(false);
            mMsgFieldView.setText("");
        }

        @Override
        protected Boolean doInBackground(Void... params) {

            try{
                Log.d(TAG, "Saving message " + messageString + " for conversation " + conversation.getObjectId());
                Message message = new Message();

                message.setConversation(conversation);
                message.setData(messageString);

                message.setSender((AppUser) AppUser.getCurrentUser());

                Log.d(TAG, "Basic data added to message object.");

                message.save();

                Log.d(TAG, "Message saved to Parse service");

                conversation.update(message);
                Log.d(TAG, "Conversation updated.");
                conversation.save();

                ParsePush push = new ParsePush();
                push.setChannel(CampusPolitoApplication.CONVERSATION_CHANNEL + conversation.getObjectId());
                push.setData(message.getJSONRepresentation());
                push.send();
                Log.d(TAG, "Pushing message: " + message.getJSONRepresentation().toString());

                if(messagesCount == 0){
                    Log.d(TAG, "New conversation is created. Notify remote users with student channel.");

                    if(!conversation.isGroupChat()){
                        for(AppUser user : conversation.getUsers()){
                            if(!user.getObjectId().equals(AppUser.getCurrentUser().getObjectId())){
                                ParsePush studentPush = new ParsePush();
                                studentPush.setChannel(CampusPolitoApplication.STUDENT_CHANNEL + user.getObjectId());
                                studentPush.setData(getJSONStudentPushMessage(message.getSender().toString(),conversation.getObjectId()));
                                studentPush.send();
                                Log.d(TAG, "Message pushed to recipient to notify new conversation.");
                            }
                        }
                    }
                    else{
                        ParsePush studentsPush = new ParsePush();
                        List<String> studentsChannels = new ArrayList<String>();
                        for(AppUser user : conversation.getUsers()){
                            if(!user.getObjectId().equals(AppUser.getCurrentUser().getObjectId()))
                                studentsChannels.add(CampusPolitoApplication.STUDENT_CHANNEL + user.getObjectId());
                        }
                        studentsPush.setChannels(studentsChannels);
                        studentsPush.setData(getJSONGroupChatPushMessage(conversation.getGroupName(),conversation.getObjectId()));
                        studentsPush.send();
                        Log.d(TAG, "Message pushed to " + studentsChannels.size() + " group members to notify new conversation");
                    }

                }
                else{
                    Log.d(TAG, "No need to push notification on student channel (messagesCount = "+ messagesCount+ " )");
                }

                return true;
            }
            catch (ParseException e) {
                Log.e(TAG, "Error while sending message occurred.", e);

                return false;
            }
        }

        @Override
        protected void onPostExecute(Boolean success) {
            super.onPostExecute(success);
            mMsgSendView.setClickable(true);

            if(!success){
                Snackbar.make(mMsgSendView, R.string.parse_generic_error,Snackbar.LENGTH_LONG);
            }
            else
                adapter.loadObjects();
        }
    }

    private JSONObject getJSONGroupChatPushMessage(String groupName,String conversationId) {
        JSONObject message = new JSONObject();
        try{
            message.put("title",getString(R.string.new_group_conversation_push_msg));
            message.put("alert", groupName);
            message.put(Message.CONVERSATION, conversationId);

            return message;

        }catch (JSONException e){
            Log.e(TAG, "Error while creating push message for student.",e);
            return null;
        }
    }

    private JSONObject getJSONStudentPushMessage(String sender,String conversationId) {

        JSONObject message = new JSONObject();
        try{
            message.put("title",getString(R.string.new_conversation_push_msg));
            message.put("alert", sender);
            message.put(Message.CONVERSATION, conversationId);

            return message;

        }catch (JSONException e){
            Log.e(TAG, "Error while creating push message for student.",e);
            return null;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(requestCode == 1)
            if(resultCode == GroupChatDetailsFragment.UPDATE_LIST){
                getActivity().setResult(ConversationDetailActivity.UPDATE_LIST);
                getActivity().finish();
            }
    }

    private class HideConversation extends AsyncTask<Void,Void,Boolean>{
        private final Context ctx;
        private final Conversation conversation;

        public HideConversation(Context ctx, Conversation conversation){
            this.ctx = ctx;
            this.conversation = conversation;
        }

        @Override
        protected void onPreExecute() {
            mProgressBar.setVisibility(View.VISIBLE);
            mMsgSendView.setVisibility(View.GONE);
            mMsgInputView.setVisibility(View.GONE);
            mListView.setVisibility(View.GONE);
            Snackbar.make(mProgressBar, R.string.conversation_hidden, Snackbar.LENGTH_LONG).show();
        }

        @Override
        protected Boolean doInBackground(Void... params) {
            try{

                AppUser currentUser = (AppUser)AppUser.getCurrentUser();

                if(conversation != null)
                    currentUser.addHiddenConversation(conversation.getObjectId());

                currentUser.save();

                Log.d(TAG, "Conversation hidden.");
                return true;

            }
            catch (ParseException e){
                Log.e(TAG, "Error occurred while removing conversation",e);
                return false;
            }
        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {
            if(aBoolean){
                if(getActivity() instanceof ConversationDetailActivity){
                    getActivity().setResult(ConversationDetailActivity.UPDATE_LIST);
                    getActivity().finish();
                }

                else if (getActivity() instanceof ConversationListActivity){
                    getActivity().getFragmentManager().popBackStack();
                    mProgressBar.setVisibility(View.GONE);
                    ((ConversationListActivity)getActivity()).showConversations();
                    mToolbar.removeAllViews();
                }

            }
            else {
                Snackbar.make(mAddMessageLayout,R.string.parse_generic_error,Snackbar.LENGTH_LONG).show();
            }
        }


    }

    private class FetchConversationData extends AsyncTask<Void,Void,Boolean> {

        final String conversationId;
        final Context context;

        /**Used in group conversations**/
        String groupName;
        String groupDescription;

        /**Used in 1to1 conversations**/
        AppUser recipientUser;
        ParseFile picture = null;

        boolean wasHidden = false;

        List<AppUser> users;

        public FetchConversationData(Context context, String conversationId){
            this.context = context;
            this.conversationId = conversationId;
        }

        @Override
        protected void onPreExecute() {
            mProgressBar.setVisibility(View.VISIBLE);
            mMsgSendView.setVisibility(View.GONE);
            mMsgInputView.setVisibility(View.GONE);
            mListView.setVisibility(View.GONE);
        }

        @Override
        protected Boolean doInBackground(Void... voids) {

            ParseQuery<Conversation> query = ParseQuery.getQuery(Conversation.class);
            Log.d(TAG, "Performing Async Task for conversation id=" + conversationId);

            try{

                Log.d(TAG, "Retrieving object from remote store.");
                query = ParseQuery.getQuery(Conversation.class);
                conversation = query.get(conversationId);
                //conversation.pinInBackground();
                Log.d(TAG, "Object retrieved.");

                if(!ParseInstallation.getCurrentInstallation().getList("channels").contains(CampusPolitoApplication.CONVERSATION_CHANNEL+conversation.getObjectId()))
                    ParsePush.subscribeInBackground(CampusPolitoApplication.CONVERSATION_CHANNEL+conversation.getObjectId()); //Subscribe to push notifications for conversation

                users = conversation.getUsers();

                if(conversation.isGroupChat()){
                    groupName = conversation.getGroupName();
                    groupDescription = conversation.getGroupDescription();
                }
                else{
                    recipientUser = conversation.getRecipientUser();
                    recipientUser.fetchIfNeeded();
                    picture = recipientUser.getPicture();

                }

                AppUser currentUser = (AppUser)AppUser.getCurrentUser();

                if(currentUser.getHiddenConversations() != null)
                    if(currentUser.getHiddenConversations().contains(conversation.getObjectId())){
                        currentUser.removeHiddenConversation(conversation.getObjectId());
                        currentUser.save();
                        wasHidden = true;
                    }
                Log.d(TAG, "Basic data of conversation retrieved.");

                return true;
            }
            catch (ParseException e){
                Log.e(TAG, "Error while fetching conversation data",e);
                return false;
            }
        }

        @Override
        protected void onPostExecute(Boolean success) {

            if(success){
                if(wasHidden){
                    Snackbar.make(mAddMessageLayout,R.string.conversation_not_hidden,Snackbar.LENGTH_SHORT).show();
                }

                if(conversation.isGroupChat()){
                    ((TextView)mToolbar.findViewById(R.id.toolbar_title)).setText(conversation.getGroupName());
                    ((TextView)mToolbar.findViewById(R.id.toolbar_subtitle)).setText(conversation.getGroupDescription());

                    if(mToolbar.getMenu().findItem(R.id.action_show_group_members) == null)
                        mToolbar.inflateMenu(R.menu.menu_group_chat);
                    mToolbar.setOnMenuItemClickListener(new Toolbar.OnMenuItemClickListener() {
                        @Override
                        public boolean onMenuItemClick(MenuItem item) {
                            if(item.getItemId() == R.id.action_show_group_members){

                                if(getActivity() instanceof ConversationDetailActivity){
                                    Intent showDetailsIntent = new Intent(getActivity(), GroupChatDetailsActivity.class);
                                    showDetailsIntent.putExtra(GroupChatDetailsFragment.ARG_ITEM_ID,conversationId);
                                    startActivityForResult(showDetailsIntent, 1);
                                }
                                else if(getActivity() instanceof  ConversationListActivity){
                                    ((ConversationListActivity)getActivity()).onGroupDetailsSelected(conversationId);
                                }

                                return true;
                            }

                            else return getActivity().onOptionsItemSelected(item);
                        }
                    });
                }
                else{
                    ((TextView) mToolbar.findViewById(R.id.toolbar_title)).setText(recipientUser.toString());
                    ((TextView)mToolbar.findViewById(R.id.toolbar_subtitle)).setVisibility(View.GONE);
                    if(mToolbar.getMenu().findItem(R.id.action_hide_conversation) == null)
                        mToolbar.inflateMenu(R.menu.menu_single_chat);
                    mToolbar.setOnMenuItemClickListener(new Toolbar.OnMenuItemClickListener() {
                        @Override
                        public boolean onMenuItemClick(MenuItem item) {
                            if(item.getItemId() == R.id.action_hide_conversation){

                                HideConversation hideTask = new HideConversation(getActivity(),conversation);
                                hideTask.execute();

                                return true;
                            }
                            else if(item.getItemId() == R.id.action_show_profile){

                                Intent showStudentProfile = new Intent(getActivity(), ProfileActivity.class);

                                showStudentProfile.putExtra(ProfileActivity.APP_USER_OBJECT_ID,conversation.getRecipientUser().getObjectId());
                                showStudentProfile.putExtra(ProfileActivity.APP_USER_TYPE, AppUser.PROFILE_STUDENT);

                                startActivity(showStudentProfile);

                                return true;
                            }
                            else return getActivity().onOptionsItemSelected(item);
                        }
                    });
                    if(picture != null){
                        mPicture.setParseFile(picture);
                        mPicture.loadInBackground();
                        mPicture.setVisibility(View.VISIBLE);
                    }
                }

                adapter = new MessagesAdapter(context, new ParseQueryAdapter.QueryFactory<Message>() {
                    @Override
                    public ParseQuery<Message> create() {
                        ParseQuery<Message> query1 = ParseQuery.getQuery(Message.class);
                        query1.whereEqualTo(Message.CONVERSATION,conversation);
                        query1.orderByDescending("createdAt");
                        query1.setLimit(PAGE_LIMIT);

                        return query1;
                    }
                },conversation.isGroupChat());

                adapter.addOnQueryLoadListener(new ParseQueryAdapter.OnQueryLoadListener<Message>() {
                    @Override
                    public void onLoading() {
                    }

                    @Override
                    public void onLoaded(List<Message> list, Exception e) {

                        if (e != null) {
                            Log.e(TAG, "Error while loading messages.", e);
                            return;
                        }

                        if (list != null && messagesCount != list.size()) {
                            adapter.notifyDataSetChanged();

                            if (messagesCount != 0)
                                playNotificationSound();
                            messagesCount = list.size();
                            mListView.setSelection(adapter.getCount() - 1);
                            if(getActivity() instanceof ConversationListActivity){
                                ((ConversationListActivity) getActivity()).onUpdateContent();
                            }
                        }
                    }
                });
                adapter.setPaginationEnabled(false);
                mListView.setAdapter(adapter);

                mProgressBar.setVisibility(View.GONE);
                mListView.setVisibility(View.VISIBLE);
                mMsgSendView.setVisibility(View.VISIBLE);
                mMsgInputView.setVisibility(View.VISIBLE);

            }
            else {
                mProgressBar.setVisibility(View.GONE);
                Snackbar.make(mMsgSendView, R.string.parse_generic_error, Snackbar.LENGTH_SHORT).show();
                Log.d(TAG, "Fetch of data was unsuccessful");
            }
        }

        @Override
        protected void onCancelled() {
            mProgressBar.setVisibility(View.GONE);
            Snackbar.make(mMsgSendView, R.string.parse_generic_error, Snackbar.LENGTH_SHORT).show();
            Log.d(TAG, "Fetch of data was cancelled");
        }
    }
}
