package com.example.mad.campuspolito.common;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Handler;
import android.os.Message;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.NavUtils;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.util.Patterns;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.TimePicker;

import com.example.mad.campuspolito.CampusPolitoApplication;
import com.example.mad.campuspolito.R;
import com.example.mad.campuspolito.model.AppGlobals;
import com.example.mad.campuspolito.model.AppUser;
import com.example.mad.campuspolito.model.Category;
import com.example.mad.campuspolito.model.Company;
import com.example.mad.campuspolito.model.Degree;
import com.example.mad.campuspolito.model.Student;
import com.example.mad.campuspolito.model.Teacher;
import com.example.mad.campuspolito.utils.DynamicLinearLayoutViewInflater;
import com.example.mad.campuspolito.utils.ParseApplicationGlobalsQuery;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.PlaceBuffer;
import com.google.android.gms.location.places.Places;
import com.parse.ParseException;
import com.parse.ParseGeoPoint;
import com.parse.ParseQuery;
import com.google.android.gms.common.api.GoogleApiClient;
import com.parse.codec.binary.StringUtils;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.TimeUnit;

import fr.ganfra.materialspinner.MaterialSpinner;

public class EditProfileActivity extends AppCompatActivity implements GoogleApiClient.ConnectionCallbacks,GoogleApiClient.OnConnectionFailedListener, View.OnClickListener {

    public static final String TAG = EditProfileActivity.class.getName();


    private NestedScrollView mContent;
    private LinearLayout mProgress;
    private Toolbar mToolbar;
    private FloatingActionButton mUpdateProfileButton;


    private AppUser user;
    private Student student;
    private Company company;
    private Teacher teacher;

    public static final String PROFILE_EDIT_SECTION = "PROFILE_EDIT_SECTION";

    private int profileEditSectionId;

    private ShowEditableForm task;

    private GoogleApiClient mGoogleApiClient;
    private PlaceAutocompleteAdapter locationAdapter;

    DialogFragment timePickerFragment;

    protected static DateFormat timeFormat;



    protected static int startHourTimePicker;
    protected static int startMinuteTimePicker;
    protected static int endHourTimePicker;
    protected static int endMinuteTimePicker;


    protected static final String START_HOUR_PICKER_HOUR = "START_HOUR_PICKER_HOUR";
    protected static final String START_HOUR_PICKER_MINUTE = "START_HOUR_PICKER_MINUTE";
    protected static final String END_HOUR_PICKER_HOUR = "END_HOUR_PICKER_HOUR";
    protected static final String END_HOUR_PICKER_MINUTE = "END_HOUR_PICKER_MINUTE";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_profile);

        timeFormat = android.text.format.DateFormat.getTimeFormat(this);
        Calendar c = Calendar.getInstance();

        startHourTimePicker = c.get(Calendar.HOUR_OF_DAY);
        startMinuteTimePicker = c.get(Calendar.MINUTE);
        endHourTimePicker = c.get(Calendar.HOUR_OF_DAY);
        endMinuteTimePicker = c.get(Calendar.MINUTE);

        mGoogleApiClient = new GoogleApiClient
                .Builder(this)
                .addApi(Places.GEO_DATA_API)
                .addApi(Places.PLACE_DETECTION_API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build();

        mContent = (NestedScrollView) findViewById(R.id.edit_profile_content);
        mProgress = (LinearLayout) findViewById(R.id.edit_profile_progress);
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        mUpdateProfileButton = (FloatingActionButton) findViewById(R.id.update_profile_button);

        mUpdateProfileButton.setOnClickListener(this);

        user = (AppUser)AppUser.getCurrentUser();

        Log.d(TAG, "User is dirty?" + user.isDirty());
        Log.d(TAG, "User is authenticated?" + user.isAuthenticated());
        Log.d(TAG, "User is data avilable?" + user.isDataAvailable());
        Log.d(TAG, "User session tooken is " + user.getSessionToken());
        student = null;
        company = null;
        teacher = null;

        Intent intent = getIntent();

        if(intent != null && intent.hasExtra(PROFILE_EDIT_SECTION))
            profileEditSectionId = intent.getIntExtra(PROFILE_EDIT_SECTION,-1);
        else if(savedInstanceState != null)
            profileEditSectionId = savedInstanceState.getInt(PROFILE_EDIT_SECTION);

        switch (profileEditSectionId){
            case R.id.profile_basic_info_edit:

                if(user.isStudent())
                    task = new ShowStudentBasicInfoEditableForm(this);
                else if (user.isTeacher())
                    task = new ShowTeacherBasicInfoEditableForm(this);
                else if (user.isCompany())
                    task = new ShowCompanyBasicInfoEditableForm(this);
                break;

            case R.id.profile_educational_info_edit:
                task = new ShowStudentEducationalInfoEditableForm(this);
                break;

            case R.id.profile_professional_info_edit:
                task = new ShowStudentProfessionalInfoEditableForm(this);
                break;

            case R.id.profile_other_info_edit:
                task = new ShowStudentOtherInfoEditableForm(this);
                break;

            case R.id.profile_teacher_edit:
                task = new ShowTeacherDidacticInfoEditableForm(this);
                break;
        }
        task.execute();


        locationAdapter = new PlaceAutocompleteAdapter(this, android.R.layout.simple_list_item_1,
                mGoogleApiClient, CampusPolitoApplication.BOUNDS_TURIN, null);

        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

    }


    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt(PROFILE_EDIT_SECTION, profileEditSectionId);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if (id == android.R.id.home) {
            // This ID represents the Home or Up button. In the case of this
            // activity, the Up button is shown. Use NavUtils to allow users
            // to navigate up one level in the application structure. For
            // more details, see the Navigation pattern on Android Design:
            //
            // http://developer.android.com/design/patterns/navigation.html#up-vs-back
            //

            NavUtils.navigateUpFromSameTask(this);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onStart() {
        super.onStart();
        mGoogleApiClient.connect();
    }

    @Override
    public void onStop() {
        mGoogleApiClient.disconnect();
        super.onStop();
    }

    @Override
    public void onConnected(Bundle bundle) {

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {

    }

    private void hideSoftKeyboard(View view){
        InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    private static boolean containsCaseInsensitive(List<String> itemsList, String item){
        String ciItem = item.toLowerCase();

        if(itemsList == null || itemsList.isEmpty())
            return false;

        for(String s : itemsList){
            if(s.toLowerCase().equals(ciItem))
                return true;
        }

        return false;
    }

    Handler.Callback updateProfileCallback;

    @Override
    public void onClick(View v) {
        UpdateProfileData updateTask = new UpdateProfileData(this);
        updateTask.execute();
    }

    @Override
    protected void onPause() {
        super.onPause();
        if(timePickerFragment != null){
            timePickerFragment.dismiss();
        }
    }

    public static class TimePickerFragment extends DialogFragment
            implements TimePickerDialog.OnTimeSetListener {

        private volatile boolean isStartHour = false;

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            // Use the current time as the default values for the picker

            int hour = startHourTimePicker;
            int minute = startMinuteTimePicker;


            Bundle bundle = getArguments();

            if(bundle != null){
                if(bundle.containsKey(START_HOUR_PICKER_HOUR) &&
                        bundle.containsKey(START_HOUR_PICKER_MINUTE)){
                    hour = bundle.getInt(START_HOUR_PICKER_HOUR);
                    minute = bundle.getInt(START_HOUR_PICKER_MINUTE);
                    isStartHour = true;
                }
                else if (bundle.containsKey(END_HOUR_PICKER_HOUR) &&
                        bundle.containsKey(END_HOUR_PICKER_MINUTE)){
                    hour = bundle.getInt(END_HOUR_PICKER_HOUR);
                    minute = bundle.getInt(END_HOUR_PICKER_MINUTE);
                    isStartHour = false;
                }

            }


            // Create a new instance of TimePickerDialog and return it
            return new TimePickerDialog(getActivity(), this, hour, minute,
                    android.text.format.DateFormat.is24HourFormat(getActivity()));
        }

        public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
            // Do something with the time chosen by the user
            if(isStartHour){
                startHourTimePicker = hourOfDay;
                startMinuteTimePicker = minute;
                Calendar c = Calendar.getInstance();
                c.set(Calendar.HOUR_OF_DAY, startHourTimePicker);
                c.set(Calendar.MINUTE, startMinuteTimePicker);

                ((EditText)getActivity().findViewById(R.id.office_hour_start)).setText(timeFormat.format(c.getTime()));
            }
            else {
                endHourTimePicker = hourOfDay;
                endMinuteTimePicker = minute;
                Calendar c = Calendar.getInstance();
                c.set(Calendar.HOUR_OF_DAY, endHourTimePicker);
                c.set(Calendar.MINUTE, endMinuteTimePicker);

                ((EditText)getActivity().findViewById(R.id.office_hour_end)).setText(timeFormat.format(c.getTime()));
            }
        }
    }

    private abstract class ShowEditableForm extends AsyncTask<Void,Void,Boolean>{

        public EditProfileActivity ctx;

        public ShowEditableForm(EditProfileActivity ctx){
            this.ctx = ctx;
        }

        @Override
        protected void onPreExecute() {
            mContent.setVisibility(View.GONE);
            mProgress.setVisibility(View.VISIBLE);
            mUpdateProfileButton.hide();

            setUpEditableFormContent();
        }

        @Override
        protected Boolean doInBackground(Void... params) {

            try {
                user.fetchIfNeeded();

                fetchUserDataInBackground(user);

                return true;
            }
            catch (ParseException e){
                Log.e(TAG, "Error while fetching user data.", e);
                return false;
            }
        }

        @Override
        protected void onPostExecute(Boolean success) {

            if(success){
                showUserDataInForeground();
            }
            else {
                Log.e(TAG, "Error while creating editable form.");
            }
            mContent.setVisibility(View.VISIBLE);
            mProgress.setVisibility(View.GONE);
            mUpdateProfileButton.show();

        }

        protected abstract void setUpEditableFormContent();
        protected abstract void fetchUserDataInBackground(AppUser user) throws ParseException;
        protected abstract void showUserDataInForeground();

    }

    private class UpdateProfileData extends AsyncTask<Void,Void,Boolean>{


        private final EditProfileActivity ctx;

        public UpdateProfileData(EditProfileActivity ctx){
            this.ctx = ctx;
        }

        @Override
        protected void onPreExecute() {
            mContent.setVisibility(View.GONE);
            mProgress.setVisibility(View.VISIBLE);
            mUpdateProfileButton.setClickable(false);
        }

        @Override
        protected Boolean doInBackground(Void... params) {
            if(updateProfileCallback != null)
                return updateProfileCallback.handleMessage(new Message());

            else
                return false;
        }

        @Override
        protected void onPostExecute(Boolean success) {
            if(success) {
                ctx.finish();
            }
            else{
                Log.d(TAG, "The user inserted wrong content.");
                mContent.setVisibility(View.VISIBLE);
                mProgress.setVisibility(View.GONE);
                mUpdateProfileButton.setClickable(true);
                Snackbar.make(mContent, R.string.update_profile_wrong_data, Snackbar.LENGTH_SHORT).show();
            }

        }
    }


    private class ShowStudentBasicInfoEditableForm extends ShowEditableForm {

        EditText mShortDescription;
        AutoCompleteTextView mLocation;
        EditText mPhone;

        private String shortDescription;
        private String phone;

        private String locationName;
        private String locationPlaceId;

        private String newLocationId;

        public ShowStudentBasicInfoEditableForm(EditProfileActivity ctx) {
            super(ctx);

            View studentEditProfile = ctx.getLayoutInflater().inflate(R.layout.student_edit_profile,null);

            mContent.addView(studentEditProfile);
        }

        @Override
        protected void setUpEditableFormContent() {
            mToolbar.setTitle(getString(R.string.profile_basic_info));

            findViewById(R.id.profile_basic_info_edit).setVisibility(View.VISIBLE);

            mShortDescription = (EditText)findViewById(R.id.student_short_desc);
            mLocation = (AutoCompleteTextView) findViewById(R.id.student_location);
            mPhone = (EditText) findViewById(R.id.student_phone);
        }

        @Override
        protected void fetchUserDataInBackground(AppUser user) throws ParseException {

            student = user.getStudentProfile();
            student.fetchIfNeeded();
            student.getDegree().fetchIfNeeded();

            shortDescription = student.getShortDesc();
            locationName = student.getLocationName();
            locationPlaceId = student.getPlaceId();
            phone = user.getPhone();

            newLocationId = null;
        }

        @Override
        protected void showUserDataInForeground() {
            mShortDescription.setText(shortDescription);
            mLocation.setText(locationName);
            mPhone.setText(phone);

            mLocation.setAdapter(locationAdapter);

            mLocation.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                    final PlaceAutocompleteAdapter.PlaceAutocomplete item = locationAdapter.getItem(i);
                    newLocationId = String.valueOf(item.placeId);

                }
            });

            updateProfileCallback = new Handler.Callback() {
                @Override
                public boolean handleMessage(Message msg) {

                    try {
                        student.setShortDesc(mShortDescription.getText().toString());

                        if(TextUtils.isEmpty(mPhone.getText()))
                            return false;

                        user.setPhone(mPhone.getText().toString());


                        if(newLocationId != null && !newLocationId.equals(locationPlaceId)){
                            PendingResult<PlaceBuffer> placeResult =
                                    Places.GeoDataApi.getPlaceById(mGoogleApiClient,newLocationId);

                            PlaceBuffer placeBuffer = placeResult.await(10, TimeUnit.SECONDS);
                            if(!placeBuffer.getStatus().isSuccess()){
                                Log.d(TAG, "getPlaceById failed : " + placeBuffer.getStatus());
                                placeBuffer.release();
                                return false;
                            }

                            final Place place = placeBuffer.get(0);

                            ParseGeoPoint geoPoint = new ParseGeoPoint();
                            geoPoint.setLatitude(place.getLatLng().latitude);
                            geoPoint.setLongitude(place.getLatLng().longitude);

                            placeBuffer.release();

                            student.setPlaceId(newLocationId);
                            student.setGeoPoint(geoPoint);
                            student.setLocationName(mLocation.getText().toString());

                        }
                        else if(TextUtils.isEmpty(mLocation.getText())){
                            student.remove(Student.LOCATION_NAME);
                            student.remove(Student.GEOPOINT);
                            student.remove(Student.PLACE_ID);
                        }

                        student.setKeywords(student.createKeywordString());
                        user.save();
                        student.save();

                        return true;

                    }
                    catch (ParseException e){
                        Log.e(TAG, "Error while updating profile.",e);
                        return false;
                    }
                }
            };
        }
    }

    private class ShowStudentEducationalInfoEditableForm extends ShowEditableForm implements  View.OnClickListener, View.OnLongClickListener {

        AutoCompleteTextView mFormationTitle;
        AutoCompleteTextView mSkill;
        AutoCompleteTextView mLanguage;
        TextView mFormationDescription;
        CheckBox mFormationInProgress;
        TextView mFormationStartYear;
        TextView mFormationEndYear;
        LinearLayout mStudentFormationList;
        LinearLayout mStudentSkillList;
        LinearLayout mStudentLanguageList;
        FloatingActionButton mAddFormation;
        FloatingActionButton mAddSkill;
        FloatingActionButton mAddLanguage;

        List<String> allFormationTitles;
        List<String> allSkills;
        List<String> allLanguages;

        List<String> studentSkills;
        List<JSONObject> studentFormationList;
        List<String> studentLanguages;

        int SKILL_TAG = R.id.student_skills;
        int LANGUAGE_TAG = R.id.student_languages;
        int FORMATION_TAG = R.id.student_formation_list;


        public ShowStudentEducationalInfoEditableForm(EditProfileActivity ctx) {
            super(ctx);

            View studentEditProfile = ctx.getLayoutInflater().inflate(R.layout.student_edit_profile, null);

            mContent.addView(studentEditProfile);
        }

        @Override
        protected void setUpEditableFormContent() {
            mToolbar.setTitle(getString(R.string.profile_education_info));

            findViewById(R.id.profile_educational_info_edit).setVisibility(View.VISIBLE);
            mFormationTitle = (AutoCompleteTextView) findViewById(R.id.edu_title);
            mSkill = (AutoCompleteTextView) findViewById(R.id.add_skill);
            mLanguage = (AutoCompleteTextView) findViewById(R.id.add_language);
            mFormationDescription = (TextView) findViewById(R.id.edu_description);
            mFormationInProgress = (CheckBox) findViewById(R.id.edu_in_progress);
            mFormationStartYear = (TextView) findViewById(R.id.edu_start_year);
            mFormationEndYear = (TextView) findViewById(R.id.edu_end_year);
            mStudentFormationList = (LinearLayout) findViewById(R.id.student_formation_list);
            mStudentSkillList = (LinearLayout) findViewById(R.id.student_skills);
            mStudentLanguageList = (LinearLayout) findViewById(R.id.student_languages);
            mAddFormation = (FloatingActionButton) findViewById(R.id.add_edu_button);
            mAddSkill = (FloatingActionButton) findViewById(R.id.add_skill_button);
            mAddLanguage = (FloatingActionButton) findViewById(R.id.add_language_button);
        }

        @Override
        protected void fetchUserDataInBackground(AppUser user) throws ParseException {
            student = user.getStudentProfile();

            student.fetchIfNeeded();
            student.getDegree().fetchIfNeeded();

            allFormationTitles = new ArrayList<>(ParseApplicationGlobalsQuery.getInstance().
                    getStudentEducationTitles().getFirst().getStudentEducationTitles());

            allLanguages = new ArrayList<>(ParseApplicationGlobalsQuery.getInstance().
                    getStudentLanguages().getFirst().getStudentLanguages());

            allSkills = new ArrayList<>(ParseApplicationGlobalsQuery.getInstance().
                    getRequirementsAndSkills().getFirst().getRequirementSkills());

            studentFormationList = student.getEducation();
            if(studentFormationList == null)
                studentFormationList = new ArrayList<>();

            studentSkills = student.getSkills();
            if(studentSkills == null)
                studentSkills = new ArrayList<>();

            studentLanguages = student.getLanguages();
            if(studentLanguages == null)
                studentLanguages = new ArrayList<>();


        }

        @Override
        protected void showUserDataInForeground() {

            mFormationInProgress.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    if (isChecked)
                        mFormationEndYear.setVisibility(View.GONE);
                    else
                        mFormationEndYear.setVisibility(View.VISIBLE);
                }
            });

            if(mFormationInProgress.isChecked())
                mFormationEndYear.setVisibility(View.GONE);

            mFormationTitle.setAdapter(new ArrayAdapter<String>
                    (ctx, android.R.layout.simple_dropdown_item_1line, allFormationTitles));
            mSkill.setAdapter(new ArrayAdapter<String>
                    (ctx, android.R.layout.simple_dropdown_item_1line, allSkills));
            mLanguage.setAdapter(new ArrayAdapter<String>
                    (ctx, android.R.layout.simple_dropdown_item_1line, allLanguages));


            DynamicLinearLayoutViewInflater.addJSONExpEduViewsToLayout(studentFormationList, ctx, mStudentFormationList, this,FORMATION_TAG);

            DynamicLinearLayoutViewInflater.addStringViewsToLayout(studentSkills, ctx, mStudentSkillList, this, SKILL_TAG);

            DynamicLinearLayoutViewInflater.addStringViewsToLayout(studentLanguages, ctx, mStudentLanguageList, this, LANGUAGE_TAG);

            mAddFormation.setOnClickListener(this);
            mAddLanguage.setOnClickListener(this);
            mAddSkill.setOnClickListener(this);




            Snackbar.make(mContent, R.string.long_click_list_items_delete, Snackbar.LENGTH_SHORT).show();

            updateProfileCallback = new Handler.Callback(){
                @Override
                public boolean handleMessage(Message msg) {
                    try {

                        student.setLanguages(studentLanguages);
                        student.setSkills(studentSkills);
                        student.setEducation(studentFormationList);
                        student.setKeywords(student.createKeywordString());
                        student.save();

                        return true;

                    }
                    catch (ParseException e){
                        return false;
                    }
                }
            };
        }

        @Override
        public void onClick(View v) {
            switch (v.getId()){
                case R.id.add_language_button:
                    String newLanguage = mLanguage.getText().toString();

                    if(!TextUtils.isEmpty(newLanguage) && !containsCaseInsensitive(studentLanguages, newLanguage)){
                        studentLanguages.add(newLanguage);
                        DynamicLinearLayoutViewInflater.addStringViewsToLayout(Arrays.asList(newLanguage),ctx,mStudentLanguageList,this,LANGUAGE_TAG);

                        hideSoftKeyboard(v);
                        mLanguage.setText("");
                    }
                    else {
                        Snackbar.make(mContent,R.string.update_profile_wrong_data_language,Snackbar.LENGTH_SHORT).show();
                    }
                    break;

                case R.id.add_skill_button:

                    String newSkill = mSkill.getText().toString();

                    if(!TextUtils.isEmpty(newSkill) && !containsCaseInsensitive(studentSkills, newSkill)){
                        studentSkills.add(newSkill);
                        DynamicLinearLayoutViewInflater.addStringViewsToLayout(Arrays.asList(newSkill),ctx,mStudentSkillList,this,SKILL_TAG);

                        hideSoftKeyboard(v);
                        mSkill.setText("");
                    }
                    else {
                        Snackbar.make(mContent,R.string.update_profile_wrong_data_skill,Snackbar.LENGTH_SHORT).show();
                    }

                    break;

                case R.id.add_edu_button:

                    final String title = mFormationTitle.getText().toString();
                    final String description = mFormationDescription.getText().toString();
                    final String start = mFormationStartYear.getText().toString();
                    final String end = mFormationEndYear.getText().toString();
                    boolean isInProgress = mFormationInProgress.isChecked();

                    if(TextUtils.isEmpty(title) || TextUtils.isEmpty(start)
                            || (TextUtils.isEmpty(end) && !isInProgress)||
                            (!TextUtils.isEmpty(end) && Integer.valueOf(end).compareTo(Integer.valueOf(start))<0))
                        Snackbar.make(mContent,R.string.update_profile_wrong_data_exp_edu,Snackbar.LENGTH_SHORT).show();

                    else {
                        try{
                            JSONObject newElement;

                            if(!isInProgress)
                                newElement = Student.createJSONEduExp(
                                    title,description,Integer.valueOf(start),Integer.valueOf(end)
                            );
                            else {
                                newElement = Student.createJSONEduExp(
                                        title,description,Integer.valueOf(start)
                                );
                            }

                            studentFormationList.add(newElement);
                            DynamicLinearLayoutViewInflater.addJSONExpEduViewsToLayout(Arrays.asList(newElement), ctx, mStudentFormationList, this,FORMATION_TAG);


                            hideSoftKeyboard(v);
                            mFormationTitle.setText("");
                            mFormationDescription.setText("");
                            mFormationStartYear.setText("");
                            mFormationEndYear.setText("");
                            mFormationInProgress.setChecked(false);
                        }
                        catch (JSONException e){
                            Snackbar.make(mContent,R.string.update_profile_wrong_data,Snackbar.LENGTH_SHORT).show();
                        }
                    }
                    break;

            }
        }

        @Override
        public boolean onLongClick(View v) {

            final int listId;

            final Object removedElement;

            if(v.getTag(SKILL_TAG)!= null){
                studentSkills.remove(v.getTag(SKILL_TAG));
                listId = R.id.student_skills;
                removedElement = v.getTag(SKILL_TAG);
            }

            else if (v.getTag(LANGUAGE_TAG) != null){
                studentLanguages.remove(v.getTag(LANGUAGE_TAG));
                listId = R.id.student_languages;
                removedElement = v.getTag(LANGUAGE_TAG);
            }
            else if (v.getTag(FORMATION_TAG) != null){
                studentFormationList.remove(v.getTag(FORMATION_TAG));
                listId = R.id.student_formation_list;
                removedElement = v.getTag(FORMATION_TAG);
            }
            else
                return false;

            ((ViewManager)v.getParent()).removeView(v);

            final ShowStudentEducationalInfoEditableForm form = this;
            Snackbar.make(mContent, R.string.undo_delete_items, Snackbar.LENGTH_LONG).
                    setAction(R.string.undo, new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            switch (listId) {
                                case R.id.student_formation_list:
                                    JSONObject newElement;
                                    if (removedElement instanceof HashMap) {

                                        HashMap<String, Object> mapObject = (HashMap<String, Object>) removedElement;
                                        try {
                                            if (mapObject.containsKey(Student.EDU_EXP_END_YEAR))
                                                newElement = Student.createJSONEduExp((String)(mapObject.get(Student.EDU_EXP_TITLE)),
                                                        (String)mapObject.get(Student.EDU_EXP_DESC),
                                                        (Integer)mapObject.get(Student.EDU_EXP_START_YEAR),
                                                        (Integer)mapObject.get(Student.EDU_EXP_END_YEAR));
                                            else
                                                newElement = Student.createJSONEduExp((String)mapObject.get(Student.EDU_EXP_TITLE),
                                                        (String)mapObject.get(Student.EDU_EXP_DESC),
                                                        (Integer)mapObject.get(Student.EDU_EXP_START_YEAR));

                                        } catch (JSONException e) {
                                            Log.e(TAG, "Unable to undo creation of JSON object.", e);
                                            return;
                                        }

                                    } else {
                                        newElement = (JSONObject)removedElement;
                                    }

                                    studentFormationList.add(newElement);

                                    DynamicLinearLayoutViewInflater.addJSONExpEduViewsToLayout(Arrays.asList(newElement),ctx,mStudentFormationList,form,FORMATION_TAG);
                                    break;
                                case R.id.student_skills:
                                    studentSkills.add((String) removedElement);
                                    DynamicLinearLayoutViewInflater.addStringViewsToLayout(Arrays.asList((String)removedElement),ctx,mStudentSkillList,form,SKILL_TAG);
                                    break;
                                case R.id.student_languages:
                                    studentLanguages.add((String) removedElement);
                                    DynamicLinearLayoutViewInflater.addStringViewsToLayout(Arrays.asList((String) removedElement), ctx, mStudentLanguageList, form, LANGUAGE_TAG);
                                    break;
                            }
                        }
                    }).show();


            return true;
        }
    }






    private class ShowStudentProfessionalInfoEditableForm extends ShowEditableForm implements View.OnClickListener, View.OnLongClickListener {

        AutoCompleteTextView mExperienceTitle;
        MaterialSpinner mAvailability;
        TextView mExperienceDescription;
        CheckBox mExperienceInProgress;
        TextView mExperienceStartYear;
        TextView mExperienceEndYear;
        LinearLayout mStudentExperienceList;

        FloatingActionButton mAddExperience;


        List<String> allExperienceTitles;
        List<String> allAvailability;

        List<JSONObject> studentExperienceList;
        String studentAvailability = null;

        int EXPERIENCE_TAG = R.id.student_experience_list;


        public ShowStudentProfessionalInfoEditableForm(EditProfileActivity ctx) {
            super(ctx);

            View studentEditProfile = ctx.getLayoutInflater().inflate(R.layout.student_edit_profile, null);

            mContent.addView(studentEditProfile);
        }

        @Override
        protected void setUpEditableFormContent() {
            mToolbar.setTitle(getString(R.string.profile_job_info));

            findViewById(R.id.profile_professional_info_edit).setVisibility(View.VISIBLE);
            mExperienceTitle = (AutoCompleteTextView)findViewById(R.id.exp_title);
            mAvailability = (MaterialSpinner)findViewById(R.id.student_availability);
            mExperienceDescription = (TextView)findViewById(R.id.exp_description);
            mExperienceInProgress = (CheckBox)findViewById(R.id.exp_in_progress);
            mExperienceStartYear = (TextView)findViewById(R.id.exp_start_year);
            mExperienceEndYear = (TextView)findViewById(R.id.exp_end_year);
            mStudentExperienceList = (LinearLayout)findViewById(R.id.student_experience_list);
            mAddExperience = (FloatingActionButton)findViewById(R.id.add_exp_button);


        }

        @Override
        protected void fetchUserDataInBackground(AppUser user) throws ParseException {
            student = user.getStudentProfile();

            student.fetchIfNeeded();
            student.getDegree().fetchIfNeeded();

            allExperienceTitles = new ArrayList<>(ParseApplicationGlobalsQuery.getInstance().
                    getStudentExperienceTitles().getFirst().getStudentExperienceTitles());

            allAvailability = new ArrayList<>(ParseApplicationGlobalsQuery.getInstance().
                    getJobTypesAndAvailability().getFirst().getJobTypeAvailability());


            studentExperienceList = student.getExperience();
            if(studentExperienceList == null)
                studentExperienceList = new ArrayList<>();

            studentAvailability = student.getAvailability();


        }

        @Override
        protected void showUserDataInForeground() {

            mExperienceInProgress.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    if (isChecked)
                        mExperienceEndYear.setVisibility(View.GONE);
                    else
                        mExperienceEndYear.setVisibility(View.VISIBLE);
                }
            });

            if(mExperienceInProgress.isChecked())
                mExperienceEndYear.setVisibility(View.GONE);

            mExperienceTitle.setAdapter(new ArrayAdapter<String>
                    (ctx, android.R.layout.simple_dropdown_item_1line, allExperienceTitles));

            mAvailability.setAdapter(new ArrayAdapter<String>
                    (ctx, android.R.layout.simple_spinner_dropdown_item, allAvailability));

            DynamicLinearLayoutViewInflater.addJSONExpEduViewsToLayout(studentExperienceList, ctx, mStudentExperienceList, this, EXPERIENCE_TAG);

            if(studentAvailability != null){
                mAvailability.setSelection(CampusPolitoApplication.getStringItemPosition(allAvailability, studentAvailability) + 1);
            }

            mAddExperience.setOnClickListener(this);

            Snackbar.make(mContent, R.string.long_click_list_items_delete, Snackbar.LENGTH_SHORT).show();

            updateProfileCallback = new Handler.Callback(){
                @Override
                public boolean handleMessage(Message msg) {
                    try {
                        studentAvailability = (String)mAvailability.getSelectedItem();

                        if(studentAvailability.equals(getString(R.string.student_availability))){
                            return false;
                        }
                        student.setAvailability(studentAvailability);
                        student.setExperience(studentExperienceList);

                        student.setKeywords(student.createKeywordString());
                        student.save();

                        return true;

                    }
                    catch (ParseException e){
                        return false;
                    }
                }
            };
        }

        @Override
        public void onClick(View v) {
            final String title = mExperienceTitle.getText().toString();
            final String description = mExperienceDescription.getText().toString();
            final String start = mExperienceStartYear.getText().toString();
            final String end = mExperienceEndYear.getText().toString();
            boolean isInProgress = mExperienceInProgress.isChecked();

            if(TextUtils.isEmpty(title) || TextUtils.isEmpty(start)
                    || (TextUtils.isEmpty(end) && !isInProgress) ||
                    (!TextUtils.isEmpty(end) && Integer.valueOf(end).compareTo(Integer.valueOf(start))<0))
                Snackbar.make(mContent,R.string.update_profile_wrong_data_exp_edu,Snackbar.LENGTH_SHORT).show();

            else {

                try{
                    JSONObject newElement;

                    if(!isInProgress)
                        newElement = Student.createJSONEduExp(
                                title,description,Integer.valueOf(start),Integer.valueOf(end)
                        );
                    else {
                        newElement = Student.createJSONEduExp(
                                title,description,Integer.valueOf(start)
                        );
                    }

                    studentExperienceList.add(newElement);
                    DynamicLinearLayoutViewInflater.addJSONExpEduViewsToLayout(Arrays.asList(newElement),ctx,mStudentExperienceList,this,EXPERIENCE_TAG);

                    hideSoftKeyboard(v);
                    mExperienceTitle.setText("");
                    mExperienceDescription.setText("");
                    mExperienceStartYear.setText("");
                    mExperienceEndYear.setText("");
                    mExperienceInProgress.setChecked(false);
                }
                catch (JSONException e){
                    Snackbar.make(mContent,R.string.update_profile_wrong_data,Snackbar.LENGTH_SHORT).show();
                }
            }

        }

        @Override
        public boolean onLongClick(View v) {
            final Object removedElement = v.getTag(EXPERIENCE_TAG);

            if(removedElement == null)
                return false;

            final int listId = R.id.student_experience_list;

            studentExperienceList.remove(removedElement);

            ((ViewManager)v.getParent()).removeView(v);

            final ShowStudentProfessionalInfoEditableForm form = this;

            Snackbar.make(mContent, R.string.undo_delete_items, Snackbar.LENGTH_LONG).
                    setAction(R.string.undo, new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            switch (listId) {
                                case R.id.student_formation_list:
                                    JSONObject newElement;
                                    if (removedElement instanceof HashMap) {

                                        HashMap<String, String> mapObject = (HashMap<String, String>) removedElement;
                                        try {
                                            String end = mapObject.get(Student.EDU_EXP_END_YEAR);
                                            if (mapObject.containsKey(Student.EDU_EXP_END_YEAR))
                                                newElement = Student.createJSONEduExp(mapObject.get(Student.EDU_EXP_TITLE),
                                                        mapObject.get(Student.EDU_EXP_DESC),
                                                        Integer.valueOf(mapObject.get(Student.EDU_EXP_START_YEAR)),
                                                        Integer.valueOf(mapObject.get(Student.EDU_EXP_END_YEAR)));
                                            else
                                                newElement = Student.createJSONEduExp(mapObject.get(Student.EDU_EXP_TITLE),
                                                        mapObject.get(Student.EDU_EXP_DESC),
                                                        Integer.valueOf(mapObject.get(Student.EDU_EXP_START_YEAR)));

                                        } catch (JSONException e) {
                                            Log.e(TAG, "Unable to undo creation of JSON object.", e);
                                            return;
                                        }

                                    } else {
                                        newElement = (JSONObject)removedElement;
                                    }

                                    studentExperienceList.add(newElement);
                                    DynamicLinearLayoutViewInflater.addJSONExpEduViewsToLayout(Arrays.asList(newElement), ctx, mStudentExperienceList, form, EXPERIENCE_TAG);
                                    break;
                            }
                        }
                    }).show();

            return true;
        }
    }

    private class ShowStudentOtherInfoEditableForm extends ShowEditableForm implements View.OnLongClickListener, View.OnClickListener {

        EditText mHobby;
        FloatingActionButton mAddHobby;
        LinearLayout mHobbyList;

        List<String> studentHobbies;

        public ShowStudentOtherInfoEditableForm(EditProfileActivity ctx) {
            super(ctx);

            View studentEditProfile = ctx.getLayoutInflater().inflate(R.layout.student_edit_profile,null);

            mContent.addView(studentEditProfile);
        }

        @Override
        protected void setUpEditableFormContent() {
            mToolbar.setTitle(getString(R.string.profile_other_info));

            findViewById(R.id.profile_other_info_edit).setVisibility(View.VISIBLE);

            mHobby = (EditText)findViewById(R.id.add_hobby);
            mAddHobby = (FloatingActionButton)findViewById(R.id.add_hobby_button);
            mHobbyList = (LinearLayout)findViewById(R.id.student_hobbies);
        }

        @Override
        protected void fetchUserDataInBackground(AppUser user) throws ParseException {
            student = user.getStudentProfile();

            student.fetchIfNeeded();
            student.getDegree().fetchIfNeeded();

            studentHobbies = student.getHobbies();
            if(studentHobbies == null)
                studentHobbies = new ArrayList<>();
        }

        @Override
        protected void showUserDataInForeground() {

            mAddHobby.setOnClickListener(this);
            DynamicLinearLayoutViewInflater.addStringViewsToLayout(studentHobbies, ctx, mHobbyList, this, R.id.student_hobbies);

            Snackbar.make(mContent, R.string.long_click_list_items_delete, Snackbar.LENGTH_SHORT).show();

            updateProfileCallback = new Handler.Callback(){
                @Override
                public boolean handleMessage(Message msg) {
                    try {

                        student.setHobbies(studentHobbies);
                        student.setKeywords(student.createKeywordString());
                        student.save();

                        return true;

                    }
                    catch (ParseException e){
                        return false;
                    }
                }
            };
        }

        @Override
        public boolean onLongClick(View v) {
            final Object removedElement = v.getTag(R.id.student_hobbies);

            if(removedElement == null)
                return false;

            final int listId = R.id.student_hobbies;

            studentHobbies.remove(removedElement);

            ((ViewManager)v.getParent()).removeView(v);

            final ShowStudentOtherInfoEditableForm form = this;

            Snackbar.make(mContent, R.string.undo_delete_items, Snackbar.LENGTH_LONG).
                    setAction(R.string.undo, new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            switch (listId) {
                                case R.id.student_hobbies:

                                    studentHobbies.add((String) removedElement);
                                    DynamicLinearLayoutViewInflater.addStringViewsToLayout(Arrays.asList((String) removedElement), ctx, mHobbyList, form, R.id.student_hobbies);
                                    break;
                            }
                        }
                    }).show();

            return true;
        }

        @Override
        public void onClick(View v) {
            final String hobby = mHobby.getText().toString();

            if(TextUtils.isEmpty(hobby) || containsCaseInsensitive(studentHobbies,hobby))
                Snackbar.make(mContent,R.string.update_profile_wrong_data_exp_edu,Snackbar.LENGTH_SHORT).show();

            else {

                studentHobbies.add(hobby);
                DynamicLinearLayoutViewInflater.addStringViewsToLayout(Arrays.asList(hobby), ctx, mHobbyList, this, R.id.student_hobbies);


                hideSoftKeyboard(v);
                mHobby.setText("");
            }
        }
    }

    private class ShowTeacherDidacticInfoEditableForm extends ShowEditableForm implements View.OnLongClickListener, View.OnClickListener, View.OnFocusChangeListener {

        LinearLayout mOfficeHours;
        EditText mOfficeHourStart;
        EditText mOfficeHourEnd;
        MaterialSpinner mOfficeHourDay;
        FloatingActionButton mAddOfficeHour;

        List<JSONObject> teacherOfficeHours;

        public ShowTeacherDidacticInfoEditableForm(EditProfileActivity ctx) {
            super(ctx);

            View teacherEditProfile = ctx.getLayoutInflater().inflate(R.layout.teacher_edit_profile,null);
            mContent.addView(teacherEditProfile);
        }

        @Override
        protected void setUpEditableFormContent() {
            mToolbar.setTitle(getString(R.string.profile_teacher_info));

            findViewById(R.id.profile_teacher_edit).setVisibility(View.VISIBLE);
            mOfficeHours = (LinearLayout) findViewById(R.id.teacher_office_hours);
            mOfficeHourStart = (EditText)findViewById(R.id.office_hour_start);
            mOfficeHourEnd = (EditText)findViewById(R.id.office_hour_end);
            mOfficeHourDay = (MaterialSpinner)findViewById(R.id.office_hour_day_spinner);
            mAddOfficeHour = (FloatingActionButton)findViewById(R.id.add_office_hour_button);
        }

        @Override
        protected void fetchUserDataInBackground(AppUser user) throws ParseException {
            teacher = user.getTeacherProfile();

            teacher.fetchIfNeeded();

            teacherOfficeHours = teacher.getOfficePeriods();
            if(teacherOfficeHours == null)
                teacherOfficeHours = new ArrayList<>();
        }

        @Override
        protected void showUserDataInForeground() {

            mOfficeHourDay.setAdapter(new DayOfWeekSimpleAdapter(ctx));

            mAddOfficeHour.setOnClickListener(this);

            DynamicLinearLayoutViewInflater.addOfficeHourViewsToLayout(teacherOfficeHours, ctx, mOfficeHours, this, R.id.teacher_office_hours);



            mOfficeHourStart.setFocusable(false);
            mOfficeHourStart.setFocusableInTouchMode(true);
            mOfficeHourStart.setOnClickListener(this);

            mOfficeHourEnd.setFocusable(false);
            mOfficeHourEnd.setFocusableInTouchMode(true);
            mOfficeHourEnd.setOnClickListener(this);

            mOfficeHourStart.setOnFocusChangeListener(this);

            mOfficeHourEnd.setOnFocusChangeListener(this);

            Snackbar.make(mContent, R.string.long_click_list_items_delete, Snackbar.LENGTH_SHORT).show();

            updateProfileCallback = new Handler.Callback(){
                @Override
                public boolean handleMessage(Message msg) {
                    try {

                        teacher.setOfficePeriods(teacherOfficeHours);
                        teacher.save();

                        return true;

                    }
                    catch (ParseException e){
                        return false;
                    }
                }
            };
        }

        @Override
        public boolean onLongClick(View v) {
            final Object removedElement = v.getTag(R.id.teacher_office_hours);

            if(removedElement == null)
                return false;

            final int listId = R.id.teacher_office_hours;

            teacherOfficeHours.remove(removedElement);

            ((ViewManager)v.getParent()).removeView(v);

            final ShowTeacherDidacticInfoEditableForm form = this;

            Snackbar.make(mContent, R.string.undo_delete_items, Snackbar.LENGTH_LONG).
                    setAction(R.string.undo, new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            switch (listId) {
                                case R.id.teacher_office_hours:
                                    JSONObject newElement;
                                    if (removedElement instanceof HashMap) {

                                        HashMap<String, Integer> mapObject = (HashMap<String, Integer>) removedElement;
                                        try {
                                            newElement = Teacher.createJSONOfficePeriod(
                                              mapObject.get(Teacher.OFFICE_PERIOD_START_HOUR),
                                                    mapObject.get(Teacher.OFFICE_PERIOD_START_MINUTE),
                                                    mapObject.get(Teacher.OFFICE_PERIOD_END_HOUR),
                                                    mapObject.get(Teacher.OFFICE_PERIOD_END_MINUTE),
                                                    mapObject.get(Teacher.OFFICE_PERIOD_DAY));

                                        } catch (JSONException e) {
                                            Log.e(TAG, "Unable to undo creation of JSON object.", e);
                                            return;
                                        }

                                    } else {
                                        newElement = (JSONObject)removedElement;
                                    }

                                    teacherOfficeHours.add(newElement);
                                    mOfficeHours.removeAllViews();
                                    DynamicLinearLayoutViewInflater.addOfficeHourViewsToLayout(teacherOfficeHours, ctx, mOfficeHours, form, R.id.teacher_office_hours);
                                    break;
                            }
                        }
                    }).show();

            return true;
        }

        @Override
        public void onClick(View v) {

            switch (v.getId()){

                case R.id.add_office_hour_button:

                    final Object selectedDayOfWeek = mOfficeHourDay.getSelectedItem();

                    if(!(selectedDayOfWeek instanceof DayOfWeekSimpleAdapter.DayOfWeekHolder)){
                        Snackbar.make(mContent,R.string.update_profile_wrong_data_hour,Snackbar.LENGTH_SHORT).show();
                        return;
                    }

                    final DayOfWeekSimpleAdapter.DayOfWeekHolder dayOfWeek = (DayOfWeekSimpleAdapter.DayOfWeekHolder)selectedDayOfWeek;
                    final String startHourString = (String)mOfficeHourStart.getText().toString();
                    final String endHourString = (String)mOfficeHourEnd.getText().toString();

                    if(TextUtils.isEmpty(startHourString) || TextUtils.isEmpty(endHourString))
                        Snackbar.make(mContent,R.string.update_profile_wrong_data_hour,Snackbar.LENGTH_SHORT).show();
                    else {

                        try{

                            Calendar c = Calendar.getInstance();
                            c.set(Calendar.DAY_OF_WEEK,dayOfWeek.dayId);

                            c.set(Calendar.HOUR_OF_DAY,startHourTimePicker);
                            c.set(Calendar.MINUTE,startMinuteTimePicker);

                            Date start = c.getTime();

                            c.set(Calendar.HOUR_OF_DAY, endHourTimePicker);
                            c.set(Calendar.MINUTE, endMinuteTimePicker);

                            Date end = c.getTime();

                            if(!start.before(end)){
                                Snackbar.make(mContent,R.string.update_profile_wrong_data_hour,Snackbar.LENGTH_SHORT).show();
                                return;
                            }

                            JSONObject officeHour = Teacher.createJSONOfficePeriod(start,end);

                            teacherOfficeHours.add(officeHour);
                            mOfficeHours.removeAllViews();
                            DynamicLinearLayoutViewInflater.addOfficeHourViewsToLayout(teacherOfficeHours, ctx, mOfficeHours, this, R.id.teacher_office_hours);

                            hideSoftKeyboard(v);
                            mOfficeHourDay.setSelection(0);
                            mOfficeHourStart.setText("");
                            mOfficeHourEnd.setText("");
                        }
                        catch (JSONException e){
                            Snackbar.make(mContent,R.string.update_profile_wrong_data_hour,Snackbar.LENGTH_SHORT).show();
                        }
                    }


                    break;

                case R.id.office_hour_start:
                    activateStartTimePicker();
                    break;

                case R.id.office_hour_end:
                    activateEndTimePicker();
                    break;
            }

        }


        @Override
        public void onFocusChange(View v, boolean hasFocus) {
            if(hasFocus){
                switch (v.getId()){
                    case R.id.office_hour_start:
                        activateStartTimePicker();
                        break;

                    case R.id.office_hour_end:
                        activateEndTimePicker();
                        break;
                }
            }
        }

        private void activateStartTimePicker(){
            Log.d(TAG, "Showing office hour start dialog.");
            timePickerFragment = new TimePickerFragment();
            Bundle b = new Bundle();
            b.putInt(START_HOUR_PICKER_HOUR,startHourTimePicker);
            b.putInt(START_HOUR_PICKER_MINUTE, startMinuteTimePicker);

            timePickerFragment.setArguments(b);
            timePickerFragment.show(getSupportFragmentManager(), "timePickerStart");

        }

        private void activateEndTimePicker(){
            Log.d(TAG, "Showing office hour end dialog.");
            timePickerFragment = new TimePickerFragment();
            Bundle b = new Bundle();
            b.putInt(END_HOUR_PICKER_HOUR,endHourTimePicker);
            b.putInt(END_HOUR_PICKER_MINUTE, endMinuteTimePicker);

            timePickerFragment .setArguments(b);
            timePickerFragment.show(getSupportFragmentManager(), "timePickerEnd");

        }
    }

    private class ShowTeacherBasicInfoEditableForm extends ShowEditableForm {
        EditText mPhone;
        private String phone;

        public ShowTeacherBasicInfoEditableForm(EditProfileActivity ctx) {
            super(ctx);

            View teacherEditProfile = ctx.getLayoutInflater().inflate(R.layout.teacher_edit_profile,null);

            mContent.addView(teacherEditProfile);
        }

        @Override
        protected void setUpEditableFormContent() {
            mToolbar.setTitle(getString(R.string.profile_basic_info));

            findViewById(R.id.profile_basic_info_edit).setVisibility(View.VISIBLE);

            mPhone = (EditText)findViewById(R.id.teacher_phone);
        }

        @Override
        protected void fetchUserDataInBackground(AppUser user) throws ParseException {
            teacher = user.getTeacherProfile();
            teacher.fetchIfNeeded();

            phone = user.getPhone();

        }

        @Override
        protected void showUserDataInForeground() {
            mPhone.setText(phone);


            updateProfileCallback = new Handler.Callback() {
                @Override
                public boolean handleMessage(Message msg) {
                    try {
                        if (TextUtils.isEmpty(mPhone.getText()))
                            return false;

                        user.setPhone(mPhone.getText().toString());

                        user.save();

                        return true;

                    }
                    catch (ParseException e){
                        return false;
                    }
                }
            };
        }
    }

    private class ShowCompanyBasicInfoEditableForm extends ShowEditableForm {

        EditText mMission;
        EditText mVATNumber;
        MaterialSpinner mCategory;
        MaterialSpinner mSubcategory;
        EditText mUrl;
        AutoCompleteTextView mLocation;
        MaterialSpinner mSize;
        EditText mPhone;

        private String mission;
        private String vatnumber;
        private String category;
        private String subcategory;
        private String url;
        private String locationName;
        private String locationPlaceId;
        private String newLocationId;
        private String size;
        private String phone;

        private HashMap<String, ArrayList<String>> categoriesMap;
        private ArrayList<String> categoriesKeys;
        private List<String> enterpriseSizes;


        public ShowCompanyBasicInfoEditableForm(EditProfileActivity ctx) {
            super(ctx);

            View companyEditProfile = ctx.getLayoutInflater().inflate(R.layout.company_edit_profile,null);

            mContent.addView(companyEditProfile);
        }

        @Override
        protected void setUpEditableFormContent() {
            mToolbar.setTitle(getString(R.string.profile_basic_info));

            findViewById(R.id.profile_basic_info_edit).setVisibility(View.VISIBLE);

            mMission = (EditText)findViewById(R.id.company_mission);
            mVATNumber = (EditText)findViewById(R.id.company_vatnumber);
            mCategory = (MaterialSpinner)findViewById(R.id.company_category);
            mSubcategory = (MaterialSpinner)findViewById(R.id.company_subcategory);
            mUrl = (EditText)findViewById(R.id.company_url);
            mLocation = (AutoCompleteTextView)findViewById(R.id.company_location);
            mSize = (MaterialSpinner)findViewById(R.id.company_size);
            mPhone = (EditText)findViewById(R.id.company_phone);
        }

        @Override
        protected void fetchUserDataInBackground(AppUser user) throws ParseException {
            company = user.getCompanyProfile();
            company.fetchIfNeeded();

            enterpriseSizes = ParseApplicationGlobalsQuery.getInstance()
                    .getCompanySize().getFirst().getCompanySize();

            List<Category> categories = ParseApplicationGlobalsQuery.getInstance()
                    .getCompanyAndOfferCategories().find();


            categoriesMap = new HashMap<String, ArrayList<String>>();
            categoriesKeys = new ArrayList<String>();
            for(Category c : categories){
                categoriesMap.put(c.getName(),new ArrayList<String>(c.getSubcategories()));
                categoriesKeys.add(c.getName());
            }

            mission = company.getMission();
            vatnumber = company.getVATNumber();
            category = company.getCategory();
            subcategory = company.getSubcategory();
            url = company.getCompanyURL();
            locationName = company.getLocationName();
            locationPlaceId = company.getPlaceId();
            size = company.getEnterpriseSize();
            phone = user.getPhone();

            newLocationId = null;
        }

        @Override
        protected void showUserDataInForeground() {
            mMission.setText(mission);
            mVATNumber.setText(vatnumber);
            mUrl.setText(url);

            mLocation.setText(locationName);
            mLocation.setAdapter(locationAdapter);
            mLocation.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                    final PlaceAutocompleteAdapter.PlaceAutocomplete item = locationAdapter.getItem(i);
                    newLocationId = String.valueOf(item.placeId);

                }
            });

            mCategory.setAdapter(
                    new ArrayAdapter<String>(ctx, android.R.layout.simple_spinner_dropdown_item, categoriesKeys));

            mCategory.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    String selectedItem = (String) parent.getSelectedItem();
                    if (!selectedItem.equals(getString(R.string.company_category))) {
                        List<String> subcategoryValues = categoriesMap.get(selectedItem);

                        mSubcategory.setAdapter(
                                new ArrayAdapter<String>(ctx,
                                        android.R.layout.simple_spinner_dropdown_item, subcategoryValues));

                        if(subcategory != null){
                            mSubcategory.setSelection(CampusPolitoApplication.getStringItemPosition(subcategoryValues, subcategory) + 1);
                        }
                        Log.d(TAG, "Adapter added to subcategory");
                    }
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });

            if(category != null && subcategory != null){
                mCategory.setSelection(CampusPolitoApplication.getStringItemPosition(categoriesKeys, category) + 1);

            }

            mSize.setAdapter(
                    new ArrayAdapter<String>(ctx, android.R.layout.simple_spinner_dropdown_item, enterpriseSizes));

            if(size != null){
                mSize.setSelection(CampusPolitoApplication.getStringItemPosition(enterpriseSizes, size)+1);
            }

            mPhone.setText(phone);

            updateProfileCallback = new Handler.Callback() {
                @Override
                public boolean handleMessage(Message msg) {
                    try{
                        company.setMission(mMission.getText().toString());

                        if(TextUtils.isEmpty(mVATNumber.getText()))
                            return false;

                        company.setVATNumber(mVATNumber.getText().toString());

                        if(!TextUtils.isEmpty(mUrl.getText()))
                            if(!Patterns.WEB_URL.matcher(mUrl.getText()).matches())
                                return false;

                        company.setCompanyURL(mUrl.getText().toString());

                        String enterpriseSize = (String)mSize.getSelectedItem();
                        String category = (String)mCategory.getSelectedItem();
                        String subcategory = (String)mSubcategory.getSelectedItem();

                        if(category.equals(getString(R.string.company_category))||
                                subcategory.equals(getString(R.string.company_subcategory)))
                            return false;
                        company.setCategory(category);
                        company.setSubcategory(subcategory);

                        if(!enterpriseSize.equals(getString(R.string.company_size)))
                            company.setEnterpriseSize(enterpriseSize);


                        if(TextUtils.isEmpty(mPhone.getText()))
                            return false;

                        user.setPhone(mPhone.getText().toString());

                        if(newLocationId != null && !newLocationId.equals(locationPlaceId)){
                            PendingResult<PlaceBuffer> placeResult =
                                    Places.GeoDataApi.getPlaceById(mGoogleApiClient,newLocationId);

                            PlaceBuffer placeBuffer = placeResult.await(10, TimeUnit.SECONDS);
                            if(!placeBuffer.getStatus().isSuccess()){
                                Log.d(TAG, "getPlaceById failed : " + placeBuffer.getStatus());
                                placeBuffer.release();
                                return false;
                            }

                            final Place place = placeBuffer.get(0);

                            ParseGeoPoint geoPoint = new ParseGeoPoint();
                            geoPoint.setLatitude(place.getLatLng().latitude);
                            geoPoint.setLongitude(place.getLatLng().longitude);

                            placeBuffer.release();

                            company.setPlaceId(newLocationId);
                            company.setGeoPoint(geoPoint);
                            company.setLocationName(mLocation.getText().toString());

                        }
                        else if(TextUtils.isEmpty(mLocation.getText())){
                            company.remove(Company.LOCATION_NAME);
                            company.remove(Company.GEOPOINT);
                            company.remove(Company.PLACE_ID);
                        }


                        user.save();
                        company.save();
                        return true;
                    }
                    catch (ParseException e){
                        Log.e(TAG, "Error while editing company profile.",e);
                        return false;
                    }
                }
            };

        }
    }
}
