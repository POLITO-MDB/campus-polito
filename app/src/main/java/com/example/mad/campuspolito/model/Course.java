package com.example.mad.campuspolito.model;

import android.content.Context;

import com.example.mad.campuspolito.R;
import com.parse.ParseClassName;
import com.parse.ParseObject;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;
import java.util.List;

/**
 * Created by mdb on 20/07/15.
 */
@ParseClassName("Course")
public class Course extends ParseObject{

  public static final String TITLE = "title";
    public static final String DESCRIPTION = "description";
    public static final String LECTURES = "lectures";
    public static final String DEGREE = "degree";
    public static final String CODE = "code";
    public static final String CFU = "cfu";
    public static final String SEMESTER = "semester";

    public static final String NOTICES = "notices";
    public static final String NOTICE_TEACHER = "notice_teacher";
    public static final String NOTICE_CONTENT = "notice_content";
    public static final String NOTICE_TIME = "notice_time";



    public Course(){

    }

    public void setTitle(String title){
        put(TITLE, title);
    }
    public String getTitle(){
        return getString(TITLE);
    }
    public void setDescription(String description){
        put(DESCRIPTION, description);
    }
    public String getDescription(){
        return getString(DESCRIPTION);
    }

    public void setLectures(List<Lecture> lectures){
        put(LECTURES, lectures);
    }
    public List<Lecture> getLectures(){
        return getList(LECTURES);
    }

    public void addLecture(Lecture lecture){
        addUnique(LECTURES, lecture);
    }
    public void removeLecture(Lecture lecture){
        removeAll(LECTURES, Arrays.asList(lecture));
    }

    public void addLectures(List<Lecture> lectures){
        addAll(LECTURES, lectures);
    }

    public void removeLectures(List<Lecture> lectures){
        removeAll(LECTURES,lectures);
    }

    public void setDegree(Degree degree){
        put(DEGREE, degree);
    }
    public Degree getDegree(){
        return (Degree)getParseObject(DEGREE);
    }

    public void setCode(String code){
        put(CODE, code);
    }
    public String getCode(){
        return getString(CODE);
    }

    public void setCfu(int cfu){
        put(CFU, cfu);
    }

    public int getCfu(){
        return getInt(CFU);
    }

    public void setSemester(String semester){
        put(SEMESTER, semester);
    }
    public String getSemester(){
        return getString(SEMESTER);
    }

    public void setNotices(List<JSONObject> notices){
        put(NOTICES,notices);
    }
    public void addNotice(JSONObject notice){
        addUnique(NOTICES, notice);
    }
    public void removeNotice(JSONObject notice){
        removeAll(NOTICES, Arrays.asList(notice));
    }
    public List<JSONObject> getNotices(){
        return getList(NOTICES);
    }

    public static JSONObject createNotice(String teacherName, String content){
        JSONObject object = new JSONObject();
        try{
            object.put(NOTICE_TEACHER, teacherName);
            object.put(NOTICE_CONTENT, content);
            object.put(NOTICE_TIME, System.currentTimeMillis());

            return object;
        }
        catch (JSONException e){
            return null;
        }
    }

    public JSONObject getJSONRepresentation(String message, String teacher, Context ctx) {
        JSONObject msgJSON = new JSONObject();

        try {
            msgJSON.put("title", getCode() + " - "
                    +  getTitle());
            msgJSON.put("alert",  ctx.getString(R.string.from)+ " " + teacher + " - " + message);
            msgJSON.put(Lecture.COURSE, getObjectId());
        }
        catch (JSONException e){
            return null;
        }
        return msgJSON;
    }
}
