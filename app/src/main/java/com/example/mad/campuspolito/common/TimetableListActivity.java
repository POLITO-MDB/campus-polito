package com.example.mad.campuspolito.common;

import android.content.Intent;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.NavUtils;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
;

import com.example.mad.campuspolito.CampusPolitoApplication;
import com.example.mad.campuspolito.R;
import com.example.mad.campuspolito.model.AppUser;
import com.example.mad.campuspolito.model.Course;
import com.example.mad.campuspolito.model.Lecture;
import com.example.mad.campuspolito.model.Location;
import com.example.mad.campuspolito.teacher.LectureEditorActivity;
import com.parse.ParseException;
import com.parse.ParseQuery;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

/**
 * @author kurado
 */

public class TimetableListActivity extends AppCompatActivity implements TimetableListFragment.OnFragmentInteractionListener {

    private final String TAG = TimetableListActivity.class.getName();
    public static final String CourseId = "CourseId";

    private Toolbar toolbar;
    private LecturesAdapter adapter;
    private FloatingActionButton fabNewLecture;

    private String courseId;

    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_timetable_list);

        toolbar = (Toolbar)findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        fabNewLecture = (FloatingActionButton)findViewById(R.id.new_lecture);

        courseId = getIntent().getStringExtra(Lecture.COURSE);


        if(courseId!=null && ((AppUser)AppUser.getCurrentUser()).isTeacher()){
            fabNewLecture.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(getApplicationContext(), LectureEditorActivity.class);
                    intent.putExtra(Lecture.COURSE,courseId);
                    startActivityForResult(intent, LectureEditorActivity.EDIT_LECTURE);
                }
            });
            fabNewLecture.setVisibility(View.VISIBLE);
        }

        Log.d(TAG, "Loading fragment");
        TimetableListFragment fragment = new TimetableListFragment();
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.contentFragment,fragment).commit();


    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_timetable_list, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if (id == R.id.action_logout) {
            CampusPolitoApplication.unsubscribeFromAllChannels();
            AppUser.logOut();
            Intent homeIntent = new Intent(this, MainActivity.class);
            homeIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(homeIntent);
            finish();
            return true;
        }


        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == LectureDetailActivity.DETAIL_STATE){
            if(resultCode == LectureDetailActivity.LECTURE_STATUS_MODIFIED){
                adapter.loadObjects();
            }
        }else if(requestCode == LectureEditorActivity.EDIT_LECTURE){
            if(resultCode == LectureEditorActivity.LECTURE_EDITED){
                adapter.loadObjects();
            }
        }
    }

    @Override
    public void onLectureSelected(String id) {
        Intent intent = new Intent(this,LectureDetailActivity.class);
        intent.putExtra(LectureDetailActivity.LECTURE_ID, id);
        startActivityForResult(intent, LectureDetailActivity.DETAIL_STATE);

    }

    @Override
    public void onRetrieveAdapter(LecturesAdapter adapter) {
        this.adapter = adapter;
    }

    @Override
    public String onCourseIdRetrieved(){
        return courseId;
    }
}
