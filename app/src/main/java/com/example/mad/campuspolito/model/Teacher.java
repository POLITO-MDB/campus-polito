package com.example.mad.campuspolito.model;

import com.parse.ParseClassName;
import com.parse.ParseObject;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * Created by mdb on 20/07/15.
 */
@ParseClassName("Teacher")
public class Teacher extends ParseObject{

    public static final String ID_NUMBER = "id_number";
    public static final String USER = "user";
    public static final String NAME = "name";
    public static final String SURNAME = "surname";
    public static final String OFFICE_PERIOD_START_HOUR = "office_period_start_hour";
    public static final String OFFICE_PERIOD_END_HOUR = "office_period_end_hour";
    public static final String OFFICE_PERIOD_START_MINUTE = "office_period_start_minute";
    public static final String OFFICE_PERIOD_END_MINUTE = "office_period_end_minute";
    public static final String OFFICE_PERIOD_DAY = "office_period_day";
    public static final String OFFICE_HOURS = "office_periods";
    public static final String COURSES = "courses";
    public static final String DEPARTMENT_ID = "department_id";
    public static final String EMAIL = "email";
    public static final String ROLE = "role";
    public static final String PHONE = "tel";
    public static final String COURSE_IDS_STRING = "course_ids";

    public Teacher(){

    }

    @Override
    public String toString() {
        return getName() + " " + getSurname();
    }


    public void setDepartmentId(String departmentId){
        put(DEPARTMENT_ID, departmentId);
    }
    public String getDepartmentId(){
        return getString(DEPARTMENT_ID);
    }

    public void setEmail(String email){
        put(EMAIL, email);
    }
    public String getEmail(){
        return getString(EMAIL);
    }
    public void setRole(String role){
        put(ROLE, role);
    }
    public String getRole(){
        return getString(ROLE);
    }
    public void setPhone(String phone){
        put(PHONE, phone);
    }
    public String getPhone(){
        return getString(PHONE);
    }
    public void setIdNumber(String idNumber){
        put(ID_NUMBER, idNumber);
    }
    public String getIdNumber(){
        return getString(ID_NUMBER);
    }
    public void setName(String name){
        put(NAME, name);
    }
    public String getName(){
        return getString(NAME);
    }

    public void setSurname(String surname){
        put(SURNAME, surname);
    }
    public String getSurname(){
        return getString(SURNAME);
    }


    public void setUser(AppUser user){
        put(USER, user);
    }

    public AppUser getUser(){
        return (AppUser)getParseUser(USER);
    }

    public void setOfficePeriods(List<JSONObject> officePeriods){
        put(OFFICE_HOURS, officePeriods);
    }
    public List<JSONObject> getOfficePeriods(){
        return getList(OFFICE_HOURS);
    }

    public void addOfficePeriod(JSONObject officePeriod){
        addUnique(OFFICE_HOURS, officePeriod);
    }

    public void removeOfficePeriod(JSONObject officePeriod){
        removeAll(OFFICE_HOURS, Arrays.asList(officePeriod));
    }

    public static JSONObject createJSONOfficePeriod(Date start, Date end) throws JSONException {
        JSONObject officePeriod = new JSONObject();
        Calendar c = Calendar.getInstance();

        c.setTime(start);
        officePeriod.put(OFFICE_PERIOD_START_HOUR, c.get(Calendar.HOUR_OF_DAY));
        officePeriod.put(OFFICE_PERIOD_START_MINUTE, c.get(Calendar.MINUTE));
        c.setTime(end);
        officePeriod.put(OFFICE_PERIOD_END_HOUR, c.get(Calendar.HOUR_OF_DAY));
        officePeriod.put(OFFICE_PERIOD_END_MINUTE, c.get(Calendar.MINUTE));
        officePeriod.put(OFFICE_PERIOD_DAY, c.get(Calendar.DAY_OF_WEEK));
        return officePeriod;
    }

    public static JSONObject createJSONOfficePeriod(Integer startHour, Integer startMinute,
                                                    Integer endHour, Integer endMinute,
                                                    Integer dayOfWeek) throws JSONException{
        JSONObject officePeriod = new JSONObject();

        officePeriod.put(OFFICE_PERIOD_START_HOUR, startHour);
        officePeriod.put(OFFICE_PERIOD_START_MINUTE, startMinute);
        officePeriod.put(OFFICE_PERIOD_END_HOUR, endHour);
        officePeriod.put(OFFICE_PERIOD_END_MINUTE, endMinute);
        officePeriod.put(OFFICE_PERIOD_DAY, dayOfWeek);


        return officePeriod;
    }

    public static Date getStartDateOfPeriod(JSONObject jsonOfficePeriod) throws JSONException{
        Calendar calendar = Calendar.getInstance();

        calendar.set(Calendar.HOUR_OF_DAY, jsonOfficePeriod.getInt(OFFICE_PERIOD_START_HOUR));
        calendar.set(Calendar.MINUTE, jsonOfficePeriod.getInt(OFFICE_PERIOD_START_MINUTE));
        calendar.set(Calendar.DAY_OF_WEEK, jsonOfficePeriod.getInt(OFFICE_PERIOD_DAY));

        return calendar.getTime();
    }

    public static Date getEndDateOfPeriod(JSONObject jsonOfficePeriod) throws JSONException{
        Calendar calendar = Calendar.getInstance();

        calendar.set(Calendar.HOUR_OF_DAY, jsonOfficePeriod.getInt(OFFICE_PERIOD_END_HOUR));
        calendar.set(Calendar.MINUTE, jsonOfficePeriod.getInt(OFFICE_PERIOD_END_MINUTE));
        calendar.set(Calendar.DAY_OF_WEEK, jsonOfficePeriod.getInt(OFFICE_PERIOD_DAY));

        return calendar.getTime();
    }

    public void setCourses(List<Course> courses){
        put(COURSES, courses);
    }

    public List<Course> getCourses(){
        return getList(COURSES);
    }

    public void addCourse(Course course){
        addUnique(COURSES, course);
    }
    public void removeCourse(Course course){
        removeAll(COURSES, Arrays.asList(course));
    }

    public void setCourseIdsString(String courseIdsString){
        put(COURSE_IDS_STRING, courseIdsString);
    }

    public String getCourseIdsString(){
        return getString(COURSE_IDS_STRING);
    }

}
