package com.example.mad.campuspolito.offer;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Intent;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.SwitchCompat;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.mad.campuspolito.R;
import com.example.mad.campuspolito.model.AppUser;
import com.example.mad.campuspolito.model.Category;
import com.example.mad.campuspolito.model.Company;
import com.example.mad.campuspolito.model.Notice;
import com.example.mad.campuspolito.model.Offer;
import com.example.mad.campuspolito.utils.ParseApplicationGlobalsQuery;
import com.parse.FindCallback;
import com.parse.GetCallback;
import com.parse.ParseException;
import com.parse.ParseQuery;
import com.parse.SaveCallback;
import com.wefika.flowlayout.FlowLayout;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

//TODO skills

import fr.ganfra.materialspinner.MaterialSpinner;

public class EditOfferActivity extends AppCompatActivity implements View.OnClickListener {

    public static final String OFFER_ID = "offer_id";

    private static final String TAG = EditOfferActivity.class.getName();
    public static final int EDIT_REQUEST = 90;
    public static final int CREATE_REQUEST = 91;
    private static final String IS_PUBLISHED = "is_published";
    private static final String TITLE = "title";
    private static final String DESCRIPTION = "description";
    private static final String CATEGORY = "category";
    private static final String JOB_TYPE = "job_type";
    private static final String DURATION_LENGTH = "duration_length";
    private static final String DURATION_TYPE = "duration_type";
    private static final String SKILLS = "skills";
    private static final String CATEGORIES = "categories";
    private static final String JOB_TYPES = "job_types";
    private static final String DURATION_TYPES = "duration_types";
    private static final String EXPIRY_DATE = "expiry_date";

    private Toolbar mToolbar;
    private LinearLayout mProgressBar;
    private NestedScrollView mOfferForm;
    private SwitchCompat mPublishSwitch;
    private EditText mTitle;
    private EditText mDescription;
    private FloatingActionButton mAddSkillButton;
    private EditText mSkill;
    private FlowLayout mSkillTags;
    private TextView mNoTags;
    private MaterialSpinner mCategories;
    private MaterialSpinner mJobTypes;
    private EditText mDurationLength;
    private MaterialSpinner mDurationTypes;
    private TextView mExpiryDate;
    private FloatingActionButton mSaveButton;

    private boolean is_published;
    private String offer_id;
    private String title;
    private String description;
    private String category;
    private String job_type;
    private String duration_length;
    private String duration_type;
    private Date expiry_date;
    private ArrayList<String> skills;
    private ArrayList<String> categories;
    private ArrayList<String> job_types;
    private ArrayList<String> duration_types;
    private int mYear;
    private int mMonth;
    private int mDay;
    private DateFormat format;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_offer);

        final Calendar c = Calendar.getInstance();
        mYear = c.get(Calendar.YEAR);
        mMonth = c.get(Calendar.MONTH);
        mDay = c.get(Calendar.DAY_OF_MONTH);
        format = android.text.format.DateFormat.getDateFormat(getApplicationContext());

        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        mProgressBar = (LinearLayout) findViewById(R.id.progress_bar);
        mOfferForm = (NestedScrollView) findViewById(R.id.offer_form);
        mPublishSwitch = (SwitchCompat) findViewById(R.id.publish_switch);
        mTitle = (EditText) findViewById(R.id.title);
        mDescription = (EditText) findViewById(R.id.description);
        mAddSkillButton = (FloatingActionButton) findViewById((R.id.add_skill));
        mSkill = (EditText) findViewById(R.id.skill);
        mSkillTags = (FlowLayout) findViewById(R.id.skill_tags);
        mNoTags = (TextView) findViewById(R.id.no_tags);
        mCategories = (MaterialSpinner) findViewById(R.id.category);
        mJobTypes = (MaterialSpinner) findViewById(R.id.job_type);
        mDurationLength = (EditText) findViewById(R.id.duration_length);
        mDurationTypes = (MaterialSpinner) findViewById(R.id.duration_type);
        mExpiryDate = (TextView) findViewById(R.id.expiry_date);
        mSaveButton = (FloatingActionButton) findViewById(R.id.save_button);

        setSupportActionBar(mToolbar);

        if (getIntent() != null && getIntent().hasExtra(OFFER_ID)) {
            offer_id = getIntent().getStringExtra(OFFER_ID);
            getSupportActionBar().setTitle(R.string.title_activity_edit_offer);
        } else {
            offer_id = null;
            getSupportActionBar().setTitle(R.string.title_activity_new_offer);
        }

        mProgressBar.setVisibility(View.VISIBLE);
        mOfferForm.setVisibility(View.GONE);

        if (savedInstanceState != null) {
            is_published = savedInstanceState.getBoolean(IS_PUBLISHED);
            offer_id = savedInstanceState.getString(OFFER_ID);
            title = savedInstanceState.getString(TITLE);
            description = savedInstanceState.getString(DESCRIPTION);
            category = savedInstanceState.getString(CATEGORY);
            job_type = savedInstanceState.getString(JOB_TYPE);
            duration_length = savedInstanceState.getString(DURATION_LENGTH);
            duration_type = savedInstanceState.getString(DURATION_TYPE);
            skills = savedInstanceState.getStringArrayList(SKILLS);
            categories = savedInstanceState.getStringArrayList(CATEGORIES);
            job_types = savedInstanceState.getStringArrayList(JOB_TYPES);
            duration_types = savedInstanceState.getStringArrayList(DURATION_TYPES);
            expiry_date = (Date)savedInstanceState.getSerializable(EXPIRY_DATE);

            setData(true);
        } else if (offer_id != null) {
            getOffer(offer_id, new GetCallback<Offer>() {
                @Override
                public void done(Offer offer, ParseException e) {
                    if (e == null) {
                        try {
                            is_published = offer.getIsPublished();
                            title = offer.getTitle();
                            description = offer.getDescription();
                            category = offer.getCategory();
                            job_type = offer.getJobType();
                            duration_length = offer.getDuration().getString(Offer.DURATION_PERIOD_LENGTH);
                            duration_type = offer.getDuration().getString(Offer.DURATION_PERIOD_TYPE);
                            skills = (ArrayList<String>) offer.getRequirements();
                            expiry_date = offer.getExpireDate();

                            setData(true);
                        } catch (JSONException e1) {
                            e1.printStackTrace();
                            finish();
                        }
                    } else {
                        e.printStackTrace();
                        finish();
                    }
                }
            });
        } else {
            setData(false);
        }

        mExpiryDate.setFocusable(false);
        mExpiryDate.setFocusableInTouchMode(true);
        mExpiryDate.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                if (b)
                    showDialog(0);
            }
        });
        mExpiryDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showDialog(0);
            }
        });

        mAddSkillButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                if(mSkill.getText() != null && !TextUtils.isEmpty(mSkill.getText())){
                    String tagString = Notice.generateValidTag(mSkill.getText().toString());
                    if(skills == null)
                        skills = new ArrayList<String>();

                    if (!skills.contains(tagString) && !tagString.isEmpty()) {
                        skills.add(tagString);
                        mSkill.setText("");
                        addTagToFlowLayout(tagString, getLayoutInflater(), mSkillTags, false);
                    }
                }

            }
        });

    }

    private void setData(boolean restore) {
        Log.i(TAG, "setData");
        try {
            if (categories == null) {
                categories = new ArrayList<String>();
                ParseQuery<Category> query = ParseQuery.getQuery(Category.class);
                List<Category> list = query.find();
                for (Category c : list) {
                    categories.addAll(c.getSubcategories());
                }
            }
            if (job_types == null) {
                job_types = new ArrayList<String>(ParseApplicationGlobalsQuery.getInstance().getJobTypesAndAvailability().getFirst().getJobTypeAvailability());
            }
            if (duration_types == null) {
                duration_types = new ArrayList<String>(ParseApplicationGlobalsQuery.getInstance().getOffersPeriodTypes().getFirst().getOfferPeriodTypes());
            }

            mCategories.setAdapter(new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, categories));
            mJobTypes.setAdapter(new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, job_types));
            mDurationTypes.setAdapter(new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, duration_types));

            mSaveButton.setOnClickListener(this);


            if (restore) {
                Log.i(TAG, "setData, restoring values");
                mPublishSwitch.setChecked(is_published);
                mTitle.setText(title);
                mDescription.setText(description);
                mCategories.setSelection(categories.indexOf(category) + 1);
                mJobTypes.setSelection(job_types.indexOf(job_type) + 1);
                mDurationLength.setText(duration_length);
                mDurationTypes.setSelection(duration_types.indexOf(duration_type) + 1);
                for (String s : skills) {
                    addTagToFlowLayout(s, getLayoutInflater(), mSkillTags, true);
                }
                mExpiryDate.setText(format.format(expiry_date.getTime()));

            }

            mProgressBar.setVisibility(View.GONE);
            mOfferForm.setVisibility(View.VISIBLE);
        } catch (ParseException e) {
            e.printStackTrace();
            finish();
        }
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        switch (id) {
            case R.id.save_button:
                saveOffer();
        }
    }


    protected void updateDisplay() {

        Calendar c = Calendar.getInstance();
        c.set(Calendar.DAY_OF_MONTH, mDay);
        c.set(Calendar.MONTH, mMonth);
        c.set(Calendar.YEAR, mYear);
        mExpiryDate.setText(format.format(c.getTime()));
    }

    protected Dialog onCreateDialog(int id) {
        return new DatePickerDialog(this,
                mDateSetListener,
                mYear, mMonth, mDay);
    }

    /**
     * Creation of the listener for the date picker dialog, which assign values of mYear, mMonth and mDay attributes
     */
    protected DatePickerDialog.OnDateSetListener mDateSetListener =
            new DatePickerDialog.OnDateSetListener() {
                public void onDateSet(DatePicker view, int year,
                                      int monthOfYear, int dayOfMonth) {
                    mYear = year;
                    mMonth = monthOfYear;
                    mDay = dayOfMonth;
                    updateDisplay();
                }
            };


    private void saveOffer() {

        boolean cancel = false;
        View focusView = null;

        Boolean publish = mPublishSwitch.isChecked();
        String title = mTitle.getText().toString();
        String description = mDescription.getText().toString();
        String category = (String) mCategories.getSelectedItem();
        String jobType = (String) mJobTypes.getSelectedItem();
        String durationLength = mDurationLength.getText().toString();
        String durationType = (String) mDurationTypes.getSelectedItem();
        String expiryDate = mExpiryDate.getText().toString();

        if (TextUtils.isEmpty(expiryDate)) {
            mExpiryDate.setError(getString(R.string.error_field_required));
            focusView = mExpiryDate;
            cancel = true;
        }

        if (TextUtils.isEmpty(title)) {
            mTitle.setError(getString(R.string.error_field_required));
            cancel = true;
            focusView = mTitle;
        }

        if (TextUtils.isEmpty(description)) {
            mDescription.setError(getString(R.string.error_field_required));
            cancel = true;
            focusView = mDescription;
        }

        if (category.equals(getString(R.string.prompt_category))) {
            mCategories.setError(getString(R.string.error_field_required));
            cancel = true;
            focusView = mCategories;
        }

        if (jobType.equals(getString(R.string.prompt_job_type))) {
            mJobTypes.setError(getString(R.string.error_field_required));
            cancel = true;
            focusView = mJobTypes;
        }

        if (TextUtils.isEmpty(durationType) || jobType.equals(getString(R.string.prompt_duration_type))) {
            mDurationTypes.setError(getString(R.string.error_field_required));
            cancel = true;
            focusView = mDurationTypes;
        }


        if (TextUtils.isEmpty(durationLength)) {
            mDurationLength.setError(getString(R.string.error_field_required));
            cancel = true;
            focusView = mDurationLength;
        } else {
            try {
                int d = Integer.parseInt(durationLength);
            } catch (NumberFormatException e) {
                mDurationLength.setError(getString(R.string.error_field_wrong_format));
                cancel = true;
                focusView = mDurationLength;
            }
        }


        if (cancel) {
            // There was an error; don't attempt save and focus the first
            // form field with an error.
            focusView.requestFocus();
        } else {
            try {
                mProgressBar.setVisibility(View.VISIBLE);
                final Offer o;

                final Company company = ((AppUser) AppUser.getCurrentUser()).getCompanyProfile();
                company.fetchIfNeeded();

                if (offer_id != null) {
                    ParseQuery<Offer> query = ParseQuery.getQuery(Offer.class);
                    o = query.get(offer_id);
                } else {
                    o = new Offer();
                }

                try {
                    expiry_date = android.text.format.DateFormat.getDateFormat(this).parse(mExpiryDate.getText().toString());
                } catch (java.text.ParseException e) {
                    Log.e(TAG, "Expiry date invalid format.", e);
                }

                o.setIsPublished(publish);
                o.setTitle(title);
                o.setDescription(description);
                o.setCategory(category);
                o.setJobType(jobType);
                JSONObject d = new JSONObject();
                d.put(Offer.DURATION_PERIOD_TYPE, durationType);
                d.put(Offer.DURATION_PERIOD_LENGTH, durationLength);
                o.setDuration(d);
                if (skills == null) skills = new ArrayList<String>();
                o.setRequirements(skills);
                if (publish) {
                    o.setPublishedAt(Calendar.getInstance().getTime());
                }
                o.setExpireDate(expiry_date);
                o.setCompany(company);
                if (company.getLocationName() != null) {
                    o.setLocationName(company.getLocationName());
                }

                o.setKeywords(extractKeywords(o));

                o.saveInBackground(new SaveCallback() {
                    @Override
                    public void done(ParseException e) {
                        if (e == null) {
                            company.addOffer(o);
                            company.saveInBackground(new SaveCallback() {
                                @Override
                                public void done(ParseException e) {
                                    if (e == null) {
                                        Intent returnIntent = new Intent();
                                        setResult(Activity.RESULT_OK, returnIntent);
                                        finish();
                                    } else {
                                        Snackbar.make(mSaveButton, getString(R.string.error_try_again), Snackbar.LENGTH_SHORT).show();
                                        mProgressBar.setVisibility(View.GONE);
                                        e.printStackTrace();
                                    }
                                }
                            });

                        } else {
                            Snackbar.make(mSaveButton, getString(R.string.error_try_again), Snackbar.LENGTH_SHORT).show();
                            mProgressBar.setVisibility(View.GONE);
                            e.printStackTrace();
                        }
                    }
                });

            } catch (JSONException | ParseException e) {
                e.printStackTrace();
                finish();
            }
        }
    }

    List<String> extractKeywords(Offer o){
        List<String> temp =  new ArrayList<String>();
        List<String> keywords =  new ArrayList<String>();
        temp.addAll(Arrays.asList(o.getTitle().split(" ")));
        temp.addAll(Arrays.asList(o.getCategory().split(" ")));
        temp.addAll(Arrays.asList(o.getDescription().replace("\n", " ").split(" ")));
        temp.addAll(Arrays.asList(o.getJobType().split(" ")));
        temp.addAll(o.getRequirements());
        temp.addAll(Arrays.asList(o.getCompany().getName().split(" ")));
        if(o.getLocationName() != null){
            temp.addAll(Arrays.asList(o.getLocationName().split(" ")));
        }

        for(String k : temp){
            keywords.add(k.toLowerCase());
        }

        return keywords;
    }

    public void getOffer(String objectId, GetCallback<Offer> callback) {
        ParseQuery<Offer> query = ParseQuery.getQuery(Offer.class);
        query.getInBackground(objectId, callback);
    }

    public void getCategories(FindCallback<Category> callback) {
        ParseQuery<Category> query = ParseQuery.getQuery(Category.class);
        query.findInBackground(callback);
    }

    private void addTagToFlowLayout(String tag, LayoutInflater inflater, ViewGroup container, final boolean silent) {
        View t = inflater.inflate(R.layout.tag_layout_2, container, false);
        FlowLayout.LayoutParams params = new FlowLayout.LayoutParams(FlowLayout.LayoutParams.WRAP_CONTENT, FlowLayout.LayoutParams.WRAP_CONTENT);
        params.setMargins(2, 2, 2, 2);
        t.setLayoutParams(params);

        TextView tagText = (TextView) t.findViewById(R.id.tag);
        tagText.setText(tag);
        t.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                ViewGroup parentView = (ViewGroup) v.getParent();
                TextView t = (TextView) v.findViewById(R.id.tag);
                int index = skills.indexOf(t.getText().toString());

                if (index > -1 && index < skills.size()) {
                    skills.remove(index);
                    parentView.removeView(v);
                    if (!silent)
                        Snackbar.make(mAddSkillButton, getString(R.string.requirement_removed), Snackbar.LENGTH_SHORT).show();

                }
                if (skills.size() == 0)
                    mNoTags.setVisibility(View.VISIBLE);
                return true;
            }
        });
        mSkillTags.addView(t);
        if (!silent)
            Snackbar.make(mAddSkillButton, getString(R.string.tag_added), Snackbar.LENGTH_SHORT).show();
        if (mNoTags.getVisibility() == View.VISIBLE)
            mNoTags.setVisibility(View.GONE);
        Log.i(TAG,"Added tag "+tag+" to flowLayout");
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_home, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        outState.putString(OFFER_ID, offer_id);
        outState.putBoolean(IS_PUBLISHED, mPublishSwitch.isChecked());
        outState.putString(TITLE, mTitle.getText().toString());
        outState.putString(DESCRIPTION, mDescription.getText().toString());
        outState.putString(CATEGORY, (String) mCategories.getSelectedItem());
        outState.putString(JOB_TYPE, (String) mJobTypes.getSelectedItem());
        outState.putString(DURATION_LENGTH, mDurationLength.getText().toString());
        outState.putString(DURATION_TYPE, (String) mDurationTypes.getSelectedItem());
        outState.putStringArrayList(CATEGORIES, categories);
        outState.putStringArrayList(JOB_TYPES, job_types);
        outState.putStringArrayList(SKILLS, skills);
        outState.putStringArrayList(DURATION_TYPES, duration_types);
        outState.putSerializable(EXPIRY_DATE, expiry_date);
    }
}