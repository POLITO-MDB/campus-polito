package com.example.mad.campuspolito.teacher;

import android.content.Context;
import android.support.design.widget.FloatingActionButton;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.mad.campuspolito.R;
import com.example.mad.campuspolito.model.Course;
import com.parse.ParseException;

import java.util.List;

/**
 * Created by federico on 01/09/15.
 */
public class CourseAdapter extends BaseAdapter {

    private List<Course> courses = null;
    private Context context = null;
    private View.OnClickListener listener = null;

    public CourseAdapter(Context context, List<Course> courses, View.OnClickListener listener)
    {
        this.courses = courses;
        this.context = context;
        this.listener = listener;
    }

    @Override
    public int getCount()
    {
        return courses.size();
    }

    @Override
    public Object getItem(int position)
    {
        return courses.get(position);
    }

    @Override
    public long getItemId(int position)
    {
        return getItem(position).hashCode();
    }

    @Override
    public View getView(int position, View v, ViewGroup vg)
    {
        if (v == null)
        {
            v = LayoutInflater.from(context).inflate(R.layout.course_list_item, null);
        }

        Course c = (Course)getItem(position);
        TextView mTitle = (TextView)v.findViewById(R.id.title);
        TextView mDescription = (TextView)v.findViewById(R.id.description);
        mTitle.setText(c.getCode() + " - " + c.getTitle());
        try {
            mDescription.setText(v.getResources().getString(R.string.semester) + " " + c.getSemester()
                    + " - " + c.getCfu() + " " + v.getResources().getString(R.string.cfu)
                    + " - " + c.getDegree().fetchIfNeeded().getString("title"));
        } catch (ParseException e) {
            e.printStackTrace();
        }


        return v;
    }

}
