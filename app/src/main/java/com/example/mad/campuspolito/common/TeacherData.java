package com.example.mad.campuspolito.common;

import android.text.TextUtils;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;

/**
 * Created by mdb on 06/09/15.
 */
public class TeacherData implements Serializable{

    private static final long serialVersionUID = 7526471155622775147L;

    private String name;
    private String idNumber;
    private String role;
    private String departmentId;
    private String email;
    private String phone;
    private String userId;

    public TeacherData(String name, String idNumber, String role, String departmentId,
                       String email, String phone, String userId){
        setName(name);
        setIdNumber(idNumber);
        setRole(role);
        setDepartmentId(departmentId);
        setEmail(email);
        setPhone(phone);
        setUserId(userId);
    }

    public String getName(){
        return name;
    }
    public String getIdNumber(){
        return idNumber;
    }
    public String getRole(){
        return role;
    }
    public String getDepartmentId(){
        return departmentId;
    }
    public String getEmail(){
        return email;
    }
    public String getPhone(){
        return phone;
    }
    public String getUserId(){
        return userId;
    }

    public void setName(String name){
        if(name == null || TextUtils.isEmpty(name))
            this.name = "";
        else
            this.name = name;
    }

    public void setIdNumber(String idNumber){
        this.idNumber = idNumber;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public void setDepartmentId(String departmentId) {
        this.departmentId = departmentId;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public boolean isLinkedToAppUser(){
        return (this.userId != null && !TextUtils.isEmpty(userId));
    }

    /**
     * Always treat de-serialization as a full-blown constructor, by
     * validating the final state of the de-serialized object.
     */
    private void readObject(
            ObjectInputStream aInputStream
    ) throws ClassNotFoundException, IOException {
        //always perform the default de-serialization first
        aInputStream.defaultReadObject();
    }

    /**
     * This is the default implementation of writeObject.
     * Customise if necessary.
     */
    private void writeObject(
            ObjectOutputStream aOutputStream
    ) throws IOException {
        //perform the default serialization for all non-transient, non-static fields
        aOutputStream.defaultWriteObject();
    }
}
