package com.example.mad.campuspolito.model;

import com.parse.ParseClassName;
import com.parse.ParseObject;

import java.util.Arrays;
import java.util.List;

/**
 * Created by mdb on 27/07/15.
 */
@ParseClassName("AppGlobals")
public class AppGlobals extends ParseObject{

    /**
     * Company global elements
     */
    public static final String COMPANY_SIZE = "company_size"; // e.g. Small


    /**
     * Notice global elements
     */
    public static final String NOTICE_CATEGORIES = "notice_categories"; //e.g. Used books
    /**
     * Offer global elements
     */
    public static final String OFFER_PERIOD_TYPES = "offer_period_types"; //e.g. Hours
    /**
     * Student global elements
     */
    public static final String STUDENT_LANGUAGES = "student_languages"; //e.g. English
    public static final String STUDENT_EDUCATION_TITLES = "student_education_titles"; //e.g. Technical High school
    public static final String STUDENT_EXPERIENCE_TITLES = "student_experience_titles"; //e.g. Volunteering
    /**
     * The following global elements are shared between Offers and Students
     */
    public static final String REQUIREMENTS_SKILLS = "requirements_skills"; //e.g. MySQL, PHP
    public static final String JOB_TYPE_AVAILABILITY = "job_type_availability"; //e.g. Part-time

    public AppGlobals(){

    }

    public void setCompanySize(List<String> companySize){
        put(COMPANY_SIZE, companySize);
    }
    public List<String> getCompanySize(){
        return getList(COMPANY_SIZE);
    }
    public void addCompanySize(String companySize){
        addUnique(COMPANY_SIZE, companySize);
    }
    public void removeCompanySize(String companySize){
        removeAll(COMPANY_SIZE, Arrays.asList(companySize));
    }


    public void setNoticeCategories(List<String> noticeCategories){
        put(NOTICE_CATEGORIES, noticeCategories);
    }
    public List<String> getNoticeCategories(){
        return getList(NOTICE_CATEGORIES);
    }
    public void addNoticeCategory(String noticeCategory){
        addUnique(NOTICE_CATEGORIES, noticeCategory);
    }
    public void removeNoticeCategory(String noticeCategory){
        removeAll(NOTICE_CATEGORIES, Arrays.asList(noticeCategory));
    }

    public void setOfferPeriodTypes(List<String> offerPeriodTypes){
        put(OFFER_PERIOD_TYPES, offerPeriodTypes);
    }
    public List<String> getOfferPeriodTypes(){
        return getList(OFFER_PERIOD_TYPES);
    }
    public void addOfferPeriodType(String offerPeriodType){
        addUnique(OFFER_PERIOD_TYPES, offerPeriodType);
    }
    public void removeOfferPeriodType(String offerPeriodType){
        removeAll(OFFER_PERIOD_TYPES, Arrays.asList(offerPeriodType));
    }

    public void setRequirementSkills(List<String> requirementSkills){
        put(REQUIREMENTS_SKILLS, requirementSkills);
    }
    public List<String> getRequirementSkills(){
        return getList(REQUIREMENTS_SKILLS);
    }
    public void addRequirementSkill(String requirementSkill){
        addUnique(REQUIREMENTS_SKILLS, requirementSkill);
    }
    public void removeRequirementSkill(String requirementSkill){
        removeAll(REQUIREMENTS_SKILLS, Arrays.asList(requirementSkill));
    }

    public void setJobTypeAvailability(List<String> jobTypeAvailability){
        put(JOB_TYPE_AVAILABILITY, jobTypeAvailability);
    }
    public List<String> getJobTypeAvailability(){
        return getList(JOB_TYPE_AVAILABILITY);
    }
    public void addJobTypeAvailability(String jobTypeAvailability){
        addUnique(JOB_TYPE_AVAILABILITY, jobTypeAvailability);
    }
    public void removeJobTypeAvailability(String jobTypeAvailability){
        removeAll(JOB_TYPE_AVAILABILITY, Arrays.asList(jobTypeAvailability));
    }

    public void setStudentLanguages(List<String> studentLanguages){
        put(STUDENT_LANGUAGES, studentLanguages);
    }
    public List<String> getStudentLanguages(){
        return getList(STUDENT_LANGUAGES);
    }
    public void addStudentLanguage(String studentLanguage){
        addUnique(STUDENT_LANGUAGES, studentLanguage);
    }
    public void removeStudentLanguage(String studentLanguage){
        removeAll(STUDENT_LANGUAGES, Arrays.asList(studentLanguage));
    }

    public void setStudentEducationTitles(List<String> studentEducationTitles){
        put(STUDENT_EDUCATION_TITLES, studentEducationTitles);
    }
    public List<String> getStudentEducationTitles(){
        return getList(STUDENT_EDUCATION_TITLES);
    }
    public void addStudentEducationTitle(String studentEducationTitle){
        addUnique(STUDENT_EDUCATION_TITLES, studentEducationTitle);
    }
    public void removeStudentEducationTitle(String studentEducationTitle){
        removeAll(STUDENT_EDUCATION_TITLES, Arrays.asList(studentEducationTitle));
    }

    public void setStudentExperienceTitles(List<String> studentExperienceTitles){
        put(STUDENT_EXPERIENCE_TITLES, studentExperienceTitles);
    }
    public List<String> getStudentExperienceTitles(){
        return getList(STUDENT_EXPERIENCE_TITLES);
    }
    public void addStudentExperienceTitle(String studentExperienceTitle){
        addUnique(STUDENT_EXPERIENCE_TITLES, studentExperienceTitle);
    }
    public void removeStudentExperienceTitle(String studentExperienceTitle){
        removeAll(STUDENT_EXPERIENCE_TITLES, Arrays.asList(studentExperienceTitle));
    }
}
