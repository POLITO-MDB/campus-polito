package com.example.mad.campuspolito.student;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.mad.campuspolito.CampusPolitoApplication;
import com.example.mad.campuspolito.R;
import com.example.mad.campuspolito.common.MainActivity;
import com.example.mad.campuspolito.model.AppUser;
import com.example.mad.campuspolito.model.Notice;
import com.example.mad.campuspolito.utils.ParseApplicationGlobalsQuery;
import com.parse.ParseException;
import com.parse.ParseQuery;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import fr.ganfra.materialspinner.MaterialSpinner;

public class NoticesListActivity extends AppCompatActivity implements View.OnClickListener {

    private static final String TAG = NoticesListActivity.class.getName();
    public static final int UPDATE_LIST = 5;
    private static final int CREATE_NOTICE = 4;
    Toolbar mToolbar;
    private static final String SHOW_FAVOURITES = "SHOW_FAVOURITES";
    private static final String NOTICES_LIST_TAG = "NOTICES_LIST_TAG";
    private boolean showFavourites;
    MenuItem showFavouritesButton;
    MenuItem showAllNoticesButton;
    FloatingActionButton mAddNoticeButton;
    TabLayout mTabLayout;

    private ArrayList<String> noticesCategories;


    Map<TabLayout.Tab,String> tabMap;
    private static final String NOTICES_CATEGORIES = "NOTICES_CATEGORIES";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notices_list);

        mToolbar = (Toolbar)findViewById(R.id.toolbar);
        mAddNoticeButton = (FloatingActionButton)findViewById(R.id.create_notice_button);
        mAddNoticeButton.setOnClickListener(this);


        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        mTabLayout = (TabLayout) findViewById(R.id.tabLayout);


        if(savedInstanceState != null){
            showFavourites = savedInstanceState.getBoolean(SHOW_FAVOURITES);
            noticesCategories = savedInstanceState.getStringArrayList(NOTICES_CATEGORIES);
        }
        else{
            showFavourites = false;
            noticesCategories = null;
        }

        LoadBasicLayoutDataTask task = new LoadBasicLayoutDataTask(this);
        task.execute();
    }


    public class LoadBasicLayoutDataTask extends AsyncTask<Void,Void,Boolean> {

        private final Context ctx;

        public LoadBasicLayoutDataTask(Context ctx){
            this.ctx = ctx;
        }

        @Override
        protected void onPreExecute() {
            findViewById(R.id.list_progress).setVisibility(View.VISIBLE);
        }

        @Override
        protected Boolean doInBackground(Void... params) {
            try{
                if(noticesCategories != null)
                    return true;
                noticesCategories = new ArrayList<String>(ParseApplicationGlobalsQuery.getInstance().getNoticeCategories().getFirst().getNoticeCategories());
                return true;
            }
            catch (ParseException e){
                return false;
            }
        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {

            final String allResultsTabName = getString(R.string.all_results);
            TabLayout.Tab allResultsTab = mTabLayout.newTab().setText(allResultsTabName);

            tabMap = new HashMap<>();
            tabMap.put(allResultsTab,allResultsTabName);


            for(String category : noticesCategories){
                tabMap.put(mTabLayout.newTab().setText(category),category);
            }

            mTabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
                @Override
                public void onTabSelected(TabLayout.Tab tab) {
                    String category = tabMap.get(tab);

                    if(category.equals(allResultsTabName)){
                        if(showFavourites){
                            if(!isFinishing())
                                getSupportFragmentManager().beginTransaction().replace(R.id.contentFragment, NoticesFragment.newInstance(NoticesFragment.STUDENT_FAVOURITES, null, true), NOTICES_LIST_TAG).commit();
                        }
                        else {
                            if(!isFinishing())
                                getSupportFragmentManager().beginTransaction().replace(R.id.contentFragment, NoticesFragment.newInstance(NoticesFragment.STUDENT_SEARCH, null, true), NOTICES_LIST_TAG).commit();
                        }
                    }
                    else {
                        if(showFavourites){
                            if(!isFinishing())
                                getSupportFragmentManager().beginTransaction().replace(R.id.contentFragment, NoticesFragment.newInstance(NoticesFragment.STUDENT_FAVOURITES, category, false), NOTICES_LIST_TAG).commit();
                        }
                        else {
                            if(!isFinishing())
                                getSupportFragmentManager().beginTransaction().replace(R.id.contentFragment, NoticesFragment.newInstance(NoticesFragment.STUDENT_SEARCH, category, false), NOTICES_LIST_TAG).commit();
                        }
                    }
                }

                @Override
                public void onTabUnselected(TabLayout.Tab tab) {

                }

                @Override
                public void onTabReselected(TabLayout.Tab tab) {

                }
            });



            mTabLayout.addTab(allResultsTab);
            for(TabLayout.Tab tab : tabMap.keySet()){
                if(!tabMap.get(tab).equals(allResultsTabName))
                    mTabLayout.addTab(tab);
            }

            allResultsTab.select();

            findViewById(R.id.list_progress).setVisibility(View.GONE);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_notices_list, menu);
        getMenuInflater().inflate(R.menu.menu_home, menu);

        showFavouritesButton = menu.findItem(R.id.show_fav_notices);
        showAllNoticesButton = menu.findItem(R.id.show_all_notices);

        if(showFavourites){
            showFavouritesButton.setVisible(false);
            showAllNoticesButton.setVisible(true);
        }
        else{
            showAllNoticesButton.setVisible(false);
            showFavouritesButton.setVisible(true);
        }
        return true;
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putBoolean(SHOW_FAVOURITES, showFavourites);
        outState.putStringArrayList(NOTICES_CATEGORIES,noticesCategories);
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {

        showFavouritesButton = menu.findItem(R.id.show_fav_notices);
        showAllNoticesButton = menu.findItem(R.id.show_all_notices);

        if(showFavourites){
            showAllNoticesButton.setVisible(true);
            showFavouritesButton.setVisible(false);
        }
        else{
            showAllNoticesButton.setVisible(false);
            showFavouritesButton.setVisible(true);
        }

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        if (id == R.id.action_logout) {
            CampusPolitoApplication.unsubscribeFromAllChannels();
            AppUser.logOut();
            Intent homeIntent = new Intent(this, MainActivity.class);
            homeIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(homeIntent);
            finish();
            return true;
        }

        if(id == R.id.show_fav_notices){
            getSupportFragmentManager().beginTransaction().replace(R.id.contentFragment, NoticesFragment.newInstance(NoticesFragment.STUDENT_FAVOURITES, null, true)).commit();
            showFavourites = true;
            invalidateOptionsMenu();
        }

        if(id == R.id.show_all_notices){
            getSupportFragmentManager().beginTransaction().replace(R.id.contentFragment, NoticesFragment.newInstance(NoticesFragment.STUDENT_SEARCH, null, true)).commit();
            showFavourites = false;
            invalidateOptionsMenu();
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View v) {
        if(v.getId() == R.id.create_notice_button){
            Intent createNotice = new Intent(this,CreateNoticeActivity.class);
            startActivityForResult(createNotice, CREATE_NOTICE);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(requestCode == CREATE_NOTICE){
            Fragment frag = getSupportFragmentManager().findFragmentByTag(NOTICES_LIST_TAG);

            if(frag != null && frag instanceof NoticesFragment)
                ((NoticesFragment)frag).updateList();
        }
    }

    public void onShowNoticeRequested(String objectId) {
        if(objectId != null){
            ShowNoticeData task = new ShowNoticeData(this, objectId);
            task.execute();
        }
    }

    public class ShowNoticeData extends AsyncTask<Void,Void,Boolean>{

        private final Context ctx;
        private final String objectId;

        private Notice notice;
        private boolean isOwnedByCurrentUser;

        public ShowNoticeData(Context ctx, String objectId){
            this.ctx = ctx;
            this.objectId = objectId;
        }

        @Override
        protected void onPreExecute() {
            findViewById(R.id.list_progress).setVisibility(View.VISIBLE);
            findViewById(R.id.notices_list).setVisibility(View.GONE);
        }

        @Override
        protected Boolean doInBackground(Void... params) {
            try{

                ParseQuery<Notice> query = ParseQuery.getQuery(Notice.class);

                query.include(Notice.ADVERTISER);
                notice = query.get(objectId);

                if(notice == null)
                    return false;

                String currentStudentId = ((AppUser)AppUser.getCurrentUser()).getStudentProfile().getObjectId();

                notice.fetchIfNeeded();

                if(notice.getAdvertiser().getObjectId().equals(currentStudentId))
                    isOwnedByCurrentUser = true;
                else isOwnedByCurrentUser = false;


                return true;
            }
            catch (ParseException e){

                return false;
            }
        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {


            if(aBoolean){
                Intent showNotice = new Intent(ctx, ShowNoticeDetailsActivity.class);
                showNotice.putExtra(ShowNoticeDetailsActivity.NOTICE_ID, objectId);
                showNotice.putExtra(ShowNoticeDetailsActivity.IS_OWNED_BY_CURRENT_USER,isOwnedByCurrentUser);

                startActivityForResult(showNotice,CREATE_NOTICE);

            }

            findViewById(R.id.notices_list).setVisibility(View.VISIBLE);
            findViewById(R.id.list_progress).setVisibility(View.GONE);

        }
    }
}
