package com.example.mad.campuspolito.student;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.mad.campuspolito.CampusPolitoApplication;
import com.example.mad.campuspolito.R;
import com.example.mad.campuspolito.common.FullscreenImageActivity;
import com.example.mad.campuspolito.common.MainActivity;
import com.example.mad.campuspolito.common.ProfileActivity;
import com.example.mad.campuspolito.model.AppUser;
import com.example.mad.campuspolito.model.Conversation;
import com.example.mad.campuspolito.model.Degree;
import com.example.mad.campuspolito.model.Notice;
import com.example.mad.campuspolito.model.Student;
import com.example.mad.campuspolito.utils.AspectRatioImageView;
import com.example.mad.campuspolito.utils.CircularImageView;
import com.example.mad.campuspolito.utils.DynamicLinearLayoutViewInflater;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseQuery;
import com.wefika.flowlayout.FlowLayout;

import java.util.ArrayList;
import java.util.List;

public class ShowNoticeDetailsActivity extends AppCompatActivity implements View.OnClickListener {

    private static final String TAG = ShowNoticeDetailsActivity.class.getName();
    public static final String NOTICE_ID = "NOTICE_ID";
    public static final String IS_OWNED_BY_CURRENT_USER = "IS_OWN_CURR_USR";
    private static final String IS_FAVOURITE_NOTICE = "IS_FAVOURITE_NOTICE";
    private static final String TITLE = "TITLE";
    private static final String CATEGORY = "CATEGORY";
    private static final String COST = "COST";
    private static final String SEARCH_TAGS = "SEARCH_TAGS";
    private static final String DESCRIPTION = "DESCRIPTION";
    private static final String PICTURES_URL = "PICTURES_URL";
    private static final String ADV_NAME = "ADV_NAME";
    private static final String ADV_TITLE = "ADV_TITLE";
    private static final String ADV_LOCATION = "ADV_LOCATION";
    private static final String MAIN_PICTURE = "MAIN_PICTURE";
    private static final String ADV_PICTURE = "ADV_PICTURE";
    private static final String ADV_ID = "ADV_ID";
    private static final String ADV_CONVERSATION = "ADV_CONVERSATION";
    private static final String FLAGGED = "FLAGGED";
    private static final int EDIT_NOTICE = 5;
    public static final int NOTICE_UPDATED = 8;
    public static final int NOTICE_DELETED = 9;


    private AspectRatioImageView mMainPicture;
    private CollapsingToolbarLayout mCollapsingToolbarLayout;
    private Toolbar mToolbar;
    private LinearLayout mProgress;
    private NestedScrollView mContent;
    private TextView mTitle;
    private TextView mCategory;
    private TextView mCost;
    private FlowLayout mSearchTags;
    private TextView mDescription;
    private LinearLayout mPictures;
    private CircularImageView mAdvertiserIcon;
    private TextView mAdvertiserName;
    private TextView mAdvertiserTitle;
    private TextView mAdvertiserLocation;
    private TextView mPicturesInfo;
    private FloatingActionButton mEditNoticeButton;
    private LinearLayout mAdvertiserBox;
    private TextView mFlagBanner;

    private boolean isOwnedByCurrentUser;
    private String noticeId;
    private String title;
    private String category;
    private String cost;
    private ArrayList<String> searchTags;
    private String description;
    private ArrayList<String> pictureUrlList;
    private String advertiserName;
    private String advertiserTitle;
    private String advertiserLocation;
    private String advertiserId;
    private String advertiserConversationId;
    private boolean flagged;

    private ParseFile mainPicture;
    private ParseFile advertiserPicture;

    private byte[] mainPictureBytes;
    private byte[] advertiserPictureBytes;

    private FetchNoticeData task;
    private boolean doOnlineFetch;
    private boolean isFavouriteNotice;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_notice_details);

        doOnlineFetch = true;
        isFavouriteNotice = false;
        flagged = false;

        if(getIntent() != null){
            noticeId = getIntent().getStringExtra(NOTICE_ID);
            isOwnedByCurrentUser = getIntent().getBooleanExtra(IS_OWNED_BY_CURRENT_USER,false);
        }
        if(savedInstanceState != null){
            noticeId = savedInstanceState.getString(NOTICE_ID);
            isOwnedByCurrentUser = savedInstanceState.getBoolean(IS_OWNED_BY_CURRENT_USER);
            title = savedInstanceState.getString(TITLE);
            category = savedInstanceState.getString(CATEGORY);
            cost = savedInstanceState.getString(COST);
            searchTags = savedInstanceState.getStringArrayList(SEARCH_TAGS);
            description = savedInstanceState.getString(DESCRIPTION);
            pictureUrlList = savedInstanceState.getStringArrayList(PICTURES_URL);
            advertiserName = savedInstanceState.getString(ADV_NAME);
            advertiserTitle = savedInstanceState.getString(ADV_TITLE);
            advertiserLocation = savedInstanceState.getString(ADV_LOCATION);
            mainPictureBytes = savedInstanceState.getByteArray(MAIN_PICTURE);
            advertiserPictureBytes = savedInstanceState.getByteArray(ADV_PICTURE);
            advertiserId = savedInstanceState.getString(ADV_ID);
            advertiserConversationId = savedInstanceState.getString(ADV_CONVERSATION);
            isFavouriteNotice = savedInstanceState.getBoolean(IS_FAVOURITE_NOTICE);
            flagged = savedInstanceState.getBoolean(FLAGGED);

            doOnlineFetch = false;
        }


        mMainPicture = (AspectRatioImageView)findViewById(R.id.notice_picture);
        mCollapsingToolbarLayout = (CollapsingToolbarLayout)findViewById(R.id.collapsingToolbarLayout);
        mToolbar = (Toolbar)findViewById(R.id.toolbar);
        mProgress = (LinearLayout)findViewById(R.id.notice_progress);
        mContent = (NestedScrollView)findViewById(R.id.notice_content);
        mTitle = (TextView)findViewById(R.id.title);
        mCategory = (TextView)findViewById(R.id.category);
        mCost = (TextView)findViewById(R.id.cost);
        mSearchTags = (FlowLayout)findViewById(R.id.search_tags);
        mDescription = (TextView)findViewById(R.id.description);
        mPictures = (LinearLayout)findViewById(R.id.photosGridView);
        mAdvertiserIcon = (CircularImageView)findViewById(R.id.student_icon);
        mAdvertiserName = (TextView)findViewById(R.id.student_name);
        mAdvertiserTitle = (TextView)findViewById(R.id.student_degree_title);
        mAdvertiserLocation = (TextView)findViewById(R.id.student_location);
        mPicturesInfo = (TextView)findViewById(R.id.pictures_info);
        mEditNoticeButton = (FloatingActionButton)findViewById(R.id.edit_notice_button);
        mAdvertiserBox = (LinearLayout)findViewById(R.id.advertiser_box);
        mFlagBanner = (TextView)findViewById(R.id.flag_banner);

        mEditNoticeButton.setOnClickListener(this);
        mAdvertiserBox.setOnClickListener(this);

        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);



        task = new FetchNoticeData(this,noticeId,doOnlineFetch);
        task.execute();

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        if(!isOwnedByCurrentUser)
            getMenuInflater().inflate(R.menu.menu_show_notices,menu);

        getMenuInflater().inflate(R.menu.menu_home, menu);
        return true;
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {

        if(!isOwnedByCurrentUser){
            MenuItem favItem = menu.findItem(R.id.action_fav_notice);

            if(favItem != null){
                if(isFavouriteNotice){
                    favItem.setIcon(R.drawable.ic_star_outline);
                    favItem.setTitle(R.string.action_unfav_notice);
                }
                else {
                    favItem.setIcon(R.drawable.ic_star_inv);
                    favItem.setTitle(R.string.action_fav_notice);
                }
            }
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        if (id == R.id.action_logout) {
            CampusPolitoApplication.unsubscribeFromAllChannels();
            AppUser.logOut();
            Intent homeIntent = new Intent(this, MainActivity.class);
            homeIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(homeIntent);
            finish();
            return true;
        }

        if(!isOwnedByCurrentUser){
            if(id == R.id.action_contact_adv){
                Intent showConversation = new Intent(this, ConversationListActivity.class);
                showConversation.putExtra(ConversationListActivity.INTENT_DISPLAY_CONVERSATION, advertiserConversationId);
                startActivity(showConversation);
            }
            if(id == R.id.action_fav_notice){
                ManageFavouriteNotice task = new ManageFavouriteNotice(this, noticeId,!isFavouriteNotice,item);
                task.execute();
            }

            if(id == R.id.action_flag_notice){
                final Activity thisActivity = this;
                AlertDialog.Builder builder = new AlertDialog.Builder(this);

                builder.setTitle(getString(R.string.flag_notice_title));
                builder.setMessage(getString(R.string.flag_notice_message));

                builder.setPositiveButton(getString(R.string.dialog_confirm), new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int which) {

                        FlagInadequateNotice task = new FlagInadequateNotice(thisActivity, noticeId);
                        task.execute();
                        dialog.dismiss();
                    }

                });

                builder.setNegativeButton(getString(R.string.dialog_cancel), new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        // Do nothing
                        dialog.dismiss();
                    }
                });

                AlertDialog alert = builder.create();
                alert.show();
            }
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(requestCode == EDIT_NOTICE){

            if(resultCode == NOTICE_DELETED){

                setResult(NoticesListActivity.UPDATE_LIST);
                finish();
            }
            else {
                doOnlineFetch = true;
                if(task != null)
                    task.cancel(true);
                task = new FetchNoticeData(this,noticeId,doOnlineFetch);
                task.execute();
            }
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString(NOTICE_ID, noticeId);
        outState.putBoolean(IS_OWNED_BY_CURRENT_USER, isOwnedByCurrentUser);
        outState.putString(TITLE, title);
        outState.putString(CATEGORY, category);
        outState.putString(COST, cost);
        outState.putStringArrayList(SEARCH_TAGS, searchTags);
        outState.putString(DESCRIPTION, description);
        outState.putStringArrayList(PICTURES_URL, pictureUrlList);
        outState.putString(ADV_NAME, advertiserName);
        outState.putString(ADV_TITLE, advertiserTitle);
        outState.putString(ADV_LOCATION, advertiserLocation);
        outState.putByteArray(MAIN_PICTURE, mainPictureBytes);
        outState.putByteArray(ADV_PICTURE,advertiserPictureBytes);
        outState.putBoolean(IS_FAVOURITE_NOTICE, isFavouriteNotice);
        outState.putBoolean(FLAGGED,flagged);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.edit_notice_button:

                Intent modifyNotice = new Intent(this, CreateNoticeActivity.class);
                modifyNotice.putExtra(CreateNoticeActivity.NOTICE_ID, noticeId);
                startActivityForResult(modifyNotice, EDIT_NOTICE);
                break;

            case R.id.advertiser_box:

                Intent showAdvertiserProfile = new Intent(this, ProfileActivity.class);
                showAdvertiserProfile.putExtra(ProfileActivity.APP_USER_OBJECT_ID, advertiserId);
                showAdvertiserProfile.putExtra(ProfileActivity.APP_USER_TYPE, AppUser.PROFILE_STUDENT);

                startActivity(showAdvertiserProfile);
        }
    }

    public class FlagInadequateNotice extends AsyncTask<Void,Void,Boolean>{
        private final Context ctx;
        private final String noticeId;

        public FlagInadequateNotice(Context ctx, String noticeId) {
            this.ctx = ctx;
            this.noticeId = noticeId;
        }

        @Override
        protected void onPreExecute() {
            mContent.setVisibility(View.GONE);
            mProgress.setVisibility(View.VISIBLE);
        }

        @Override
        protected Boolean doInBackground(Void... params) {
            try{


                ParseQuery<Notice> query1 = ParseQuery.getQuery(Notice.class);
                Notice notice = query1.get(noticeId);

                if(notice == null)
                    return false;
                notice.fetchIfNeeded();

                Log.d(TAG, "Notice data fetched");

                notice.setFlagged(true);
                notice.save();

                return true;

            }
            catch (ParseException e){
                Log.e(TAG, "Error while managing flag notice task.");
                return false;
            }
        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {


            mContent.setVisibility(View.VISIBLE);
            mProgress.setVisibility(View.GONE);

            if(aBoolean){

                if(mFlagBanner.getVisibility()!=View.VISIBLE)
                    mFlagBanner.setVisibility(View.VISIBLE);

                Snackbar.make(mContent,getString(R.string.flag_notice_success),Snackbar.LENGTH_SHORT).show();
                flagged = true;
            }
        }
    }

    public class ManageFavouriteNotice extends AsyncTask<Void,Void,Boolean>{


        private final boolean addToFav;
        private final Context ctx;
        private final String noticeId;
        private final MenuItem item;

        public ManageFavouriteNotice(Context ctx, String noticeId, boolean addToFav, MenuItem item) {
            this.ctx = ctx;
            this.noticeId = noticeId;
            this.addToFav = addToFav;
            this.item = item;
        }

        @Override
        protected void onPreExecute() {
            mContent.setVisibility(View.GONE);
            mProgress.setVisibility(View.VISIBLE);
        }

        @Override
        protected Boolean doInBackground(Void... params) {
            try{

                Student student = ((AppUser)AppUser.getCurrentUser()).getStudentProfile();

                if(student == null)
                    return false;
                student.fetchIfNeeded();

                ParseQuery<Notice> query1 = ParseQuery.getQuery(Notice.class);
                Notice notice = query1.get(noticeId);

                if(notice == null)
                    return false;
                notice.fetchIfNeeded();

                Log.d(TAG, "Student and notice data fetched (addToFav="+addToFav);
                if(addToFav)
                    student.addFavouriteNotice(notice);
                else
                    student.removeFavouriteNotice(notice);

                student.save();

                return true;

            }
            catch (ParseException e){
                Log.e(TAG, "Error while managing favourite notice (addToFav="+addToFav);
                return false;
            }
        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {


            mContent.setVisibility(View.VISIBLE);
            mProgress.setVisibility(View.GONE);

            if(addToFav){
                Snackbar.make(mContent,getString(R.string.notice_add_fav),Snackbar.LENGTH_SHORT)
                        .setAction(R.string.undo, new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {

                                ManageFavouriteNotice undo = new ManageFavouriteNotice(ctx, noticeId, false,item);
                                undo.execute();
                            }
                        }).show();

                item.setIcon(R.drawable.ic_star_outline);
                item.setTitle(R.string.action_unfav_notice);
            }
            else {
                Snackbar.make(mContent,getString(R.string.notice_remove_fav),Snackbar.LENGTH_SHORT)
                        .setAction(R.string.undo, new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                ManageFavouriteNotice undo = new ManageFavouriteNotice(ctx, noticeId, true,item);
                                undo.execute();

                            }
                        }).show();
                item.setIcon(R.drawable.ic_star_inv);
                item.setTitle(R.string.action_fav_notice);
            }

            isFavouriteNotice = !isFavouriteNotice;
        }
    }

    public class FetchNoticeData extends AsyncTask<Void,Void,Boolean>{

        private final Context ctx;
        private final String noticeId;
        private final boolean doOnlineFetch;
        private Notice notice;
        private Student advertiser;
        private Degree advertiserDegree;
        private AppUser advertiserUser;

        public FetchNoticeData(Context ctx, String noticeId, boolean doOnlineFetch){
            this.ctx = ctx;
            this.noticeId = noticeId;
            this.doOnlineFetch = doOnlineFetch;
        }

        @Override
        protected void onPreExecute() {
            mContent.setVisibility(View.GONE);
            mProgress.setVisibility(View.VISIBLE);
        }

        @Override
        protected Boolean doInBackground(Void... params) {

            if(doOnlineFetch){
                try{

                    if(noticeId == null)
                        return false;

                    ParseQuery<Notice> query = ParseQuery.getQuery(Notice.class);
                    query.include(Notice.ADVERTISER);
                    notice = query.get(noticeId);
                    notice.fetchIfNeeded();

                    advertiser = notice.getAdvertiser();
                    advertiser.fetchIfNeeded();
                    advertiserUser = advertiser.getUser();
                    advertiserUser.fetchIfNeeded();
                    advertiserDegree = advertiser.getDegree();
                    advertiserDegree.fetchIfNeeded();


                    title = notice.getTitle();
                    category = notice.getCategory();
                    cost = notice.getCost().toString();
                    searchTags = new ArrayList<String>(notice.getTags());
                    description = notice.getDescription();
                    pictureUrlList = new ArrayList<>();

                    if(notice.getPhotos() != null)
                        for(ParseFile picture : notice.getPhotos()){
                            pictureUrlList.add(picture.getUrl());
                        }

                    advertiserName = advertiserUser.toString();
                    advertiserTitle = advertiserDegree.getTitle();
                    advertiserLocation = advertiser.getLocationName();


                    if(notice.getPhotos() != null && notice.getPhotos().size() > 0){
                        mainPicture = notice.getPhotos().get(0);
                        mainPictureBytes = mainPicture.getData();
                    }
                    else{
                        mainPicture = null;
                        mainPictureBytes = null;
                    }

                    if(advertiserUser.getPicture() != null){
                        advertiserPicture = advertiserUser.getPicture();
                        advertiserPictureBytes = advertiserPicture.getData();
                    }
                    else{
                        advertiserPicture = null;
                        advertiserPictureBytes = null;
                    }

                    advertiserId = advertiserUser.getObjectId();

                    if(!isOwnedByCurrentUser){
                        Log.d(TAG, "Fetching conversation data.");
                        ParseQuery<Conversation> queryConversation = ParseQuery.getQuery(Conversation.class);
                        List<AppUser> conversationUsers = new ArrayList<AppUser>();
                        conversationUsers.add((AppUser)AppUser.getCurrentUser());
                        conversationUsers.add(advertiserUser);
                        queryConversation.whereContainsAll(Conversation.USERS, conversationUsers);
                        queryConversation.whereDoesNotExist(Conversation.OPT_GRP_NAME);

                        List<Conversation> conversationList = queryConversation.find();
                        Conversation conversation;

                        if(conversationList == null || conversationList.size() == 0){
                            Log.d(TAG, "Conversation is null, creating a new one.");
                            conversation = new Conversation();
                            conversation.setUsers(conversationUsers);

                            conversation.save();

                            advertiserConversationId = conversation.getObjectId();
                        }
                        else{
                            Log.d(TAG, "Conversation found.");
                            conversation = conversationList.get(0);
                            advertiserConversationId = conversation.getObjectId();
                        }


                        Student currentStudent = ((AppUser)AppUser.getCurrentUser()).getStudentProfile();
                        if(currentStudent == null){
                            Log.e(TAG, "Current user is not a student.!");
                            return false;
                        }
                        currentStudent.fetchIfNeeded();
                        if(currentStudent.getFavouriteNotices() == null)
                            isFavouriteNotice = false;
                        else if(currentStudent.getFavouriteNotices().contains(notice))
                            isFavouriteNotice = true;
                        else
                            isFavouriteNotice = false;

                    }

                    flagged = notice.getFlagged();


                    return true;
                }
                catch (ParseException e){
                    Log.e(TAG, "Error while fetching notice data.", e);
                    return false;
                }
            }
            else {

                if(mainPictureBytes != null)
                    mainPicture = new ParseFile(mainPictureBytes);
                else
                    mainPicture = null;

                if(advertiserPictureBytes != null)
                    advertiserPicture = new ParseFile(advertiserPictureBytes);
                else
                    advertiserPicture = null;

                return true;
            }

        }


        @Override
        protected void onPostExecute(Boolean aBoolean) {

            if(isCancelled())
                return;

            if(aBoolean){
                mCollapsingToolbarLayout.setTitle(title);

                if(mainPicture != null){
                    mMainPicture.setScaleType(ImageView.ScaleType.MATRIX);
                    mMainPicture.setParseFile(mainPicture);
                    mMainPicture.loadInBackground();
                }

                mCategory.setText(category);
                mTitle.setText(title);
                mCost.setText(cost);

                mSearchTags.removeAllViews();

                if(searchTags != null)
                    for(String tag : searchTags)
                        addTagToFlowLayout(tag, mSearchTags);

                mDescription.setText(description);


                DynamicLinearLayoutViewInflater.getInstance().addBitmapViewsToLayout(pictureUrlList, ctx, mPictures, new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        Intent fullscreenIntent = new Intent(ctx, FullscreenImageActivity.class);
                        fullscreenIntent.putExtra(FullscreenImageActivity.INTENT_IMAGE_URL, (String)v.getTag(R.id.photosGridView));
                        ctx.startActivity(fullscreenIntent);
                    }
                }, R.id.photosGridView);


                if(pictureUrlList == null || pictureUrlList.size() == 0)
                    mPicturesInfo.setText(getString(R.string.no_notices_pictures_info));

                if(advertiserPicture != null){
                    mAdvertiserIcon.setParseFile(advertiserPicture);
                    mAdvertiserIcon.setScaleType(ImageView.ScaleType.CENTER_CROP);
                    mAdvertiserIcon.loadInBackground();

                }
                else {
                    mAdvertiserIcon.setPlaceholder(ctx.getResources().getDrawable(R.mipmap.ic_missing_icon_person));
                    mAdvertiserIcon.setParseFile(null);
                }

                mAdvertiserName.setText(advertiserName);
                mAdvertiserTitle.setText(advertiserTitle);
                if(advertiserLocation == null || TextUtils.isEmpty(advertiserLocation))
                   mAdvertiserLocation.setVisibility(View.GONE);
                else
                    mAdvertiserLocation.setText(advertiserLocation);

            }

            if(isOwnedByCurrentUser)
                mEditNoticeButton.setVisibility(View.VISIBLE);

            mContent.setVisibility(View.VISIBLE);
            mProgress.setVisibility(View.GONE);

            invalidateOptionsMenu();

            if(flagged)
                mFlagBanner.setVisibility(View.VISIBLE);
        }


        private void addTagToFlowLayout(String tag, FlowLayout tagsContainer) {
            View t = ((LayoutInflater)ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.tag_layout_inactive,tagsContainer, false);
            FlowLayout.LayoutParams params = new FlowLayout.LayoutParams(FlowLayout.LayoutParams.WRAP_CONTENT, FlowLayout.LayoutParams.WRAP_CONTENT);
            params.setMargins(2, 2, 2, 2);
            t.setLayoutParams(params);
            TextView tagText = (TextView) t.findViewById(R.id.tag);
            tagText.setText(tag);
            tagsContainer.addView(t);
        }

        @Override
        protected void onCancelled() {
            mContent.setVisibility(View.VISIBLE);
            mProgress.setVisibility(View.GONE);
        }
    }
}
