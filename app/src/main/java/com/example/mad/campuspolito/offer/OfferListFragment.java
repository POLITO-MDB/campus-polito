package com.example.mad.campuspolito.offer;

import android.app.Activity;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ListFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;

import com.example.mad.campuspolito.R;
import com.example.mad.campuspolito.model.AppUser;
import com.example.mad.campuspolito.model.Offer;
import com.example.mad.campuspolito.model.Student;
import com.parse.ParseException;
import com.parse.ParseQuery;
import com.parse.ParseQueryAdapter;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by lg on 11/08/15.
 */
public class OfferListFragment extends ListFragment implements ParseQueryAdapter.OnQueryLoadListener<Offer>{

    private static final String TAG = OfferListFragment.class.getName();
    public static final String QUERY_TYPE = "query_type";
    public static final String QUERY_STRING = "query_string";
    private static final String QUERY_SORT = "query_sort";
    private static final int DATE_ASC = 90;
    private static final int DATE_DESC = 91;
    public static final int STUDENT_ALL = 5;
    public static final int STUDENT_FAVOURITES = 1;
    public static final int COMPANY_ALL = 2;
    public static final int SEARCH_OFFERS = 3;

    private ProgressBar mProgressBar;
    private LinearLayout mContent;
    private ListView mListView;
    private LinearLayout mEmptyView;
    private List<Offer> offerList;
    private OffersAdapter adapter;
    private AppUser user;
    private int query_type;
    private int query_sort;
    private String query_string;

    //The serialization (saved instance state) Bundle key representing the
    //activated item position. Only used on tablets.
    private static final String STATE_ACTIVATED_POSITION = "activated_position";
    //The current activated item position. Only used on tablets.
    private int mActivatedPosition = ListView.INVALID_POSITION;
    private Callbacks mCallbacks = sDummyCallbacks;

    private static OfferListFragment instance = null;


    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public OfferListFragment() {
    }

    public static OfferListFragment newInstance(int query_type, String query_string, int query_sort) {
        Log.i(TAG, "newInstance, query type = " + query_type + " query_string = " + query_string);

        OfferListFragment fragment = null;
        Bundle args = new Bundle();
        if(instance != null){
            Log.i(TAG, "instance != null");
            fragment = instance;
            args.putInt(QUERY_TYPE, instance.query_type);
            args.putString(QUERY_STRING, instance.query_string);
            args.putInt(QUERY_SORT, instance.query_sort);

        }
        else {
            fragment = new OfferListFragment();
            args.putInt(QUERY_TYPE, query_type);
            args.putString(QUERY_STRING, query_string);
            args.putInt(QUERY_SORT, query_sort);
        }


        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        user = (AppUser)AppUser.getCurrentUser();

        if (getArguments() != null) {
            Log.i(TAG, "getArguments != null");
            query_type = getArguments().getInt(QUERY_TYPE);
            query_string = getArguments().getString(QUERY_STRING);
            query_sort = getArguments().getInt(QUERY_SORT);
        }
        else{
            query_type = savedInstanceState.getInt(QUERY_TYPE);
            query_string = savedInstanceState.getString(QUERY_STRING);
            query_sort = savedInstanceState.getInt(QUERY_SORT);
        }
        Log.i(TAG, "onCreate, query type = " + query_type + " query_string = " + query_string + " query_sort = " + query_sort);

        doQuery();

        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.offers_list, container, false);

        if(savedInstanceState != null){
            query_type = savedInstanceState.getInt(QUERY_TYPE);
            query_string = savedInstanceState.getString(QUERY_STRING);
            query_sort = savedInstanceState.getInt(QUERY_SORT);
        }

        mProgressBar = (ProgressBar)v.findViewById(R.id.progress_bar);
        mContent = (LinearLayout)v.findViewById(R.id.content);
        mListView = (ListView)v.findViewById(android.R.id.list);
        mEmptyView = (LinearLayout)v.findViewById(R.id.empty_list);

        return v;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        // Restore the previously serialized activated item position.
        if (savedInstanceState != null
                && savedInstanceState.containsKey(STATE_ACTIVATED_POSITION)) {
            setActivatedPosition(savedInstanceState.getInt(STATE_ACTIVATED_POSITION));
        }

        doQuery();

        if(((OfferListActivity)getActivity()).mTwoPane) {
            setActivateOnItemClick(true);
        }

        Log.d(TAG, "List Fragment created and adapter set.");
    }

    public void onUpdateContent() {
        if(adapter != null)
            adapter.loadObjects();
    }


    private void doQuery() {

        Log.i(TAG, "doQuery, query type = " + query_type);

        switch(query_type) {

            case STUDENT_ALL:
                Log.i(TAG, "student all offers");
                adapter = new OffersAdapter(getActivity(), user, new ParseQueryAdapter.QueryFactory<Offer>() {
                    @Override
                    public ParseQuery<Offer> create() {
                        ParseQuery<Offer> query = ParseQuery.getQuery(Offer.class);
                        if(query_string != null && !query_string.isEmpty()){
                            List<String> keywords = Arrays.asList(query_string.split(" "));
                            query.whereContainedIn(Offer.KEYWORDS, keywords);
                        }
                        query.whereEqualTo(Offer.IS_PUBLISHED, true);
                        if(query_sort == DATE_ASC)
                            query.orderByAscending(Offer.PUBLISHED_AT);
                        else if(query_sort == DATE_DESC)
                            query.orderByDescending(Offer.PUBLISHED_AT);
                        return query;
                    }
                });
                break;

            case STUDENT_FAVOURITES:
                Log.i(TAG, "student favourite offers");
                try {
                    final Student s = user.getStudentProfile().fetchIfNeeded();
                    final List<String> offer_ids = new ArrayList<String>();
                    if(s.getFavouriteOffers() != null) {
                        for (Offer o : s.getFavouriteOffers()) {
                            offer_ids.add(o.getObjectId());
                        }
                    }

                    adapter = new OffersAdapter(getActivity(), user, new ParseQueryAdapter.QueryFactory<Offer>() {
                        @Override
                        public ParseQuery<Offer> create() {
                            ParseQuery<Offer> query = ParseQuery.getQuery(Offer.class);
                            query.whereEqualTo(Offer.IS_PUBLISHED, true);
                            query.whereContainedIn("objectId", offer_ids);
                            if(query_string != null && !query_string.isEmpty()){
                                List<String> keywords = Arrays.asList(query_string.split(" "));
                                query.whereContainedIn(Offer.KEYWORDS, keywords);
                            }
                            if(query_sort == DATE_ASC)
                                query.orderByAscending(Offer.PUBLISHED_AT);
                            else if(query_sort == DATE_DESC)
                                query.orderByDescending(Offer.PUBLISHED_AT);

                            return query;
                        }
                    });
                }
                catch (ParseException e){
                    e.printStackTrace();
                }
                break;

            case SEARCH_OFFERS:
                Log.i(TAG, "search offers");
                adapter = new OffersAdapter(getActivity(), user, new ParseQueryAdapter.QueryFactory<Offer>() {
                    @Override
                    public ParseQuery<Offer> create() {
                        ParseQuery<Offer> query = ParseQuery.getQuery(Offer.class);
                        List<String> keywords = Arrays.asList(query_string.split(" "));
                        query.whereContainedIn(Offer.KEYWORDS, keywords);
                        if(query_sort == DATE_ASC)
                            query.orderByAscending(Offer.PUBLISHED_AT);
                        else if(query_sort == DATE_DESC)
                            query.orderByDescending(Offer.PUBLISHED_AT);
                        return query;
                    }
                });
                break;


            default: //company
                Log.i(TAG, "company offers");
                adapter = new OffersAdapter(getActivity(), user, new ParseQueryAdapter.QueryFactory<Offer>() {
                    @Override
                    public ParseQuery<Offer> create() {
                        ParseQuery<Offer> query = ParseQuery.getQuery(Offer.class);
                        query.whereEqualTo(Offer.COMPANY, user.getCompanyProfile());
                        query.orderByAscending(Offer.EXPIRE_DATE);
                        return query;
                    }
                });
                break;
        }

        if(adapter != null) {
            adapter.setTextKey(Offer.TITLE);
            adapter.addOnQueryLoadListener(this);
            setListAdapter(adapter);
        }

    }



    @Override
    public void onListItemClick(ListView listView, View view, int position, long id) {
        super.onListItemClick(listView, view, position, id);

        // Notify the active callbacks interface (the activity, if the
        // fragment is attached to one) that an item has been selected.
        mCallbacks.onOfferSelected(offerList.get(position).getObjectId());
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        Log.i(TAG, "onSaveInstanceState");

        if (mActivatedPosition != ListView.INVALID_POSITION) {
            // Serialize and persist the activated item position.
            outState.putInt(STATE_ACTIVATED_POSITION, mActivatedPosition);
        }
        outState.putInt(QUERY_TYPE, query_type);
        outState.putString(QUERY_STRING, query_string);
        outState.putInt(QUERY_SORT, query_sort);

        super.onSaveInstanceState(outState);
    }

    /**
     * Turns on activate-on-click mode. When this mode is on, list items will be
     * given the 'activated' state when touched.
     */
    public void setActivateOnItemClick(boolean activateOnItemClick) {
        // When setting CHOICE_MODE_SINGLE, ListView will automatically
        // give items the 'activated' state when touched.
        getListView().setChoiceMode(activateOnItemClick
                ? ListView.CHOICE_MODE_SINGLE
                : ListView.CHOICE_MODE_NONE);
    }

    private void setActivatedPosition(int position) {
        if (position == ListView.INVALID_POSITION) {
            getListView().setItemChecked(mActivatedPosition, false);
        } else {
            getListView().setItemChecked(position, true);
        }

        mActivatedPosition = position;
    }

    @Override
    public void onLoading() {
        Log.d(TAG, "Loading offers...");
        mProgressBar.setVisibility(View.VISIBLE);
        mContent.setVisibility(View.GONE);
    }

    @Override
    public void onLoaded(List<Offer> list, Exception e) {

        if(e == null) {
            offerList = list;
            if(list != null && !list.isEmpty()) {
                Log.d(TAG, "Offers loaded...(number " + list.size() + " )");
                mListView.setVisibility(View.VISIBLE);
                mEmptyView.setVisibility(View.GONE);

            }
            else {
                Log.d(TAG, "Offers loaded...empty list");
                mListView.setVisibility(View.GONE);
                mEmptyView.setVisibility(View.VISIBLE);
            }
        }
        else{
            Log.e(TAG, "An error occurred while loading offers list from Parse service..." , e);
            Snackbar.make(mContent, R.string.parse_generic_error, Snackbar.LENGTH_LONG).show();
        }

        mProgressBar.setVisibility(View.GONE);
        mContent.setVisibility(View.VISIBLE);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        Log.i(TAG, "onActivityCreated");
        super.onActivityCreated(savedInstanceState);
        if(savedInstanceState != null){
            query_type = savedInstanceState.getInt(QUERY_TYPE);
            query_string = savedInstanceState.getString(QUERY_STRING);
            query_sort = savedInstanceState.getInt(QUERY_SORT);
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        //setRetainInstance(true);

        // Activities containing this fragment must implement its callbacks.
        if (!(activity instanceof Callbacks)) {
            throw new IllegalStateException("Activity must implement fragment's callbacks.");
        }

        mCallbacks = (Callbacks) activity;
    }

    @Override
    public void onDetach() {
        super.onDetach();

        // Reset the active callbacks interface to the dummy implementation.
        mCallbacks = sDummyCallbacks;
    }

    /**
     * A callback interface that all activities containing this fragment must
     * implement. This mechanism allows activities to be notified of item
     * selections.
     */
    public interface Callbacks {
        /**
         * Callback for when an item has been selected.
         */
        void onOfferSelected(String id);
    }

    /**
     * A dummy implementation of the {@link Callbacks} interface that does
     * nothing. Used only when this fragment is not attached to an activity.
     */
    private static Callbacks sDummyCallbacks = new Callbacks() {
        @Override
        public void onOfferSelected(String id) {
        }
    };
}
