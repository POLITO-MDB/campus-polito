package com.example.mad.campuspolito.common;

import android.content.Context;
import android.provider.ContactsContract;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.mad.campuspolito.R;
import com.example.mad.campuspolito.model.Course;
import com.example.mad.campuspolito.model.Lecture;
import com.example.mad.campuspolito.model.Location;
import com.parse.ParseException;
import com.parse.ParseQueryAdapter;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

/**
 * @author kurado
 */
public class LecturesAdapter extends ParseQueryAdapter<Lecture> {

    private Context context;

    public LecturesAdapter(Context context, com.parse.ParseQueryAdapter.QueryFactory<Lecture> queryFactory) {
        super(context, queryFactory);
        this.context = context;
    }

    @Override
    public View getItemView(Lecture object,View v,ViewGroup parent){
        DateFormat dateFormat = new SimpleDateFormat("dd MMMM, HH:mm",Locale.getDefault());
        DateFormat timeFormat = new SimpleDateFormat("HH:mm", Locale.getDefault());

        if(v==null){
            v = View.inflate(getContext(), R.layout.lecture_list_item,null);
        }

        super.getItemView(object,v,parent);

        TextView lecture_date = (TextView) v.findViewById(R.id.lecture_date);
        TextView course_name = (TextView) v.findViewById(R.id.course_name);
        TextView lecture_room = (TextView) v.findViewById(R.id.lecture_room);

        Date startDate = object.getStartCalendar().getTime();
        Date endDate = object.getEndCalendar().getTime();
        String Day_name = object.getStartCalendar().getDisplayName(Calendar.DAY_OF_WEEK, Calendar.LONG, Locale.getDefault());
        String DateAndHour =Day_name+" "+dateFormat.format(startDate) +
             " - " + timeFormat.format(endDate);
        lecture_date.setText(DateAndHour);

        Location l = object.getRoom();
        try {
            l.fetchIfNeeded();
            lecture_room.setText(object.getRoom().getLocation());
        } catch (ParseException e) {
            e.printStackTrace();
        }

        Course course = object.getCourse();
        try {
            course.fetchIfNeeded();
            course_name.setText(object.getCourse().getTitle());
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return v;
    }
}
