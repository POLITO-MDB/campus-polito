package com.example.mad.campuspolito.utils;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import com.example.mad.campuspolito.common.LectureDetailActivity;
import com.example.mad.campuspolito.common.ShowCourseDetailsActivity;
import com.example.mad.campuspolito.model.AppUser;
import com.example.mad.campuspolito.model.Lecture;
import com.example.mad.campuspolito.model.Message;
import com.example.mad.campuspolito.student.ConversationDetailActivity;
import com.example.mad.campuspolito.student.ConversationDetailFragment;
import com.example.mad.campuspolito.student.ConversationListActivity;
import com.parse.ParseAnalytics;
import com.parse.ParsePushBroadcastReceiver;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

/**
 * Created by mdb on 24/07/15.
 */
public class MessagePushBroadcastReceiver extends ParsePushBroadcastReceiver{

    private static final String TAG = MessagePushBroadcastReceiver.class.getName();

    private static HashMap<String, Boolean> channelsNotified = new HashMap<String,Boolean>();

    @Override
    protected void onPushReceive(Context context, Intent intent) {
        try {
            JSONObject json = new JSONObject(intent.getExtras().getString("com.parse.Data"));

            if(json.has(Message.CONVERSATION)){
                String conversationId = json.getString(Message.CONVERSATION);

                if(ConversationDetailFragment.FOREGROUND && ConversationDetailFragment.CONV_ID.equals(conversationId)){

                    Intent i = new Intent(ConversationDetailFragment.PUSH_MESSAGE_RECEIVED);
                    context.sendBroadcast(i);

                }
                else {
                    if (conversationId != null) {
                        Boolean isNotified = channelsNotified.get(conversationId);

                        if(isNotified == null || !isNotified){
                            channelsNotified.put(conversationId,true);
                            super.onPushReceive(context,intent);
                        }
                    } else {
                        super.onPushReceive(context, intent);

                    }
                }
            }


            if(json.has(Lecture.COURSE)){
                AppUser currentUser = ((AppUser)AppUser.getCurrentUser());
                if(!currentUser.isTeacher())
                    super.onPushReceive(context,intent);

            }


        } catch (JSONException e) {
            Log.e(TAG, "Error while retrieving JSON object from push notification.", e);
        }
    }

    @Override
    protected void onPushOpen(Context context, Intent intent) {
        ParseAnalytics.trackAppOpenedInBackground(intent);


        try {
            JSONObject json = new JSONObject(intent.getExtras().getString("com.parse.Data"));

            if(json.has(Message.CONVERSATION)){
                channelsNotified.put(json.getString(Message.CONVERSATION),false);
                Intent i = new Intent(context, ConversationListActivity.class);
                Log.d(TAG, "Push notification data: " + json.toString());
                i.putExtra(ConversationListActivity.INTENT_DISPLAY_CONVERSATION, json.getString(Message.CONVERSATION));
                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(i);
            }
            else if(json.has(Lecture.COURSE)){
                String courseId = json.getString(Lecture.COURSE);
                //String lectureId = json.getString(Lecture.LECTURE);
                //String teacher = json.getString(Lecture.TEACHER);
                Log.d(TAG, "Opening push notification for course " + courseId );
                //" lecture " + lectureId + " and from teacher " + teacher);

                if(json.has(Lecture.LECTURE)){
                    String lectureId = json.getString(Lecture.LECTURE);
                    if(lectureId != null){
                        Intent showLectureDetails = new Intent(context, LectureDetailActivity.class);
                        showLectureDetails.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        showLectureDetails.putExtra(LectureDetailActivity.LECTURE_ID, lectureId);
                        context.startActivity(showLectureDetails);
                    }
                }
                else {
                    if(courseId != null){
                        Intent showCourseDetails = new Intent(context, ShowCourseDetailsActivity.class);
                        showCourseDetails.putExtra(ShowCourseDetailsActivity.COURSE_ID, courseId);
                        showCourseDetails.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        context.startActivity(showCourseDetails);
                    }
                }
            }
            else{
                Intent i = new Intent(context, ConversationListActivity.class);
                Log.d(TAG, "Push notification data: "+ json.toString());
                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(i);
            }

        } catch (JSONException e) {
            Log.e(TAG, "Error while retrieving JSON object from push notification.",e);
        }

    }
}
