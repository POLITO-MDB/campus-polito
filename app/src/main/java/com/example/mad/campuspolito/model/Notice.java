package com.example.mad.campuspolito.model;

import com.parse.ParseClassName;
import com.parse.ParseFile;
import com.parse.ParseObject;

import java.util.Arrays;
import java.util.List;

/**
 * Created by mdb on 20/07/15.
 */
@ParseClassName("Notice")
public class Notice extends ParseObject {

    public static final String TITLE = "title";
    public static final String DESCRIPTION = "description";
    public static final String COST = "cost";
    public static final String CATEGORY = "category";
    public static final String TAGS = "tags";
    public static final String PHOTOS = "photos";
    public static final String ADVERTISER = "advertiser";
    public static final String FLAGGED = "flagged";
    public static final String TAGSTRING = "tagstring";
    public static final String TAGSTRING_SEPARATOR = "_";
    public static final String TAG_SEPARATOR = "-";

    public Notice(){

    }


    public void setTitle(String title){
        put(TITLE, title);
    }
    public String getTitle(){
        return getString(TITLE);
    }

    public void setDescription(String description){
        put(DESCRIPTION, description);
    }
    public String getDescription(){
        return getString(DESCRIPTION);
    }

    public void setCost(Number cost){
        put(COST, cost);
    }
    public Number getCost(){
        return getNumber(COST);
    }

    public void setCategory(String category){
        put(CATEGORY, category);
    }

    public String getCategory(){
        return getString(CATEGORY);
    }

    public void setTags(List<String> tags){
        put(TAGS, tags);
    }
    public List<String> getTags(){
        return getList(TAGS);
    }

    public void addTag(String tag) {
        addUnique(TAGS, tag);
    }
    public void removeTag(String tag){
        removeAll(TAGS, Arrays.asList(tag));
    }

    public void setPhotos(List<ParseFile> photos){
        put(PHOTOS, photos);
    }
    public List<ParseFile> getPhotos(){
        return getList(PHOTOS);
    }

    public void addPhoto(ParseFile photo) {
        addUnique(PHOTOS,photo);
    }
    public void removePhoto(ParseFile photo){
        removeAll(PHOTOS, Arrays.asList(photo));
    }

    public void setAdvertiser(Student advertiser){
        put(ADVERTISER, advertiser);
    }

    public Student getAdvertiser(){
        return (Student)getParseObject(ADVERTISER);
    }

    public void setFlagged(boolean flagged){
        put(FLAGGED, flagged);
    }
    public boolean getFlagged(){
        return getBoolean(FLAGGED);
    }


    public void setTagString(String tagString){
        put(TAGSTRING, tagString);
    }
    public String getTagString(){
        return getString(TAGSTRING);
    }

    /**
     * Utility method used to create a valid tag from the user input, to perform creation of notices
     * tags and search on tag
     * @param input
     * @return
     */
    public static String generateValidTag(String input) {

        return input.toLowerCase().trim().
                replaceAll(" +", " ").replace(" ", Notice.TAG_SEPARATOR);

    }

}
