package com.example.mad.campuspolito.student;

import android.content.Intent;
import android.support.v4.app.NavUtils;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import com.example.mad.campuspolito.CampusPolitoApplication;
import com.example.mad.campuspolito.R;
import com.example.mad.campuspolito.common.MainActivity;
import com.example.mad.campuspolito.model.AppUser;

public class GroupChatDetailsActivity extends AppCompatActivity {

    private static final String TAG = GroupChatDetailsActivity.class.getName();

    private String argItemId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_conversation_detail);


        // savedInstanceState is non-null when there is fragment state
        // saved from previous configurations of this activity
        // (e.g. when rotating the screen from portrait to landscape).
        // In this case, the fragment will automatically be re-added
        // to its container so we don't need to manually add it.
        // For more information, see the Fragments API guide at:
        //
        // http://developer.android.com/guide/components/fragments.html
        //
        if (savedInstanceState == null) {
            // Create the detail fragment and add it to the activity
            // using a fragment transaction.
            Bundle arguments = new Bundle();
            arguments.putString(GroupChatDetailsFragment.ARG_ITEM_ID,
                    getIntent().getStringExtra(GroupChatDetailsFragment.ARG_ITEM_ID));
            GroupChatDetailsFragment fragment = new GroupChatDetailsFragment();
            fragment.setArguments(arguments);
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.conversation_detail_container, fragment)
                    .commit();

            argItemId = getIntent().getStringExtra(GroupChatDetailsFragment.ARG_ITEM_ID);
        }
        else
            argItemId = savedInstanceState.getString(GroupChatDetailsFragment.ARG_ITEM_ID);
    }


    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString(GroupChatDetailsFragment.ARG_ITEM_ID,argItemId);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            // This ID represents the Home or Up button. In the case of this
            // activity, the Up button is shown. Use NavUtils to allow users
            // to navigate up one level in the application structure. For
            // more details, see the Navigation pattern on Android Design:
            //
            // http://developer.android.com/design/patterns/navigation.html#up-vs-back
            //
            Intent upIntent = new Intent(this,ConversationDetailActivity.class);
            upIntent.putExtra(GroupChatDetailsFragment.ARG_ITEM_ID,argItemId);
            NavUtils.navigateUpTo(this, upIntent);
            return true;
        }
        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        if (id == R.id.action_logout) {
            CampusPolitoApplication.unsubscribeFromAllChannels();
            AppUser.logOut();
            Intent homeIntent = new Intent(this, MainActivity.class);
            homeIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(homeIntent);
            finish();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_home,menu);

        return super.onCreateOptionsMenu(menu);
    }
}

