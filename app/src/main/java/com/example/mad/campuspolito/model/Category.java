package com.example.mad.campuspolito.model;

import com.parse.ParseClassName;
import com.parse.ParseObject;

import java.util.Arrays;
import java.util.List;


/**
 * Parse class used to define a generic category item, with a name and a list of subcategories (strings)
 */
@ParseClassName("Category")
public class Category extends ParseObject{

    public static final String NAME = "name";
    public static final String SUBCATEGORIES = "subcategories";

    public Category(){

    }

    public void setName(String name){
        put(NAME, name);
    }
    public String getName() {
        return getString(NAME);
    }

    public void setSubcategories(List<String> subcategories){
        put(SUBCATEGORIES, subcategories);
    }
    public List<String> getSubcategories(){
        return getList(SUBCATEGORIES);
    }
    public void addSubcategory(String subcategory){
        addUnique(SUBCATEGORIES,subcategory);
    }
    public void removeSubcategory(String subcategory){
        removeAll(SUBCATEGORIES, Arrays.asList(subcategory));
    }
}
