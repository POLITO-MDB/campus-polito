package com.example.mad.campuspolito.model;

import com.parse.ParseClassName;
import com.parse.ParseObject;

/**
 * Created by mdb on 20/07/15.
 */
@ParseClassName("Candidature")
public class Candidature extends ParseObject {

    public static final String OFFER = "offer";
    public static final String STUDENT = "student";
    public static final String STATUS = "status";
    public static final String STATUS_DISCARDED = "STATUS_DISCARDED";
    public static final String STATUS_NEW = "STATUS_NEW";

    /**
     * Default zero-argument constructor required by Parse API
     */
    public Candidature(){

    }


    public Offer getOffer(){
        return (Offer)getParseObject(OFFER);
    }
    public void setOffer(Offer offer){
        put(OFFER, offer);
    }


    public Student getStudent(){
        return (Student)getParseObject(STUDENT);
    }
    public void setStudent(Student student){
        put(STUDENT, student);
    }

    public String getStatus(){
        return getString(STATUS);
    }
    public void setStatus(String status){
        put(STATUS, status);
    }
}