package com.example.mad.campuspolito.student;

import android.content.Context;
import android.content.res.ColorStateList;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.TextView;

import com.example.mad.campuspolito.R;
import com.example.mad.campuspolito.model.AppUser;
import com.example.mad.campuspolito.model.Notice;
import com.example.mad.campuspolito.model.Student;
import com.parse.ParseImageView;
import com.parse.ParseObject;
import com.parse.ParseQueryAdapter;
import com.wefika.flowlayout.FlowLayout;

import java.text.DateFormat;
import java.util.List;
/**
 * Created by mdb on 18/08/15.
 */
public class NoticesAdapter extends ParseQueryAdapter<Notice>{


    private DateFormat format;
    private Context context;


    public NoticesAdapter(Context context, com.parse.ParseQueryAdapter.QueryFactory<Notice> queryFactory) {
        super(context, queryFactory);
        this.context = context;
        this.format = android.text.format.DateFormat.getDateFormat(context);
    }


    @Override
    public View getItemView(Notice object, View v, ViewGroup parent) {
        if (v == null) {
            v = View.inflate(getContext(), R.layout.notice_list_item, null);
        }

        // Take advantage of ParseQueryAdapter's getItemView logic for
        // populating the main TextView/ImageView.
        // The IDs in your custom layout must match what ParseQueryAdapter expects
        // if it will be populating a TextView or ImageView for you.
        super.getItemView(object, v, parent);

        // Do additional configuration before returning the View.

        ParseImageView imageView = (ParseImageView) v.findViewById(R.id.notice_icon);

        if(object.getPhotos() != null && object.getPhotos().size() > 0){
            imageView.setParseFile(object.getPhotos().get(0));
            imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
            imageView.loadInBackground();
        }
        else{
            imageView.setParseFile(null);
            imageView.setPlaceholder(context.getResources().getDrawable(R.drawable.ic_picture_inv));
        }

        if(object.getCost() != null && object.getCost().intValue() != 0){
            TextView costView = (TextView) v.findViewById(R.id.notice_cost);
            costView.setText(object.getCost()+" "+ context.getString(R.string.money));
        }
        else {
            TextView costView = (TextView) v.findViewById(R.id.notice_cost);
            costView.setText(context.getString(R.string.no_cost));
        }

        TextView categoryView = (TextView)v.findViewById(R.id.notice_category);
        categoryView.setText(object.getCategory());

        TextView descriptionView = (TextView)v.findViewById(R.id.notice_description);
        descriptionView.setText(object.getDescription());

        TextView dateView = (TextView) v.findViewById(R.id.notice_date);
        dateView.setText(format.format(object.getCreatedAt()));


        FlowLayout searchTags = (FlowLayout) v.findViewById(R.id.search_tags);

        searchTags.removeAllViews();

        List<String> tags = object.getTags();

        if(tags != null)
            for(String tag : tags)
                addTagToFlowLayout(tag, searchTags);


        return v;
    }

    private void addTagToFlowLayout(String tag, FlowLayout tagsContainer) {
        View t = ((LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.tag_layout_inactive,tagsContainer, false);
        FlowLayout.LayoutParams params = new FlowLayout.LayoutParams(FlowLayout.LayoutParams.WRAP_CONTENT, FlowLayout.LayoutParams.WRAP_CONTENT);
        params.setMargins(2, 2, 2, 2);
        t.setLayoutParams(params);
        TextView tagText = (TextView) t.findViewById(R.id.tag);
        tagText.setText(tag);
        tagsContainer.addView(t);
    }
}
