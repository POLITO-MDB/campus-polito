package com.example.mad.campuspolito.model;

import com.parse.ParseClassName;
import com.parse.ParseObject;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by mdb on 20/07/15.
 */
@ParseClassName("Message")
public class Message extends ParseObject {

    public static String CONVERSATION = "conversation";
    public static String DATA = "data";
    public static String SENDER = "sender";

    /**
     * Default zero-argument constructor required by Parse API
     */
    public Message(){

    }

    public String getData(){
        return getString(DATA);
    }
    public void setData(String data){
        put(DATA, data);
    }

    public Conversation getConversation(){
        return (Conversation)getParseObject(CONVERSATION);
    }
    public void setConversation(Conversation conversation){
        put(CONVERSATION, conversation);
    }

    public void setSender(AppUser sender){
        put(SENDER, sender);
    }
    public AppUser getSender(){
        return (AppUser)getParseUser(SENDER);
    }

    public JSONObject getJSONRepresentation(){

        JSONObject msgJSON = new JSONObject();

        try {
            msgJSON.put("title",getSender().toString());
            msgJSON.put("alert", getData());
            msgJSON.put(SENDER, getSender().toString());
            msgJSON.put(DATA, getData());
            msgJSON.put(CONVERSATION, getConversation().getObjectId());
        }
        catch (JSONException e){
            return null;
        }
        return msgJSON;
    }
}
