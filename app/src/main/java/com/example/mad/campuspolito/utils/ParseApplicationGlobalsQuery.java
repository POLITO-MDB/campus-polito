package com.example.mad.campuspolito.utils;

import com.example.mad.campuspolito.model.AppGlobals;
import com.example.mad.campuspolito.model.Category;
import com.parse.ParseQuery;

import java.util.Arrays;

/**
 * Singleton utility class for selection of global values used through the whole application
 *
 * Use ''getFirst()'' method to obtain the only instance of AppUsers class
 */
public class ParseApplicationGlobalsQuery {

    private static ParseApplicationGlobalsQuery instance = null;

    private ParseApplicationGlobalsQuery(){

    }

    public static ParseApplicationGlobalsQuery getInstance(){
        if(instance == null)
            instance = new ParseApplicationGlobalsQuery();

        return instance;
    }

    private ParseQuery<AppGlobals> getGlobal(String globalKey){
        ParseQuery<AppGlobals> query = ParseQuery.getQuery(AppGlobals.class);
        query.selectKeys(Arrays.asList(globalKey));

        return query;
    }

    public ParseQuery<AppGlobals> getCompanySize(){
        return getGlobal(AppGlobals.COMPANY_SIZE);
    }


    public ParseQuery<AppGlobals> getNoticeCategories(){
        return getGlobal(AppGlobals.NOTICE_CATEGORIES);
    }

    public ParseQuery<AppGlobals> getOffersPeriodTypes(){
        return getGlobal(AppGlobals.OFFER_PERIOD_TYPES);
    }

    public ParseQuery<AppGlobals> getStudentLanguages(){
        return getGlobal(AppGlobals.STUDENT_LANGUAGES);
    }

    public ParseQuery<AppGlobals> getStudentEducationTitles(){
        return getGlobal(AppGlobals.STUDENT_EDUCATION_TITLES);
    }

    public ParseQuery<AppGlobals> getStudentExperienceTitles(){
        return getGlobal(AppGlobals.STUDENT_EXPERIENCE_TITLES);
    }

    public ParseQuery<AppGlobals> getRequirementsAndSkills(){
        return getGlobal(AppGlobals.REQUIREMENTS_SKILLS);
    }

    public ParseQuery<AppGlobals> getJobTypesAndAvailability(){
        return getGlobal(AppGlobals.JOB_TYPE_AVAILABILITY);
    }

    public ParseQuery<Category> getCompanyAndOfferCategories(){
        ParseQuery<Category> query = ParseQuery.getQuery(Category.class);
        query.orderByAscending(Category.NAME);
        return query;
    }
}
