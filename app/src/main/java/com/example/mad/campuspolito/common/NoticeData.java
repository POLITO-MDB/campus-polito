package com.example.mad.campuspolito.common;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;

/**
 * Created by mdb on 06/09/15.
 */
public class NoticeData implements Serializable{


    private static final long serialVersionUID = 7626471255622775147L;

    private String teacher;
    private String message;
    private long time;

    public NoticeData(String teacher, String message, long time){
        setTeacher(teacher);
        setMessage(message);
        setTime(time);
    }

    public long getTime() {
        return time;
    }

    public String getMessage() {
        return message;
    }

    public String getTeacher() {
        return teacher;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public void setTeacher(String teacher) {
        this.teacher = teacher;
    }

    public void setTime(long time) {
        this.time = time;
    }

    /**
     * Always treat de-serialization as a full-blown constructor, by
     * validating the final state of the de-serialized object.
     */
    private void readObject(
            ObjectInputStream aInputStream
    ) throws ClassNotFoundException, IOException {
        //always perform the default de-serialization first
        aInputStream.defaultReadObject();
    }

    /**
     * This is the default implementation of writeObject.
     * Customise if necessary.
     */
    private void writeObject(
            ObjectOutputStream aOutputStream
    ) throws IOException {
        //perform the default serialization for all non-transient, non-static fields
        aOutputStream.defaultWriteObject();
    }
}
