package com.example.mad.campuspolito.common;

import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.example.mad.campuspolito.CampusPolitoApplication;
import com.example.mad.campuspolito.R;
import com.example.mad.campuspolito.model.AppUser;
import com.example.mad.campuspolito.utils.CircularImageView;
import com.parse.ParseException;
import com.parse.ParseInstallation;
import com.parse.ParsePush;

import java.util.List;

/**
 * Created by mdb on 05/08/15.
 */
public abstract class HomeActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, View.OnClickListener {

    public static final String TAG = HomeActivity.class.getName();
    public Toolbar toolbar;
    public ActionBarDrawerToggle drawerToggle;
    public DrawerLayout drawerLayout;
    public CoordinatorLayout rootLayout;
    public NavigationView navigationView;
    public AppUser currentUser;
    public TextView profileName;
    public CircularImageView profilePic;
    public FrameLayout profileView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        currentUser = (AppUser)AppUser.getCurrentUser();

        try {
            currentUser.fetchIfNeeded();
            onSetHomeContentView();
        } catch (ParseException e) {
            Log.e(TAG, "Error while setting home content view.", e);
            finish();
        }

        initToolbar();
        initInstances();

        onSubscribeToPushChannels();
    }

    private void initToolbar() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
    }

    private void initInstances() {
        drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawerToggle = new ActionBarDrawerToggle(HomeActivity.this, drawerLayout, R.string.open_drawer, R.string.close_drawer);
        drawerLayout.setDrawerListener(drawerToggle);

        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        rootLayout = (CoordinatorLayout) findViewById(R.id.rootLayout);
        navigationView = (NavigationView)findViewById(R.id.navigation);
        navigationView.setNavigationItemSelectedListener(this);
        profileView = (FrameLayout)findViewById(R.id.drawer_header);
        profileView.setOnClickListener(this);

        profileName = (TextView)navigationView.findViewById(R.id.name);
        profileName.setText(currentUser.toString());
        if(currentUser.getPicture() != null){
            profilePic = (CircularImageView)navigationView.findViewById(R.id.picture);
            profilePic.setParseFile(currentUser.getPicture());
            profilePic.loadInBackground();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        profilePic = (CircularImageView)navigationView.findViewById(R.id.picture);
        if(currentUser.getPicture() != null){
            profilePic.setParseFile(currentUser.getPicture());
            profilePic.loadInBackground();
        }
        else {
            profilePic.setImageDrawable(getResources().getDrawable(R.mipmap.ic_missing_icon_person));
        }

        onResumeHomeActivity();
    }


    @Override
    public boolean onNavigationItemSelected(MenuItem menuItem) {
        return OnDrawerMenuItemSelected(menuItem.getItemId());
    }


    @Override
    public void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        drawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        drawerToggle.onConfigurationChanged(newConfig);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.

        onInflateHomeMenu(menu);
        getMenuInflater().inflate(R.menu.menu_home, menu);
        return true;
    }

   @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (drawerToggle.onOptionsItemSelected(item))
            return true;
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        if (id == R.id.action_logout) {

            CampusPolitoApplication.unsubscribeFromAllChannels();

            AppUser.logOut();
            Intent homeIntent = new Intent(this, MainActivity.class);
            homeIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(homeIntent);
            finish();
            return true;
        }

        return onHomeMenuItemSelected(id);
    }



    @Override
    public void onClick(View v) {
        if(v.getId() == R.id.drawer_header){
            Intent profileIntent = new Intent(this, ProfileActivity.class);
            startActivity(profileIntent);
        }

        else onHomeViewClick(v);
    }

    protected abstract void onSetHomeContentView() throws ParseException;
    protected abstract void onSubscribeToPushChannels();
    protected abstract boolean OnDrawerMenuItemSelected(int itemId);
    protected abstract void onInflateHomeMenu(Menu menu);
    protected abstract boolean onHomeMenuItemSelected(int id);
    protected abstract void onHomeViewClick(View v);
    protected abstract void onResumeHomeActivity();



}
