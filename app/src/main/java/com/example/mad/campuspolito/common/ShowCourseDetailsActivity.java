package com.example.mad.campuspolito.common;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.design.widget.Snackbar;
import android.support.v4.app.NavUtils;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.InputType;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.mad.campuspolito.CampusPolitoApplication;
import com.example.mad.campuspolito.R;
import com.example.mad.campuspolito.model.AppUser;
import com.example.mad.campuspolito.model.Course;
import com.example.mad.campuspolito.model.Lecture;
import com.example.mad.campuspolito.model.Teacher;
import com.example.mad.campuspolito.teacher.LectureEditorActivity;
import com.example.mad.campuspolito.utils.DynamicLinearLayoutViewInflater;
import com.parse.Parse;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParsePush;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.parse.RequestPasswordResetCallback;
import com.parse.SendCallback;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;


public class ShowCourseDetailsActivity extends AppCompatActivity implements View.OnClickListener {

    private static final String TAG = ShowCourseDetailsActivity.class.getName();

    /**
     * This activity can be invoked with the following intent:
     *
     * Intent showCourseDetails = new Intent(this, ShowCourseDetailsActivity.class);
     * showCourseDetails.putExtra(ShowCourseDetailsActivity.COURSE_ID, courseId);
     * startActivity(showCourseDetails);
     *
     */
    public static final String COURSE_ID = "COURSE_ID";
    private static final java.lang.String FETCH_ONLINE_DATA = "FETCH_ONLINE_DATA";
    private static final java.lang.String IS_OWN_COURSE = "IS_OWN_COURSE";
    private static final java.lang.String TITLE = "TITLE";
    private static final java.lang.String DESCRIPTION = "DESCRIPTION";
    private static final java.lang.String DEGREE_TITLE = "DEGREE_TITLE";
    private static final java.lang.String DEGREE_LEVEL = "DEGREE_LEVEL";
    private static final java.lang.String CODE = "CODE";
    private static final java.lang.String CFU = "CFU";
    private static final java.lang.String SEMESTER = "SEMESTER";
    private static final String TEACHERS = "TEACHERS";
    private static final String LECTURES = "LECTURES";
    private static final String NOTICES = "NOTICES";

    /**Only the lectures from the upcoming days, specified by the following fields, are shown**/
    private static final int UPCOMING_LECTURES_THRESHOLD = 5;
    private static final int UPCOMING_LECTURES_MAX_SIZE = 10;

    private NestedScrollView mContent;
    private LinearLayout mProgress;
    private TextView mCourseTitle;
    private TextView mCourseDescription;
    private TextView mDegreeTitle;
    private TextView mDegreeLevel;
    private TextView mCourseSemester;
    private TextView mCourseCfu;
    private LinearLayout mTeachersLayout;
    private LinearLayout mLecturesLayout;
    private LinearLayout mVisitTimetable;
    private Button addNoticeButton;
    private LinearLayout mNoticesLayout;

    private String courseId;
    private boolean doFetchOnlineData;
    private boolean isOwnCourse;
    private FetchCourseData task;
    private String title;
    private String description;
    private String degreeTitle;
    private String degreeLevel;
    private String code;
    private int cfu;
    private String semester;
    private ArrayList<TeacherData> teacherDataList;
    private ArrayList<LectureData> lectureDataList;
    private ArrayList<NoticeData> noticeDataList;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_course_details);

        Toolbar toolbar = (Toolbar)findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        doFetchOnlineData = true;

        if(getIntent() != null){
            courseId = getIntent().getStringExtra(COURSE_ID);

            if(courseId == null){
                Log.e(TAG, "Course Id is null from non null intent. Something wrong happened.");
                finish();
            }
        }

        if (savedInstanceState != null){
            courseId = savedInstanceState.getString(COURSE_ID);
            doFetchOnlineData = savedInstanceState.getBoolean(FETCH_ONLINE_DATA);
            title = savedInstanceState.getString(TITLE);
            description = savedInstanceState.getString(DESCRIPTION);
            degreeTitle = savedInstanceState.getString(DEGREE_TITLE);
            degreeLevel = savedInstanceState.getString(DEGREE_LEVEL);
            code = savedInstanceState.getString(CODE);
            cfu = savedInstanceState.getInt(CFU);
            semester = savedInstanceState.getString(SEMESTER);
            teacherDataList = (ArrayList<TeacherData>) savedInstanceState.getSerializable(TEACHERS);
            lectureDataList = (ArrayList<LectureData>) savedInstanceState.getSerializable(LECTURES);
            isOwnCourse = savedInstanceState.getBoolean(IS_OWN_COURSE);
            noticeDataList = (ArrayList<NoticeData>) savedInstanceState.getSerializable(NOTICES);
        }

        mContent = (NestedScrollView)findViewById(R.id.course_content);
        mProgress = (LinearLayout)findViewById(R.id.course_progress);
        mCourseTitle = (TextView)findViewById(R.id.course_title);
        mCourseDescription = (TextView)findViewById(R.id.course_description);
        mDegreeTitle = (TextView)findViewById(R.id.course_degree_title);
        mDegreeLevel = (TextView)findViewById(R.id.course_degree_level);
        mCourseSemester = (TextView)findViewById(R.id.semester);
        mCourseCfu = (TextView)findViewById(R.id.cfu);
        mTeachersLayout = (LinearLayout)findViewById(R.id.teachers_data);
        mLecturesLayout = (LinearLayout)findViewById(R.id.lectures_data);
        mVisitTimetable = (LinearLayout)findViewById(R.id.visit_course_timetable);
        addNoticeButton = (Button)findViewById(R.id.add_notice);
        mNoticesLayout = (LinearLayout)findViewById(R.id.notices_data);

        mVisitTimetable.setOnClickListener(this);
        addNoticeButton.setOnClickListener(this);

        Log.d(TAG, "Fetching online data: " + doFetchOnlineData);

        task = new FetchCourseData(this,courseId);
        task.execute();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString(COURSE_ID, courseId);
        outState.putBoolean(FETCH_ONLINE_DATA, doFetchOnlineData);
        outState.putString(TITLE, title);
        outState.putString(DESCRIPTION, description);
        outState.putString(DEGREE_TITLE, degreeTitle);
        outState.putString(DEGREE_LEVEL, degreeLevel);
        outState.putString(CODE, code);
        outState.putInt(CFU, cfu);
        outState.putString(SEMESTER, semester);
        outState.putSerializable(TEACHERS, teacherDataList);
        outState.putSerializable(LECTURES, lectureDataList);
        outState.putBoolean(IS_OWN_COURSE, isOwnCourse);
        outState.putSerializable(NOTICES, noticeDataList);
    }

    @Override
    protected void onStop() {
        super.onStop();
        if(task != null && !task.isCancelled())
            task.cancel(true);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_home, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if (id == android.R.id.home) {
            // This ID represents the Home or Up button. In the case of this
            // activity, the Up button is shown. Use NavUtils to allow users
            // to navigate up one level in the application structure. For
            // more details, see the Navigation pattern on Android Design:
            //
            // http://developer.android.com/design/patterns/navigation.html#up-vs-back
            //

            NavUtils.navigateUpFromSameTask(this);
            return true;
        }

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        if (id == R.id.action_logout) {
            CampusPolitoApplication.unsubscribeFromAllChannels();
            AppUser.logOut();
            Intent homeIntent = new Intent(this, MainActivity.class);
            homeIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(homeIntent);
            finish();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View v) {

        if(v.getId() == R.id.visit_course_timetable){
            Intent showTimetable = new Intent(this,TimetableListActivity.class);
            showTimetable.putExtra(Lecture.COURSE, courseId);
            startActivity(showTimetable);
        }
        else if (v.getId() == R.id.add_notice){
            if(isOwnCourse){
                final EditText input = new EditText(this);
                final Context ctx = this;

                input.setInputType(InputType.TYPE_TEXT_FLAG_MULTI_LINE | InputType.TYPE_TEXT_FLAG_CAP_SENTENCES);
                input.setSingleLine(false);

                new AlertDialog.Builder(ShowCourseDetailsActivity.this)
                        .setTitle(getString(R.string.teacher_note_title))
                        .setMessage(getString(R.string.teacher_note_message))
                        .setView(input, 4,4,4,4)
                        .setPositiveButton(getString(R.string.teacher_note_confirm), new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int whichButton) {
                                String message = input.getText().toString();

                                if (!TextUtils.isEmpty(message)) {
                                    AddNoticeToCourse task = new AddNoticeToCourse(ctx, message);
                                    task.execute();

                                } else {
                                    //Toast.makeText(getApplicationContext(), getResources().getString(R.string.invalid_email_error), Toast.LENGTH_SHORT).show();
                                    Snackbar.make(mContent, R.string.empty_course_note, Snackbar.LENGTH_LONG).show();
                                }

                            }
                        }).setNegativeButton(getString(R.string.teacher_note_cancel), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        //Do nothing
                    }
                }).show();

            }
        }

    }

    private class AddNoticeToCourse extends AsyncTask<Void,Void,Boolean>{

        private final Context ctx;
        private final String message;

        private JSONObject notice;

        public AddNoticeToCourse(Context ctx, String message){
            this.ctx = ctx;
            this.message = message;
            notice = null;
        }

        @Override
        protected void onPreExecute() {
            mContent.setVisibility(View.GONE);
            mProgress.setVisibility(View.VISIBLE);
        }

        @Override
        protected Boolean doInBackground(Void... params) {
            try{
                AppUser currentUser = ((AppUser)AppUser.getCurrentUser());
                currentUser.fetchIfNeeded();

                if(currentUser.isTeacher()){
                    notice = Course.createNotice(currentUser.toString(),message);


                    ParseQuery<Course> query = ParseQuery.getQuery(Course.class);
                    Course c = query.get(courseId);
                    c.addNotice(notice);

                    c.save();

                    noticeDataList.add(new NoticeData(
                       notice.getString(Course.NOTICE_TEACHER),
                            notice.getString(Course.NOTICE_CONTENT),
                            notice.getLong(Course.NOTICE_TIME)
                    ));

                    ParsePush push = new ParsePush();
                    push.setChannel(CampusPolitoApplication.COURSE_CHANNEL + courseId);
                    push.setData(c.getJSONRepresentation(message, currentUser.toString(),ctx));
                    push.sendInBackground(new SendCallback() {
                        @Override
                        public void done(ParseException e) {
                            if (e == null) {
                                Log.d(TAG, "Push notification sent correctly.");
                            } else
                                Log.e(TAG, "Error while sending push notification", e);
                        }
                    });
                    Log.d(TAG, "Pushing message: " + c.getJSONRepresentation(message, currentUser.toString(),ctx).toString());


                    return true;
                }

                else return false;
            }
            catch (JSONException e){
                Log.e(TAG, "Error while retrieving JSON notice data.",e);
                return false;
            }
            catch (ParseException e){
                Log.e(TAG, "Error while adding notice to course.",e);
                return false;
            }
        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {

            if(aBoolean){
                if(notice != null){
                    DynamicLinearLayoutViewInflater.addNoticeViewsToLayout(noticeDataList, ctx,
                            mNoticesLayout);

                    Snackbar.make(mContent, R.string.course_notice_success, Snackbar.LENGTH_SHORT).show();
                }
            }
            else {
                Snackbar.make(mContent, R.string.parse_generic_error, Snackbar.LENGTH_LONG).show();
            }
            mContent.setVisibility(View.VISIBLE);
            mProgress.setVisibility(View.GONE);

        }
    }

    private class FetchCourseData extends AsyncTask<Void,Void,Boolean>{

        private final Context ctx;
        private final String courseId;

        public FetchCourseData(Context ctx, String courseId) {
            this.ctx = ctx;
            this.courseId = courseId;
        }

        @Override
        protected void onPreExecute() {
            mProgress.setVisibility(View.VISIBLE);
            mContent.setVisibility(View.GONE);
        }

        @Override
        protected Boolean doInBackground(Void... params) {
            try{
                if(doFetchOnlineData){
                    if(isCancelled() || isFinishing())
                        return false;
                    Log.d(TAG, "Fetching course data.");
                    ParseQuery<Course> courseQuery = ParseQuery.getQuery(Course.class);
                    courseQuery.include(Course.DEGREE);
                    Course c = courseQuery.get(courseId);

                    title = c.getTitle();
                    description = c.getDescription();
                    degreeTitle = c.getDegree().getTitle();
                    degreeLevel = c.getDegree().getLevel();
                    code = c.getCode();
                    cfu = c.getCfu();
                    semester = c.getSemester();


                    if(isCancelled() || isFinishing())
                        return false;
                    Log.d(TAG, "Fetching teachers data.");
                    ParseQuery<Teacher> teachersQuery = ParseQuery.getQuery(Teacher.class);
                    teachersQuery.include(Teacher.USER);
                    teachersQuery.whereContains(Teacher.COURSE_IDS_STRING, code);
                    teachersQuery.orderByAscending(Teacher.SURNAME);
                    List<Teacher> teacherList = new ArrayList<>(teachersQuery.find());

                    teacherDataList = new ArrayList<>();
                    for(Teacher t : teacherList){
                        teacherDataList.add(
                                new TeacherData(
                                        t.getName()+ " " + t.getSurname(),
                                        t.getIdNumber(),
                                        t.getRole(),
                                        t.getDepartmentId(),
                                        t.getEmail(),
                                        t.getPhone(),
                                        t.getUser() != null ?
                                        t.getUser().getObjectId() : null)
                        );
                    }


                    if(isCancelled() || isFinishing())
                        return false;
                    Log.d(TAG, "Fetching upcoming lectures data.");

                    Calendar cal = Calendar.getInstance();
                    ParseQuery<Lecture> minLectureQuery = ParseQuery.getQuery(Lecture.class);
                    minLectureQuery.whereGreaterThanOrEqualTo(Lecture.DAY, cal.get(Calendar.DAY_OF_MONTH));
                    minLectureQuery.whereEqualTo(Lecture.MONTH, cal.get(Calendar.MONTH));
                    minLectureQuery.whereEqualTo(Lecture.YEAR, cal.get(Calendar.YEAR));
                    minLectureQuery.whereEqualTo(Lecture.COURSE, c);

                    ParseQuery<Lecture> maxLectureQuery = ParseQuery.getQuery(Lecture.class);
                    maxLectureQuery.whereLessThanOrEqualTo(Lecture.DAY,
                            Math.min(cal.getActualMaximum(Calendar.DAY_OF_MONTH),
                                    cal.get(Calendar.DAY_OF_MONTH) + UPCOMING_LECTURES_THRESHOLD));

                    if(cal.get(Calendar.MONTH) == Calendar.DECEMBER){
                        maxLectureQuery.whereEqualTo(Lecture.MONTH, Calendar.JANUARY);
                        maxLectureQuery.whereEqualTo(Lecture.YEAR, cal.get(Calendar.YEAR)+ 1);
                    }
                    else{
                        maxLectureQuery.whereEqualTo(Lecture.MONTH, cal.get(Calendar.MONTH)+1);
                        maxLectureQuery.whereEqualTo(Lecture.YEAR, cal.get(Calendar.YEAR));
                    }

                    maxLectureQuery.whereEqualTo(Lecture.COURSE, c);

                    ParseQuery<Lecture> lessonsQuery = ParseQuery.or(Arrays.asList(
                            minLectureQuery, maxLectureQuery
                    ));

                    lessonsQuery.orderByAscending(Lecture.YEAR);
                    lessonsQuery.addAscendingOrder(Lecture.MONTH);
                    lessonsQuery.addAscendingOrder(Lecture.DAY);
                    lessonsQuery.include(Lecture.ROOM);
                    lessonsQuery.setLimit(UPCOMING_LECTURES_MAX_SIZE);
                    List<Lecture> lectureList = new ArrayList<>(lessonsQuery.find());

                    lectureDataList = new ArrayList<>();
                    for(Lecture l : lectureList){
                        lectureDataList.add(new LectureData(
                                l.getDate(),
                                CampusPolitoApplication.getDayString(l.getStartCalendar()),
                                CampusPolitoApplication.getTimeString(l.getStartCalendar()),
                                CampusPolitoApplication.getTimeString(l.getEndCalendar()),
                                l.getRoom().getType(),
                                l.getRoom().getLocation(),
                                l.getObjectId()
                        ));
                    }

                    AppUser currentUser = (AppUser)AppUser.getCurrentUser();
                    currentUser.fetchIfNeeded();

                    if(currentUser.isTeacher()) {
                        Log.d(TAG, "Fetching additional info about teacher (check if course is owned by current)");
                        Teacher profile = currentUser.getTeacherProfile();
                        profile.fetchIfNeeded();
                        List<Course> teacherCourses = new ArrayList<>(profile.getCourses());
                        ParseObject.fetchAllIfNeeded(teacherCourses);

                        isOwnCourse = teacherCourses.contains(c);
                    }
                    else
                        isOwnCourse = false;

                    if(isCancelled() || isFinishing())
                        return false;

                    Log.d(TAG, "Fetching teacher notices data.");

                    List<JSONObject> noticeList = c.getNotices();
                    noticeDataList = new ArrayList<>();
                    if(noticeList != null){
                        for(Object n : noticeList){
                            if(n instanceof HashMap){
                                HashMap notice = (HashMap)n;
                                noticeDataList.add(new NoticeData(
                                        (String)notice.get(Course.NOTICE_TEACHER),
                                        (String)notice.get(Course.NOTICE_CONTENT),
                                        (long)notice.get(Course.NOTICE_TIME)
                                ));
                            }
                            else if (n instanceof JSONObject){
                                JSONObject notice = (JSONObject)n;
                                noticeDataList.add(new NoticeData(
                                                notice.getString(Course.NOTICE_TEACHER),
                                                notice.getString(Course.NOTICE_CONTENT),
                                                notice.getLong(Course.NOTICE_TIME))
                                );
                            }
                        }
                    }

                    doFetchOnlineData = false;
                }


                if(isCancelled() || isFinishing())
                    return false;

                return true;
            }
            catch (ParseException e){
                Log.e(TAG, "Error while fetching course data.",e);
                return false;
            } catch (JSONException e) {
                Log.e(TAG, "Error with JSON elaboration for course notices.",e);
                return false;
            }
        }

        @Override
        protected void onPostExecute(Boolean success) {

            if(success){
                mCourseTitle.setText(title + " ["+code+"]");
                mCourseDescription.setText(description);
                mDegreeTitle.setText(degreeTitle);
                mDegreeLevel.setText(degreeLevel);
                mCourseSemester.setText(semester);
                mCourseCfu.setText(cfu + "");

                DynamicLinearLayoutViewInflater.addTeacherViewsToLayout(teacherDataList, ctx,
                        mTeachersLayout, new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                String teacherUserId = (String) v.getTag(R.id.teachers_data);
                                if (teacherUserId != null) {
                                    Intent showTeacherProfile = new Intent(ctx, ProfileActivity.class);
                                    showTeacherProfile.putExtra(ProfileActivity.APP_USER_TYPE, AppUser.PROFILE_TEACHER);
                                    showTeacherProfile.putExtra(ProfileActivity.APP_USER_OBJECT_ID, teacherUserId);
                                    startActivity(showTeacherProfile);
                                }
                            }
                        }, R.id.teachers_data);




                DynamicLinearLayoutViewInflater.addLectureDataViewsToLayout(lectureDataList, ctx,
                        mLecturesLayout, isOwnCourse?
                                new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                String lectureId = (String)v.getTag(R.id.lectures_data);
                                if(lectureId != null) {
                                    Intent intent = new Intent(v.getContext(), LectureEditorActivity.class);
                                    intent.putExtra(LectureDetailActivity.LECTURE_ID, lectureId);
                                    startActivityForResult(intent, LectureEditorActivity.EDIT_LECTURE);
                                }
                            }
                        }
                                :null, //If the user is not a teacher or doesn't own a course, no listener is added
                        R.id.lectures_data);

                if(isOwnCourse)
                    addNoticeButton.setVisibility(View.VISIBLE);
                else
                    addNoticeButton.setVisibility(View.GONE);


                if(noticeDataList.size() > 0){
                    DynamicLinearLayoutViewInflater.addNoticeViewsToLayout(noticeDataList, ctx,
                            mNoticesLayout);
                }
            }
            else {
                Snackbar.make(mContent,getString(R.string.parse_generic_error),Snackbar.LENGTH_LONG).show();
            }
            mContent.setVisibility(View.VISIBLE);
            mProgress.setVisibility(View.GONE);
        }
    }
}
