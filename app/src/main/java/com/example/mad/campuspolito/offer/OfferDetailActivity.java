package com.example.mad.campuspolito.offer;

import android.content.ComponentName;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.example.mad.campuspolito.CampusPolitoApplication;
import com.example.mad.campuspolito.R;
import com.example.mad.campuspolito.common.MainActivity;
import com.example.mad.campuspolito.model.AppUser;

/**
 * Created by lg on 15/08/15.
 */
public class OfferDetailActivity extends AppCompatActivity {

    private static final String TAG = OfferDetailActivity.class.getName();
    public static final int DETAIL_REQUEST = 90;
    public static final int OFFER_DELETED = 91;
    private ComponentName parentName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_offer_detail);

        setSupportActionBar((Toolbar) findViewById(R.id.toolbar));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        parentName = getCallingActivity();
        if(parentName != null)
            Log.d(TAG, parentName.getClassName());

        // savedInstanceState is non-null when there is fragment state
        // saved from previous configurations of this activity
        // (e.g. when rotating the screen from portrait to landscape).
        // In this case, the fragment will automatically be re-added
        // to its container so we don't need to manually add it.
        // For more information, see the Fragments API guide at:
        //
        // http://developer.android.com/guide/components/fragments.html
        //
        if (savedInstanceState == null) {
            Log.i(TAG, "onCreate, savedInstanceState is null");
            OfferDetailFragment fragment = new OfferDetailFragment();
            Bundle arguments = new Bundle();
            Intent intent = getIntent();

            if(intent.getAction() == null || intent.getAction().isEmpty()){
                Log.i(TAG, "bypassing offer list activity");
                final String offer_id = intent.getStringExtra(OfferDetailFragment.OFFER_ID);
                if(offer_id == null || offer_id.isEmpty()){
                    Toast.makeText(getApplicationContext(), getString(R.string.offer_not_found), Toast.LENGTH_SHORT).show();
                    onBackPressed();
                    return;
                }
                arguments.putString(OfferDetailFragment.OFFER_ID, getIntent().getStringExtra(OfferDetailFragment.OFFER_ID));
                fragment.setArguments(arguments);
                getFragmentManager().beginTransaction()
                        .add(R.id.offer_detail_container, fragment)
                        .commit();
            }
            else if(intent.getAction().equals(OfferListActivity.class.getName())){
                Log.i(TAG, "from offer list activity");
                arguments.putString(OfferDetailFragment.OFFER_ID, intent.getStringExtra(OfferDetailFragment.OFFER_ID));
                fragment.setArguments(arguments);
                getFragmentManager().beginTransaction()
                        .add(R.id.offer_detail_container, fragment)
                        .commit();
            }
        }

    }

    /*
     * Overridden in order to force the update of the offers list when the switches state changes
     */
    @Override
    public void onBackPressed() {
        Log.i(TAG, "onBackPressed");
        NavUtils.navigateUpTo(this, new Intent(this, OfferListActivity.class));
        super.onBackPressed();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        if (id == R.id.action_logout) {
            CampusPolitoApplication.unsubscribeFromAllChannels();
            AppUser.logOut();
            Intent homeIntent = new Intent(this, MainActivity.class);
            homeIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(homeIntent);
            finish();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_home,menu);
        return super.onCreateOptionsMenu(menu);
    }
}
