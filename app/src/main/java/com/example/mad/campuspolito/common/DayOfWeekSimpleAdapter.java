package com.example.mad.campuspolito.common;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.mad.campuspolito.R;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 * Created by mdb on 01/08/15.
 */
public class DayOfWeekSimpleAdapter extends BaseAdapter{

    private final Context ctx;
    private List<DayOfWeekHolder> daysOfWeek;

    public DayOfWeekSimpleAdapter(Context ctx){
        this.ctx = ctx;
        daysOfWeek = new ArrayList<>();
        daysOfWeek.add(new DayOfWeekHolder(Calendar.MONDAY,ctx.getString(R.string.monday)));
        daysOfWeek.add(new DayOfWeekHolder(Calendar.TUESDAY,ctx.getString(R.string.tuesday)));
        daysOfWeek.add(new DayOfWeekHolder(Calendar.WEDNESDAY,ctx.getString(R.string.wednesday)));
        daysOfWeek.add(new DayOfWeekHolder(Calendar.THURSDAY,ctx.getString(R.string.thursday)));
        daysOfWeek.add(new DayOfWeekHolder(Calendar.FRIDAY,ctx.getString(R.string.friday)));
    }
    @Override
    public int getCount() {
        return daysOfWeek.size();
    }

    @Override
    public Object getItem(int position) {
        return daysOfWeek.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        DayOfWeekHolder dayOfWeek = daysOfWeek.get(position);

        if(convertView == null)
            convertView = ((LayoutInflater)ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(android.R.layout.simple_spinner_dropdown_item,parent,false);

        ((TextView)convertView.findViewById(android.R.id.text1)).setText(dayOfWeek.dayName);

        return convertView;
    }

    class DayOfWeekHolder{

        final int dayId;
        final String dayName;

        public DayOfWeekHolder(int dayId, String dayName){
            this.dayId = dayId;
            this.dayName = dayName;
        }
    }
}
