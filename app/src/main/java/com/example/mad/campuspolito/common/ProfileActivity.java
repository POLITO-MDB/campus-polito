package com.example.mad.campuspolito.common;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Paint;
import android.net.Uri;
import android.os.AsyncTask;
import android.provider.MediaStore;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.app.NavUtils;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.SwitchCompat;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.mad.campuspolito.CampusPolitoApplication;
import com.example.mad.campuspolito.R;
import com.example.mad.campuspolito.model.AppUser;
import com.example.mad.campuspolito.model.Company;
import com.example.mad.campuspolito.model.Degree;
import com.example.mad.campuspolito.model.Notice;
import com.example.mad.campuspolito.model.Student;
import com.example.mad.campuspolito.model.Teacher;
import com.example.mad.campuspolito.utils.DynamicLinearLayoutViewInflater;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseImageView;
import com.parse.ParseQuery;
import com.parse.ProgressCallback;
import com.parse.SaveCallback;

import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Date;
import java.util.List;
import java.util.ListIterator;

import bolts.Task;

public class ProfileActivity extends AppCompatActivity {

    public static final String TAG = ProfileActivity.class.getName();
    //intent data, used to show a certain profile
    public static final String APP_USER_OBJECT_ID = "app_user_object_id";
    public static final String APP_USER_TYPE = "app_user_type";
    private static final int APP_BAR_HEIGHT_LANDSCAPE = 128;
    private static final int APP_BAR_HEIGHT_PORTRAIT = 256;
    private static final int REQ_EDIT_PROFILE = 10;
    private static final int RESULT_LOAD_IMG = 20;

    /**
     * Common profile layout
     **/
    CollapsingToolbarLayout mCollapsingToolbarLayout;
    ParseImageView mProfileImageView;
    View mProfileView;
    View mProgressView;
    Toolbar mToolbar;
    TextView mEmail;
    TextView mPhone;
    TextView mType;
    LinearLayout mProfileBasicInfo;
    LinearLayout mProfileContentViews;
    SwitchCompat mPublicProfile;

    /**
     * AppUser object Id and type, mandatory in intents for specific users profiles
     **/
    private String specificAppUserId;
    private String specificAppUserType;

    /**
     * Async task reference
     **/
    FetchProfile fetchProfileTask;

    /**
     * Parse pinned object
     **/
    public AppUser appUser;
    public Student student;
    public Teacher teacher;
    public Company company;


    /**
     * User profile picture related
     **/
    private Uri pictureUri;
    private Bitmap pictureBitmap;
    private boolean isFavouriteStudent;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);

        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        mCollapsingToolbarLayout = (CollapsingToolbarLayout) findViewById(R.id.collapsingToolbarLayout);
        mProfileImageView = (ParseImageView) findViewById(R.id.profile_picture);
        mProfileView = findViewById(R.id.profile_content);
        mProgressView = findViewById(R.id.profile_progress);
        mEmail = (TextView) findViewById(R.id.email);
        mPhone = (TextView) findViewById(R.id.phone);
        mType = (TextView) findViewById(R.id.type);
        mProfileBasicInfo = (LinearLayout) findViewById(R.id.profile_basic_info);
        mProfileContentViews = (LinearLayout) findViewById(R.id.profile_cardviews);
        mPublicProfile = (SwitchCompat) findViewById(R.id.public_profile_toggle);

        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        Intent intent = getIntent();

        if (intent != null && intent.hasExtra(APP_USER_OBJECT_ID) && intent.hasExtra(APP_USER_TYPE))
            fetchSpecificUserProfile(intent.getStringExtra(APP_USER_OBJECT_ID), intent.getStringExtra(APP_USER_TYPE));
        else if (savedInstanceState != null && savedInstanceState.containsKey(APP_USER_OBJECT_ID) && savedInstanceState.containsKey(APP_USER_TYPE))
            fetchSpecificUserProfile(savedInstanceState.getString(APP_USER_OBJECT_ID), savedInstanceState.getString(APP_USER_TYPE));
        else
            fetchCurrentUserProfile();
    }

    private void fetchSpecificUserProfile(String appUserObjectId, String appUserType) {

        if(appUserObjectId != null && appUserObjectId.equals(AppUser.getCurrentUser().getObjectId()))
            fetchCurrentUserProfile();
        else {
            this.specificAppUserId = appUserObjectId;
            this.specificAppUserType = appUserType;


            switch (appUserType) {
                case AppUser.PROFILE_STUDENT:
                    fetchProfileTask = new FetchStudentProfile(this, appUserObjectId);
                    break;
                case AppUser.PROFILE_TEACHER:
                    fetchProfileTask = new FetchTeacherProfile(this, appUserObjectId);
                    break;
                case AppUser.PROFILE_COMPANY:
                    fetchProfileTask = new FetchCompanyProfile(this, appUserObjectId);
                    break;
            }

            fetchProfileTask.execute();
        }
    }

    private void fetchCurrentUserProfile() {

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        this.specificAppUserId = null;
        this.specificAppUserType = null;
        AppUser currentUser = (AppUser) AppUser.getCurrentUser();

        mPublicProfile.setVisibility(View.VISIBLE);

        if (currentUser.isStudent()) {
            fetchProfileTask = new FetchStudentProfile(this, currentUser.getObjectId());
        } else if (currentUser.isTeacher()) {
            fetchProfileTask = new FetchTeacherProfile(this, currentUser.getObjectId());
        } else if (currentUser.isCompany()) {
            fetchProfileTask = new FetchCompanyProfile(this, currentUser.getObjectId());
        }

        fetchProfileTask.execute();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        if (specificAppUserId != null && specificAppUserType != null) {
            outState.putString(APP_USER_OBJECT_ID, specificAppUserId);
            outState.putString(APP_USER_TYPE, specificAppUserType);
        }
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {


            MenuItem favItem = menu.findItem(R.id.action_fav_student);

            if(favItem != null){
                Log.d(TAG, "Add fav student menu is placed on toolbar.");
                if(isFavouriteStudent){
                    favItem.setIcon(R.drawable.ic_star_outline);
                    favItem.setTitle(R.string.action_unfav_student);
                }
                else {
                    favItem.setIcon(R.drawable.ic_star_inv);
                    favItem.setTitle(R.string.action_fav_student);
                }
            }
        return true;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_home, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if (id == android.R.id.home) {
            // This ID represents the Home or Up button. In the case of this
            // activity, the Up button is shown. Use NavUtils to allow users
            // to navigate up one level in the application structure. For
            // more details, see the Navigation pattern on Android Design:
            //
            // http://developer.android.com/design/patterns/navigation.html#up-vs-back
            //

            NavUtils.navigateUpFromSameTask(this);
            return true;
        }

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        if (id == R.id.action_logout) {
            CampusPolitoApplication.unsubscribeFromAllChannels();
            AppUser.logOut();
            Intent homeIntent = new Intent(this, MainActivity.class);
            homeIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(homeIntent);
            finish();
            return true;
        }

        if (id == R.id.action_remove_profile_pic) {
            pictureUri = null;
            mProfileImageView.setScaleType(ImageView.ScaleType.CENTER);
            mProfileImageView.setImageDrawable(getResources().getDrawable(R.mipmap.ic_picture));
            mProfileImageView.invalidate();

            if (pictureBitmap != null) {
                pictureBitmap.recycle();
                pictureBitmap = null;
            }

            UpdateProfilePicture task = new UpdateProfilePicture(this, null, null);
            task.execute();
        }

        if(id == R.id.action_fav_student){
            ManageFavouriteStudent task = new ManageFavouriteStudent(this, specificAppUserId,!isFavouriteStudent,item);
            task.execute();
        }

        return super.onOptionsItemSelected(item);
    }

    public class ManageFavouriteStudent extends AsyncTask<Void,Void,Boolean>{


        private final boolean addToFav;
        private final Context ctx;
        private final String studentUserId;
        private final MenuItem item;

        public ManageFavouriteStudent(Context ctx, String studentId, boolean addToFav, MenuItem item) {
            this.ctx = ctx;
            this.studentUserId = studentId;
            this.addToFav = addToFav;
            this.item = item;
        }

        @Override
        protected void onPreExecute() {
            mProfileView.setVisibility(View.GONE);
            mProgressView.setVisibility(View.VISIBLE);
        }

        @Override
        protected Boolean doInBackground(Void... params) {
            try{

                AppUser appUser = (AppUser)AppUser.getCurrentUser();
                appUser.fetchIfNeeded();
                Company company = appUser.getCompanyProfile();

                if(company == null)
                    return false;
                company.fetchIfNeeded();

                ParseQuery<Student> query1 = ParseQuery.getQuery(Student.class);
                ParseQuery<AppUser> matchingQuery = ParseQuery.getQuery(AppUser.class);
                matchingQuery.whereEqualTo("objectId", studentUserId);
                query1.whereMatchesQuery(Student.USER, matchingQuery);
                List<Student> results = query1.find();

                if(results == null || results.size() == 0)
                    return false;

                Student student = results.get(0);

                student.fetchIfNeeded();

                Log.d(TAG, "Student data fetched (addToFav="+addToFav);
                if(addToFav)
                    company.addFavouriteStudent(student);
                else
                    company.removeFavouriteStudent(student);

                company.save();

                return true;

            }
            catch (ParseException e){
                Log.e(TAG, "Error while managing favourite student (addToFav="+addToFav);
                return false;
            }
        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {


            mProfileView.setVisibility(View.VISIBLE);
            mProgressView.setVisibility(View.GONE);

            if(addToFav){
                Snackbar.make(mProfileView,getString(R.string.student_add_fav),Snackbar.LENGTH_SHORT)
                        .setAction(R.string.undo, new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {

                                ManageFavouriteStudent undo = new ManageFavouriteStudent(ctx, studentUserId, false,item);
                                undo.execute();
                            }
                        }).show();

                item.setIcon(R.drawable.ic_star_outline);
                item.setTitle(R.string.action_unfav_notice);
            }
            else {
                Snackbar.make(mProfileView,getString(R.string.student_remove_fav),Snackbar.LENGTH_SHORT)
                        .setAction(R.string.undo, new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                ManageFavouriteStudent undo = new ManageFavouriteStudent(ctx, studentUserId, true,item);
                                undo.execute();

                            }
                        }).show();
                item.setIcon(R.drawable.ic_star_inv);
                item.setTitle(R.string.action_fav_notice);
            }

            isFavouriteStudent = !isFavouriteStudent;
        }
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == REQ_EDIT_PROFILE) {
            fetchCurrentUserProfile();
        }

        if (requestCode == RESULT_LOAD_IMG && resultCode == Activity.RESULT_OK && null != data) {
            try {

                if (pictureBitmap != null) {
                    Log.d(TAG, "Recycling profile pic bitmap");
                    pictureBitmap.recycle();
                    pictureBitmap = null;
                }
                pictureUri = data.getData();
                if (pictureUri != null) {
                    Log.d(TAG, "New picture Uri is : " + pictureUri.toString());
                    pictureBitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), pictureUri);
                    mProfileImageView.setScaleType(ImageView.ScaleType.MATRIX);
                    mProfileImageView.setImageBitmap(pictureBitmap);
                    mProfileImageView.invalidate();

                    UpdateProfilePicture task = new UpdateProfilePicture(this, pictureUri, pictureBitmap);
                    task.execute();

                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private void showLocationOnMap(String locationQuery) {
        Uri gmmIntentUri = Uri.parse("geo:45.0628484,7.6602297?q=" + locationQuery);
        Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
        mapIntent.setPackage("com.google.android.apps.maps");
        startActivity(mapIntent);
    }

    private String getStringRepresentation(List<String> list) {

        if (list == null || list.isEmpty())
            return "";

        StringBuffer sb = new StringBuffer();

        ListIterator<String> iterator = list.listIterator();

        String string;

        while (iterator.hasNext()) {
            string = iterator.next();
            sb.append(string);
            if (iterator.hasNext())
                sb.append(", ");
        }

        return sb.toString();

    }

    private class UpdateProfilePicture extends AsyncTask<Void, Integer, Boolean> {

        private Context ctx;
        private Uri mPictureUri;
        private Bitmap bitmapObject;

        public UpdateProfilePicture(Context ctx, Uri mPictureUri, Bitmap bitmapObject) {
            this.ctx = ctx;
            this.mPictureUri = mPictureUri;
            this.bitmapObject = bitmapObject;
        }

        @Override
        protected Boolean doInBackground(Void... params) {

            ByteArrayOutputStream outputStream;

            try {
                if (mPictureUri != null && bitmapObject != null) {
                    Log.d(TAG, "Processing picture");
                    bitmapObject = CampusPolitoApplication.decodeSampledBitmapFromUri(mPictureUri,
                            CampusPolitoApplication.SCALE_SIZE + 1,
                            CampusPolitoApplication.SCALE_SIZE + 1);


                    bitmapObject = CampusPolitoApplication.scaleBitmapAndKeepRatio(bitmapObject,
                            CampusPolitoApplication.SCALE_SIZE,
                            CampusPolitoApplication.SCALE_SIZE);
                    Log.d(TAG, "Bitmap object saved from content provider.");

                    outputStream = new ByteArrayOutputStream();
                    bitmapObject.compress(Bitmap.CompressFormat.JPEG, 100, outputStream);
                    Log.d(TAG, "Bitmap object compressed to output stream (size " + outputStream.size() + ")");
                    ParseFile image = new ParseFile("profile_pic.jpg", outputStream.toByteArray());

                    outputStream.close();
                    Log.d(TAG, "Image loaded to Parse File object, starting upload...");


                    Task<Void> saveTask = image.saveInBackground(new ProgressCallback() {
                        @Override
                        public void done(Integer integer) {
                            publishProgress(integer);
                        }
                    });

                    saveTask.waitForCompletion();


                    if (saveTask.isCompleted()) {
                        Log.d(TAG, "Picture saved to Parse.com");
                        AppUser currentUser = (AppUser) AppUser.getCurrentUser();
                        currentUser.setPicture(image);
                        currentUser.save();
                    }
                } else {
                    Log.d(TAG, "Picture deleted from Parse.com");
                    AppUser currentUser = (AppUser) AppUser.getCurrentUser();
                    currentUser.remove(AppUser.PICTURE);
                    currentUser.save();
                }


                return true;

            } catch (Exception e) {
                Log.e(TAG, "Error while updating profile picture", e);
                return false;
            }
        }
    }

    private abstract class FetchProfile extends AsyncTask<Void, Void, Boolean> {

        public final ProfileActivity ctx;
        public final String appUserObjectId;
        public ParseFile profilePicture;
        public String profileName;
        public String profilePhone;
        public String profileEmail;
        public String profileType;

        public FetchProfile(ProfileActivity ctx, String appUserObjectId) {
            this.ctx = ctx;
            this.appUserObjectId = appUserObjectId;

            setUpBasicInfoLayout();
        }

        @Override
        protected void onPreExecute() {
            mProfileView.setVisibility(View.GONE);
            mProgressView.setVisibility(View.VISIBLE);
        }

        @Override
        protected Boolean doInBackground(Void... params) {

            ParseQuery<AppUser> query;
            try {
                if (isCancelled())
                    return false;

                query = ParseQuery.getQuery(AppUser.class);
                query.include(AppUser.PROFILE_STUDENT);
                query.include(AppUser.PROFILE_TEACHER);
                query.include(AppUser.PROFILE_COMPANY);
                appUser = query.get(appUserObjectId);

                profileName = appUser.toString();
                profileType = appUser.getType();

                if (appUser.getPicture() != null)
                    profilePicture = appUser.getPicture();
                else
                    profilePicture = null;

                profilePhone = appUser.getPhone();
                profileEmail = appUser.getEmail();

                if (isCancelled())
                    return false;
                Log.d(TAG, "Perform additional work on profile in background.");
                backgroundWorkOnFetchProfile(appUser);
                Log.d(TAG, "Additional work on profile in background completed.");

                return true;
            } catch (ParseException e) {
                Log.e(TAG, "Error while loading app user profile data.", e);
                return false;
            }
        }

        @Override
        protected void onPostExecute(Boolean success) {

            if (isCancelled())
                return;

            if (success) {
                mCollapsingToolbarLayout.setTitle(profileName);


                if (profilePicture != null) {
                    mProfileImageView.setScaleType(ImageView.ScaleType.MATRIX);
                    mProfileImageView.setParseFile(profilePicture);
                    mProfileImageView.loadInBackground();
                }

                switch (profileType) {
                    case AppUser.PROFILE_STUDENT:
                        mType.setText(getString(R.string.profile_type_student));
                        Log.d(TAG, "Checking if current user is company");
                        if((((AppUser)AppUser.getCurrentUser()).isCompany())){
                            if (mToolbar.getMenu().findItem(R.id.action_fav_student) == null) {
                                mToolbar.inflateMenu(R.menu.menu_add_fav_student);
                                Log.d(TAG, "Fav menu added to profile activity");
                            }
                        }

                        break;
                    case AppUser.PROFILE_TEACHER:
                        mType.setText(getString(R.string.profile_type_teacher));
                        break;

                    case AppUser.PROFILE_COMPANY:
                        mType.setText(getString(R.string.profile_type_company));
                        break;
                }
                mEmail.setText(profileEmail);
                mPhone.setText(profilePhone);

                if (isCancelled())
                    return;

                if (!appUser.toString().equals(AppUser.getCurrentUser().toString())) {
                    findViewById(R.id.profile_basic_info_edit).setVisibility(View.GONE);
                    hideEditProfileViews();
                } else {
                    mProfileImageView.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Intent galleryIntent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                            // Start the Intent
                            startActivityForResult(galleryIntent, RESULT_LOAD_IMG);
                        }
                    });
                    if (mToolbar.getMenu().findItem(R.id.action_remove_profile_pic) == null) {
                        mToolbar.inflateMenu(R.menu.menu_profile);
                    }

                    setUpEditProfileListeners();
                }

                Log.d(TAG, "Perform additional work on profile in foreground.");
                foregroundWorkOnFetchProfile();
                Log.d(TAG, "Additional work in foreground completed.");

            } else {
                Log.d(TAG, "Something wrong happened while loading profile data.");
                Snackbar.make(mProfileView, R.string.parse_generic_error, Snackbar.LENGTH_LONG);
            }
;


            mProfileView.setVisibility(View.VISIBLE);
            mProgressView.setVisibility(View.GONE);
        }

        @Override
        protected void onCancelled() {
            Log.d(TAG, "Fetch of profile info cancelled.");
        }

        protected abstract void setUpBasicInfoLayout();

        protected abstract void backgroundWorkOnFetchProfile(AppUser profileObject) throws ParseException;

        protected abstract void foregroundWorkOnFetchProfile();

        protected abstract void hideEditProfileViews();

        protected abstract void setUpEditProfileListeners();
    }

    private class ChangeProfileVisibility extends AsyncTask<Void,Void,Boolean> {
        private final Context ctx;
        private final boolean isChecked;
        private final String userType;

        public ChangeProfileVisibility(Context ctx, boolean isChecked, String userType){
            this.ctx = ctx;
            this.isChecked = isChecked;
            this.userType = userType;
        }

        @Override
        protected Boolean doInBackground(Void... params) {
            try{
                switch (userType){
                    case AppUser.PROFILE_STUDENT:

                        student.setIsPublic(isChecked);
                        student.save();

                        break;

                    case AppUser.PROFILE_COMPANY:

                        company.setIsPublic(isChecked);
                        company.save();

                        break;

                    default:
                        break;
                }
            }
            catch (ParseException e){
                Log.e(TAG, "Error while changing profile visibility",e);
                return false;
            }

            return true;
        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {
            if(aBoolean){
                if (!isFinishing()) {
                    Log.d(TAG, "Profile visibility changed to : " + isChecked);                }
            }
            else {
                if(!isFinishing()){
                    Log.e(TAG, "Error while changing visibility of student profile to " + isChecked);
                    Snackbar.make(mProfileView, getString(R.string.parse_generic_error), Snackbar.LENGTH_LONG).show();
                }
            }
        }
    }


    private class FetchStudentProfile extends FetchProfile implements View.OnClickListener, CompoundButton.OnCheckedChangeListener {

        TextView mShortDescription;
        TextView mDateOfBirth;
        TextView mDegreeTitle;
        TextView mDegreeLevel;
        TextView mLocation;
        TextView mSkills;
        TextView mLanguages;
        TextView mAvailability;
        LinearLayout mFormations;
        LinearLayout mExperiences;
        TextView mHobbies;

        String shortDescription;
        Date dateOfBirth;
        String location;
        Degree degree;
        List<JSONObject> formationList;
        List<JSONObject> experienceList;
        String skills;
        String languages;
        String availability;
        String hobbies;
        String idNumber;
        boolean isPublic;
        private boolean isCompanyProfile;

        public FetchStudentProfile(ProfileActivity ctx, String appUserObjectId) {
            super(ctx, appUserObjectId);
        }

        @Override
        protected void setUpBasicInfoLayout() {

            View mStudentBasicInfo;
            View mStudentAdditionalInfo;

            if (ctx.findViewById(R.id.student_profile_basic_info) == null &&
                    ctx.findViewById(R.id.student_profile_additional_info) == null) {

                mStudentBasicInfo = View.inflate(ctx, R.layout.student_profile_basic_info, null);
                mStudentAdditionalInfo = View.inflate(ctx, R.layout.student_profile_additional_info, null);
                mProfileBasicInfo.addView(mStudentBasicInfo, 1);
                mProfileContentViews.addView(mStudentAdditionalInfo, 1);
            } else {
                mStudentBasicInfo = ctx.findViewById(R.id.student_profile_basic_info);
                mStudentAdditionalInfo = ctx.findViewById(R.id.student_profile_additional_info);
            }

            mShortDescription = (TextView) mStudentBasicInfo.findViewById(R.id.student_short_desc);
            mDateOfBirth = (TextView) mStudentBasicInfo.findViewById(R.id.student_date_of_birth);
            mDegreeTitle = (TextView) mStudentBasicInfo.findViewById(R.id.student_degree_title);
            mDegreeLevel = (TextView) mStudentBasicInfo.findViewById(R.id.student_degree_level);
            mLocation = (TextView) mStudentBasicInfo.findViewById(R.id.student_location);


            mSkills = (TextView) mStudentAdditionalInfo.findViewById(R.id.student_skills);
            mLanguages = (TextView) mStudentAdditionalInfo.findViewById(R.id.student_languages);
            mAvailability = (TextView) mStudentAdditionalInfo.findViewById(R.id.student_availability);
            mFormations = (LinearLayout) mStudentAdditionalInfo.findViewById(R.id.student_formation_list);
            mExperiences = (LinearLayout) mStudentAdditionalInfo.findViewById(R.id.student_experience_list);
            mHobbies = (TextView) mStudentAdditionalInfo.findViewById(R.id.student_hobbies);
        }

        @Override
        protected void backgroundWorkOnFetchProfile(AppUser profileObject) throws ParseException {

            student = profileObject.getStudentProfile();
            shortDescription = student.getShortDesc();
            dateOfBirth = student.getDateOfBirth();
            degree = student.getDegree();
            degree.fetchIfNeeded();
            location = student.getLocationName();
            skills = getStringRepresentation(student.getSkills());
            languages = getStringRepresentation(student.getLanguages());
            availability = student.getAvailability();
            formationList = student.getEducation();
            experienceList = student.getExperience();
            hobbies = getStringRepresentation(student.getHobbies());
            idNumber = student.getIdNumber();
            isPublic = student.getIsPublic();

            AppUser currentUser  = (AppUser)AppUser.getCurrentUser();
            currentUser.fetchIfNeeded();
            isCompanyProfile = currentUser.isCompany();

            if(isCompanyProfile && profileType.equals(AppUser.PROFILE_STUDENT)){

                Company profile = currentUser.getCompanyProfile();
                profile.fetchIfNeeded();
                if(profile.getFavouriteStudents() == null)
                    isFavouriteStudent = false;
                else if(profile.getFavouriteStudents().contains(student))
                    isFavouriteStudent = true;
                else
                    isFavouriteStudent = false;
            }

        }



        @Override
        protected void foregroundWorkOnFetchProfile() {

            if (shortDescription != null) {
                if (!shortDescription.isEmpty())
                    mShortDescription.setText(shortDescription);
                else
                    mShortDescription.setText(getResources().getString(R.string.place_holder));

                mLocation.setVisibility(View.VISIBLE);
            } else {
                mShortDescription.setVisibility(View.GONE);
            }
            mDateOfBirth.setText(android.text.format.DateFormat.getDateFormat(ctx).format(dateOfBirth));
            mDegreeTitle.setText(degree.getTitle());
            mDegreeLevel.setText(degree.getLevel());
            if (location != null) {
                mLocation.setOnClickListener(this);
                mLocation.setPaintFlags(mLocation.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);

                if (!location.isEmpty()) {
                    mLocation.setText(location);
                } else {
                    mLocation.setText(getResources().getString(R.string.place_holder));
                }
                mLocation.setVisibility(View.VISIBLE);
            } else {
                mLocation.setText(getResources().getString(R.string.place_holder));
            }

            if (languages != null) {
                if (!skills.isEmpty())
                    mSkills.setText(skills);
                else
                    mSkills.setText(getResources().getString(R.string.place_holder));
            } else
                mSkills.setText(getResources().getString(R.string.place_holder));


            if (languages != null) {
                if (!languages.isEmpty())
                    mLanguages.setText(languages);
                else
                    mLanguages.setText(getResources().getString(R.string.place_holder));
            } else
                mLanguages.setText(getResources().getString(R.string.place_holder));

            if (availability != null) {
                if (!availability.isEmpty())
                    mAvailability.setText(availability);
                else
                    mAvailability.setText(getResources().getString(R.string.place_holder));
            } else
                mAvailability.setText(getResources().getString(R.string.place_holder));

            if (formationList != null) {
                mFormations.removeAllViews();
                if (!formationList.isEmpty())
                    DynamicLinearLayoutViewInflater.addJSONExpEduViewsToLayout(formationList, ctx, mFormations, null, R.id.student_formation_list);
                else {
                    TextView tv = new TextView(ctx);
                    tv.setText(getResources().getString(R.string.place_holder));
                    mFormations.addView(tv);
                }
            } else {
                TextView tv = new TextView(ctx);
                tv.setText(getResources().getString(R.string.place_holder));
                mFormations.addView(tv);
            }


            if (experienceList != null) {
                mExperiences.removeAllViews();
                if (!experienceList.isEmpty())
                    DynamicLinearLayoutViewInflater.addJSONExpEduViewsToLayout(experienceList, ctx, mExperiences, null, R.id.student_experience_list);
                else {
                    TextView tv = new TextView(ctx);
                    tv.setText(getResources().getString(R.string.place_holder));
                    mExperiences.addView(tv);
                }
            } else {
                TextView tv = new TextView(ctx);
                tv.setText(getResources().getString(R.string.place_holder));
                mExperiences.addView(tv);
            }

            if (hobbies != null) {
                if (!hobbies.isEmpty())
                    mHobbies.setText(hobbies);
                else
                    mHobbies.setText(getResources().getString(R.string.place_holder));
            } else
                mHobbies.setText(getResources().getString(R.string.place_holder));

            mType.setText(getString(R.string.profile_type_student) + " " + idNumber);

            mPublicProfile.setChecked(isPublic);

            if(((AppUser)AppUser.getCurrentUser()).isCompany()){
                MenuItem favItem = mToolbar.getMenu().findItem(R.id.action_fav_student);

                if(favItem != null){
                    Log.d(TAG, "Add fav student menu is placed on toolbar.");
                    if(isFavouriteStudent){
                        favItem.setIcon(R.drawable.ic_star_outline);
                        favItem.setTitle(R.string.action_unfav_student);
                    }
                    else {
                        favItem.setIcon(R.drawable.ic_star_inv);
                        favItem.setTitle(R.string.action_fav_student);
                    }
                }
            }
        }

        @Override
        protected void hideEditProfileViews() {

            findViewById(R.id.profile_educational_info_edit).setVisibility(View.GONE);
            findViewById(R.id.profile_professional_info_edit).setVisibility(View.GONE);
            findViewById(R.id.profile_other_info_edit).setVisibility(View.GONE);

            if (!isPublic) {
                dismissProfileActivityForPrivateProfile();
            }

        }

        @Override
        protected void setUpEditProfileListeners() {
            findViewById(R.id.profile_basic_info_edit).setOnClickListener(this);
            findViewById(R.id.profile_educational_info_edit).setOnClickListener(this);
            findViewById(R.id.profile_professional_info_edit).setOnClickListener(this);
            findViewById(R.id.profile_other_info_edit).setOnClickListener(this);
            mPublicProfile.setOnCheckedChangeListener(this);

        }

        @Override
        public void onClick(View v) {
            if (v.getId() == R.id.student_location) {
                showLocationOnMap(location);
            } else if (v.getId() == R.id.profile_basic_info_edit) {
                Intent editIntent = new Intent(ctx, EditProfileActivity.class);
                editIntent.putExtra(EditProfileActivity.PROFILE_EDIT_SECTION, R.id.profile_basic_info_edit);
                ctx.startActivityForResult(editIntent, REQ_EDIT_PROFILE);
            } else if (v.getId() == R.id.profile_educational_info_edit) {
                Intent editIntent = new Intent(ctx, EditProfileActivity.class);
                editIntent.putExtra(EditProfileActivity.PROFILE_EDIT_SECTION, R.id.profile_educational_info_edit);
                ctx.startActivityForResult(editIntent, REQ_EDIT_PROFILE);
            } else if (v.getId() == R.id.profile_professional_info_edit) {
                Intent editIntent = new Intent(ctx, EditProfileActivity.class);
                editIntent.putExtra(EditProfileActivity.PROFILE_EDIT_SECTION, R.id.profile_professional_info_edit);
                ctx.startActivityForResult(editIntent, REQ_EDIT_PROFILE);
            } else if (v.getId() == R.id.profile_other_info_edit) {
                Intent editIntent = new Intent(ctx, EditProfileActivity.class);
                editIntent.putExtra(EditProfileActivity.PROFILE_EDIT_SECTION, R.id.profile_other_info_edit);
                ctx.startActivityForResult(editIntent, REQ_EDIT_PROFILE);
            }

        }

        @Override
        public void onCheckedChanged(CompoundButton buttonView, final boolean isChecked) {
            ChangeProfileVisibility task = new ChangeProfileVisibility(ctx,isChecked,AppUser.PROFILE_STUDENT);
            task.execute();
        }
    }

    private class FetchTeacherProfile extends FetchProfile implements View.OnClickListener {

        LinearLayout mOfficeHours;
        TextView mRole;
        TextView mDepartmentId;
        TextView mInstEmail;
        List<JSONObject> teacherOfficeHours;
        String idNumber;
        String departmentId;
        String role;
        String email;


        public FetchTeacherProfile(ProfileActivity ctx, String appUserObjectId) {
            super(ctx, appUserObjectId);
        }

        @Override
        protected void setUpBasicInfoLayout() {

            View mTeacherBasicInfo;
            View mTeacherAdditionalInfo;

            if (ctx.findViewById(R.id.teacher_profile_additional_info) == null &&
                    ctx.findViewById(R.id.teacher_profile_basic_info) == null) {
                mTeacherBasicInfo = View.inflate(ctx, R.layout.teacher_profile_basic_info, null);
                mTeacherAdditionalInfo = View.inflate(ctx, R.layout.teacher_profile_additional_info, null);

                mProfileBasicInfo.addView(mTeacherBasicInfo, 1);
                mProfileContentViews.addView(mTeacherAdditionalInfo, 1);
            } else {
                mTeacherBasicInfo = ctx.findViewById(R.id.teacher_profile_basic_info);
                mTeacherAdditionalInfo = ctx.findViewById(R.id.teacher_profile_additional_info);

            }

            mOfficeHours = (LinearLayout) mTeacherAdditionalInfo.findViewById(R.id.teacher_office_hours);

            mRole = (TextView) mTeacherBasicInfo.findViewById(R.id.teacher_role);
            mDepartmentId = (TextView) mTeacherBasicInfo.findViewById(R.id.teacher_department_id);
            mInstEmail = (TextView) mTeacherBasicInfo.findViewById(R.id.teacher_work_email);
        }

        @Override
        protected void backgroundWorkOnFetchProfile(AppUser profileObject) throws ParseException {

            teacher = profileObject.getTeacherProfile();

            teacherOfficeHours = teacher.getOfficePeriods();
            idNumber = teacher.getIdNumber();
            departmentId = teacher.getDepartmentId();
            role = teacher.getRole();
            email = teacher.getEmail();

        }

        @Override
        protected void foregroundWorkOnFetchProfile() {

            if (teacherOfficeHours != null) {
                mOfficeHours.removeAllViews();
                DynamicLinearLayoutViewInflater.addOfficeHourViewsToLayout(
                        teacherOfficeHours, ctx, mOfficeHours, null, R.id.teacher_office_hours);
            }


            mType.setText(getString(R.string.profile_type_teacher) + " " + idNumber);

            mRole.setText(role);
            mDepartmentId.setText(departmentId);
            mInstEmail.setText(email);


            mPublicProfile.setVisibility(View.GONE);

        }

        @Override
        protected void hideEditProfileViews() {
            findViewById(R.id.profile_teacher_edit).setVisibility(View.GONE);
        }

        @Override
        protected void setUpEditProfileListeners() {
            findViewById(R.id.profile_basic_info_edit).setOnClickListener(this);
            findViewById(R.id.profile_teacher_edit).setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {

            if (v.getId() == R.id.profile_teacher_edit) {
                Intent editIntent = new Intent(ctx, EditProfileActivity.class);
                editIntent.putExtra(EditProfileActivity.PROFILE_EDIT_SECTION, R.id.profile_teacher_edit);
                startActivityForResult(editIntent, REQ_EDIT_PROFILE);
            } else if (v.getId() == R.id.profile_basic_info_edit) {
                Intent editIntent = new Intent(ctx, EditProfileActivity.class);
                editIntent.putExtra(EditProfileActivity.PROFILE_EDIT_SECTION, R.id.profile_basic_info_edit);
                ctx.startActivityForResult(editIntent, REQ_EDIT_PROFILE);
            }

        }
    }

    private class FetchCompanyProfile extends FetchProfile implements View.OnClickListener, CompoundButton.OnCheckedChangeListener {

        private TextView mVatNumber;
        private TextView mCompanyUrl;
        private TextView mEnterpriseSize;
        private TextView mCategory;
        private TextView mSubcategory;
        private TextView mMission;
        private TextView mLocation;
        private String vatNumber;
        private String url;
        private String enterpriseSize;
        private String category;
        private String subcategory;
        private String mission;
        private String location;
        private boolean isPublic;

        public FetchCompanyProfile(ProfileActivity ctx, String appUserObjectId) {
            super(ctx, appUserObjectId);
        }

        @Override
        protected void setUpBasicInfoLayout() {

            View mCompanyBasicInfo;

            if (ctx.findViewById(R.id.company_profile_basic_info) == null) {
                mCompanyBasicInfo = View.inflate(ctx, R.layout.company_profile_basic_info, null);
                mProfileBasicInfo.addView(mCompanyBasicInfo, 1);

            } else {
                mCompanyBasicInfo = ctx.findViewById(R.id.company_profile_basic_info);
            }

            mVatNumber = (TextView) mCompanyBasicInfo.findViewById(R.id.company_vatnumber);
            mCompanyUrl = (TextView) mCompanyBasicInfo.findViewById(R.id.company_url);
            mEnterpriseSize = (TextView) mCompanyBasicInfo.findViewById(R.id.company_size);
            mCategory = (TextView) mCompanyBasicInfo.findViewById(R.id.company_category);
            mSubcategory = (TextView) mCompanyBasicInfo.findViewById(R.id.company_subcategory);
            mMission = (TextView) mCompanyBasicInfo.findViewById(R.id.company_mission);
            mLocation = (TextView) mCompanyBasicInfo.findViewById(R.id.company_location);

        }

        @Override
        protected void backgroundWorkOnFetchProfile(AppUser profileObject) throws ParseException {
            company = profileObject.getCompanyProfile();
            vatNumber = company.getVATNumber();
            url = company.getCompanyURL();
            enterpriseSize = company.getEnterpriseSize();
            category = company.getCategory();
            subcategory = company.getSubcategory();
            mission = company.getMission();
            location = company.getLocationName();
            isPublic = company.getIsPublic();
        }

        @Override
        protected void foregroundWorkOnFetchProfile() {

            mVatNumber.setText(vatNumber);
            mVatNumber.setText(vatNumber);
            if (url != null) {
                mCompanyUrl.setText(url);
                mCompanyUrl.setVisibility(View.VISIBLE);
            } else
                mCompanyUrl.setVisibility(View.GONE);

            if (enterpriseSize != null) {
                mEnterpriseSize.setText(enterpriseSize);
                mEnterpriseSize.setVisibility(View.VISIBLE);
            } else mEnterpriseSize.setVisibility(View.GONE);

            if (category == null) {
                mCategory.setVisibility(View.GONE);
                mSubcategory.setVisibility(View.GONE);
            } else {
                mCategory.setText(category);
                mSubcategory.setText(subcategory);
                mCategory.setVisibility(View.VISIBLE);
                mSubcategory.setVisibility(View.VISIBLE);
            }
            if (mission != null) {
                mMission.setText(mission);
                mMission.setVisibility(View.VISIBLE);
            } else
                mMission.setVisibility(View.GONE);

            if (location != null) {
                mLocation.setOnClickListener(this);
                mLocation.setPaintFlags(mLocation.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
                mLocation.setText(location);
                mLocation.setVisibility(View.VISIBLE);
            } else
                mLocation.setVisibility(View.GONE);

            mPublicProfile.setChecked(isPublic);
        }

        @Override
        protected void hideEditProfileViews() {
            if (!isPublic) {
                dismissProfileActivityForPrivateProfile();
            }
        }

        @Override
        protected void setUpEditProfileListeners() {
            findViewById(R.id.profile_basic_info_edit).setOnClickListener(this);
            mPublicProfile.setOnCheckedChangeListener(this);
        }

        @Override
        public void onClick(View v) {
            if (v.getId() == R.id.company_location) {
                showLocationOnMap(location);
            } else if (v.getId() == R.id.profile_basic_info_edit) {
                Intent editIntent = new Intent(ctx, EditProfileActivity.class);
                editIntent.putExtra(EditProfileActivity.PROFILE_EDIT_SECTION, R.id.profile_basic_info_edit);
                ctx.startActivityForResult(editIntent, REQ_EDIT_PROFILE);
            }
        }

        @Override
        public void onCheckedChanged(CompoundButton buttonView, final boolean isChecked) {
            ChangeProfileVisibility task = new ChangeProfileVisibility(ctx,isChecked,AppUser.PROFILE_COMPANY);
            task.execute();
        }
    }

    private void dismissProfileActivityForPrivateProfile() {
        Toast.makeText(getApplicationContext(), R.string.private_profile, Toast.LENGTH_SHORT).show();
        this.finish();
    }
}
