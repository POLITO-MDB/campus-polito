package com.example.mad.campuspolito.common;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.example.mad.campuspolito.CampusPolitoApplication;
import com.example.mad.campuspolito.R;
import com.example.mad.campuspolito.company.CompanyHomeActivity;
import com.example.mad.campuspolito.model.AppUser;
import com.example.mad.campuspolito.student.StudentHomeActivity;
import com.example.mad.campuspolito.teacher.TeacherHomeActivity;

public class MainActivity extends AppCompatActivity {


    public static final int LOGIN_REQUEST_CODE = 0;
    private static final String TAG = MainActivity.class.getName();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        if(!CampusPolitoApplication.checkConnectivity(getApplicationContext()))
            Toast.makeText(getApplicationContext(), getString(R.string.no_connectivity), Toast.LENGTH_LONG).show();

        AppUser user = (AppUser)AppUser.getCurrentUser();
        if(user == null){
            Intent loginIntent = new Intent(this,LoginActivity.class);
            startActivityForResult(loginIntent, LOGIN_REQUEST_CODE);
        }
        else {
            Intent homeSpecificIntent = null;
            if(user.isStudent()){
                homeSpecificIntent = new Intent(this, StudentHomeActivity.class);
            }

            else if (user.isCompany()){
                homeSpecificIntent = new Intent(this, CompanyHomeActivity.class);
            }

            else if (user.isTeacher()){
                homeSpecificIntent = new Intent(this, TeacherHomeActivity.class);
            }
            startActivity(homeSpecificIntent);
            finish();
        }

    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == LOGIN_REQUEST_CODE){

            if(resultCode == RESULT_OK){
                Log.d(TAG, "Result OK from login activity");

                AppUser user = (AppUser)AppUser.getCurrentUser();

                if(user != null){
                    Intent homeSpecificIntent = null;
                    if(user.isStudent()){
                        homeSpecificIntent = new Intent(this, StudentHomeActivity.class);
                    }

                    else if (user.isCompany()){
                        homeSpecificIntent = new Intent(this, CompanyHomeActivity.class);
                    }

                    else if (user.isTeacher()){
                        homeSpecificIntent = new Intent(this, TeacherHomeActivity.class);
                    }

                    startActivity(homeSpecificIntent);
                }
                finish();
            }

            else if (resultCode == RESULT_CANCELED){
                Log.d(TAG, "Result canceled from login activity");
                this.finish();
            }
        }
    }
}
