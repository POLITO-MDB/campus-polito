
// Use Parse.Cloud.define to define as many cloud functions as you want.
// For example:
Parse.Cloud.define("hello", function(request, response) {
	response.success("Hello world!");
});

Parse.Cloud.beforeSave("Message", function(request, response) {
	console.log("Message received from installation.");
	response.success();
});

Parse.Cloud.beforeSave("Conversation", function(request, response) {
	console.log("Message received from installation.");
	response.success();
});

Parse.Cloud.define("addCoursesToTeachers", function(request, response) {

	console.log("Launching addCoursesToTeachers job.");
  // Set up to modify user data
  Parse.Cloud.useMasterKey();
  
  console.log("Using master key.");
  // Query for all teachers
  var query = new Parse.Query("Teacher");
  query.limit(999);
  query.ascending("surname");
  
  console.log("Teachers query prepared.");

  query.find().then(function(teachers) {
  	var promise = Parse.Promise.as();

  	for (var i = 0; i < teachers.length; i++) {
  		var teacher = teachers[i];

  		var course_ids = teacher.get("course_ids");

  		if(course_ids == null)
  			continue;

  		var course_ids_array = course_ids.trim().split(/\s+/);


  		console.log("Teacher " + teacher.get("surname") + " " + teacher.get("name") + 
  			" for courses: " + course_ids_array);

			var query_courses = new Parse.Query("Course");
	    query_courses.limit(100);
	    query_courses.containedIn("code",course_ids_array);

	    query_courses.find().then(function(courses){
	    	promise = promise.then(function(){

	    		for (var j = 0; j < courses.length; j++){
	   				promise = promise.then(function(){
    					var course = courses[j];
	      			console.log("Course " + course.get("title") + " found.");
	      			course.addUnique("teachers", teacher);
	      			teacher.addUnique("courses", course);

      				return course.save();
	    			});
	    		}
	    		return teacher.save();
	    	});
	    });
  	
  	}

  	return promise;

  }).then(function(){
  	console.log("Courses added to teachers");
  });

});