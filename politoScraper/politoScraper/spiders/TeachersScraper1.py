import scrapy
import re
from scrapy.selector import Selector
from scrapy import Request
from politoScraper.items import *
from selenium import webdriver
from selenium.webdriver.common.by import By
import time

class PolitoscraperSpider(scrapy.Spider):
	name = 'teachers1'
	allowed_domains = ["polito.it"]
	start_urls = list()

	def __init__(self, output_file):
		self.driver = webdriver.Firefox()
		self.driver.implicitly_wait(10) # seconds
		self.file_name = output_file		
		for i in range(1, 16):
			if(i < 10):
				index = '0' + str(i)
			else:
				index = str(i)
			self.start_urls.append('https://didattica.polito.it/pls/portal30/sviluppo.scheda_collegio.html?c=CL0' + index)
			print self.start_urls[-1]


	def parse(self, response):
		self.driver.get(response.url)		
		elems = self.driver.find_elements_by_xpath("//div[contains(@class, 'ZDataGrid_firstColumn')]/a")
		count = 0
		for elem in elems:
			name_surname = elem.text			
			link = elem.get_attribute("href")
			if "mailto" not in link:
				name = ""
				surname = ""
				for match in re.finditer(r'[A-Z][a-z]+ ?', name_surname):
					if match:
						 name += match.group()
				for match in re.finditer(r'[A-Z][A-Z]+ ?', name_surname):
					if match:
						surname += match.group().lower().capitalize()

				with open(self.file_name, 'ab') as outf:
					outf.write(name.rstrip() + "," + surname.rstrip() + "," + link + '\n')
					outf.flush()

