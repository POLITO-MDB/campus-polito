import scrapy
import re
from scrapy.selector import Selector
from scrapy import Request
from politoScraper.items import *

class PolitoscraperSpider(scrapy.Spider):
	name = 'departments'
	allowed_domains = ["polito.it"]
	start_urls = ["http://www.polito.it/ateneo/organizzazione/collegi/"]

	def parse(self, response):
		sel = Selector(response)
		elems = sel.xpath('//tr')	#//*[@id="corpo"]/table/tbody/tr[2]	
		elems.pop(0) #discard table header
		print "#elems = ", len(elems)
		items = []

		for row in elems:
			name = ''.join(row.xpath('.//td[1]/a/text()').extract()).strip() #collegio
			next_url = ''.join(row.xpath('.//td[1]/a/@href').extract()).strip()	#link collegio (== next_url contenente elenco prof)
			coord = ''.join(row.xpath('.//td[2]/a/text()').extract()).strip() #coordinatore collegio
			coord_link = ''.join(row.xpath('.//td[2]/a/@href').extract()).strip() #pagina personale coordinatore
			department = ''.join(row.xpath('.//td[3]/a/text()').extract()).strip() #department name
			dep_link = ''.join(row.xpath('.//td[3]/a/@href').extract()).strip() #link al dipartimento

			print "------------------------------------------------------------"
			print name, " ", next_url
			print coord, " ", coord_link
			print department, " ", dep_link
			
			item = Collegio()
			item['name'] = name
			item['department'] = department
			item['dep_link'] = dep_link
			m = re.match(".*www\.([a-z]+)\.polito.*", dep_link)
			if m:
				item['dep_code'] = m.group(1)
				print "dep_code: ", item['dep_code']
			item['next_url'] = next_url
			
			items.append(item)

			yield item
