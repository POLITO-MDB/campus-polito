import scrapy
import re
from scrapy.selector import Selector
from scrapy import Request
from politoScraper.items import PolitoscraperItem

class PolitoscraperSpider(scrapy.Spider):
	name = 'locations'
	allowed_domains = ["polito.it"]
	start_urls = ["http://www.polito.it/ateneo/sedi/index.php?tipo=AULA&lang=en", "http://www.polito.it/ateneo/sedi/index.php?tipo=AULA_DIS&lang=en", "http://www.polito.it/ateneo/sedi/index.php?tipo=AULA_INF&lang=en", "http://www.polito.it/ateneo/sedi/index.php?tipo=AULA_LAB&lang=en", "http://www.polito.it/ateneo/sedi/index.php?tipo=BAR&lang=en", "http://www.polito.it/ateneo/sedi/index.php?tipo=BIBLIO&lang=en", "http://www.polito.it/ateneo/sedi/index.php?tipo=BIBLIO_DIP&lang=en", "http://www.polito.it/ateneo/sedi/index.php?tipo=BOX&lang=en", "http://www.polito.it/ateneo/sedi/index.php?tipo=CEN_STAMP&lang=en", "http://www.polito.it/ateneo/sedi/index.php?tipo=INFERM&lang=en", "http://www.polito.it/ateneo/sedi/index.php?tipo=INGRESSO-DIP&lang=en", "http://www.polito.it/ateneo/sedi/index.php?tipo=LAB_DIDAT&lang=en", "http://www.polito.it/ateneo/sedi/index.php?tipo=LAB_RIC&lang=en", "http://www.polito.it/ateneo/sedi/index.php?tipo=LIBRERIA&lang=en"]
	#start_urls = ["http://www.polito.it/ateneo/sedi/index.php?tipo=AULA&lang=en"]

	def parse(self, response):
		sel = Selector(response)
		elems = sel.xpath('//option')		
		items = []
		start = response.url.find('tipo=') + 5
		end = response.url.find('&lang', start)
		tipo = response.url[start:end]
		print "TIPO"
		print tipo

		for elem in elems:
			item = PolitoscraperItem()
			item['location'] = elem.xpath('text()').extract()
			item['next_url'] = elem.xpath('@value').extract()
			item['next_url'] = item['next_url'][0].replace('%3D', '=').replace('%26', '&').strip()			
			items.append(item)

			request = Request("http://www.polito.it/ateneo/sedi/index.php?" + item['next_url'] + "&lang=en", callback = self.parse_details)
			request.meta['item'] = item
			yield request	


	def parse_details(self, response):
		item = response.meta['item']
		sel = Selector(response)
		elems = sel.xpath('//tr')
		descr = list()

		for elem in elems:
			th = elem.xpath('th/text()').extract()
			th[0] = th[0].replace(':', '').lower().strip()
			descr = elem.xpath('td/text()').extract()
			split_descr = '\n'.join(descr).strip()
			split_descr = re.split('\n', split_descr)
			item['type'] = split_descr[0]

			item[th[0]] = ""
			for s in split_descr:
				s = s.strip()
				print "s" + s
				if len(s) > 3:
					item[th[0]] += s
					item[th[0]] += '\n'			
			item[th[0]] = item[th[0]][:len(item[th[0]])-1]	

			descr = ''.join(descr).strip()
			descr = re.split(" {2,}", descr)

			if len(descr) > 1:
				item["building"] = descr[1]

		latlng = sel.xpath('//div[contains(@id, "map_canvas_static")]/img/@src').extract()
		latlng = latlng[0]

		start = latlng.find('%7C') + 3
		end = latlng.find('&sensor', start)
		latlng = latlng[start:end]
		latlng = latlng.split(',')
		
		item["lat"] = latlng[0]
		item["lng"] = latlng[1]

		return item

