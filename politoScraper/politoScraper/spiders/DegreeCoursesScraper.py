#!/usr/bin/env python
# -*- coding: utf-8 -*-

import scrapy
import re
from scrapy.selector import Selector
from scrapy import Request
from politoScraper.items import *
from selenium import webdriver
from scrapy import signals
from scrapy.xlib.pydispatch import dispatcher
import time
import codecs
import csv
import json


class PolitoscraperSpider(scrapy.Spider):
	name = 'degree_courses'
	allowed_domains = ["polito.it"]
	start_urls = ["https://didattica.polito.it/pls/portal30/gap.a_mds.init_new?p_a_acc=2014"]
	d = ["ingegneria_1", "ingegneria_2", "architettura_1", "architettura_2", "ingegneria_dottorati"]
	c_list = list()
	d_list = list()

	def __init__(self, degree_file, course_file):
		self.driver = webdriver.Firefox()
		self.driver.implicitly_wait(120) # seconds	
		self.degree_file = degree_file
		self.course_file = course_file
		dispatcher.connect(self.on_quit, signals.spider_closed)

		
	
	def on_quit(self):
		self.driver.close()

		cc = {}
		dd = {}
		cc['results'] = self.c_list
		dd['results'] = self.d_list
		with open(self.degree_file, 'w') as d_out:
			json.dump(dd, d_out, sort_keys = True, indent = 4, ensure_ascii=False)

		with open(self.course_file, 'w') as c_out:
			json.dump(cc, c_out, sort_keys = True, indent = 4, ensure_ascii=False)			
		

	def parse(self, response):
		self.driver.get(response.url)	
		self.driver.find_element_by_id("l_ENG").click()
		for b in self.d:
			self.driver.find_element_by_id("b_" + b).click()

		for section in self.d:

			if "1" in section:
				level = "Bachelor's degree"
			elif "2" in section:
				level = "Master's degree"
			else:
				level = "Doctorate of Philosophy"

			tbody = self.driver.find_element_by_xpath('//div[@id="d_%s"]/table/tbody' % section)
			#print tbody.text
			trs = tbody.find_elements_by_xpath('.//tr')
			#print section, len(trs)
			for tr in trs[2:]: #skip first 2 rows
				d = {}
				d['level'] = level
				
				link = tr.find_element_by_xpath(".//td/a") 
				url = link.get_attribute('href')

				#match = re.search(r'cds=([0-9]+)&', url)
				#if match:
					#d['cds'] = int(match.group(1))

				d['title'] = link.text.lower().capitalize()
				cities = tr.find_element_by_xpath(".//td[2]").text
				d['city'] = ' '.join(w.capitalize() for w in cities.split('/'))

				request = Request(url, callback = self.parse_courses)
				request.meta['item'] = d
				yield request
				#break #####
			#break #####		


	def parse_courses(self, response):
		d = response.meta['item']
		sel = Selector(response)
		cods = list()

		#cds = d['cds']
		
		if d['level'] != "Doctorate of Philosophy":
			divs = sel.xpath('//div[@onmouseover="this.style.background=\'#fff\';"]')
			div_td = 'div' 
		else:
			divs = sel.xpath('//tr[@onmouseover="this.className=\'tr_bianco\';"]')
			div_td = 'td'

		for div in divs:
			#discard divs/tds not describing courses, and anchors to tables in the same page
			if (div_td == 'td' and not div.xpath("./td[2]/b")) or (div_td == 'div' and (not div.xpath("./div[2]/strong") or div.xpath("./div[2]/strong/font"))): 
				pass
			else:
				if d['level'] != "Doctorate of Philosophy":	
					cod = ''.join(div.xpath("./div[2]/strong/text()").extract()).strip()
				else:
					cod = ''.join(div.xpath("./td[2]/b/text()").extract()).strip()
				if cod not in cods:
					cods.append(cod)
					title = ''.join(div.xpath("./{0}[4]/a/text()".format(div_td)).extract()).encode('utf-8').strip()
					cfu = ''.join(div.xpath("./{0}[5]/text()".format(div_td)).extract()).strip()
					semester = ''.join(div.xpath("./{0}[1]/text()".format(div_td)).extract()).encode('utf-8').strip()
					semester = re.sub(r'[^0-9,]', '', semester)
					semester = re.sub(r',', '-', semester)
					t_ids = div.xpath(".//{0}[6]/a/@href".format(div_td)).extract()

					lang = ''.join(div.xpath(".//{0}[3]/img/@src".format(div_td)).extract()).encode('utf-8').strip()
					if re.search(r'\/en.jpg', lang):
						lcode = 'EN'
					else:
						lcode = 'IT'
					
					teacher_ids = list()
					for t_id in t_ids:
						match = re.search(r'show\?m=([0-9]+)', t_id.encode('utf-8').strip())
						if match:
							m = match.group(1)
							while len(m) < 6:
								m = '0' + m
							teacher_ids.append(m)

					c = {}
					c['code'] = cod
					#c['cds'] = int(cds)
					c['title'] = title


					if re.search(r'\.', cfu):
						c['cfu'] = float(cfu)
					else:
						c['cfu'] = int(cfu)

					c['semester'] = semester
					c['teachers'] = ' '.join(teacher_ids).encode('utf-8').strip()

					
					request = Request('https://didattica.polito.it/pls/portal30/sviluppo.guide.visualizza?p_cod_ins=' + cod + '&p_a_acc=2014&p_lang=' + lcode, callback = self.parse_course_description)
					request.meta['item'] = c
					yield request
					

		d['course_ids'] = ' '.join(cods)		
		#yield d
		self.d_list.append(d)

	
	def parse_course_description(self, response):
		c = response.meta['item']
		sel = Selector(response)
		descr = sel.xpath('//table[2]//tr[1]//text()').extract()
		c['description'] = u''.join(descr).encode('utf-8').strip()
		c['description'] = re.sub(r'\r+', '\n', c['description'])
		#c['description'] = re.sub(r'[’`]', "'", c['description'])
		c['description'] = re.sub(r'Presentazione\n?', '', c['description']).strip() #
		c['description'] = re.sub(r'Subject fundamentals\n', '', c['description']).strip() #
		
		self.c_list.append(c)

		
		
			
