# -*- coding: utf-8 -*-

# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: http://doc.scrapy.org/en/latest/topics/item-pipeline.html

from scrapy.xlib.pydispatch import dispatcher
from scrapy import signals


class PolitoscraperPipeline(object):
    def process_item(self, item, spider):
        return item

class CSVItemPipeline(object):
	files = dict()
	exporters = dict()

	def __init__(self):
		dispatcher.connect(self.spider_opened, signal=signals.spider_opened)
		dispatcher.connect(self.spider_closed, signal=signals.spider_closed)
		self.files['collegio']
		self.files['teacher']		
		self.exporters['collegio']
		self.exporters['teacher']

	def spider_opened(self, spider):
		self.files['collegio'] = open('collegio' + '.csv','w+b')
		self.files['teacher'] = open('teacher' + '.csv','w+b')
		
		self.exporters['collegio'] = CsvItemExporter(self.files['collegio'])
		self.exporters['teacher'] = CsvItemExporter(self.files['teacher'])

		self.exporters['collegio'].start_exporting()
		self.exporters['teacher'].start_exporting()

	def spider_closed(self, spider):
		self.exporters['collegio'].finish_exporting()
		self.exporters['teacher'].finish_exporting()

		self.files['collegio'].close()
		self.files['teacher'].close()

	def process_item(self, item, spider):
		print 'item name = ' + type(item).__name__
		if type(item).__name__ == 'Collegio':
			self.exporters['collegio'].export_item(item)
		elif type(item).__name__ == 'Teacher':
			self.exporters['teacher'].export_item(item)	
		return item


#http://stackoverflow.com/questions/12230332/how-can-scrapy-export-items-to-separate-csv-files-per-item
def item_type(item):
    return type(item).__name__ # Item class name

class MultiCSVItemPipeline(object):
	SaveTypes = ['Collegio', 'Teacher']
	exporters = dict()
	files = dict()

	def __init__(self):
		dispatcher.connect(self.spider_opened, signal=signals.spider_opened)
		dispatcher.connect(self.spider_closed, signal=signals.spider_closed)

	def spider_opened(self, spider):
		self.files = dict([ (name, open(CSVDir + name + '.csv','w+b')) for name in self.SaveTypes ])
		self.exporters = dict([ (name , CsvItemExporter(self.files[name])) for name in self.SaveTypes])
		[e.start_exporting() for e in self.exporters.values()]

	def spider_closed(self, spider):
		[e.finish_exporting() for e in self.exporters.values()]
		[f.close() for f in self.files.values()]

	def process_item(self, item, spider):
		what = item_type(item)
		print what
		if what in set(self.SaveTypes):
			self.exporters[what].export_item(item)
		return item

